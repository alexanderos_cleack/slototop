<?php

namespace common\traits;

use common\models\images\Images;

/**
 * Class ImagesTrait
 * @package common\traits
 *
 * @property Images $logo
 * @property Images $logo_mini
 * @property Images $logo_casino
 * @property Images $bigscreen
 * @property Images[] $screens
 * @property Images[] $screens_view
 * @property Images[] $screens_registration
 * @property Images[] $screens_games
 * @property Images[] images
 * @property array imagesAlt
 */
trait ImagesTrait
{

}