<?php

namespace common\traits;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class SameTrait
 * @package common\traits
 *
 * @property ActiveRecord[] $same
 *
 * @method ActiveQuery getSame
 */
trait SameTrait
{

}