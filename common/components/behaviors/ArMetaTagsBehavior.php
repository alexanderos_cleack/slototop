<?php

namespace common\components\behaviors;

use yii\db\ActiveRecord;
use yii\base\Behavior;

use common\models\metatags\MetaTags;

/**
 * Class ArMetaTagsBehavior
 * @package common\components\behaviors
 */
class ArMetaTagsBehavior extends Behavior
{
    private $_model;

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
        ];
    }

    public function afterSave($event)
    {
        if ($this->model->isNewRecord) {
            $this->model->model_schema = $this->owner->tableName();
            $this->model->model_id = $this->owner->id;
        }
        $this->model->save();
    }

    public function getMeta_Title()
    {
        return $this->model->title;
    }

    public function setMeta_Title($value)
    {
        $this->model->title = $value;
    }

    public function getMeta_H1()
    {
        return $this->model->h1;
    }

    public function setMeta_H1($value)
    {
        $this->model->h1 = $value;
    }

    public function getMeta_Description()
    {
        return $this->model->description;
    }

    public function setMeta_Description($value)
    {
        $this->model->description = $value;
    }

    public function getMeta_Keywords()
    {
        return $this->model->keywords;
    }

    public function setMeta_Keywords($value)
    {
        $this->model->keywords = $value;
    }

    protected function getModel()
    {
        if (!$this->_model && $this->owner->isNewRecord) {
            $this->_model = new MetaTags;
        } else {
            if (!$this->_model) {
                $this->_model = MetaTags::find()->where(['model_schema' => $this->owner->tableName(), 'model_id' => $this->owner->id])->one();
            }
            if (!$this->_model) {
                $this->_model = new MetaTags;
            }
        }
        return $this->_model;
    }

}