<?php

namespace common\components\behaviors;

use Yii;
use yii\db\ActiveRecord;
use yii\base\Behavior;

/**
 * Class ArSortableBehavior
 * @package common\components\behaviors
 */
class ArSortableBehavior extends Behavior
{
    public $attribute = 'sort';

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
        ];
    }

    public function afterSave($event)
    {
        $owner = $this->owner;
        $owner->{$this->attribute} = $owner->getPrimaryKey();
        $owner->update();
    }

}