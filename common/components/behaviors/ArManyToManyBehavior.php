<?php

namespace common\components\behaviors;

use yii\db\ActiveRecord;
use yii\base\Behavior;

/**
 * Class ArManyToManyBehavior
 * @package common\components\behaviors
 */
class ArManyToManyBehavior extends Behavior
{
    public $intermediaries = [];
    public $attributes = [];
    public $table_a_fkey;
    public $table_b_fkeys = [];

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
        ];
    }

    public function afterSave($events)
    {
        foreach ($this->intermediaries as $intermediary) {
            $attribute = array_shift($this->attributes);
            $table_b_fkey = array_shift($this->table_b_fkeys);
            $values = $this->owner->{$attribute};

            $reflection_method = new \ReflectionMethod($intermediary, 'deleteAll');
            $reflection_method->invoke(null, [$this->table_a_fkey => $this->owner->id]);

            if (is_array($values) && count($values)) {
                $_values = [];
                foreach ($this->owner->{$attribute} as $id) {
                    if (is_object($id)) continue;
                    $_values[] = [$this->owner->id, $id];
                }
                if (count($_values)) {
                    $reflection_method = new \ReflectionMethod($intermediary, 'tableName');
                    $tableName = $reflection_method->invoke(null);
                    $this->owner->getDb()->createCommand()->batchInsert($tableName, [$this->table_a_fkey, $table_b_fkey], $_values)->execute();
                }
            } elseif ((int)$values > 0) {
                $reflection_method = new \ReflectionMethod($intermediary, 'tableName');
                $tableName = $reflection_method->invoke(null);
                $this->owner->getDb()->createCommand()->insert($tableName, [$this->table_a_fkey => $this->owner->id, $table_b_fkey => $values])->execute();
            }
        }
    }

}