<?php

namespace common\components\behaviors;

use common\models\same\Same2table;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Class ArSamesBehavior
 * @package common\components\behaviors
 *
 * @property array $same
 */
class ArSameBehavior extends Behavior
{
    protected $same;

    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
        ];
    }

    public function afterSave()
    {
        /** @var ActiveRecord $owner */
        $owner = $this->owner;

        $oldIDs = ArrayHelper::map($owner->same, 'id', 'id');
        $addIDs = array_diff($this->same, $oldIDs);
        $deleteIDs = array_diff($oldIDs, $this->same);

        if ($addIDs) {
            foreach ($addIDs as $sameID) {
                $tags2table = new Same2table();
                $tags2table->attributes = [
                    'model_schema' => $owner::tableName(),
                    'model_id' => $owner->id,
                    'same_id' => $sameID,
                ];
                $tags2table->save();
            }
        }

        if ($deleteIDs) {
            foreach ($deleteIDs as $sameID) {
                Same2table::deleteAll([
                    'model_schema' => $owner::tableName(),
                    'model_id' => $owner->id,
                    'same_id' => $sameID,
                ]);
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSame2table()
    {
        /** @var ActiveRecord $owner */
        $owner = $this->owner;

        return $owner->hasMany(Same2table::class, ['model_id' => 'id'])->andWhere(['same2table.model_schema' => $owner->tableName()]);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSame()
    {
        /** @var ActiveRecord $owner */
        $owner = $this->owner;

        return $owner->hasMany(get_class($owner), ['id' => 'same_id'])
            ->from(['t' => $owner::tableName()])
            ->andWhere(['!=', 't.id', $owner->id])->via('same2table');
    }

    /**
     * @param array $same
     */
    public function setSame($same)
    {
        if (!$same) {
            $same = [];
        }

        $this->same = $same;
    }
}