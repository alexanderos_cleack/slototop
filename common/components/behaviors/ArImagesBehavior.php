<?php

namespace common\components\behaviors;

use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\web\UploadedFile;

use common\models\images\Images;

/**
 * Class ArImagesBehavior
 * @package common\components\behaviors
 *
 * @property array $imagesAlt
 */
class ArImagesBehavior extends Behavior
{
    public $model;
    public $attributesOne = array();
    public $attributesMany = array();

    private $_logo;
    private $_logo_mini;
    private $_logo_casino;
    private $_bigscreen;
    private $_screens;
    private $_screens_view;
    private $_screens_registration;
    private $_screens_games;

    protected $imagesAlt = [];

    private $_model_schema;
    private $_path;

    public function init()
    {
        $this->_path = Yii::getAlias('@uploads');
    }

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
            ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
        ];
    }

    public function afterSave($events)
    {
        foreach ($this->attributesOne as $attributeOne) {
            $_attributeOne = $attributeOne;
            if ($this->owner->tabular !== false) {
                $_attributeOne = "[{$this->owner->tabular}]" . $attributeOne;
            }
            $file = UploadedFile::getInstance($this->model, $_attributeOne);
            if ($file && $file instanceof \yii\web\UploadedFile) {
                $image = Images::findOne(['model_schema' => $this->owner->tableName(), 'model_id' => $this->owner->id, 'attribute' => $attributeOne]);
                if ($image) $image->delete();
                $this->saveFile($file, $attributeOne);
            }
        }

        foreach ($this->attributesMany as $attributeMany) {
            $files = UploadedFile::getInstances($this->model, $attributeMany);
            if ($files && is_array($files)) {
                foreach ($files as $file) {
                    if ($file instanceof \yii\web\UploadedFile) $this->saveFile($file, $attributeMany);
                }
            }
        }

        foreach ($this->imagesAlt as $id => $alt) {
            Images::updateAll(['alt' => $alt], ['id' => $id]);
        }

    }

    public function beforeDelete()
    {
        $images = Images::find()->where(['model_schema' => $this->owner->tableName(), 'model_id' => $this->owner->id])->all();
        if ($images) {
            foreach ($images as $image) {
                $image->delete();
            }
        }
    }

    private function saveFile($file, $attribute)
    {
        // $file_name = \yii\helpers\Inflector::slug($file->baseName) . '.' . $file->extension;
        $file_name = $file->name;

        if ($file->saveAs($this->_path . DIRECTORY_SEPARATOR . $file_name)) {
            $image = new Images();
            $image->attributes = [
                'model_schema' => $this->owner->tableName(),
                'model_id' => $this->owner->id,
                'attribute' => $attribute,
                'filename' => $file_name,
            ];
            if ($image->save()) {
                Yii::info("Файл сохранен - {$file_name} | ID - {$image->id}", 'debug.files');
            } elseif ($image->hasErrors()) {
                Yii::error("Файл не сохранен - {$file_name} " . var_export($image->getErrors(), true), 'debug.file');
            }
        }

        if ($file->getHasError()) {
            $errors = [
                0 => 'There is no error, the file uploaded with success.',
                1 => 'The uploaded file exceeds the upload_max_filesize directive in php.ini.',
                2 => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.',
                3 => 'The uploaded file was only partially uploaded.',
                4 => 'No file was uploaded.',
                6 => 'Missing a temporary folder.',
                7 => 'Failed to write file to disk.',
                8 => 'A PHP extension stopped the file upload. PHP does not provide a way to ascertain which extension caused the file upload to stop; examining the list of loaded extensions with phpinfo() may help. Introduced in PHP 5.2.0.',
            ];

            Yii::error("Ошибка при сохранение файла '{$file_name}' - " . $errors[$file->error], 'debug.files');
        }

    }

    private function getClassNameOwner()
    {
        $class_name = $this->owner->className();
        return substr($class_name, strrpos($class_name, '\\') + 1);
    }

    public function getLogo()
    {
        if (!$this->_logo) {
            $this->_logo = Images::find()->where([
                'model_schema' => $this->owner->tableName(),
                'model_id' => $this->owner->id,
                'attribute' => 'logo',
            ])->one();
        }
        return $this->_logo;
    }

    public function setLogo($logo)
    {
        $this->owner->logo = $logo;
    }

    public function getLogo_mini()
    {
        if (!$this->_logo_mini) {
            $this->_logo_mini = Images::find()->where([
                'model_schema' => $this->owner->tableName(),
                'model_id' => $this->owner->id,
                'attribute' => 'logo_mini',
            ])->one();
        }
        return $this->_logo_mini;
    }

    public function setLogo_mini($logo)
    {
        $this->owner->logo_mini = $logo;
    }

    public function getLogo_casino()
    {
        if (!$this->_logo_casino) {
            $this->_logo_casino = Images::find()->where([
                'model_schema' => $this->owner->tableName(),
                'model_id' => $this->owner->id,
                'attribute' => 'logo_casino',
            ])->one();
        }
        return $this->_logo_casino;
    }

    public function setLogo_casino($logo)
    {
        $this->owner->logo_casino = $logo;
    }

    public function getBigscreen()
    {
        if (!$this->_bigscreen) {
            $this->_bigscreen = Images::find()->where([
                'model_schema' => $this->owner->tableName(),
                'model_id' => $this->owner->id,
                'attribute' => 'bigscreen',
            ])->one();
        }
        return $this->_bigscreen;
    }

    public function setBigscreen($bigscreen)
    {
        $this->owner->bigscreen = $bigscreen;
    }

    public function getScreens()
    {
        if (!$this->_screens) {
            $this->_screens = Images::find()->where([
                'model_schema' => $this->owner->tableName(),
                'model_id' => $this->owner->id,
                'attribute' => 'screens',
            ])->orderBy('model_id')->all();
        }
        return $this->_screens;
    }

    public function setScreens($screens)
    {
        $this->owner->screens = $screens;
    }

    public function getScreens_view()
    {
        if (!$this->_screens_view) {
            $this->_screens_view = Images::find()->where([
                'model_schema' => $this->owner->tableName(),
                'model_id' => $this->owner->id,
                'attribute' => 'screens_view',
            ])->orderBy('model_id')->all();
        }
        return $this->_screens_view;
    }

    public function setScreens_view($screens)
    {
        $this->owner->screens = $screens;
    }

    public function getScreens_registration()
    {
        if (!$this->_screens_registration) {
            $this->_screens_registration = Images::find()->where([
                'model_schema' => $this->owner->tableName(),
                'model_id' => $this->owner->id,
                'attribute' => 'screens_registration',
            ])->orderBy('model_id')->all();
        }
        return $this->_screens_registration;
    }

    public function setScreens_registration($screens)
    {
        $this->owner->screens = $screens;
    }

    public function getScreens_games()
    {
        if (!$this->_screens_games) {
            $this->_screens_games = Images::find()->where([
                'model_schema' => $this->owner->tableName(),
                'model_id' => $this->owner->id,
                'attribute' => 'screens_games',
            ])->orderBy('model_id')->all();
        }
        return $this->_screens_games;
    }

    public function setScreens_games($screens)
    {
        $this->owner->screens = $screens;
    }


    public function getTabular()
    {
        if (isset($this->owner->tabular)) return $this->owner->tabular;
        return false;
    }

    public function setTabular($tabular)
    {
        $this->owner->tabular = $tabular;
    }

    public function getImages()
    {
        /** @var ActiveRecord $owner */
        $owner = $this->owner;

        return $owner->hasMany(Images::class, ['model_id' => 'id'])->andWhere(['images.model_schema' => $owner->tableName()]);
    }

    /**
     * @return array
     */
    public function getImagesAlt()
    {
        /** @var ActiveRecord $owner */
        $owner = $this->owner;

        return ArrayHelper::map($owner->images, 'id', 'alt');
    }

    public function setImagesAlt($imagesAlt)
    {
        $this->imagesAlt = $imagesAlt ?? [];
    }

}