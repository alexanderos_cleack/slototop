<?php

namespace common\components\behaviors;

use yii\db\ActiveRecord;
use yii\base\Behavior;
use yii\helpers\ArrayHelper;

use common\models\tags\Tags2table;
use common\models\tags\Tags;

/**
 * Class ArTagsBehavior
 * @package common\components\behaviors
 *
 * @property array $tags
 * @property string $tagsTitles
 */
class ArTagsBehavior extends Behavior
{
    protected $tags;
    protected $tagsTitles;

    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
        ];
    }

    public function afterSave()
    {
        /** @var ActiveRecord $owner */
        $owner = $this->owner;

        $oldIDs = ArrayHelper::map($owner->tags, 'id', 'id');
        $addIDs = array_diff($this->tags, $oldIDs);
        $deleteIDs = array_diff($oldIDs, $this->tags);

        if ($addIDs) {
            foreach ($addIDs as $tagID) {
                $tags2table = new Tags2table();
                $tags2table->attributes = [
                    'model_schema' => $owner::tableName(),
                    'model_id' => $owner->id,
                    'tag_id' => $tagID,
                ];
                $tags2table->save();
            }
        }

        if ($deleteIDs) {
            foreach ($deleteIDs as $tagID) {
                Tags2table::deleteAll([
                    'model_schema' => $owner::tableName(),
                    'model_id' => $owner->id,
                    'tag_id' => $tagID,
                ]);
            }
        }

        if ($this->tagsTitles) {
            foreach (array_map('trim', explode(',', $this->tagsTitles)) as $tagTitle) {
                $tags = new Tags();
                $tags->attributes = [
                    'model_schema' => $owner::tableName(),
                    'title' => $tagTitle,
                ];
                $tags->save();

                $tags2table = new Tags2table();
                $tags2table->attributes = [
                    'model_schema' => $owner::tableName(),
                    'model_id' => $owner->id,
                    'tag_id' => $tags->id,
                ];
                $tags2table->save();
            }
        }

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags2table()
    {
        /** @var ActiveRecord $owner */
        $owner = $this->owner;

        return $owner->hasMany(Tags2table::class, ['model_id' => 'id'])->andWhere(['tags2table.model_schema' => $owner->tableName()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        /** @var ActiveRecord $owner */
        $owner = $this->owner;

        return $owner->hasMany(Tags::class, ['id' => 'tag_id'])->via('tags2table');
    }

    /**
     * @return string
     */
    public function getTagsTitles()
    {
        return $this->tagsTitles;
    }

    /**
     * @param string $tagsTitles
     */
    public function setTagsTitles($tagsTitles)
    {
        $this->tagsTitles = $tagsTitles;
    }

    /**
     * @param array $tags
     */
    public function setTags($tags)
    {
        if (!$tags) {
            $tags = [];
        }

        $this->tags = $tags;
    }
}