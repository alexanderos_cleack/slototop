<?php

namespace common\components\behaviors;

use Yii;
use yii\base\Behavior;

use common\models\rating\Rating;

/**
 * Class ArRatingBehavior
 * @package common\components\behaviors
 */
class ArRatingBehavior extends Behavior
{
    public function getRating()
    {
        $owner = $this->owner;
        $rating = $this->getModel($owner, Yii::$app->request->getUserIP());
        if ($rating) return $rating->score;
    }

    public function setRating($score, $ip = '0.0.0.0')
    {
        $owner = $this->owner;
        $rating = $this->getModel($owner, $ip);
        if (!$rating) {
            $rating = new Rating;
            $rating->model_schema = $owner::tableName();
            $rating->model_id = $owner->id;
            $rating->score = $score;
            $rating->ip = $ip;
            $rating->date = new \yii\db\Expression('NOW()');
            $rating->save();

            $query = Rating::find()->where(['model_schema' => $owner::tableName(), 'model_id' => $owner->id]);
            $count_votes = $query->count();
            $sum_votes = $query->sum('score');
            $rating_users = $count_votes ? (int)(($sum_votes / ($count_votes * 5)) * 100) : 1;

            $owner::updateAll(['rating_users' => $rating_users], ['id' => $owner->id]);

            // $owner->rating_users = $rating_users;
            // $owner->save();

        }
    }

    public function getRatingAll()
    {
        $owner = $this->owner;
        return round(($owner->rating_users + $owner->rating_admins) / 2);
    }

    protected function getModel($owner, $ip = '0.0.0.0')
    {
        return Rating::find()->where(['model_schema' => $owner::tableName(), 'model_id' => $owner->id, 'ip' => $ip])->one();
    }

}