<?php

return [
    'translations' => [
        'app*' => [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@common/messages',
            'fileMap' => [
                'app' => 'app.php',
                'app/f' => 'f.php',
                'app/b' => 'b.php',
            ],
        ],
    ],
];