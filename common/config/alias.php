<?php

Yii::setAlias('@root', dirname(dirname(__DIR__)));
Yii::setAlias('@common', '@root/common');
Yii::setAlias('@backend', '@root/backend');
Yii::setAlias('@frontend', '@root/frontend');
Yii::setAlias('@vendor', '@root/common/vendor');
Yii::setAlias('@uploads', '@frontend/www/uploads');
Yii::setAlias('@uploads_url', '/uploads');