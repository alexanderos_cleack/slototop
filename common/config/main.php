<?php

// Config common

require('alias.php');

$config = [
    'name' => 'Sloto.Top',
    'language' => 'ru-RU',
    'charset' => 'UTF-8',
    'timeZone' => 'Europe/Kiev',
    'vendorPath' => '@vendor',
    'bootstrap' => ['log'],
    'extensions' => require(Yii::getAlias('@vendor/yiisoft/extensions.php')),

    'components' => [

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],

        'i18n' => require(__DIR__ . '/i18n.php'),
        'db' => require(__DIR__ . '/db.php'),

    ],

    'aliases' => [
        '@bower' => '@vendor/bower-asset',
    ],

];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['bootstrap'][] = 'gii';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['127.0.0.1', '88.198.240.53'],
    ];
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;