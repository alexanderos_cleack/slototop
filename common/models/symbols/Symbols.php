<?php

namespace common\models\symbols;

use Yii;

use common\components\behaviors\ArImagesBehavior;
use common\traits\ImagesTrait;

/**
 * This is the model class for table "symbols".
 *
 * @property integer $id
 * @property string $multi
 * @property string $description
 */
class Symbols extends \yii\db\ActiveRecord
{
    use ImagesTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'symbols';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slot_id', 'description'], 'required'],
            [['slot_id'], 'integer'],
            [['multi', 'description'], 'string', 'max' => 256],

            [['imagesAlt'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('app', 'ID'),
            'slot_id'     => Yii::t('app', 'Slot id'),
            'multi'       => Yii::t('app', 'Multi'),
            'description' => Yii::t('app', 'Description'),
            'logo'        => Yii::t('app', 'Logo'),
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => ArImagesBehavior::className(),
                'model' => $this,
                'attributesOne' => ['logo'],
            ],
        ];
    }

    public function getMultiSite() {
        $multi = explode(',', $this->multi);
        $multi = array_map(function ($item) {
            return explode('=', $item);
        }, $multi);

        $left = [];
        $right = [];

        foreach ($multi as $item) {
            if (isset($item[0]) && strlen($item[0])) $left[] = $item[0];
            if (isset($item[1]) && strlen($item[1])) $right[] = $item[1];
        }

        return [
            'left' => $left,
            'right' => $right,
        ];
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
            Yii::info("Удаление символа '{$this->multi} | ID - {$this->id} | SLOT_ID - {$this->slot_id}'", 'debug.files');
            return true;
        }
        return false;
    }


}
