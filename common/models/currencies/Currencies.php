<?php

namespace common\models\currencies;

use Yii;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;

use common\components\behaviors\ArImagesBehavior;
use common\components\behaviors\ArSortableBehavior;
use common\models\bonuses\Bonuses;
use common\models\bonuses\Bonuses2Currencies;
use common\traits\ImagesTrait;

/**
 * This is the model class for table "currencies".
 *
 * @property integer $id
 * @property string $title
 * @property string $iso
 * @property integer $sort
 *
 * @property Bonuses2Currencies[] $bonuses2currencies
 * @property Bonuses[] $bonuses
 */
class Currencies extends \yii\db\ActiveRecord {

    use ImagesTrait;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'currencies';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 128],
            [['iso'], 'string', 'max' => 4],
            [['sort'], 'integer'],

            [['id', 'title', 'iso'], 'safe', 'on' => 'search'],

            [['imagesAlt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id'    => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => ArImagesBehavior::className(),
                'model' => $this,
                'attributesOne' => ['logo_mini'],
            ],
            [
                'class' => ArSortableBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonuses2currencies() {
        return $this->hasMany(Bonuses2Currencies::className(), ['currency_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonuses() {
        return $this->hasMany(Bonuses::className(), ['id' => 'bonus_id'])->viaTable('bonuses2currencies', ['currency_id' => 'id']);
    }

    /**
     * @return \yii\data\ActiveDataProvider
     */
    public function search() {
        $query = Currencies::find();

        $query->andFilterWhere(['id' => $this->id]);

        $query->andFilterWhere(['ilike', 'title', $this->title])
              ->andFilterWhere(['ilike', 'iso', $this->iso]);

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['title' => SORT_ASC],
            ],
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
    }

    public static function getForList() {
        return ArrayHelper::map(Currencies::find()->asArray()->orderBy('title')->all(), 'id', 'title');
    }

}
