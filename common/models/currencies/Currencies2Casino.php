<?php

namespace common\models\currencies;

use Yii;

/**
 * This is the model class for table "currencies2casino".
 *
 * @property integer $currency_id
 * @property integer $casino_id
 * @property float $min_deposit
 * @property float $min_output
 *
 * @property Casino $casino
 * @property Currencies $currency
 */
class Currencies2Casino extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currencies2casino';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['currency_id', 'casino_id'], 'required'],
            [['currency_id', 'casino_id'], 'integer'],
            [['min_deposit', 'min_output'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'currency_id' => Yii::t('app/b', 'Currency ID'),
            'casino_id'   => Yii::t('app/b', 'Casino ID'),
            'min_deposit' => Yii::t('app/b', 'currencies2casino min_deposit'),
            'min_output'  => Yii::t('app/b', 'currencies2casino min_output'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCasino()
    {
        return $this->hasOne(Casino::className(), ['id' => 'casino_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currencies::className(), ['id' => 'currency_id']);
    }
}
