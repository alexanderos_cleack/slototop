<?php

namespace common\models\demogames\providers;

use common\models\demogames\Provider;

class LightningBoxGames extends Provider {

    public static function getProviderName() {
        return 'Lightning Box Games';
    }

    public function getProviderIframeUrl() {}

    public function getProviderReplace() {}
}

?>