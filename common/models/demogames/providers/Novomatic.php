<?php

namespace common\models\demogames\providers;

use common\models\demogames\Provider;

class Novomatic extends Provider {

    private $iframe_url = '[url]';

    public static function getProviderGameTitle($item) {
        if ($item['title']) return $item['title'];
        return false;
    }

    public static function getProviderGameOptions($item) {
        $result = ['url' => $item['url']];
        foreach (array_values($result) as $value) {
            if (!$value) return false;
        }
        return json_encode($result);
    }

    public static function getProviderName() {
        return 'Novomatic';
    }

    public function getProviderReplace() {
        $options = $this->getDemoGameOptions();
        return [
            '[url]' => $options->url,
        ];
    }

    public function getProviderIframeUrl() {
        return $this->iframe_url;
    }

}

?>