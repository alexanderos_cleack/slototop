<?php

namespace common\models\demogames\providers;

use common\models\demogames\Provider;

class EGT extends Provider {

    private $iframe_url_gameId = 'https://free.egtmgs.com/sloto.php?gameId=[gameId]';
    private $iframe_url_url = '[url]';

    public static function getProviderGameTitle($item) {
        if ($item['title']) return $item['title'];
        return false;
    }

    public static function getProviderGameOptions($item) {
        if (isset($item['gameId'])) {
            $result = ['gameId' => $item['gameId']];
        }

        if (isset($item['url'])) {
            $result = ['url' => $item['url']];
        }

        foreach (array_values($result) as $value) {
            if (!$value) return false;
        }
        return json_encode($result);
    }

    public static function getProviderName() {
        return 'EGT';
    }

    public function getProviderReplace() {
        $options = $this->getDemoGameOptions();

        $return = array();

        if (isset($options->gameId)) {
            $return['[gameId]'] = $options->gameId;
        }

        if (isset($options->url)) {
            $return['[url]'] = $options->url;
        }

        return $return;
    }

    public function getProviderIframeUrl() {
        $options = $this->getDemoGameOptions();

        if (isset($options->gameId)) {
            return $this->iframe_url_gameId;
        }

        if (isset($options->url)) {
            return $this->iframe_url_url;
        }
    }

}

?>