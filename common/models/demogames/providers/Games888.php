<?php

namespace common\models\demogames\providers;

use common\models\demogames\Provider;

class Games888 extends Provider {

    private $iframe_url = '[url]';

    public static function getProviderGameTitle($item) {
        if ($item['title']) return $item['title'];
        return false;
    }

    public static function getProviderGameOptions($item) {
        $result = ['url' => $item['url']];

        if (preg_match('/id="gameIframe" src="(.*?)"/', $item['url'], $match)) {
            $result['url'] = $match[1];
            // $content = file_get_contents($match[1]);
            // if (preg_match('/gamePath = "(.*?)"/', $content, $match)) {
            //     $result['url'] = 'http://images.images4us.com/' . $match[1];
            // }
        }

        foreach (array_values($result) as $value) {
            if (!$value) return false;
        }
        return json_encode($result);
    }

    public static function getProviderName() {
        return 'Games888';
    }

    public function getProviderReplace() {
        $options = $this->getDemoGameOptions();
        return [
            '[url]' => $options->url,
        ];
    }

    public function getProviderIframeUrl() {
        return $this->iframe_url;
    }

}

?>