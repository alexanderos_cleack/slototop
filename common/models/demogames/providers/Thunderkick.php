<?php

namespace common\models\demogames\providers;

use common\models\demogames\Provider;

class Thunderkick extends Provider {

    public static function getProviderName() {
        return 'Thunderkick';
    }

    public function getProviderIframeUrl() {}

    public function getProviderReplace() {}
}

?>