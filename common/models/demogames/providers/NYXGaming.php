<?php

namespace common\models\demogames\providers;

use common\models\demogames\Provider;

class NYXGaming extends Provider {

    public static function getProviderName() {
        return 'NYX Gaming';
    }

    public function getProviderIframeUrl() {}

    public function getProviderReplace() {}
}

?>