<?php

namespace common\models\demogames\providers;

use common\models\demogames\Provider;

class Realistic extends Provider {

    public static function getProviderName() {
        return 'Realistic';
    }

    public function getProviderIframeUrl() {}

    public function getProviderReplace() {}
}

?>