<?php

namespace common\models\demogames\providers;

use common\models\demogames\Provider;

class Microgaming extends Provider {

    private $iframe_url = 'http://redirector3.valueactive.eu/Casino/Default.aspx?applicationid=1024&theme=quickfire&usertype=5&sext1=demo&sext2=demo&csid=1866&serverid=1866&variant=MIT-Demo&gameid=[gameid]&ul=EN';

    public static function getProviderGameTitle($item) {
        if ($item['GameName']) return $item['GameName'];
        return false;
    }

    public static function getProviderGameOptions($item) {
        $result = ['ServerGameID' => $item['ServerGameID']];
        foreach (array_values($result) as $value) {
            if (!$value) return false;
        }
        return json_encode($result);
    }

    public static function getProviderName() {
        return 'Microgaming';
    }

    public function getProviderReplace() {
        $options = $this->getDemoGameOptions();
        return [
            '[gameid]' => $options->ServerGameID,
        ];
    }

    public function getProviderIframeUrl() {
        return $this->iframe_url;
    }

}

?>