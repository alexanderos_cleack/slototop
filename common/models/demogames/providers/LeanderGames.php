<?php

namespace common\models\demogames\providers;

use common\models\demogames\Provider;

class LeanderGames extends Provider {

    public static function getProviderName() {
        return 'Leander Games';
    }

    public function getProviderIframeUrl() {}

    public function getProviderReplace() {}
}

?>