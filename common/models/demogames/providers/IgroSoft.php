<?php

namespace common\models\demogames\providers;

use common\models\demogames\Provider;

class IgroSoft extends Provider {

    private $iframe_url = '[url]';

    public static function getProviderGameTitle($item) {
        if ($item['title']) return $item['title'];
        return false;
    }

    public static function getProviderGameOptions($item) {

        $subject = file_get_contents($item['url']);

        $pattern = '/<param name="movie" value="(.*?)">/';
        preg_match($pattern, $subject, $url);

        $pattern = '/<param name=FlashVars value="(.*?)" \/>/';
        preg_match($pattern, $subject, $var);

        $url = 'http://www.igrosoft.ru' . $url[1] . '?' . $var[1];

        $result = ['url' => $url];

        foreach (array_values($result) as $value) {
            if (!$value) return false;
        }
        return json_encode($result);
    }

    public static function getProviderName() {
        return 'IgroSoft';
    }

    public function getProviderReplace() {
        $options = $this->getDemoGameOptions();
        return [
            '[url]' => $options->url,
        ];
    }

    public function getProviderIframeUrl() {
        return $this->iframe_url;
    }

}

?>