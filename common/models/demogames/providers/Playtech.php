<?php

namespace common\models\demogames\providers;

use common\models\demogames\Provider;

class Playtech extends Provider {

    private $iframe_url = 'http://cache.download.europacasino.com/safclient.html?game=[game]&advertisercode=slototop&profile=flashga&affiliateRedirectMode=dl&language=en&affiliateWinLimit=999&affiliateStreakLimit=999';

    public static function getProviderGameTitle($item) {
        if ($item['NAME']) return $item['NAME'];
        return false;
    }

    public static function getProviderGameOptions($item) {
        $result = ['TYPE' => $item['TYPE']];
        foreach (array_values($result) as $value) {
            if (!$value) return false;
        }
        return json_encode($result);
    }

    public static function getProviderName() {
        return 'Playtech';
    }

    public function getProviderReplace() {
        $options = $this->getDemoGameOptions();
        return [
            '[game]' => $options->TYPE,
        ];
    }

    public function getProviderIframeUrl() {
        return $this->iframe_url;
    }

}

?>