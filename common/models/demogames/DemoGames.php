<?php

namespace common\models\demogames;

use Yii;
use yii\helpers\ArrayHelper;

use common\models\slots\Slots;

/**
 * This is the model class for table "demo_games".
 *
 * @property integer $id
 * @property string $title
 * @property string $provider
 * @property string $options
 * @property string $url
 * @property string $object
 *
 * @property Slots[] $slots
 */
class DemoGames extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'demo_games';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title', 'provider'], 'string', 'max' => 255],
            [['options', 'url'], 'string', 'max' => 1000],
            [['object'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'provider' => Yii::t('app', 'Vendor'),
            'options' => Yii::t('app', 'Options'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlots() {
        return $this->hasMany(Slots::className(), ['demo_game_id' => 'id']);
    }

    public static function getForList() {
        // return ArrayHelper::map(DemoGames::find()->asArray()->orderBy('title')->all(), 'id', 'title . provider');
        return ArrayHelper::map(DemoGames::findBySql('SELECT id, title || \' - \' || provider as name FROM demo_games ORDER by title')->asArray()->all(), 'id', 'name');
    }

    public function getIframeUrl() {
        return Provider::factory($this)->getGameIframeUrl();
    }

    public function getIframeUrlAdmin() {
        return Provider::factory($this)->getGameIframeUrlAdmin();
    }
}
