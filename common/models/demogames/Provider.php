<?php

namespace common\models\demogames;

use yii\base\Object;

use common\models\demogames\DemoGames;
use common\models\demogames\providers\Ainsworth;
use common\models\demogames\providers\Amatic;
use common\models\demogames\providers\Amaya;
use common\models\demogames\providers\Aristocrat;
use common\models\demogames\providers\BetSoft;
use common\models\demogames\providers\EGT;
use common\models\demogames\providers\ELK;
use common\models\demogames\providers\Games888;
use common\models\demogames\providers\Gaming1;
use common\models\demogames\providers\Gaming1x2;
use common\models\demogames\providers\High5Games;
use common\models\demogames\providers\IgroSoft;
use common\models\demogames\providers\IsoftBet;
use common\models\demogames\providers\Microgaming;
use common\models\demogames\providers\Nektan;
use common\models\demogames\providers\NetEnt;
use common\models\demogames\providers\NextGen;
use common\models\demogames\providers\Novomatic;
use common\models\demogames\providers\Playtech;
use common\models\demogames\providers\Quickspin;
use common\models\demogames\providers\Skillonnet;
use common\models\demogames\providers\Wms;
use common\models\demogames\providers\YdDrassil;
use common\models\demogames\providers\Playson;
use common\models\demogames\providers\Thunderkick;
use common\models\demogames\providers\Realistic;
use common\models\demogames\providers\LightningBoxGames;
use common\models\demogames\providers\NYXGaming;
use common\models\demogames\providers\LeanderGames;
use common\models\demogames\providers\CasionTechnology;
use common\models\demogames\providers\BoomingGames;
use common\models\demogames\providers\Endorphina;
use common\models\demogames\providers\GenesisGaming;
use common\models\demogames\providers\PlayNGo;
use common\models\demogames\providers\GameArt;
use common\models\demogames\providers\Habanero;
use common\models\demogames\providers\GamesOS;
use common\models\demogames\providers\TomHorn;

abstract class Provider extends Object
{

    private $demo_game;

    public function __construct(DemoGames $demo_game, $config = [])
    {
        $this->demo_game = $demo_game;
        parent::__construct($config);
    }

    public static function factory(DemoGames $demo_games)
    {
        $provider = 'common\models\demogames\providers\\' . $demo_games->provider; // @TODO:
        return new $provider($demo_games);
    }

    /**
     * @param  array $item
     * @return string|boolean=false
     */
    public static function getProviderGameTitle($item)
    {
    }

    /**
     * @param  array $item
     * @return string|boolean=false return a string as json
     */
    public static function getProviderGameOptions($item)
    {
    }

    /**
     * @return string
     */
    public static function getProviderName()
    {
    }

    /**
     * @param  array $item
     * @return string
     */
    public static function getProviderObject($item)
    {
    }

    /**
     * @return string
     */
    abstract public function getProviderIframeUrl();

    /**
     * @return array array that contains are keys that used for replace options in iframe url the corresponding values
     */
    abstract public function getProviderReplace();

    /**
     * @param  array $item
     * @return array
     */
    public static function parse($item)
    {
        $title = static::getProviderGameTitle($item);
        $result = [
            'title' => trim($title),
            'options' => static::getProviderGameOptions($item),
            'provider' => static::getProviderName(),
        ];
        foreach (array_values($result) as $value) {
            if (!$value) return false;
        }
        return $result;
    }

    public static function parseObject($item)
    {
        $result = [
            'title' => static::getProviderGameTitle($item),
            'provider' => static::getProviderName(),
            'object' => static::getProviderObject($item),
        ];
        foreach (array_values($result) as $value) {
            if (!$value) return false;
        }
        return $result;
    }

    public static function parseItem($item)
    {
        $result = [
            'title' => $item['title'],
            'provider' => static::getProviderName(),
        ];

        if (strpos($item['item'], 'http') == 0) {
            $result['url'] = $item['item'];
        } else {
            $result['object'] = $item['item'];
        }

        foreach (array_values($result) as $value) {
            if (!$value) return false;
        }
        return $result;
    }

    /**
     * @return array
     */
    public static function getProviders()
    {
        return [
            'BluePrint Gaming' => 'BluePrint Gaming',
            'Booongo' => 'Booongo',
            'EvoPlay' => 'EvoPlay',
            'Fugaso' => 'Fugaso',
            'Gameplay Interactive' => 'Gameplay Interactive',
            'IGT' => 'IGT',
            'Pragmatic Play' => 'Pragmatic Play',
            'Rabcat' => 'Rabcat',
            'Stakelogic' => 'Stakelogic',
            Ainsworth::getProviderName() => Ainsworth::getProviderName(),
            Amatic::getProviderName() => Amatic::getProviderName(),
            Amaya::getProviderName() => Amaya::getProviderName(),
            Aristocrat::getProviderName() => Aristocrat::getProviderName(),
            BetSoft::getProviderName() => BetSoft::getProviderName(),
            BoomingGames::getProviderName() => BoomingGames::getProviderName(),
            CasionTechnology::getProviderName() => CasionTechnology::getProviderName(),
            EGT::getProviderName() => EGT::getProviderName(),
            ELK::getProviderName() => ELK::getProviderName(),
            Endorphina::getProviderName() => Endorphina::getProviderName(),
            GameArt::getProviderName() => GameArt::getProviderName(),
            Games888::getProviderName() => Games888::getProviderName(),
            GamesOS::getProviderName() => GamesOS::getProviderName(),
            Gaming1::getProviderName() => Gaming1::getProviderName(),
            Gaming1x2::getProviderName() => Gaming1x2::getProviderName(),
            GenesisGaming::getProviderName() => GenesisGaming::getProviderName(),
            Habanero::getProviderName() => Habanero::getProviderName(),
            High5Games::getProviderName() => High5Games::getProviderName(),
            IgroSoft::getProviderName() => IgroSoft::getProviderName(),
            IsoftBet::getProviderName() => IsoftBet::getProviderName(),
            LeanderGames::getProviderName() => LeanderGames::getProviderName(),
            LightningBoxGames::getProviderName() => LightningBoxGames::getProviderName(),
            Microgaming::getProviderName() => Microgaming::getProviderName(),
            Nektan::getProviderName() => Nektan::getProviderName(),
            NetEnt::getProviderName() => NetEnt::getProviderName(),
            NextGen::getProviderName() => NextGen::getProviderName(),
            Novomatic::getProviderName() => Novomatic::getProviderName(),
            NYXGaming::getProviderName() => NYXGaming::getProviderName(),
            PlayNGo::getProviderName() => PlayNGo::getProviderName(),
            Playson::getProviderName() => Playson::getProviderName(),
            Playtech::getProviderName() => Playtech::getProviderName(),
            Quickspin::getProviderName() => Quickspin::getProviderName(),
            Realistic::getProviderName() => Realistic::getProviderName(),
            Skillonnet::getProviderName() => Skillonnet::getProviderName(),
            Thunderkick::getProviderName() => Thunderkick::getProviderName(),
            TomHorn::getProviderName() => TomHorn::getProviderName(),
            Wms::getProviderName() => Wms::getProviderName(),
            YdDrassil::getProviderName() => YdDrassil::getProviderName(),
        ];
    }

    public static function getProvidersForFilter()
    {
        return [
            'BluePrint Gaming' => 'BluePrint Gaming',
            'Booongo' => 'Booongo',
            'EvoPlay' => 'EvoPlay',
            'Fugaso' => 'Fugaso',
            'Gameplay Interactive' => 'Gameplay Interactive',
            'Pragmatic Play' => 'Pragmatic Play',
            'Rabcat' => 'Rabcat',
            'Stakelogic' => 'Stakelogic',
            Ainsworth::getProviderName() => Ainsworth::getProviderName(),
            Amatic::getProviderName() => Amatic::getProviderName(),
            Amaya::getProviderName() => Amaya::getProviderName(),
            Aristocrat::getProviderName() => Aristocrat::getProviderName(),
            BetSoft::getProviderName() => BetSoft::getProviderName(),
            BoomingGames::getProviderName() => BoomingGames::getProviderName(),
            CasionTechnology::getProviderName() => CasionTechnology::getProviderName(),
            EGT::getProviderName() => EGT::getProviderName(),
            ELK::getProviderName() => ELK::getProviderName(),
            Endorphina::getProviderName() => Endorphina::getProviderName(),
            GameArt::getProviderName() => GameArt::getProviderName(),
            Games888::getProviderName() => Games888::getProviderName(),
            GamesOS::getProviderName() => GamesOS::getProviderName(),
            Gaming1::getProviderName() => Gaming1::getProviderName(),
            Gaming1x2::getProviderName() => Gaming1x2::getProviderName(),
            GenesisGaming::getProviderName() => GenesisGaming::getProviderName(),
            Habanero::getProviderName() => Habanero::getProviderName(),
            High5Games::getProviderName() => High5Games::getProviderName(),
            IgroSoft::getProviderName() => IgroSoft::getProviderName(),
            IsoftBet::getProviderName() => IsoftBet::getProviderName(),
            LeanderGames::getProviderName() => LeanderGames::getProviderName(),
            LightningBoxGames::getProviderName() => LightningBoxGames::getProviderName(),
            Microgaming::getProviderName() => Microgaming::getProviderName(),
            Nektan::getProviderName() => Nektan::getProviderName(),
            NetEnt::getProviderName() => NetEnt::getProviderName(),
            NextGen::getProviderName() => NextGen::getProviderName(),
            Novomatic::getProviderName() => Novomatic::getProviderName(),
            NYXGaming::getProviderName() => NYXGaming::getProviderName(),
            PlayNGo::getProviderName() => PlayNGo::getProviderName(),
            Playson::getProviderName() => Playson::getProviderName(),
            Playtech::getProviderName() => Playtech::getProviderName(),
            Quickspin::getProviderName() => Quickspin::getProviderName(),
            Realistic::getProviderName() => Realistic::getProviderName(),
            Skillonnet::getProviderName() => Skillonnet::getProviderName(),
            Thunderkick::getProviderName() => Thunderkick::getProviderName(),
            TomHorn::getProviderName() => TomHorn::getProviderName(),
            Wms::getProviderName() => Wms::getProviderName(),
            YdDrassil::getProviderName() => YdDrassil::getProviderName(),
        ];
    }

    /**
     * @return string return iframe url with the corresponding values
     */
    public function getGameIframeUrl()
    {
        $iframe_url = $this->getProviderIframeUrl();
        $replace = $this->getProviderReplace();
        return str_replace(array_keys($replace), array_values($replace), $this->getProviderIframeUrl());
    }

    public function getGameIframeUrlAdmin()
    {
        $iframe_url = $this->getProviderIframeUrl();
        $replace = $this->getProviderReplace();
        return str_replace(array_keys($replace), array_values($replace), $this->getProviderIframeUrl());
    }

    /**
     * @return object
     */
    protected function getDemoGameOptions()
    {
        // print_r(json_decode($this->demo_game->options, true)); exit;
        return json_decode($this->demo_game->options);
    }

}

?>