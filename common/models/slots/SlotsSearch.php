<?php

namespace common\models\slots;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * SlotsSearch represents the model behind the search form about `common\models\slots\Slots`.
 */
class SlotsSearch extends Slots
{
    public $vendor;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'rating_users', 'rating_admins', 'amount_wheels', 'amount_hands', 'min_boxes', 'max_boxes', 'amount_lines', 'amount_reels', 'zero', 'demo_game_id', 'jackpot', 'amount_combinations'], 'integer'],
            [['title', 'description', 'demo_game_url', 'video_viewer_url', 'video_interesting_url', 'kind_of_roulette', 'kind_of_poker', 'kind_of_lottery', 'joker', 'min_combination', 'special_combination', 'version', 'payout', 'new_to_date', 'slug', 'numeric_series'], 'safe'],
            [['new', 'wild_symbol', 'skatter', 'bonus_game', 'free_spins', 'doubling_game', 'auto_play', 'bet_color', 'bet_more_less', 'bet_even_odd', 'bet_dozen', 'bet_line', 'risk_game', 'triplication_game', 'doubling_games', 'retake', 'splite', 'surrender', 'insurance', 'enabled'], 'boolean'],
            [['bet_min', 'bet_max', 'bet_min_number', 'bet_max_number', 'bet_min_hand', 'bet_max_hand', 'win_max'], 'number'],

            [['vendor'], 'integer'],

            [['updated_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = Slots::find()->select(['slots.id', 'slots.title', 'slots.slug', 'slots.enabled', 'slots.updated_time', 'slots.category_id', 'slots.main_page', 'slots.rating_users', 'slots.rating_admins', '(slots.rating_users + slots.rating_admins) as rating_all'])->joinWith(['vendor', 'category']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'title' => SORT_ASC,
                ],
                'attributes' => [
                    'id',
                    'title' => [
                        'asc' => ['slots.title' => SORT_ASC],
                        'desc' => ['slots.title' => SORT_DESC],
                    ],
                    'vendor' => [
                        'asc' => ['vendors.title' => SORT_ASC],
                        'desc' => ['vendors.title' => SORT_DESC],
                    ],
                    'updated_time' => [
                        'asc' => ['slots.updated_time' => SORT_ASC],
                        'desc' => ['slots.updated_time' => SORT_DESC],
                    ],
                    'category_id' => [
                        'asc' => ['category.title' => SORT_ASC],
                        'desc' => ['category.title' => SORT_DESC],
                    ],
                    'main_page' => [
                        'asc' => ['slots.main_page' => SORT_ASC],
                        'desc' => ['slots.main_page' => SORT_DESC],
                    ],
                    'rating_users' => [
                        'asc' => ['slots.rating_users' => SORT_ASC],
                        'desc' => ['slots.rating_users' => SORT_DESC],
                    ],
                    'rating_admins' => [
                        'asc' => ['slots.rating_admins' => SORT_ASC],
                        'desc' => ['slots.rating_admins' => SORT_DESC],
                    ],
                    'rating_all' => [
                        'asc' => ['(slots.rating_admins + slots.rating_users)' => SORT_ASC],
                        'desc' => ['(slots.rating_admins + slots.rating_users)' => SORT_DESC]
                    ],
                ],
            ],
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'new' => $this->new,
            'rating_users' => $this->rating_users,
            'rating_admins' => $this->rating_admins,
            'wild_symbol' => $this->wild_symbol,
            'skatter' => $this->skatter,
            'bonus_game' => $this->bonus_game,
            'free_spins' => $this->free_spins,
            'doubling_game' => $this->doubling_game,
            'auto_play' => $this->auto_play,
            'bet_color' => $this->bet_color,
            'bet_more_less' => $this->bet_more_less,
            'bet_even_odd' => $this->bet_even_odd,
            'bet_dozen' => $this->bet_dozen,
            'bet_line' => $this->bet_line,
            'risk_game' => $this->risk_game,
            'triplication_game' => $this->triplication_game,
            'amount_wheels' => $this->amount_wheels,
            'amount_hands' => $this->amount_hands,
            'min_boxes' => $this->min_boxes,
            'max_boxes' => $this->max_boxes,
            'amount_lines' => $this->amount_lines,
            'amount_reels' => $this->amount_reels,
            'bet_min' => $this->bet_min,
            'bet_max' => $this->bet_max,
            'new_to_date' => $this->new_to_date,
            'bet_min_number' => $this->bet_min_number,
            'bet_max_number' => $this->bet_max_number,
            'doubling_games' => $this->doubling_games,
            'zero' => $this->zero,
            'bet_min_hand' => $this->bet_min_hand,
            'bet_max_hand' => $this->bet_max_hand,
            'retake' => $this->retake,
            'splite' => $this->splite,
            'surrender' => $this->surrender,
            'insurance' => $this->insurance,
            'win_max' => $this->win_max,
            'demo_game_id' => $this->demo_game_id,
            'jackpot' => $this->jackpot,
            'amount_combinations' => $this->amount_combinations,
            'enabled' => $this->enabled,
            'slots.main_page' => $this->main_page,

            'vendor_id' => $this->vendor,

            'slots.updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['ilike', 'slots.title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'demo_game_url', $this->demo_game_url])
            ->andFilterWhere(['like', 'video_viewer_url', $this->video_viewer_url])
            ->andFilterWhere(['like', 'video_interesting_url', $this->video_interesting_url])
            ->andFilterWhere(['like', 'kind_of_roulette', $this->kind_of_roulette])
            ->andFilterWhere(['like', 'kind_of_poker', $this->kind_of_poker])
            ->andFilterWhere(['like', 'kind_of_lottery', $this->kind_of_lottery])
            ->andFilterWhere(['like', 'joker', $this->joker])
            ->andFilterWhere(['like', 'min_combination', $this->min_combination])
            ->andFilterWhere(['like', 'special_combination', $this->special_combination])
            ->andFilterWhere(['like', 'version', $this->version])
            ->andFilterWhere(['like', 'payout', $this->payout])
            ->andFilterWhere(['like', 'slots.slug', $this->slug])
            ->andFilterWhere(['like', 'numeric_series', $this->numeric_series]);

        return $dataProvider;
    }

}