<?php

namespace common\models\slots;

use Yii;

/**
 * This is the model class for table "slots2categories".
 *
 * @property integer $slot_id
 * @property integer $category_id
 *
 * @property Slots $slot
 * @property SlotsCategories $category
 */
class Slots2Categories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slots2categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slot_id', 'category_id'], 'required'],
            [['slot_id', 'category_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'slot_id' => 'Slot ID',
            'category_id' => 'Category ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlot()
    {
        return $this->hasOne(Slots::className(), ['id' => 'slot_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(SlotsCategories::className(), ['id' => 'category_id']);
    }
}
