<?php

namespace common\models\slots;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use common\components\behaviors\ArImagesBehavior;
use common\components\behaviors\ArManyToManyBehavior;
use common\components\behaviors\ArMetaTagsBehavior;
use common\components\behaviors\ArRatingBehavior;
use common\components\behaviors\ArTagsBehavior;
use common\models\casino\Casino;
use common\models\demogames\DemoGames;
use common\models\images\Images;
use common\models\symbols\Symbols;
use common\models\vendors\Vendors2Casino;
use common\models\vendors\Vendors;
use common\models\slots\Slots2Vendors;
use common\models\currencies\Currencies;
use common\models\currencies\Currencies2Casino;
use common\traits\ImagesTrait;

/**
 * This is the model class for table "slots".
 *
 * @property boolean $auto_play
 * @property boolean $bet_color
 * @property boolean $bet_dozen
 * @property boolean $bet_even_odd
 * @property boolean $bet_line
 * @property boolean $bet_more_less
 * @property boolean $bonus_game
 * @property boolean $doubling_game
 * @property boolean $doubling_games
 * @property boolean $enabled
 * @property boolean $free_spins
 * @property boolean $insurance
 * @property boolean $new
 * @property boolean $retake
 * @property boolean $risk_game
 * @property boolean $skatter
 * @property boolean $splite
 * @property boolean $surrender
 * @property boolean $triplication_game
 * @property boolean $wild_symbol
 * @property double $bet_max
 * @property double $bet_max_hand
 * @property double $bet_max_number
 * @property double $bet_min
 * @property double $bet_min_hand
 * @property double $bet_min_number
 * @property string $win_max
 * @property integer $amount_combinations
 * @property integer $amount_hands
 * @property integer $amount_lines
 * @property integer $amount_reels
 * @property integer $amount_wheels
 * @property integer $category_id
 * @property integer $demo_game_id
 * @property integer $id
 * @property integer $jackpot
 * @property integer $max_boxes
 * @property integer $min_boxes
 * @property integer $rating_admins
 * @property integer $rating_users
 * @property integer $zero
 * @property string $demo_game_url
 * @property string $description
 * @property string $joker
 * @property string $kind_of_cards
 * @property string $kind_of_lottery
 * @property string $kind_of_poker
 * @property string $kind_of_roulette
 * @property string $min_combination
 * @property string $new_to_date
 * @property string $numeric_series
 * @property string $payout
 * @property string $slug
 * @property string $special_combination
 * @property string $title
 * @property string $version
 * @property string $video_interesting_url
 * @property string $video_viewer_url
 * @property string $another_titles
 *
 * @property DemoGames $demoGame
 * @property Vendors[] $vendors
 * @property Vendors $vendor
 * @property Symbols[] $symbols
 *
 * @property string $meta_title
 * @property string $meta_h1
 * @property string $meta_keywords
 * @property string $meta_description
 *
 * @property SlotsCategories $category
 *
 * @method ActiveQuery getTags
 */
class Slots extends \yii\db\ActiveRecord {
    use ImagesTrait;

    public $rating_all;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'slots';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title', 'description', 'category_id'], 'required'],
            [['description'], 'string'],
            [['category_id', 'rating_users', 'rating_admins', 'amount_wheels', 'amount_hands', 'min_boxes', 'max_boxes', 'amount_lines', 'amount_reels', 'zero', 'demo_game_id', 'jackpot', 'amount_combinations'], 'integer'],
            [['new', 'wild_symbol', 'skatter', 'bonus_game', 'free_spins', 'doubling_game', 'auto_play', 'bet_color', 'bet_more_less', 'bet_even_odd', 'bet_dozen', 'bet_line', 'risk_game', 'triplication_game', 'doubling_games', 'retake', 'splite', 'surrender', 'insurance', 'enabled'], 'boolean'],
            [['bet_min', 'bet_max', 'bet_min_number', 'bet_max_number', 'bet_min_hand', 'bet_max_hand'], 'number'],
            [['new_to_date'], 'safe'],
            [['rating_users', 'rating_admins'], 'default', 'value' => 0],
            [['title', 'win_max'], 'string', 'max' => 128],
            [['another_titles'], 'string', 'max' => 512],
            [['demo_game_url', 'video_viewer_url', 'video_interesting_url'], 'string', 'max' => 256],
            [['kind_of_cards', 'kind_of_roulette', 'kind_of_poker', 'kind_of_lottery', 'joker', 'min_combination', 'special_combination', 'version', 'payout', 'slug', 'numeric_series'], 'string', 'max' => 255],
            [['title'], 'unique'],
            [['main_page'], 'boolean'],

            [['meta_title', 'meta_h1', 'meta_keywords', 'meta_description'], 'string', 'max' => 256],

            [['vendors'], 'required'],

            ['slug', 'required'],
            [['title', 'slug'], 'unique'],

            [['tags', 'tagsTitles', 'imagesAlt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id'                    => Yii::t('app','ID'),
            'title'                 => Yii::t('app', 'Title'),
            'description'           => Yii::t('app', 'Description'),
            'demo_game_url'         => Yii::t('app', 'Demo game url'),
            'video_viewer_url'      => Yii::t('app', 'Video viewer url'),
            'video_interesting_url' => Yii::t('app', 'Video interesting url'),
            'payout'                => Yii::t('app', 'Payout'),
            'rating_users'          => Yii::t('app', 'Rating users'),
            'rating_admins'         => Yii::t('app', 'Rating admins'),
            'rating_all'            => Yii::t('app/b', 'Rating'),
            'category_id'           => Yii::t('app', 'Category'),
            'vendors'               => Yii::t('app', 'Vendor'),
            'new'                   => Yii::t('app', 'New'),
            'new_to_date'           => Yii::t('app', 'New to date'),
            'logo'                  => Yii::t('app', 'Logo'),
            'wild_symbol'         => Yii::t('app', 'Wild symbol'),
            'skatter'             => Yii::t('app', 'Skatter'),
            'bonus_game'          => Yii::t('app', 'Bonus game'),
            'free_spins'          => Yii::t('app', 'Free spins'),
            'doubling_game'       => Yii::t('app', 'Doubling game'),
            'auto_play'           => Yii::t('app', 'Auto play'),
            'amount_combinations' => Yii::t('app', 'Amount combinations'),
            'amount_lines'        => Yii::t('app', 'Amount lines'),
            'amount_reels'        => Yii::t('app', 'Amount reels'),
            'bet_color'           => Yii::t('app', 'Bet color'),
            'bet_more_less'       => Yii::t('app', 'Bet more less'),
            'bet_even_odd'        => Yii::t('app', 'Bet even odd'),
            'bet_dozen'           => Yii::t('app', 'Bet dozen'),
            'bet_line'            => Yii::t('app', 'Bet line'),
            'risk_game'           => Yii::t('app', 'Risk game'),
            'triplication_game'   => Yii::t('app', 'Triplication game'),
            'kind_of_cards'       => Yii::t('app', 'Kind of cards'),
            'kind_of_roulette'    => Yii::t('app', 'Kind of roulette'),
            'kind_of_poker'       => Yii::t('app', 'Kind of poker'),
            'kind_of_lottery'     => Yii::t('app', 'Kind of lottery'),
            'win_max'             => Yii::t('app', 'Win max'),
            'amount_wheels'       => Yii::t('app', 'Amount wheels'),
            'amount_hands'        => Yii::t('app', 'Amount hands'),
            'numeric_series'      => Yii::t('app', 'Numeric series'),
            'joker'               => Yii::t('app', 'Joker'),
            'min_combination'     => Yii::t('app', 'Min combination'),
            'special_combination' => Yii::t('app', 'Special combination'),
            'min_boxes'           => Yii::t('app', 'Min boxes'),
            'max_boxes'           => Yii::t('app', 'Max boxes'),
            'version'             => Yii::t('app', 'Version'),
            'bet_min'             => Yii::t('app', 'Bet min'),
            'bet_max'             => Yii::t('app', 'Bet max'),
            'zero'                => Yii::t('app', 'Zero'),
            'insurance'           => Yii::t('app', 'Insurance'),
            'splite'              => Yii::t('app', 'Splite'),
            'retake'              => Yii::t('app', 'Retake'),
            'surrender'           => Yii::t('app', 'Surrender'),
            'bet_min_hand'        => Yii::t('app', 'Bet min hand'),
            'bet_max_hand'        => Yii::t('app', 'Bet max hand'),
            'bet_min_number'      => Yii::t('app', 'Bet min number'),
            'bet_max_number'      => Yii::t('app', 'Bet max number'),
            'doubling_games'      => Yii::t('app', 'Doubling games'),
            'jackpot'             => Yii::t('app', 'Jackpot'),
            'enabled'             => Yii::t('app/b', 'Show page'),
            'main_page'           => Yii::t('app/b', 'main_page'),
            'another_titles'      => Yii::t('app/b', 'Другие названия'),

            'created_time'             => Yii::t('app/b', 'created_time'),
            'updated_time'             => Yii::t('app/b', 'updated_time'),

            'meta_title' => Yii::t('app', 'Meta title'),
            'meta_keywords' => Yii::t('app', 'Meta keywords'),
            'meta_description' => Yii::t('app', 'Meta description'),

            'vendor' => Yii::t('app/b', 'Vendor'),
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => ArManyToManyBehavior::class,
                'intermediaries' => [Slots2Vendors::class],
                'attributes' => ['vendors'],
                'table_a_fkey' => 'slot_id',
                'table_b_fkeys' => ['vendor_id'],
            ],
            [
                'class' => ArImagesBehavior::class,
                'model' => $this,
                'attributesOne' => ['logo', 'bigscreen'],
                'attributesMany' => ['screens'],
            ],
            // [
            //     'class' => SluggableBehavior::class,
            //     'attribute' => 'title',
            //     'immutable' => false,
            // ],
            [
                'class' => ArMetaTagsBehavior::class,
            ],
            [
                'class' => ArRatingBehavior::class,
            ],
            [
                'class' => \yii\behaviors\TimestampBehavior::class,
                'createdAtAttribute' => 'created_time',
                'updatedAtAttribute' => 'updated_time',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            [
                'class' => ArTagsBehavior::class,
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlots2vendors() {
        return $this->hasMany(Slots2vendors::class, ['slot_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendors() {
        return $this->hasMany(Vendors::class, ['id' => 'vendor_id'])->via('slots2vendors');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor() {
        return $this->hasOne(Vendors::class, ['id' => 'vendor_id'])->via('slots2vendors');
    }

    public function setVendors($vendors) {
        $this->vendors = $vendors;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendors2casino() {
        return $this->hasMany(Vendors2Casino::class, ['vendor_id' => 'vendor_id'])->via('slots2vendors');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCasino() {
        return $this->hasMany(Casino::class, ['id' => 'casino_id'])->via('vendors2casino');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory() {
        return $this->hasOne(SlotsCategories::class, ['id' => 'category_id']);
    }


    public function getSymbols() {
        return $this->hasMany(Symbols::class, ['slot_id' => 'id'])->orderBy('symbols.id');
    }

    /**
     * @return \yii\data\ActiveDataProvider
     */
    public function search() {
        $query = Slots::find();

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'title' => SORT_ASC,
                ],
            ],
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
            foreach ($this->symbols as $symbol) {
                $symbol->delete();
            }
            return true;
        }
        return false;
    }

    public function getSymbolsSite() {
        $symbols = $this->symbols;

        $__symbols = [];

        foreach ($symbols as $i => $symbol) {
            if ($i == 0) {
                $_symbols[] = $symbol;
            } elseif (!($i % 2)) {
                $_symbols[] = $symbol;
            } else {
                $_symbols[] = $symbol;
                $__symbols[] = $_symbols;
                $_symbols = [];
            }
        }

        if (isset($i) && ($i == 0 || !($i % 2))) {
            $__symbols[] = $_symbols;
        }

        return $__symbols;
    }

    public function getPropertiesSite() {
        $properties = SlotsCategories::findOne($this->category_id);
        $properties_list = $properties->getPropertiesList();

        $_properties_list = [];

        foreach ($properties_list as $property => $css) {
            if ($properties->$property && $this->$property != null || !empty($this->$property)) {
                $_properties_list[$property] = $css;
            }
        }

        if (!count($_properties_list)) return [];

        if (count($_properties_list) == 1) return array($_properties_list);

        $result = array_chunk($_properties_list, count($_properties_list) / 2, true);
        return $result;
    }

    public function getUrl($absolute = false) {
        return Url::to('/game/' . $this->slug, $absolute);
    }

    public function getDemoGame() {
        return $this->hasOne(DemoGames::class, ['id' => 'demo_game_id']);
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->description = str_ireplace('&amp;', '&', $this->description);
            return true;
        }
        return false;
    }

    public function getLogoImage() {
        return $this->hasOne(Images::class, ['model_id' => 'id'])->where(['model_schema' => $this->tableName(), 'attribute' => 'logo', 'model_id']);
    }

    public static function getForList() {
        return ArrayHelper::map(Slots::find()->orderBy('title')->asArray()->all(), 'id', 'title');
    }

    public static function getRouletteTypeForList() {
        return ArrayHelper::map(Slots::find()->select('kind_of_roulette')->distinct()->where(['!=', 'kind_of_roulette', ''])->orderBy('kind_of_roulette')->all(), 'kind_of_roulette', 'kind_of_roulette');
    }

    public static function getCardsTypeForList() {
        return ArrayHelper::map(Slots::find()->select('kind_of_cards')->distinct()->where(['!=', 'kind_of_cards', ''])->orderBy('kind_of_cards')->all(), 'kind_of_cards', 'kind_of_cards');
    }

    public static function getLotteryTypeForList() {
        return ArrayHelper::map(Slots::find()->select('kind_of_lottery')->distinct()->where(['!=', 'kind_of_lottery', ''])->orderBy('kind_of_lottery')->all(), 'kind_of_lottery', 'kind_of_lottery');
    }

    /**
     * @return ActiveQuery
     */
    public function getGamesSameQuery() {
        $tagsIDs = array_keys(ArrayHelper::map($this->getTags()->all(), 'id', 'id'));

        $query = Slots::find()
            ->from(['t' => $this::tableName()])
            ->innerJoinWith('tags2table')
            ->andWhere(['t.enabled' => true])
            ->andWhere(['!=', 't.id', $this->id])
            ->andWhere(['in', 'tags2table.tag_id', $tagsIDs]);

        if (!$query->count()) {
            $query = Slots::find()->from(['t' => $this::tableName()])->where([
                't.category_id' => $this->category_id,
                't.bonus_game' => $this->bonus_game,
                't.free_spins' => $this->free_spins,
                't.wild_symbol' => $this->wild_symbol,
                't.enabled' => true,
            ])->andWhere(['!=', 't.id', $this->id]);
        }

        return $query;
    }

    public function getCasino_site() {
        $query = $this->hasMany(Casino::class, ['id' => 'casino_id'])->where(['enabled' => true])->via('vendors2casino');
        $count = $query->count();
        return  $query->offset(rand(1, $count) - 4)->limit(4)->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCasino_query() {
        return $this->hasMany(Casino::class, ['t.id' => 'casino_id'])->from(['t' => Casino::tableName()])->where(['t.enabled' => true])->via('vendors2casino');
    }

    /**
     * @return string
     */
    public function getTitlePreview() {
        return str_replace(['Игра ', 'Игровой автомат ', 'Видео покер ', 'Видеопокер ', 'Карточная игра ', 'Лотерея ', 'Рулетка '], '', $this->title);
    }

    /**
     * @return ActiveQuery
     */
    public function getLogo()
    {
        return $this->hasOne(Images::class, ['model_id' => 'id'])->where(['model_schema' => $this::tableName(), 'attribute' => 'logo']);
    }

    /**
     * @return Images
     */
    public function getImageLogo() {
        $query = $this->getLogo();
        if ($query->count()) {
            /** @var Images $logo */
            $logo = $query->one();
            return $logo;
        }

        return new Images();
    }
}
