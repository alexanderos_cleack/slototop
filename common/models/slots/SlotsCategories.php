<?php

namespace common\models\slots;

use Yii;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use yii\behaviors\SluggableBehavior;

use common\components\behaviors\ArImagesBehavior;
use common\traits\ImagesTrait;

/**
 * This is the model class for table "slots_categories".
 *
 * @property integer $id
 * @property string $title
 * @property boolean $wild_symbol
 * @property boolean $skatter
 * @property boolean $bonus_game
 * @property boolean $free_spins
 * @property boolean $doubling_game
 * @property boolean $auto_play
 * @property boolean $amount_lines
 * @property boolean $amount_reels
 * @property boolean $bet_color
 * @property boolean $bet_more_less
 * @property boolean $bet_even_odd
 * @property boolean $bet_dozen
 * @property boolean $bet_line
 * @property boolean $risk_game
 * @property boolean $triplication_game
 * @property boolean $kind_of_cards
 * @property boolean $kind_of_roulette
 * @property boolean $kind_of_poker
 * @property boolean $kind_of_lottery
 * @property boolean $win_max
 * @property boolean $amount_wheels
 * @property boolean $amount_hands
 * @property boolean $numeric_series
 * @property boolean $joker
 * @property boolean $min_combination
 * @property boolean $special_combination
 * @property boolean $min_boxes
 * @property boolean $max_boxes
 * @property boolean $version
 * @property boolean $bet_min
 * @property boolean $bet_max
 * @property boolean $video_viewer_url
 * @property string $slug
 * @property boolean $jackpot
 * @property boolean $bet_min_number
 * @property boolean $bet_max_number
 * @property boolean $doubling_games
 * @property boolean $zero
 * @property boolean $bet_min_hand
 * @property boolean $bet_max_hand
 * @property boolean $retake
 * @property boolean $splite
 * @property boolean $surrender
 * @property boolean $insurance
 * @property string $filter
 *
 * @property Slots2categories[] $slots2categories
 * @property Slots[] $slots
 */
class SlotsCategories extends \yii\db\ActiveRecord
{
    use ImagesTrait;

    const SLOT_CATEGORY_ARCADE = 'arcade';
    const SLOT_CATEGORY_CARDS = 'cards';
    const SLOT_CATEGORY_GAME = 'game';
    const SLOT_CATEGORY_LOTTERY = 'lottery';
    const SLOT_CATEGORY_OTHER = 'other';
    const SLOT_CATEGORY_ROULETTE = 'roulette';
    const SLOT_CATEGORY_VIDEOPOKER = 'videopoker';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slots_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['wild_symbol', 'skatter', 'bonus_game', 'free_spins', 'doubling_game', 'auto_play'], 'boolean'],
            [['amount_reels', 'amount_lines', 'amount_combinations', 'bet_color', 'bet_more_less', 'bet_max', 'bet_min', 'bet_even_odd', 'bet_dozen'], 'boolean'],
            [['bet_line', 'risk_game', 'triplication_game', 'kind_of_roulette', 'kind_of_poker', 'kind_of_lottery', 'kind_of_cards'], 'boolean'],
            [['win_max', 'amount_wheels', 'amount_hands', 'numeric_series', 'joker', 'min_combination'], 'boolean'],
            [['special_combination', 'min_boxes', 'max_boxes', 'version', 'video_viewer_url', 'bet_min_number', 'bet_max_number', 'zero'], 'boolean'],
            [['bet_min_hand', 'bet_max_hand', 'surrender', 'splite', 'insurance', 'jackpot', 'doubling_games', 'retake'], 'boolean'],
            [['title'], 'string', 'max' => 128],
            [['filter'], 'string', 'max' => 64],

            [['imagesAlt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'amount_hands' => Yii::t('app', 'Amount hands'),
            'amount_combinations' => Yii::t('app', 'Amount combinations'),
            'amount_lines' => Yii::t('app', 'Amount lines'),
            'amount_reels' => Yii::t('app', 'Amount reels'),
            'amount_wheels' => Yii::t('app', 'Amount wheels'),
            'auto_play' => Yii::t('app', 'Auto play'),
            'bet_color' => Yii::t('app', 'Bet color'),
            'bet_dozen' => Yii::t('app', 'Bet dozen'),
            'bet_even_odd' => Yii::t('app', 'Bet even odd'),
            'bet_line' => Yii::t('app', 'Bet line'),
            'bet_max' => Yii::t('app', 'Bet max'),
            'bet_max_hand' => Yii::t('app', 'Bet max hand'),
            'bet_max_number' => Yii::t('app', 'Bet max number'),
            'bet_min' => Yii::t('app', 'Bet min'),
            'bet_min_hand' => Yii::t('app', 'Bet min hand'),
            'bet_min_number' => Yii::t('app', 'Bet min number'),
            'bet_more_less' => Yii::t('app', 'Bet more less'),
            'bonus_game' => Yii::t('app', 'Bonus game'),
            'doubling_game' => Yii::t('app', 'Doubling game'),
            'doubling_games' => Yii::t('app', 'Doubling games'),
            'free_spins' => Yii::t('app', 'Free spins'),
            'id' => Yii::t('app', 'ID'),
            'insurance' => Yii::t('app', 'Insurance'),
            'jackpot' => Yii::t('app', 'Jackpot'),
            'joker' => Yii::t('app', 'Joker'),
            'kind_of_cards' => Yii::t('app', 'Kind of cards'),
            'kind_of_lottery' => Yii::t('app', 'Kind of lottery'),
            'kind_of_poker' => Yii::t('app', 'Kind of poker'),
            'kind_of_roulette' => Yii::t('app', 'Kind of roulette'),
            'logo' => Yii::t('app', 'Logo'),
            'max_boxes' => Yii::t('app', 'Max boxes'),
            'min_boxes' => Yii::t('app', 'Min boxes'),
            'min_combination' => Yii::t('app', 'Min combination'),
            'numeric_series' => Yii::t('app', 'Numeric series'),
            'retake' => Yii::t('app', 'Retake'),
            'risk_game' => Yii::t('app', 'Risk game'),
            'skatter' => Yii::t('app', 'Skatter'),
            'special_combination' => Yii::t('app', 'Special combination'),
            'splite' => Yii::t('app', 'Splite'),
            'surrender' => Yii::t('app', 'Surrender'),
            'title' => Yii::t('app', 'Title'),
            'triplication_game' => Yii::t('app', 'Triplication game'),
            'version' => Yii::t('app', 'Version'),
            'video_viewer_url' => Yii::t('app', 'Video viewer url'),
            'wild_symbol' => Yii::t('app', 'Wild symbol'),
            'win_max' => Yii::t('app', 'Win max'),
            'zero' => Yii::t('app', 'Zero'),
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => ArImagesBehavior::className(),
                'model' => $this,
                'attributesOne' => ['logo', 'logo_mini'],
            ],
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlots2categories()
    {
        return $this->hasMany(Slots2categories::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlots()
    {
        return $this->hasMany(Slots::className(), ['category_id' => 'id']);
    }

    public function getSlots2vendors()
    {
        return $this->hasMany(Slots2Vendors::className(), ['slot_id' => 'id'])->via('slots');
    }

    public static function getForList()
    {
        return ArrayHelper::map(SlotsCategories::find()->asArray()->orderBy('title')->all(), 'id', 'title');
    }

    /**
     * @return \yii\data\ActiveDataProvider
     */
    public function search()
    {
        $query = SlotsCategories::find();

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
    }

    public function getPropertiesList()
    {
        return [
            'auto_play' => 'chr28',
            'bonus_game' => 'chr11',
            'free_spins' => 'chr26',
            'version' => 'chr35',
            'jackpot' => 'chr2',
            'joker' => 'chr20',
            'wild_symbol' => 'chr24',
            'zero' => 'chr37',
            'doubling_game' => 'chr27',
            'amount_reels' => 'chr23',
            'amount_wheels' => 'chr44',
            'amount_combinations' => 'chr22',
            'amount_lines' => 'chr22',
            'amount_hands' => 'chr46',
            'max_boxes' => 'chr50',
            'win_max' => 'chr19',
            'bet_max_hand' => 'chr19',
            'bet_max_number' => 'chr19',
            'bet_max' => 'chr19',
            'min_boxes' => 'chr34',
            'min_combination' => 'chr49',
            'bet_min_hand' => 'chr18',
            'bet_min_number' => 'chr18',
            'bet_min' => 'chr18',
            'retake' => 'chr29',
            'risk_game' => 'chr48',
            'surrender' => 'chr31',
            'skatter' => 'chr25',
            'special_combination' => 'chr49',
            'splite' => 'chr30',
            'bet_more_less' => 'chr40',
            'bet_dozen' => 'chr42',
            'bet_line' => 'chr43',
            'bet_color' => 'chr39',
            'bet_even_odd' => 'chr41',
            'insurance' => 'chr33',
            'kind_of_lottery' => 'chr44',
            'kind_of_poker' => 'chr47',
            'kind_of_roulette' => 'chr36',
            'triplication_game' => 'chr32',
            'numeric_series' => 'chr38',
            'video_viewer_url' => 'chr14',
        ];
    }

    public static function getFiltersForList()
    {
        return [
            self::SLOT_CATEGORY_ARCADE => self::SLOT_CATEGORY_ARCADE,
            self::SLOT_CATEGORY_CARDS => self::SLOT_CATEGORY_CARDS,
            self::SLOT_CATEGORY_GAME => self::SLOT_CATEGORY_GAME,
            self::SLOT_CATEGORY_LOTTERY => self::SLOT_CATEGORY_LOTTERY,
            self::SLOT_CATEGORY_OTHER => self::SLOT_CATEGORY_OTHER,
            self::SLOT_CATEGORY_ROULETTE => self::SLOT_CATEGORY_ROULETTE,
            self::SLOT_CATEGORY_VIDEOPOKER => self::SLOT_CATEGORY_VIDEOPOKER,
        ];
    }

}