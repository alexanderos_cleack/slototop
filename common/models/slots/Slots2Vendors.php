<?php

namespace common\models\slots;

use Yii;

use common\models\slots\Slots;
use common\models\vendors\Vendors;

/**
 * This is the model class for table "slots2vendors".
 *
 * @property integer $slot_id
 * @property integer $vendor_id
 *
 * @property Slots $slot
 * @property Vendors $vendor
 */
class Slots2Vendors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slots2vendors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slot_id', 'vendor_id'], 'required'],
            [['slot_id', 'vendor_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'slot_id' => 'Slot ID',
            'vendor_id' => 'Vendor ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlot()
    {
        return $this->hasOne(Slots::className(), ['id' => 'slot_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(Vendors::className(), ['id' => 'vendor_id']);
    }
}
