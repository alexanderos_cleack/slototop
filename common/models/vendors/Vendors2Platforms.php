<?php

namespace common\models\vendors;

use Yii;

/**
 * This is the model class for table "vendors2platforms".
 *
 * @property integer $vendor_id
 * @property integer $platform_id
 *
 * @property Vendors $vendor
 * @property VendorsPlatforms $platform
 */
class Vendors2Platforms extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vendors2platforms';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vendor_id', 'platform_id'], 'required'],
            [['vendor_id', 'platform_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'vendor_id' => 'Vendor ID',
            'platform_id' => 'Platform ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(Vendors::className(), ['id' => 'vendor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlatform()
    {
        return $this->hasOne(VendorsPlatforms::className(), ['id' => 'platform_id']);
    }
}
