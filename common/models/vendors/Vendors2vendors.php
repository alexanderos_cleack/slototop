<?php

namespace common\models\vendors;

use Yii;

/**
 * This is the model class for table "vendors2vendors".
 *
 * @property integer $vendor_id
 * @property integer $vendor_attach_id
 *
 * @property Vendors $vendor
 * @property Vendors $vendorAttach
 */
class Vendors2vendors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vendors2vendors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vendor_id', 'vendor_attach_id'], 'required'],
            [['vendor_id', 'vendor_attach_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'vendor_id' => Yii::t('app/b', 'Vendor ID'),
            'vendor_attach_id' => Yii::t('app/b', 'Vendor Attach ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(Vendors::className(), ['id' => 'vendor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendorAttach()
    {
        return $this->hasOne(Vendors::className(), ['id' => 'vendor_attach_id']);
    }
}
