<?php

namespace common\models\vendors;

use Yii;

/**
 * This is the model class for table "vendors2casino".
 *
 * @property integer $casino_id
 * @property integer $vendor_id
 *
 * @property Casino $casino
 * @property Vendors $vendor
 */
class Vendors2Casino extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vendors2casino';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['casino_id', 'vendor_id'], 'required'],
            [['casino_id', 'vendor_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'casino_id' => Yii::t('app/b', 'Casino ID'),
            'vendor_id' => Yii::t('app/b', 'Vendor ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCasino()
    {
        return $this->hasOne(Casino::className(), ['id' => 'casino_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(Vendors::className(), ['id' => 'vendor_id']);
    }
}
