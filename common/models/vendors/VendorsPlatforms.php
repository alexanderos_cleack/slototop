<?php

namespace common\models\vendors;

use Yii;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;

use common\components\behaviors\ArImagesBehavior;
use common\traits\ImagesTrait;

/**
 * This is the model class for table "vendors_platforms".
 *
 * @property integer $id
 * @property string $title
 *
 * @property Vendors2platforms[] $vendors2platforms
 */
class VendorsPlatforms extends \yii\db\ActiveRecord {

    use ImagesTrait;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'vendors_platforms';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 128],

            [['imagesAlt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id'    => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => ArImagesBehavior::class,
                'model' => $this,
                'attributesOne' => ['logo_mini'],
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendors2platforms() {
        return $this->hasMany(Vendors2platforms::class, ['platform_id' => 'id']);
    }

    public static function getVendorsForList() {
        return ArrayHelper::map(VendorsPlatforms::find()->asArray()->orderBy('title')->all(), 'id', 'title');
    }

    /**
     * @return \yii\data\ActiveDataProvider
     */
    public function search() {
        $query = VendorsPlatforms::find();

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['title' => SORT_ASC],
            ],
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
    }

}
