<?php

namespace common\models\vendors;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use common\components\behaviors\ArImagesBehavior;
use common\components\behaviors\ArManyToManyBehavior;
use common\components\behaviors\ArMetaTagsBehavior;
use common\models\images\Images;
use common\models\bonuses\Bonuses2Casino;
use common\models\casino\Casino;
use common\models\countries\Countries;
use common\models\languages\Languages2Vendors;
use common\models\languages\Languages;
use common\models\slots\Slots2Vendors;
use common\models\slots\Slots;
use common\models\vendors\Vendors2Casino;
use common\models\vendors\Vendors2vendors;
use common\traits\ImagesTrait;

/**
 * This is the model class for table "vendors".
 *
 * @property integer $id
 * @property string $title
 * @property string $payout
 * @property string $description
 * @property string $slug
 *
 * @property Vendors2Platforms[] $Vendors2Platforms
 */
class Vendors extends \yii\db\ActiveRecord {

    use ImagesTrait;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'vendors';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title', 'description', 'country_id'], 'required'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 128],
            [['amount_games'], 'string', 'max' => 64],
            [['payout', 'rating_admins', 'rating_users', 'year_established', 'country_id'], 'integer'],
            [['rating_users', 'rating_admins'], 'default', 'value' => 0],
            [['enabled', 'main_page'], 'boolean'],

            [['languages', 'platforms', 'vendors'], 'safe'],

            ['slug', 'required'],
            [['title', 'slug'], 'unique'],

            [['meta_title', 'meta_h1', 'meta_keywords', 'meta_description'], 'string', 'max' => 256],

            [['imagesAlt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'amount_games'     => Yii::t('app/b', 'Vendors amount_games'),
            'country_id'       => Yii::t('app/b', 'Vendors country_id'),
            'description'      => Yii::t('app/b', 'Vendors description'),
            'id'               => Yii::t('app/b', 'ID'),
            'languages'        => Yii::t('app/b', 'Vendors languages'),
            'logo'             => Yii::t('app/b', 'Vendors logo'),
            'payout'           => Yii::t('app/b', 'Vendors payout'),
            'platforms'        => Yii::t('app/b', 'Vendors platforms'),
            'screens'          => Yii::t('app/b', 'Vendors screens'),
            'title'            => Yii::t('app/b', 'Vendors title'),
            'year_established' => Yii::t('app/b', 'Vendors year_established'),
            'enabled'          => Yii::t('app/b', 'Show page'),
            'main_page'        => Yii::t('app/b', 'main_page'),

            'meta_description' => Yii::t('app/b', 'Meta description'),
            'meta_keywords'    => Yii::t('app/b', 'Meta keywords'),
            'meta_title'       => Yii::t('app/b', 'Meta title'),
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => ArManyToManyBehavior::class,
                'intermediaries' => [Vendors2Platforms::class, Languages2Vendors::class, Vendors2vendors::class],
                'attributes' => ['platforms', 'languages', 'vendors'],
                'table_a_fkey' => 'vendor_id',
                'table_b_fkeys' => ['platform_id', 'language_id', 'vendor_attach_id'],
            ],
            [
                'class' => ArImagesBehavior::class,
                'model' => $this,
                'attributesOne' => ['logo', 'logo_mini', 'logo_casino'],
                'attributesMany' => ['screens'],
            ],
            [
                'class' => ArMetaTagsBehavior::class,
            ],
            // [
            //     'class' => SluggableBehavior::class,
            //     'attribute' => 'title',
            // ],
            [
                'class' => \yii\behaviors\TimestampBehavior::class,
                'createdAtAttribute' => 'created_time',
                'updatedAtAttribute' => 'updated_time',
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendors2Platforms() {
        return $this->hasMany(Vendors2Platforms::class, ['vendor_id' => 'id']);
    }

    public function getPlatforms() {
        return $this->hasMany(VendorsPlatforms::class, ['id' => 'platform_id'])->viaTable(Vendors2Platforms::tableName(), ['vendor_id' => 'id']);
    }

    public function setPlatforms($platforms) {
        $this->platforms = $platforms;
    }

    public function getLanguages() {
        return $this->hasMany(Languages::class, ['id' => 'language_id'])->viaTable(Languages2Vendors::tableName(), ['vendor_id' => 'id']);
    }

    public function setLanguages($languages) {
        $this->languages = $languages;
    }

    public function getVendors() {
        return $this->hasMany(Vendors::class, ['id' => 'vendor_attach_id'])->viaTable(Vendors2vendors::tableName(), ['vendor_id' => 'id']);
    }

    public function setVendors($vendors) {
        $this->vendors = $vendors;
    }

    public function getCountry() {
        return $this->hasOne(Countries::class, ['id' => 'country_id']);
    }

    public function getLanguages_site() {
        $languages = $this->getLanguages()->all();
        $languages = array_map(function ($item) {
            return $item->title;
        }, $languages);
        return join(' / ', $languages);
    }

    public function getGames() {
        return $this->hasMany(Slots::class, ['id' => 'slot_id'])->viaTable(Slots2Vendors::tableName(), ['vendor_id' => 'id']);
    }

    public function getGames_site() {
        $query = $this->hasMany(Slots::class, ['id' => 'slot_id'])->where(['enabled' => true])->viaTable(Slots2Vendors::tableName(), ['vendor_id' => 'id']);
        $count = $query->count();
        $query->orderBy(['(rating_users + rating_admins)' => SORT_DESC]);
        return $query->limit(8)->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCasino() {
        return $this->hasMany(Casino::class, ['id' => 'casino_id'])->viaTable(Vendors2Casino::tableName(), ['vendor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendors2casino() {
        return $this->hasMany(Vendors2Casino::class, ['vendor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonuses2casino() {
        return $this->hasMany(Bonuses2Casino::class, ['casino_id' => 'casino_id'])->via('vendors2casino');
    }

    public function getCasino_site() {
        $query = $this->hasMany(Casino::class, ['id' => 'casino_id'])->where(['enabled' => true])->viaTable(Vendors2Casino::tableName(), ['vendor_id' => 'id']);
        $count = $query->count();
        return $query->offset(rand(1, $count) - 8)->limit(8)->all();
    }

    public static function getForList() {
        return ArrayHelper::map(Vendors::find()->asArray()->orderBy('title')->all(), 'id', 'title');
    }

    public function search() {
        $query = Vendors::find();

        $query->andFilterWhere(['ilike', 'title', $this->title]);

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['title' => SORT_ASC],
                'attributes' => [
                    'id',
                    'title' => [
                        'asc' => ['title' => SORT_ASC],
                        'desc' => ['title' => SORT_DESC],
                    ],
                    'main_page' => [
                        'asc' => ['main_page' => SORT_ASC],
                        'desc' => ['main_page' => SORT_DESC],
                    ],
                ],
            ],
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
    }

    public function getUrl($absolute = false) {
        return Url::to('/vendor/' . $this->slug, $absolute);
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->description = str_ireplace('&amp;', '&', $this->description);
            return true;
        }
        return false;
    }

    public function getAmountGames_site() {
        return $this->getGames()->where(['enabled' => true])->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlots2vendors() {
        return $this->hasMany(Slots2vendors::class, ['vendor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlots() {
        return $this->hasMany(Slots::class, ['id' => 'slot_id'])->from(['t' => Slots::tableName()])->via('slots2vendors');
    }

    /**
     * @return ActiveQuery
     */
    public function getLogo()
    {
        return $this->hasOne(Images::class, ['model_id' => 'id'])->where(['model_schema' => $this::tableName(), 'attribute' => 'logo']);
    }

    /**
     * @return Images
     */
    public function getImageLogo() {
        $query = $this->getLogo();
        if ($query->count()) {
            /** @var Images $logo */
            $logo = $query->one();
            return $logo;
        }

        return new Images();
    }
}
