<?php

namespace common\models\rating;

use Yii;

/**
 * This is the model class for table "rating".
 *
 * @property string $model_schema
 * @property integer $model_id
 * @property string $ip
 * @property string $date
 * @property double $score
 */
class Rating extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'rating';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['model_schema', 'model_id'], 'required'],
            [['model_id'], 'integer'],
            [['ip'], 'string'],
            [['date'], 'safe'],
            [['score'], 'number', 'min' => 0.5, 'max' => 5],
            [['model_schema'], 'string', 'max' => 128]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'model_schema' => Yii::t('app/b', 'Model Schema'),
            'model_id'     => Yii::t('app/b', 'Model ID'),
            'ip'           => Yii::t('app/b', 'Ip'),
            'date'         => Yii::t('app/b', 'Date'),
            'score'        => Yii::t('app/b', 'Score'),
        ];
    }

    public static function getRatingInfo(\yii\db\ActiveRecord $model) {
        $query = Rating::find()->select(['score', 'date'])->where(['model_schema' => strtolower(array_slice(explode('\\', $model::className()), -1)[0]), 'model_id' => $model->id]);

        $date = $query->max('date');

        $best_rating_query = clone $query;
        $best_rating = $best_rating_query->max('score');

        $rating_count_query = clone $query;
        $rating_count = $rating_count_query->count();

        $worst_rating_query = clone $query;
        $worst_rating = $worst_rating_query->min('score');

        // $rating_value_query = clone $query;
        // $rating_value = $rating_value_query->scalar();
        $rating_value = rand($worst_rating * 10000, $best_rating * 10000) / 10000;

        if (!$rating_count) {
            $rating_value = rand(30000, 50000) / 10000;
            $best_rating = $rating_value;
            $worst_rating = 1;
            $rating_count = 1;
        }

        return [
            'date' => (new \DateTime($date))->format('Y-m-d'),
            'best_rating' => $best_rating,
            'rating_count' => $rating_count,
            'rating_value' => $rating_value,
            'worst_rating' => $worst_rating,
        ];
    }

}
