<?php

namespace common\models\seotext;

use Yii;

/**
 * This is the model class for table "seo_sorts_requests".
 *
 * @property integer $id
 * @property integer $filter_id
 * @property string $sort
 * @property string $description
 * @property string $slug
 * @property integer $count_elements
 * @property string $page
 * @property string $title
 *
 * @property SeoFiltersRequests $filter
 */
class SeoSortsRequests extends \yii\db\ActiveRecord {

    const PAGE_BONUSES = 'bonuses';
    const PAGE_CASINO  = 'casino';
    const PAGE_GAMES   = 'games';
    const PAGE_METHODS = 'methods';
    const PAGE_VENDORS = 'vendors';

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'seo_sorts_requests';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['filter_id', 'count_elements'], 'integer'],
            [['description'], 'string'],
            [['sort', 'url'], 'string', 'max' => 500],
            [['slug', 'page', 'title'], 'string', 'max' => 255],

            [['meta_title', 'meta_h1', 'meta_keywords', 'meta_description'], 'string', 'max' => 256],
        ];
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['search'] = ['title', 'page', 'url'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'count_elements' => Yii::t('app/b', 'Count Elements'),
            'description'    => Yii::t('app/b', 'Description'),
            'filter_id'      => Yii::t('app/b', 'Filter ID'),
            'id'             => Yii::t('app/b', 'ID'),
            'page'           => Yii::t('app/b', 'Page'),
            'slug'           => Yii::t('app/b', 'Slug'),
            'sort'           => Yii::t('app/b', 'SeoSortsRequests sort'),
            'title'          => Yii::t('app/b', 'Title'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilter() {
        return $this->hasOne(SeoFiltersRequests::className(), ['id' => 'filter_id']);
    }

    public function behaviors() {
        return [
            [
                'class' => \common\components\behaviors\ArMetaTagsBehavior::className(),
            ],
            // [
            //     'class' => \yii\behaviors\SluggableBehavior::className(),
            //     'attribute' => 'title',
            // ],
        ];
    }

    public function search() {
        $query = SeoSortsRequests::find();

        $query->andFilterWhere(['ilike', 'page', $this->page]);
        $query->andFilterWhere(['ilike', 'title', $this->title]);

        return new \yii\data\ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC],
                'attributes' => [
                ],
            ],
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
    }

    /**
     * @return array
     */
    public static function getSortsPagesForList() {
        return [
            self::PAGE_BONUSES => self::PAGE_BONUSES,
            self::PAGE_CASINO  => self::PAGE_CASINO,
            self::PAGE_GAMES   => self::PAGE_GAMES,
            self::PAGE_METHODS => self::PAGE_METHODS,
            self::PAGE_VENDORS => self::PAGE_VENDORS,
        ];
    }


    /**
     * @param string $page
     * @param \app\models\ABaseSort $sort
     * @param integer $filter_id
     */
    public static function addSort($page, $sort, $filter_id) {
        $attributes = $sort->attributes;

        $attributes = self::removeEmptyFields($attributes);
        $attributes_encode = json_encode($attributes);
        $count_elements = count($attributes);

        $model = SeoSortsRequests::find()->where(['page' => $page, 'sort' => $attributes_encode])->one();

        if (!$model) {
            $model = new SeoSortsRequests;
            $model->attributes = [
                'page' => $page,
                'sort' => $attributes_encode,
                'count_elements' => $count_elements,
                'filter_id' => $filter_id,
                'url' => Yii::$app->request->url,
            ];
            $model->save();
        }

        $sort->seo = $model;
    }

    /**
     * @param \app\modules\casino\models\Sort $sort
     * @param integer $filter_id
     * @return SeoSortsRequests
     */
    public static function addSortCasino(\app\modules\casino\models\Sort $sort, $filter_id = null) {
        SeoSortsRequests::addSort(self::PAGE_CASINO, $sort, $filter_id);
    }

    /**
     * @param \app\modules\games\models\Sort $sort
     * @param integer $filter_id
     * @return SeoSortsRequests
     */
    public static function addSortGames(\app\modules\games\models\Sort $sort, $filter_id = null) {
        SeoSortsRequests::addSort(self::PAGE_GAMES, $sort, $filter_id);
    }

    /**
     * @param \app\modules\bonuses\models\Sort $sort
     * @param integer $filter_id
     * @return SeoSortsRequests
     */
    public static function addSortBonuses(\app\modules\bonuses\models\Sort $sort, $filter_id = null) {
        SeoSortsRequests::addSort(self::PAGE_BONUSES, $sort, $filter_id);
    }

    /**
     * @param \app\modules\vendors\models\Sort $sort
     * @param integer $filter_id
     * @return SeoSortsRequests
     */
    public static function addSortVendors(\app\modules\vendors\models\Sort $sort, $filter_id = null) {
        SeoSortsRequests::addSort(self::PAGE_VENDORS, $sort, $filter_id);
    }

    /**
     * @param \app\modules\methods\models\Sort $sort
     * @param integer $filter_id
     * @return SeoSortsRequests
     */
    public static function addSortPayments(\app\modules\methods\models\Sort $sort, $filter_id = null) {
        SeoSortsRequests::addSort(self::PAGE_METHODS, $sort, $filter_id);
    }


    /**
     * @param  \app\modules\casino\models\Sort $sort
     * @return SeoSortsRequests
     */
    public static function getSeoCasinoBySort(\app\modules\casino\models\Sort $sort) {
        $attributes = self::removeEmptyFields($sort->attributes);
        $attributes = json_encode($attributes);
        if ($attributes) return SeoSortsRequests::find()->where(['page' => self::PAGE_CASINO, 'sort' => $attributes])
            ->andWhere(['OR',
                ['not', ['title' => null]],
                ['not', ['slug' => null]],
                ['not', ['description' => null]]
            ])->one();
    }

    /**
     * @param  \app\modules\games\models\Sort $sort
     * @return SeoSortsRequests
     */
    public static function getSeoGamesBySort(\app\modules\games\models\Sort $sort) {
        $attributes = self::removeEmptyFields($sort->attributes);
        $attributes = json_encode($attributes);
        if ($attributes) return SeoSortsRequests::find()->where(['page' => self::PAGE_GAMES, 'sort' => $attributes])
            ->andWhere(['OR',
                ['not', ['title' => null]],
                ['not', ['slug' => null]],
                ['not', ['description' => null]]
            ])->one();
    }

    /**
     * @param  \app\modules\bonuses\models\Sort $sort
     * @return SeoSortsRequests
     */
    public static function getSeoBonusesBySort(\app\modules\bonuses\models\Sort $sort) {
        $attributes = self::removeEmptyFields($sort->attributes);
        $attributes = json_encode($attributes);
        if ($attributes) return SeoSortsRequests::find()->where(['page' => self::PAGE_BONUSES, 'sort' => $attributes])
            ->andWhere(['OR',
                ['not', ['title' => null]],
                ['not', ['slug' => null]],
                ['not', ['description' => null]]
            ])->one();
    }

    /**
     * @param  \app\modules\vendors\models\Sort $sort
     * @return SeoSortsRequests
     */
    public static function getSeoVendorsBySort(\app\modules\vendors\models\Sort $sort) {
        $attributes = self::removeEmptyFields($sort->attributes);
        $attributes = json_encode($attributes);
        if ($attributes) return SeoSortsRequests::find()->where(['page' => self::PAGE_VENDORS, 'sort' => $attributes])
            ->andWhere(['OR',
                ['not', ['title' => null]],
                ['not', ['slug' => null]],
                ['not', ['description' => null]]
            ])->one();
    }

    /**
     * @param  \app\modules\methods\models\Sort $sort
     * @return SeoSortsRequests
     */
    public static function getSeoPaymentsBySort(\app\modules\methods\models\Sort $sort) {
        $attributes = self::removeEmptyFields($sort->attributes);
        $attributes = json_encode($attributes);
        if ($attributes) return SeoSortsRequests::find()->where(['page' => self::PAGE_METHODS, 'sort' => $attributes])
            ->andWhere(['OR',
                ['not', ['title' => null]],
                ['not', ['slug' => null]],
                ['not', ['description' => null]]
            ])->one();
    }


    /**
     * @param  string $page
     * @return SeoSortsRequests
     */
    public static function getSeo($page, $slug) {
        return SeoSortsRequests::find()->where(['page' => $page, 'slug' => $slug])->one();
    }

    /**
     * @param  string $page
     * @return SeoSortsRequests
     */
    public static function getSeoCasino($slug) {
        return self::getSeo(self::PAGE_CASINO, $slug);
    }

    /**
     * @param  string $page
     * @return SeoSortsRequests
     */
    public static function getSeoGames($slug) {
        return self::getSeo(self::PAGE_GAMES, $slug);
    }

    /**
     * @param  string $page
     * @return SeoSortsRequests
     */
    public static function getSeoBonuses($slug) {
        return self::getSeo(self::PAGE_BONUSES, $slug);
    }

    /**
     * @param  string $page
     * @return SeoSortsRequests
     */
    public static function getSeoVendors($slug) {
        return self::getSeo(self::PAGE_VENDORS, $slug);
    }

    /**
     * @param  string $page
     * @return SeoSortsRequests
     */
    public static function getSeoPayments($slug) {
        return self::getSeo(self::PAGE_METHODS, $slug);
    }


    /**
     * @param  string $url
     * @return string
     */
    public static function getSeoGamesUrlByUrl($url) {
        if ($model = SeoSortsRequests::find()->where(['page' => self::PAGE_GAMES, 'url' => $url])->one()) {
            return \yii\helpers\Url::to(['/games/games/sort', 'slug' => $model->slug]);
        }
        return $url;
    }

    /**
     * @param  string $url
     * @return string
     */
    public static function getSeoVendorsUrlByUrl($url) {
        if ($model = SeoSortsRequests::find()->where(['page' => self::PAGE_VENDORS, 'url' => $url])->one()) {
            return \yii\helpers\Url::to(['/vendors/vendors/sort', 'slug' => $model->slug]);
        }
        return $url;
    }


    /**
     * @param  array $items
     * @return array
     */
    public static function removeEmptyFields($items) {
        $_items = [];
        foreach ($items as $key => $value) {
            if (is_array($value)) {
                if (count($value)) {
                    $_items[$key] = $value;
                }
            } elseif (is_object($value)) {
            } elseif (strlen(trim($value))) {
                if ($key != 'isValidate') {
                    $_items[$key] = $value;
                }
            }
        }
        return $_items;
    }

    public function isFull() {
        if (!$this->title || !$this->slug || !$this->description) {
            return false;
        }
        return true;
    }

}
