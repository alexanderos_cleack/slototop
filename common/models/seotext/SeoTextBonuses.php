<?php

namespace common\models\seotext;

use Yii;
use yii\data\ActiveDataProvider;

use common\models\bonuses\Bonuses;

/**
 * This is the model class for table "seo_text".
 *
 */

class SeoTextBonuses extends SeoText {

    public function rules() {
        $rules = parent::rules();
        return array_merge($rules, [
            [['model_id', 'model_schema'], 'required'],
            [['model_id'], 'integer'],
            [['model_schema'], 'string', 'max' => 255],
        ]);
    }

    public function attributeLabels() {
        $labels = parent::attributeLabels();

        return array_merge($labels, [
            'model_id' => Yii::t('app/b' , 'Bonuses'),
        ]);
    }

    public function search() {
        $query = SeoTextBonuses::find();

        $query->where(['model_schema' => Bonuses::tableName()]);
        $query->joinWith('bonus');
        $query->andFilterWhere(['ilike' , 'bonuses.title', $this->model_id]);

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['type' => SORT_ASC],
                'attributes' => [
                    'model_id' => [
                        'asc' => ['bonuses.title' => SORT_ASC],
                        'desc' => ['bonuses.title' => SORT_DESC],
                    ],
                    'type',
                ],
            ],
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

    }

    public static function getTypesForList() {
        return [
            self::PAGE_BONUSES_SAME    => 'Похожие бонусы',
        ];
    }

    public function getBonus() {
        return $this->hasOne(Bonuses::className(), ['id' => 'model_id']);
    }

}
