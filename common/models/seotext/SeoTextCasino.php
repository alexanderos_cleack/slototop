<?php

namespace common\models\seotext;

use Yii;
use yii\data\ActiveDataProvider;

use common\models\casino\Casino;

/**
 * This is the model class for table "seo_text".
 *
 */

class SeoTextCasino extends SeoText {

    public function rules() {
        $rules = parent::rules();
        return array_merge($rules, [
            [['model_id', 'model_schema'], 'required'],
            [['model_id'], 'integer'],
            [['model_schema'], 'string', 'max' => 255],
        ]);
    }

    public function attributeLabels() {
        $labels = parent::attributeLabels();

        return array_merge($labels, [
            'model_id' => Yii::t('app/b' , 'Casino'),
        ]);
    }

    public function search() {
        $query = SeoTextCasino::find();

        $query->where(['model_schema' => Casino::tableName()]);
        $query->joinWith('casino');
        $query->andFilterWhere(['ilike' , 'casino.title', $this->model_id]);

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['type' => SORT_ASC],
                'attributes' => [
                    'model_id' => [
                        'asc' => ['casino.title' => SORT_ASC],
                        'desc' => ['casino.title' => SORT_DESC],
                    ],
                    'type',
                ],
            ],
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
    }

    public static function getTypesForList() {
        return [
            self::PAGE_BONUSES_CASINO  => 'Бонусы казино',
            self::PAGE_GAMES_POPULAR   => 'Популярные игры',
            self::PAGE_CASINO_SAME     => 'Похожие казино',
        ];
    }

    public function getCasino() {
        return $this->hasOne(Casino::className(), ['id' => 'model_id']);
    }

}
