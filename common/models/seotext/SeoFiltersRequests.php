<?php

namespace common\models\seotext;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\helpers\Url;

use common\components\behaviors\ArMetaTagsBehavior;

/**
 * This is the model class for table "seo_filters_requests".
 *
 * @property integer $count
 * @property integer $id
 * @property integer $url
 * @property string $description
 * @property string $filter
 * @property string $page
 * @property string $slug
 * @property string $title
 *
 * @property string $meta_title
 * @property string $meta_h1
 * @property string $meta_keywords
 * @property string $meta_description
 */
class SeoFiltersRequests extends ActiveRecord
{
    const PAGE_BONUSES = 'bonuses';
    const PAGE_CASINO = 'casino';
    const PAGE_GAMES = 'games';
    const PAGE_METHODS = 'methods';
    const PAGE_VENDORS = 'vendors';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seo_filters_requests';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['count', 'count_elements'], 'integer'],
            [['slug', 'page', 'title'], 'string', 'max' => 255],
            [['filter', 'url'], 'string', 'max' => 1000],

            [['meta_title', 'meta_h1', 'meta_keywords', 'meta_description'], 'string', 'max' => 256],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['search'] = ['title', 'page', 'url'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'count' => Yii::t('app/b', 'SeoFiltersRequests count'),
            'description' => Yii::t('app/b', 'Description'),
            'filter' => Yii::t('app/b', 'SeoFiltersRequests filter'),
            'id' => Yii::t('app/b', 'ID'),
            'page' => Yii::t('app/b', 'Page'),
            'slug' => Yii::t('app/b', 'Slug'),
            'title' => Yii::t('app/b', 'Title'),
            'url' => Yii::t('app/b', 'Url'),
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => ArMetaTagsBehavior::class,
            ],
        ];
    }

    public function search()
    {
        $query = SeoFiltersRequests::find();

        $query->andFilterWhere(['ilike', 'page', $this->page]);
        $query->andFilterWhere(['ilike', 'title', $this->title]);
        $query->andFilterWhere(['ilike', 'url', $this->url]);

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['count' => SORT_DESC],
                'attributes' => [
                ],
            ],
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
    }

    public function getFiltersForList()
    {
    }

    public static function getFiltersPagesForList()
    {
        return [
            self::PAGE_BONUSES => self::PAGE_BONUSES,
            self::PAGE_CASINO => self::PAGE_CASINO,
            self::PAGE_GAMES => self::PAGE_GAMES,
            self::PAGE_METHODS => self::PAGE_METHODS,
            self::PAGE_VENDORS => self::PAGE_VENDORS,
        ];
    }

    /**
     * @param string $page
     * @param \app\models\BaseFilters $filter
     * @param string $url
     */
    public static function addFilter($page, $filter, $url = null)
    {
        $attributes = $filter->attributes;
        $url = $url ?: Yii::$app->request->url;

        $attributes = self::removeEmptyFields($attributes);
        $attributes_encode = json_encode($attributes);
        $count_elements = count($attributes);

        $_attributes = [$url];

        foreach ($attributes as $key => $value) {
            $_attributes[sprintf('Filters[%s]', $key)] = $value;
        }

        $model = SeoFiltersRequests::find()->where(['page' => $page, 'filter' => $attributes_encode])->one();
        if ($model) {
            $model->updateCounters(['count' => 1]);
        } else {
            $model = new SeoFiltersRequests;
            $model->attributes = [
                'page' => $page,
                'filter' => $attributes_encode,
                'url' => Url::to($_attributes), // Т.к. тип запрос POST, а для правильного формирования фильтра нужен GET
                'count_elements' => $count_elements,
            ];
            $model->save();
        }

        $filter->seo = $model;
    }

    /**
     * @param \app\modules\casino\models\Filters $filter
     * @param string $url
     */
    public static function addFilterCasino(\app\modules\casino\models\Filters $filter, $url = null)
    {
        SeoFiltersRequests::addFilter(self::PAGE_CASINO, $filter, $url);
    }

    /**
     * @param \app\modules\games\models\Filters $filter
     * @param string $url
     */
    public static function addFilterGames(\app\modules\games\models\Filters $filter, $url = null)
    {
        SeoFiltersRequests::addFilter(self::PAGE_GAMES, $filter, $url);
    }

    /**
     * @param \app\modules\bonuses\models\Filters $filter
     * @param string $url
     */
    public static function addFilterBonuses(\app\modules\bonuses\models\Filters $filter, $url = null)
    {
        SeoFiltersRequests::addFilter(self::PAGE_BONUSES, $filter, $url);
    }

    /**
     * @param \app\modules\vendors\models\Filters $filter
     * @param string $url
     */
    public static function addFilterVendors(\app\modules\vendors\models\Filters $filter, $url = null)
    {
        SeoFiltersRequests::addFilter(self::PAGE_VENDORS, $filter, $url);
    }

    /**
     * @param \app\modules\methods\models\Filters $filter
     * @param string $url
     */
    public static function addFilterMethods(\app\modules\methods\models\Filters $filter, $url = null)
    {
        SeoFiltersRequests::addFilter(self::PAGE_METHODS, $filter, $url);
    }

    /**
     * @param  string $page
     * @return SeoFiltersRequests
     */
    public static function getSeo($page, $slug)
    {
        return SeoFiltersRequests::find()->where(['page' => $page, 'slug' => $slug])->one();
    }

    /**
     * @param  string $page
     * @return SeoFiltersRequests
     */
    public static function getSeoCasino($slug)
    {
        return self::getSeo(self::PAGE_CASINO, $slug);
    }

    /**
     * @param  string $page
     * @return SeoFiltersRequests
     */
    public static function getSeoGames($slug)
    {
        return self::getSeo(self::PAGE_GAMES, $slug);
    }

    /**
     * @param  string $page
     * @return SeoFiltersRequests
     */
    public static function getSeoBonuses($slug)
    {
        return self::getSeo(self::PAGE_BONUSES, $slug);
    }

    /**
     * @param  string $page
     * @return SeoFiltersRequests
     */
    public static function getSeoVendors($slug)
    {
        return self::getSeo(self::PAGE_VENDORS, $slug);
    }

    /**
     * @param  string $page
     * @return SeoFiltersRequests
     */
    public static function getSeoMethods($slug)
    {
        return self::getSeo(self::PAGE_METHODS, $slug);
    }


    /**
     * @param  \app\modules\casino\models\Filters $filter
     * @return SeoFiltersRequests
     */
    public static function getSeoCasinoByFilter(\app\modules\casino\models\Filters $filter)
    {
        $attributes = self::removeEmptyFields($filter->attributes);
        $attributes = json_encode($attributes);
        if ($attributes) return SeoFiltersRequests::find()->where(['page' => self::PAGE_CASINO, 'filter' => $attributes])
            ->andWhere(['OR',
                ['not', ['title' => null]],
                ['not', ['slug' => null]],
                ['not', ['description' => null]]
            ])->one();
    }

    /**
     * @param  \app\modules\games\models\Filters $filter
     * @return SeoFiltersRequests
     */
    public static function getSeoGamesByFilter(\app\modules\games\models\Filters $filter)
    {
        $attributes = self::removeEmptyFields($filter->attributes);
        $attributes = json_encode($attributes);

        if ($attributes) return SeoFiltersRequests::find()->where(['page' => self::PAGE_GAMES, 'filter' => $attributes])
            ->andWhere(['OR',
                ['not', ['title' => null]],
                ['not', ['slug' => null]],
                ['not', ['description' => null]]
            ])->one();
    }

    /**
     * @param  \app\modules\bonuses\models\Filters $filter
     * @return SeoFiltersRequests
     */
    public static function getSeoBonusesByFilter(\app\modules\bonuses\models\Filters $filter)
    {
        $attributes = self::removeEmptyFields($filter->attributes);
        $attributes = json_encode($attributes);
        if ($attributes) return SeoFiltersRequests::find()->where(['page' => self::PAGE_BONUSES, 'filter' => $attributes])
            ->andWhere(['OR',
                ['not', ['title' => null]],
                ['not', ['slug' => null]],
                ['not', ['description' => null]]
            ])->one();
    }

    /**
     * @param  \app\modules\vendors\models\Filters $filter
     * @return SeoFiltersRequests
     */
    public static function getSeoVendorsByFilter(\app\modules\vendors\models\Filters $filter)
    {
        $attributes = self::removeEmptyFields($filter->attributes);
        $attributes = json_encode($attributes);
        if ($attributes) return SeoFiltersRequests::find()->where(['page' => self::PAGE_VENDORS, 'filter' => $attributes])
            ->andWhere(['OR',
                ['not', ['title' => null]],
                ['not', ['slug' => null]],
                ['not', ['description' => null]]
            ])->one();
    }

    /**
     * @param  \app\modules\methods\models\Filters $filter
     * @return SeoFiltersRequests
     */
    public static function getSeoMethodsByFilter(\app\modules\methods\models\Filters $filter)
    {
        $attributes = self::removeEmptyFields($filter->attributes);
        $attributes = json_encode($attributes);
        if ($attributes) return SeoFiltersRequests::find()->where(['page' => self::PAGE_METHODS, 'filter' => $attributes])
            ->andWhere(['OR',
                ['not', ['title' => null]],
                ['not', ['slug' => null]],
                ['not', ['description' => null]]
            ])->one();
    }


    /**
     * @param array $items
     */
    public static function removeEmptyFields($items)
    {
        $_items = [];
        foreach ($items as $key => $value) {
            if (is_array($value)) {
                if (count($value)) {
                    $_items[$key] = $value;
                }
            } elseif (strlen(trim($value))) {
                if ($key != 'isValidate') {
                    $_items[$key] = $value;
                }
            }
        }
        return $_items;
    }

    public function isFull()
    {
        if (!$this->title || !$this->slug || !$this->description) {
            return false;
        }
        return true;
    }

}
