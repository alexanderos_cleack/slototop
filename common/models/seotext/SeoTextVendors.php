<?php

namespace common\models\seotext;

use Yii;
use yii\data\ActiveDataProvider;

use common\models\vendors\Vendors;

/**
 * This is the model class for table "seo_text".
 *
 */

class SeoTextVendors extends SeoText {

    public function rules() {
        $rules = parent::rules();
        return array_merge($rules, [
            [['model_id', 'model_schema'], 'required'],
            [['model_id'], 'integer'],
            [['model_schema'], 'string', 'max' => 255],
        ]);
    }

    public function attributeLabels() {
        $labels = parent::attributeLabels();

        return array_merge($labels, [
            'model_id' => Yii::t('app/b' , 'Vendors'),
        ]);
    }

    public function search() {
        $query = SeoTextVendors::find();

        $query->where(['model_schema' => Vendors::tableName()]);
        $query->joinWith('vendor');
        $query->andFilterWhere(['ilike' , 'slots.title', $this->model_id]);

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['type' => SORT_ASC],
                'attributes' => [
                    'model_id' => [
                        'asc' => ['slots.title' => SORT_ASC],
                        'desc' => ['slots.title' => SORT_DESC],
                    ],
                    'type',
                ],
            ],
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
    }

    public static function getTypesForList() {
        return [
            self::PAGE_VENDORS_BONUSES_CASINO => 'Бонусы казино в котрых есть эта платформа',
            self::PAGE_VENDORS_GAMES => 'Игры с вендором',
            self::PAGE_VENDORS_POPULAR_CASINO => 'Популярные казино с этой платформой',
        ];
    }

    public function getVendor() {
        return $this->hasOne(Vendors::className(), ['id' => 'model_id']);
    }

}
