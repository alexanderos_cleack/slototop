<?php

namespace common\models\seotext;

use Yii;
use yii\data\ActiveDataProvider;

use common\models\methods\MethodsPayment;

/**
 * This is the model class for table "seo_text".
 *
 */

class SeoTextMethods extends SeoText {

    public function rules() {
        $rules = parent::rules();
        return array_merge($rules, [
            [['model_id', 'model_schema'], 'required'],
            [['model_id'], 'integer'],
            [['model_schema'], 'string', 'max' => 255],
        ]);
    }

    public function attributeLabels() {
        $labels = parent::attributeLabels();

        return array_merge($labels, [
            'model_id' => Yii::t('app/b' , 'Methods'),
        ]);
    }

    public function search() {
        $query = SeoTextMethods::find();

        $query->where(['model_schema' => MethodsPayment::tableName()]);
        $query->joinWith('method');
        $query->andFilterWhere(['ilike' , 'methods_payment.title', $this->model_id]);

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['type' => SORT_ASC],
                'attributes' => [
                    'model_id' => [
                        'asc' => ['methods_payment.title' => SORT_ASC],
                        'desc' => ['methods_payment.title' => SORT_DESC],
                    ],
                    'type',
                ],
            ],
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
    }

    public static function getTypesForList() {
        return [
            self::PAGE_PAYMENTS_CASION => 'Оплату принимают',
        ];
    }

    public function getMethod() {
        return $this->hasOne(MethodsPayment::className(), ['id' => 'model_id']);
    }

}
