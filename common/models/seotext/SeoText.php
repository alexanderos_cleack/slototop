<?php

namespace common\models\seotext;

use Yii;
use yii\data\ActiveDataProvider;

use common\components\behaviors\ArMetaTagsBehavior;

/**
 * This is the model class for table "seo_text".
 *
 * @property integer $id
 * @property string $type
 * @property string $description
 */
class SeoText extends \yii\db\ActiveRecord {

    const PAGE_BONUSES         = 'bonuses';
    const PAGE_BONUSES_CASINO  = 'bonuses_casino';
    const PAGE_BONUSES_SAME    = 'bonuses_same';
    const PAGE_CASINO          = 'casino';
    const PAGE_CASINO_GAME     = 'casino_game';
    const PAGE_CASINO_SAME     = 'casino_same';
    const PAGE_CONTACTS        = 'contacts';
    const PAGE_GAMES           = 'games';
    const PAGE_GAMES_POPULAR   = 'games_popular';
    const PAGE_GAMES_SAME      = 'games_same';
    const PAGE_MAIN            = 'main';
    const PAGE_MAIN_BONUSES    = 'main_bonuses';
    const PAGE_MAIN_CASINO     = 'main_casino';
    const PAGE_MAIN_GAMES      = 'main_games';
    const PAGE_PAYMENTS        = 'payments';
    const PAGE_PAYMENTS_CASION = 'payments_casion';
    const PAGE_VENDORS         = 'vendors';
    const PAGE_VENDORS_BONUSES_CASINO = 'vendors_bonuses_casino';
    const PAGE_VENDORS_GAMES   = 'vendors_games';
    const PAGE_VENDORS_POPULAR_CASINO = 'vendors_popular_casino';
    const PAGE_SEARCH = 'search';

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'seo_text';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['type', 'description'], 'required'],
            [['description'], 'string'],
            [['type'], 'string', 'max' => 255],

            [['meta_title', 'meta_h1', 'meta_keywords', 'meta_description'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app/b', 'ID'),
            'type' => Yii::t('app/b', 'SeoText type'),
            'description' => Yii::t('app/b', 'Description'),
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => ArMetaTagsBehavior::className(),
            ],
        ];
    }

    public function search() {
        $query = SeoText::find();
        $query->where(['model_id' => null, 'model_schema' => null]);
        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['type' => SORT_ASC],
            ],
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            //$model = SeoText::find()->where(['type' => $this->type, 'model_id' => $this->model_id, 'model_schema' => $this->model_schema])->one();
            //if ($model) {
            //    $this->addError(null, 'Запись существует');
            //    return false;
            //}
            return true;
        } else {
            return false;
        }
    }

    public static function getTypesForList() {
        return [
            self::PAGE_BONUSES         => 'Бонусы',
            self::PAGE_BONUSES_CASINO  => 'Бонусы казино',
            self::PAGE_BONUSES_SAME    => 'Похожие бонусы',
            self::PAGE_CASINO          => 'Казино',
            self::PAGE_CASINO_GAME     => 'Казино с этой игрой',
            self::PAGE_CASINO_SAME     => 'Похожие казино',
            self::PAGE_CONTACTS        => 'Контакты',
            self::PAGE_GAMES           => 'Игры',
            self::PAGE_GAMES_POPULAR   => 'Популярные игры',
            self::PAGE_SEARCH   => 'Поиск',
            self::PAGE_GAMES_SAME      => 'Похожие игры',
            self::PAGE_MAIN            => 'Главная',
            self::PAGE_MAIN_BONUSES    => 'Главная бонусы',
            self::PAGE_MAIN_CASINO     => 'Главная казино',
            self::PAGE_MAIN_GAMES      => 'Главная игры',
            self::PAGE_PAYMENTS        => 'Платежные системы',
            self::PAGE_PAYMENTS_CASION => 'Оплату принимают',
            self::PAGE_VENDORS         => 'Платформы',
            self::PAGE_VENDORS_BONUSES_CASINO => 'Бонусы казино в котрых есть эта платформа',
            self::PAGE_VENDORS_GAMES   => 'Игры с вендором',
            self::PAGE_VENDORS_POPULAR_CASINO => 'Популярные казино с этой платформой',
        ];
    }

    public function getTypeLabel() {
        $lables = SeoText::getTypesForList();
        return $lables[$this->type];
    }

    public static function getSeoBonuses($model = null) {
        return self::getSeo(self::PAGE_BONUSES, $model);
    }

    public static function getSeoCasino($model = null) {
        return self::getSeo(self::PAGE_CASINO, $model);
    }

    public static function getSeoGames($model = null) {
        return self::getSeo(self::PAGE_GAMES, $model);
    }

    public static function getSeoMainBonuses($model = null) {
        return self::getSeo(self::PAGE_MAIN_BONUSES, $model);
    }

    public static function getSeoMainCasino($model = null) {
        return self::getSeo(self::PAGE_MAIN_CASINO, $model);
    }

    public static function getSeoMainGames($model = null) {
        return self::getSeo(self::PAGE_MAIN_GAMES, $model);
    }

    public static function getSeoPayments($model = null) {
        return self::getSeo(self::PAGE_PAYMENTS, $model);
    }

    public static function getSeoVendors($model = null) {
        return self::getSeo(self::PAGE_VENDORS, $model);
    }

    public static function getSeoBonusesCasino($model = null) {
        return self::getSeo(self::PAGE_BONUSES_CASINO, $model);
    }

    public static function getSeoGamesPopular($model = null) {
        return self::getSeo(self::PAGE_GAMES_POPULAR, $model);
    }

    public static function getSeoCasinoSame($model = null) {
        return self::getSeo(self::PAGE_CASINO_SAME, $model);
    }

    public static function getSeoGamesSame($model = null) {
        return self::getSeo(self::PAGE_GAMES_SAME, $model);
    }

    public static function getSeoCasinoGame($model = null) {
        return self::getSeo(self::PAGE_CASINO_GAME, $model);
    }

    public static function getSeoBonusesSame($model = null) {
        return self::getSeo(self::PAGE_BONUSES_SAME, $model);
    }

    public static function getSeoPaymentsCasion($model = null) {
        return self::getSeo(self::PAGE_PAYMENTS_CASION, $model);
    }

    public static function getSeoMain($model = null) {
        return self::getSeo(self::PAGE_MAIN, $model);
    }

    public static function getSeoContacts($model = null) {
        return self::getSeo(self::PAGE_CONTACTS, $model);
    }

    public static function getSeoGamesVendor($model = null) {
        return self::getSeo(self::PAGE_VENDORS_GAMES, $model);
    }

    public static function getSeoVendorBonusesCasino($model = null) {
        return self::getSeo(self::PAGE_VENDORS_BONUSES_CASINO, $model);
    }

    public static function getSeoVendorPopularCasino($model = null) {
        return self::getSeo(self::PAGE_VENDORS_POPULAR_CASINO, $model);
    }

    public static function getSeoSearch($model = null) {
        return self::getSeo(self::PAGE_SEARCH, $model);
    }

    protected static function getSeo($type, $model = null) {
        $query = SeoText::find()->where(['type' => $type]);
        if ($model) {
            $query->andWhere(['model_id' => $model->id, 'model_schema' => $model::tableName()]);
        } else {
            $query->andWhere(['model_id' => null, 'model_schema' => null]);
        }
        $model = $query->one();
        if ($model) return $model;
    }

}
