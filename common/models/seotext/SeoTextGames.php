<?php

namespace common\models\seotext;

use Yii;
use yii\data\ActiveDataProvider;

use common\models\slots\Slots;

/**
 * This is the model class for table "seo_text".
 *
 */

class SeoTextGames extends SeoText {

    public function rules() {
        $rules = parent::rules();
        return array_merge($rules, [
            [['model_id', 'model_schema'], 'required'],
            [['model_id'], 'integer'],
            [['model_schema'], 'string', 'max' => 255],
        ]);
    }

    public function attributeLabels() {
        $labels = parent::attributeLabels();

        return array_merge($labels, [
            'model_id' => Yii::t('app/b' , 'Slots'),
        ]);
    }

    public function search() {
        $query = SeoTextGames::find();

        $query->where(['model_schema' => Slots::tableName()]);
        $query->joinWith('game');
        $query->andFilterWhere(['ilike' , 'slots.title', $this->model_id]);

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['type' => SORT_ASC],
                'attributes' => [
                    'model_id' => [
                        'asc' => ['slots.title' => SORT_ASC],
                        'desc' => ['slots.title' => SORT_DESC],
                    ],
                    'type',
                ],
            ],
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
    }

    public static function getTypesForList() {
        return [
            self::PAGE_GAMES_SAME      => 'Похожие игры',
            self::PAGE_CASINO_GAME     => 'Казино с этой игрой',
        ];
    }

    public function getGame() {
        return $this->hasOne(Slots::className(), ['id' => 'model_id']);
    }

}
