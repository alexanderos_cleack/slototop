<?php

namespace common\models\same;

use Yii;

/**
 * This is the model class for table "same2table".
 *
 * @property string $model_schema
 * @property int $model_id
 * @property int $same_id
 */
class Same2table extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'same2table';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model_schema', 'model_id', 'same_id'], 'required'],
            [['model_id', 'same_id'], 'default', 'value' => null],
            [['model_id', 'same_id'], 'integer'],
            [['model_schema'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'model_schema' => Yii::t('app', 'Model Schema'),
            'model_id' => Yii::t('app', 'Model ID'),
            'same_id' => Yii::t('app', 'Same ID'),
        ];
    }
}
