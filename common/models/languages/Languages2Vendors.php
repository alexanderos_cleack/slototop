<?php

namespace common\models\languages;

use Yii;

/**
 * This is the model class for table "languages2vendors".
 *
 * @property integer $vendor_id
 * @property integer $language_id
 *
 * @property Languages $language
 * @property Vendors $vendor
 */
class Languages2Vendors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'languages2vendors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vendor_id', 'language_id'], 'required'],
            [['vendor_id', 'language_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'vendor_id' => Yii::t('app/b', 'Vendor ID'),
            'language_id' => Yii::t('app/b', 'Language ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Languages::className(), ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendor()
    {
        return $this->hasOne(Vendors::className(), ['id' => 'vendor_id']);
    }
}
