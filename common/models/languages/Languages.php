<?php

namespace common\models\languages;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

use common\components\behaviors\ArImagesBehavior;
use common\components\behaviors\ArSortableBehavior;
use common\traits\ImagesTrait;

/**
 * This is the model class for table "languages".
 *
 * @property integer $id
 * @property string $title
 * @property string $iso
 * @property integer $sort
 */
class Languages extends \yii\db\ActiveRecord {

    use ImagesTrait;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'languages';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title', 'iso'], 'required'],
            [['sort'], 'integer'],
            [['title'], 'string', 'max' => 32],
            [['iso'], 'string', 'max' => 2],

            [['id', 'title', 'iso'], 'safe', 'on' => 'search'],

            [['imagesAlt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id'    => Yii::t('app','ID'),
            'title' => Yii::t('app','Title'),
            'iso'   => Yii::t('app','Iso'),
            'sort'  => Yii::t('app','Sort'),
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => ArImagesBehavior::className(),
                'model' => $this,
                'attributesOne' => ['logo_mini'],
            ],
            [
                'class' => ArSortableBehavior::className(),
            ],
        ];
    }

    /**
     * @return yii\data\ActivaDataProvider
     */
    public function search() {
        $query = Languages::find();

        $query->andFilterWhere(['ilike', 'title', $this->title])
              ->andFilterWhere(['ilike', 'iso', $this->iso]);

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['title' => SORT_ASC],
            ],
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
    }

    public static function getForList() {
        return ArrayHelper::map(Languages::find()->orderBy('title')->all(), 'id', 'title');
    }

}
