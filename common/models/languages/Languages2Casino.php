<?php

namespace common\models\languages;

use common\models\casino\Casino;

/**
 * This is the model class for table "languages2casino".
 *
 * @property integer $language_id
 * @property integer $casino_id
 *
 * @property Languages $language
 * @property Casino $casino
 */
class Languages2Casino extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'languages2casino';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language_id', 'casino_id'], 'required'],
            [['language_id', 'casino_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'language_id' => 'Language ID',
            'casino_id' => 'Casino ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Languages::class, ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCasino()
    {
        return $this->hasOne(Casino::class, ['id' => 'casino_id']);
    }
}
