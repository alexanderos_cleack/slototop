<?php

namespace common\models\licenses;

use Yii;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "licenses".
 *
 * @property integer $id
 * @property string $title
 *
 * @property Casino[] $casinos
 */
class Licenses extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'licenses';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 256],

            [['id', 'title'], 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id'    => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCasinos() {
        return $this->hasMany(Casino::className(), ['license_id' => 'id']);
    }

    public static function getForList() {
        return ArrayHelper::map(Licenses::find()->orderBy('title')->all(), 'id', 'title');
    }

    public function search() {
        $query = Licenses::find();

        $query->andFilterWhere(['id' => $this->id]);
        $query->andFilterWhere(['title' => $this->title]);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
    }

}
