<?php

namespace common\models\countries;

use Yii;

use common\models\countries\Countries;

/**
 * This is the model class for table "countries2casino".
 *
 * @property integer $casino_id
 * @property integer $country_id
 */
class Countries2Casino extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'countries2casino';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['casino_id', 'country_id'], 'required'],
            [['casino_id', 'country_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'casino_id' => Yii::t('app/b', 'Casino ID'),
            'country_id' => Yii::t('app/b', 'Country ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getCountry() {
        return $this->hasOne(Countries::className(), ['id' => 'country_id']);
    }
}
