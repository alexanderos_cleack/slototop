<?php

namespace common\models\countries;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

use common\components\behaviors\ArImagesBehavior;
use common\components\behaviors\ArSortableBehavior;
use common\traits\ImagesTrait;

/**
 * This is the model class for table "countries".
 *
 * @property integer $id
 * @property string $title
 * @property string $iso
 * @property integer $sort
 */
class Countries extends \yii\db\ActiveRecord {

    use ImagesTrait;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'countries';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title', 'iso'], 'required'],
            [['title'], 'string', 'max' => 128],
            [['iso'], 'string', 'max' => 2],
            [['sort'], 'integer'],
            [['user_list'], 'boolean'],

            [['imagesAlt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id'    => Yii::t('app/b', 'ID'),
            'title' => Yii::t('app/b', 'Title'),
            'iso'   => Yii::t('app/b', 'Iso'),
            'sort'  => Yii::t('app/b', 'Sort'),
            'user_list' => Yii::t('app/b', 'Countries user_list'),
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => ArImagesBehavior::class,
                'model' => $this,
                'attributesOne' => ['logo_mini'],
            ],
            [
                'class' => ArSortableBehavior::class,
            ],
        ];
    }

    public static function getForList() {
        return ArrayHelper::map(Countries::find()->asArray()->orderBy('title')->all(), 'id', 'title');
    }

    public static function getForFilterList() {
        return ArrayHelper::map(Countries::find()->where(['user_list' => true])->asArray()->orderBy('title')->all(), 'id', 'title');
    }

}
