<?php

namespace common\models\bonuses;

use Yii;

/**
 * This is the model class for table "bonuses2vendors".
 *
 * @property integer $bonus_id
 * @property integer $vendor_id
 *
 * @property Bonuses $bonus
 * @property Vendors $vendor
 */
class Bonuses2Vendors extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'bonuses2vendors';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['bonus_id', 'vendor_id'], 'required'],
			[['bonus_id', 'vendor_id'], 'integer']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'bonus_id' => 'Bonus ID',
			'vendor_id' => 'Vendor ID',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getBonus() {
		return $this->hasOne(Bonuses::className(), ['id' => 'bonus_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getVendor() {
		return $this->hasOne(Vendors::className(), ['id' => 'vendor_id']);
	}
}
