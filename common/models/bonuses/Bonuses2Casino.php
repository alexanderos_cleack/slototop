<?php

namespace common\models\bonuses;

use Yii;

use common\models\casino\Casino;

/**
 * This is the model class for table "bonuses2casino".
 *
 * @property integer $bonus_id
 * @property integer $casino_id
 *
 * @property Bonuses $bonus
 * @property Casino $casino
 */
class Bonuses2Casino extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'bonuses2casino';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bonus_id', 'casino_id'], 'required'],
            [['bonus_id', 'casino_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'bonus_id' => 'Bonus ID',
            'casino_id' => 'Casino ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonus() {
        return $this->hasOne(Bonuses::className(), ['id' => 'bonus_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCasino() {
        return $this->hasOne(Casino::className(), ['id' => 'casino_id']);
    }
}
