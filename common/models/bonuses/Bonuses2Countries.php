<?php

namespace common\models\bonuses;

use Yii;

use common\models\countries\Countries;

/**
 * This is the model class for table "bonuses2countries".
 *
 * @property integer $bonus_id
 * @property integer $country_id
 *
 * @property Bonuses $bonus
 * @property Countries $country
 */
class Bonuses2Countries extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'bonuses2countries';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['bonus_id', 'country_id'], 'required'],
            [['bonus_id', 'country_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'bonus_id' => 'Bonus ID',
            'country_id' => 'Country ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonus() {
        return $this->hasOne(Bonuses::className(), ['id' => 'bonus_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry() {
        return $this->hasOne(Countries::className(), ['id' => 'country_id']);
    }
}
