<?php

namespace common\models\bonuses;

use Yii;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;

use common\components\behaviors\ArImagesBehavior;
use common\traits\ImagesTrait;

/**
 * This is the model class for table "bonuses_types".
 *
 * @property integer $id
 * @property string $title
 *
 * @property Bonuses[] $bonuses
 */
class BonusesTypes extends \yii\db\ActiveRecord {

    use ImagesTrait;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'bonuses_types';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 128],

            [['imagesAlt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id'    => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => ArImagesBehavior::class,
                'model' => $this,
                'attributesOne' => ['logo_mini'],
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonuses() {
        return $this->hasMany(Bonuses::class, ['bonus_type_id' => 'id']);
    }

    public static function getForList() {
        return ArrayHelper::map(BonusesTypes::find()->asArray()->orderBy('title')->all(), 'id', 'title');
    }

    public function search() {
        $query = BonusesTypes::find();
        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'title' => SORT_ASC,
                ],
            ],
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
    }

}
