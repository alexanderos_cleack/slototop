<?php

namespace common\models\bonuses;

use common\components\behaviors\ArSameBehavior;
use common\traits\SameTrait;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

use common\components\behaviors\ArManyToManyBehavior;
use common\components\behaviors\ArImagesBehavior;
use common\components\behaviors\ArMetaTagsBehavior;
use common\models\images\Images;
use common\models\currencies\Currencies;
use common\models\countries\Countries;
use common\models\countries\Countries2Casino;
use common\models\vendors\Vendors;
use common\models\casino\Casino;
use common\models\bonuses\Bonuses2Currencies;
use common\models\bonuses\Bonuses2Countries;
use common\models\bonuses\Bonuses2Vendors;
use common\models\bonuses\Bonuses2Casino;
use common\traits\ImagesTrait;

/**
 * This is the model class for table "bonuses".
 *
 * @property integer $id
 * @property string $title
 * @property integer $bonus_type_id
 * @property string $bonus_code
 * @property integer $wager
 * @property integer $amount_uses
 * @property string $descriptions
 * @property integer $rating_users
 * @property integer $rating_admins
 * @property float $size_of_bonus;
 * @property string $slug
 *
 * @property string $meta_title
 * @property string $meta_h1
 * @property string $meta_keywords
 * @property string $meta_description
 *
 * @property BonusesTypes $bonusType
 * @property Bonuses2countries[] $bonuses2countries
 * @property Bonuses2currencies[] $bonuses2currencies
 * @property Bonuses2vendors[] $bonuses2vendors
 * @property Casino $casino
 */
class Bonuses extends \yii\db\ActiveRecord {

    use ImagesTrait, SameTrait;

    private $_currencies = null;
    public $casino_search;
    public $rating_all;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'bonuses';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title'], 'unique'],
            [['title', 'bonus_type_id', 'description'], 'required'],
            [['bonus_type_id', 'wager', 'rating_users', 'rating_admins', 'amount_uses'], 'integer'],
            [['rating_users', 'rating_admins'], 'default', 'value' => 0],
            [['description', 'conditions'], 'string'],
            [['title', 'bonus_code', 'size_of_bonus'], 'string', 'max' => 128],
            [['value_on_picture'], 'string', 'max' => 32],
            [['currencies', 'countries', 'casino', 'vendors'], 'safe'],
            [['url_bonus'], 'string', 'max' => 256],
            [['enabled', 'main_page'], 'boolean'],
            [['logo'], 'file'],

            [['meta_title', 'meta_h1', 'meta_keywords', 'meta_description'], 'string', 'max' => 256],

            [['casino_search'], 'integer'],

            ['slug', 'required'],
            [['title', 'slug'], 'unique'],

            [['imagesAlt', 'same'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id'            => Yii::t('app', 'ID'),
            'title'         => Yii::t('app', 'Title'),
            'bonus_type_id' => Yii::t('app', 'Тип бонуса'),
            'bonus_code'    => Yii::t('app', 'Bonus Code'),
            'size_of_bonus' => Yii::t('app/b', 'Bonuses size_of_bonus'),
            'wager'         => Yii::t('app', 'Wager'),
            'description'   => Yii::t('app', 'Description'),
            'rating_users'  => Yii::t('app', 'Rating users'),
            'rating_admins' => Yii::t('app', 'Rating admins'),
            'rating_all'    => Yii::t('app/b', 'Rating'),
            'currencies'    => Yii::t('app', 'Currencies'),
            'countries'     => Yii::t('app', 'Countries'),
            'casino'        => Yii::t('app', 'Casino'),
            'vendors'       => Yii::t('app', 'Vendors'),
            'amount_uses'   => Yii::t('app', 'Amount uses'),
            'logo'          => Yii::t('app', 'Logo'),
            'conditions'    => Yii::t('app/b', 'Bonuses conditions'),
            'url_bonus'     => Yii::t('app/b', 'Bonuses url_bonus'),
            'enabled'       => Yii::t('app/b', 'Show page'),
            'value_on_picture' => Yii::t('app/b', 'Bonuses value_on_picture'),
            'casino_search' => Yii::t('app/b', 'Casino'),
            'main_page' => Yii::t('app/b', 'main_page'),

            'meta_title' => Yii::t('app/b', 'Meta title'),
            'meta_keywords' => Yii::t('app/b', 'Meta keywords'),
            'meta_description' => Yii::t('app/b', 'Meta description'),
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => ArManyToManyBehavior::class,
                'intermediaries' => [/*Bonuses2Currencies::class, */Bonuses2Countries::class, Bonuses2Vendors::class, Bonuses2Casino::class],
                'attributes' => [/*'currencies', */'countries', 'vendors', 'casino'],
                'table_a_fkey' => 'bonus_id',
                'table_b_fkeys' => [/*'currency_id', */'country_id', 'vendor_id', 'casino_id'],
            ],
            [
                'class' => ArImagesBehavior::class,
                'model' => $this,
                'attributesOne' => ['logo'],
                // 'attributesMany' => ,
            ],
            // [
            //     'class' => SluggableBehavior::class,
            //     'attribute' => 'title',
            //     'immutable' => false,
            // ],
            [
                'class' => ArMetaTagsBehavior::class,
            ],
            [
                'class' => ArSameBehavior::class,
            ],
            [
                'class' => \yii\behaviors\TimestampBehavior::class,
                'createdAtAttribute' => 'created_time',
                'updatedAtAttribute' => 'updated_time',
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonusType() {
        return $this->hasOne(BonusesTypes::class, ['id' => 'bonus_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencies() {
        return $this->hasMany(Currencies::class, ['id' => 'currency_id'])->viaTable(Bonuses2Currencies::tableName(), ['bonus_id' => 'id']);
    }

    /**
     * @param array $currencies
     */
    public function setCurrencies($currencies) {
        $this->currencies = $currencies;
    }

    public function getBonuses2countries() {
        return $this->hasMany(Bonuses2Countries::class, ['bonus_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountries() {
        // $countries = $this->getBonuses2countries()->all();
        // if (!$countries) {
        //     $countries = $this->hasMany(Countries::class, ['id' => 'country_id'])->via('countries2casino')->all();
        //     return $this->hasMany(Countries::class, ['id' => 'country_id'])->via('countries2casino');
        // } else {
        //     return $this->hasMany(Countries::class, ['id' => 'country_id'])->via('bonuses2countries');
        // }
        return $this->hasMany(Countries::class, ['id' => 'country_id'])->viaTable(Bonuses2Countries::tableName(), ['bonus_id' => 'id']);
    }

    /**
     * @param array $countries
     */
    public function setCountries($countries) {
        $this->countries = $countries;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCasino() {
        return $this->hasOne(Casino::class, ['id' => 'casino_id'])->viaTable(Bonuses2Casino::tableName(), ['bonus_id' => 'id']);
    }

    /**
     * @param array $casino
     */
    public function setCasino($casino) {
        $this->casino = $casino;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendors() {
        return $this->hasMany(Vendors::class, ['id' => 'vendor_id'])->viaTable(Bonuses2Vendors::tableName(), ['bonus_id' => 'id']);
    }

    /**
     * @param array $vendors
     */
    public function setVendors($vendors) {
        $this->vendors = $vendors;
    }

    public static function getForList() {
        return ArrayHelper::map(Bonuses::find()->orderBy('title')->all(), 'id', 'title');
    }

    /**
     * @return \yii\data\ActiveDataProvider
     */
    public function search() {
        $query = Bonuses::find();

        $query->select(['bonuses.*', '(bonuses.rating_users + bonuses.rating_admins) as rating_all']);

        $query->with(['bonusType']);
        $query->joinWith(['casino']);

        $query->andFilterWhere([
            'bonuses.bonus_type_id' => $this->bonus_type_id,
            'casino.id' => $this->casino_search,
        ]);
        $query->andFilterWhere(['ilike', 'bonuses.title', $this->title]);

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'title' => SORT_ASC,
                ],
                'attributes' => [
                    'id',
                    'title' => [
                        'asc' => ['title' => SORT_ASC],
                        'desc' => ['title' => SORT_DESC],
                    ],
                    'bonus_type_id' => [
                        'asc' => ['bonus_type_id' => SORT_ASC],
                        'desc' => ['bonus_type_id' => SORT_DESC],
                    ],
                    'casino_search' => [
                        'asc' => ['casino.title' => SORT_ASC],
                        'desc' => ['casino.title' => SORT_DESC],
                    ],
                    'main_page' => [
                        'asc' => ['main_page' => SORT_ASC],
                        'desc' => ['main_page' => SORT_DESC],
                    ],
                    'rating_users' => [
                        'asc' => ['rating_users' => SORT_ASC],
                        'desc' => ['rating_users' => SORT_DESC]
                    ],
                    'rating_admins' => [
                        'asc' => ['rating_admins' => SORT_ASC],
                        'desc' => ['rating_admins' => SORT_DESC]
                    ],
                    'rating_all' => [
                        'asc' => ['(bonuses.rating_admins + bonuses.rating_users)' => SORT_ASC],
                        'desc' => ['(bonuses.rating_admins + bonuses.rating_users)' => SORT_DESC]
                    ],
                ],
            ],
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
    }

    public function getUrl($absolute = false) {
        return Url::to('/bonus/' . $this->slug, $absolute);
    }


    public function getCountries2casino() {
        return $this->hasMany(Countries2Casino::class, ['casino_id' => 'casino_id'])->via('bonuses2casino');
    }

    public function getCasino_site() {
        $casino = Bonuses2Casino::find()->where(['bonus_id' => $this->id])->all();
        $casino = array_map(function ($item) {
            return Html::a($item->casino->title, Url::to(['/casino/casino/show', 'slug' => $item->casino->slug]), ['target' => '_blank']);
        }, $casino);
        return join(' / ', $casino);
    }

    public function getCountries_site() {
        $countries = Bonuses2Countries::find()->where(['bonus_id' => $this->id])->count();
        if (!$countries) {
            return $this->hasMany(Countries::class, ['id' => 'country_id'])->via('countries2casino')->orderBy('countries.title')->all();
        } else {
            return $this->hasMany(Countries::class, ['id' => 'country_id'])->via('bonuses2countries')->orderBy('countries.title')->all();
        }
    }

    public function getMaxSum_site() {
        if (!$this->_currencies) {
            $this->_currencies = Bonuses2Currencies::find()->where(['bonus_id' => $this->id])->all();
        }
        $currencies = [];
        foreach ($this->_currencies as $currency) {
            if ($currency->max_sum) {
                $currencies[] = "{$currency->max_sum} {$currency->currency->iso}";
            }
        }
        return join(' / ', $currencies);
    }

    public function getMaxPayout_site() {
        if (!$this->_currencies) {
            $this->_currencies = Bonuses2Currencies::find()->where(['bonus_id' => $this->id])->all();
        }
        $currencies = [];
        foreach ($this->_currencies as $currency) {
            if ($currency->max_payout) {
                $currencies[] = "{$currency->max_payout} {$currency->currency->iso}";
            }
        }
        return join(' / ', $currencies);
    }

    public function getBonuses2casino() {
        return $this->hasMany(Bonuses2Casino::class, ['bonus_id' => 'id']);
    }

    public function getCasino2bonuses() {
        return $this->hasMany(Bonuses2Casino::class, ['casino_id' => 'casino_id'])->via('bonuses2casino');
    }

    public function getBonusesSimilar_site() {
        $query = Bonuses::find()->where(['enabled' => 'true', 'bonus_type_id' => $this->bonus_type_id]);
        $count = $query->count();
        return $query->offset(rand(1, $count) - 4)->limit(4)->orderBy('rating_users')->all();
    }

    /**
     * @return ActiveQuery
     */
    public function getBonusesSimilar_query() {
        $query = $this->getSame();
        $query->andWhere(['t.enabled' => true]);
        if ($query->count()) {
            return $query;
        }

        return Bonuses::find()->from(['t' => $this::tableName()])->where(['t.bonus_type_id' => $this->bonus_type_id, 't.enabled' => true]);
    }

    public function getBonuses_site() {
        $query = $this->hasMany(Bonuses::class, ['id' => 'bonus_id'])->where(['enabled' => true])->andWhere(['!=', 'id', $this->id])->via('casino2bonuses');
        $count = $query->count();
        return $query->offset(rand(1, $count) - 4)->limit(4)->all();
    }

    /**
     * @return ActiveQuery
     */
    public function getBonuses_query() {
        return $this->hasMany(Bonuses::class, ['id' => 'bonus_id'])->where(['enabled' => true])->andWhere(['!=', 'id', $this->id])->via('casino2bonuses');
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->description = str_ireplace('&amp;', '&', $this->description);

            return true;
        }
        return false;
    }


    /**
     * @return ActiveQuery
     */
    public function getLogo()
    {
        return $this->hasOne(Images::class, ['model_id' => 'id'])->where(['model_schema' => $this::tableName(), 'attribute' => 'logo']);
    }

    /**
     * @return Images
     */
    public function getImageLogo() {
        $query = $this->getLogo();
        if ($query->count()) {
            /** @var Images $logo */
            $logo = $query->one();
            return $logo;
        }

        return new Images();
    }
}