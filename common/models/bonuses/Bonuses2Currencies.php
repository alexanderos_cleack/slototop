<?php

namespace common\models\bonuses;

use Yii;

use common\models\currencies\Currencies;

/**
 * This is the model class for table "bonuses2currencies".
 *
 * @property integer $bonus_id
 * @property integer $currency_id
 * @property float $max_sum
 * @property float $max_payout
 *
 * @property Bonuses $bonus
 * @property Currencies $currency
 */
class Bonuses2Currencies extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'bonuses2currencies';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['bonus_id', 'currency_id'], 'required'],
            [['bonus_id', 'currency_id'], 'integer'],
            [['max_sum', 'max_payout'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'bonus_id' => 'Bonus ID',
            'currency_id' => 'Currency ID',
            'max_sum' => Yii::t('app/b', 'Bonuses max_sum'),
            'max_payout' => Yii::t('app/b', 'Bonuses max_payout'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonus() {
        return $this->hasOne(Bonuses::className(), ['id' => 'bonus_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency() {
        return $this->hasOne(Currencies::className(), ['id' => 'currency_id']);
    }
}
