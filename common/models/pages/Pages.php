<?php

namespace common\models\pages;

use common\components\behaviors\ArMetaTagsBehavior;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

/**
 * This is the model class for table "pages".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $slug
 * @property bool $enabled
 */
class Pages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'slug'], 'required'],
            [['title', 'description', 'slug'], 'string'],
            [['slug'], 'unique'],
            [['title'], 'unique'],
            [['enabled'], 'boolean'],

            [['meta_title', 'meta_h1', 'meta_keywords', 'meta_description'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'slug' => Yii::t('app', 'Slug'),

            'meta_title' => Yii::t('app/b', 'Meta title'),
            'meta_keywords' => Yii::t('app/b', 'Meta keywords'),
            'meta_description' => Yii::t('app/b', 'Meta description'),
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => ArMetaTagsBehavior::className(),
            ],
        ];
    }

    /**
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = Pages::find();
        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
    }

    /**
     * @param bool $absolute
     * @return string
     */
    public function getUrl($absolute = false)
    {
        return Url::to('/' . $this->slug, $absolute);
    }
}
