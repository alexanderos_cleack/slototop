<?php

namespace common\models\tags;

use common\models\slots\Slots;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "tags".
 *
 * @property int $id
 * @property string $model_schema
 * @property int $model_id
 * @property string $title
 * @property string $slug
 */
class Tags extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model_schema', 'title'], 'required'],
            [['model_schema', 'title'], 'string', 'max' => 128],
            [['model_schema', 'title'], 'unique', 'targetAttribute' => ['model_schema', 'title']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'model_schema' => Yii::t('app', 'Страница'),
            'title' => Yii::t('app', 'Title'),
        ];
    }

    /**
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = Tags::find();

        $query->andFilterWhere(['model_schema' => $this->model_schema]);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
    }

    public static function getPagesForList()
    {
        return [
            Slots::tableName() => Slots::tableName(),
        ];
    }

}
