<?php

namespace common\models\tags;

use Yii;

/**
 * This is the model class for table "tags2table".
 *
 * @property string $model_schema
 * @property int $model_id
 * @property int $tag_id
 *
 * @property Tags $tag
 */
class Tags2table extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tags2table';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model_schema', 'model_id', 'tag_id'], 'required'],
            [['model_id', 'tag_id'], 'integer'],
            [['model_schema'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'model_schema' => Yii::t('app', 'Model Schema'),
            'model_id' => Yii::t('app', 'Model ID'),
            'tag_id' => Yii::t('app', 'Tag ID'),
        ];
    }

}
