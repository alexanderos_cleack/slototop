<?php

namespace common\models\metatags;

use Yii;

/**
 * This is the model class for table "meta_tags".
 *
 * @property string $model_schema
 * @property integer $model_id
 * @property string $title
 * @property string $h1
 * @property string $description
 * @property string $keywords
 */
class MetaTags extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'meta_tags';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['model_id'], 'integer'],
            [['model_schema', 'title', 'h1','description', 'keywords'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'model_schema' => Yii::t('app/b', 'Model Schema'),
            'model_id'     => Yii::t('app/b', 'Model ID'),
            'title'        => Yii::t('app/b', 'Title'),
            'h1'           => Yii::t('app/b', 'H1'),
            'description'  => Yii::t('app/b', 'Description'),
            'keywords'     => Yii::t('app/b', 'Keywords'),
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->description = str_ireplace('&amp;', '&', $this->description);
            return true;
        }
        return false;
    }

}
