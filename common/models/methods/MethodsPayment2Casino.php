<?php

namespace common\models\methods;

use Yii;

/**
 * This is the model class for table "methods_payment2casino".
 *
 * @property integer $method_id
 * @property integer $casino_id
 *
 * @property MethodsPayment $method
 * @property Casino $casino
 */
class MethodsPayment2Casino extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'methods_payment2casino';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['method_id', 'casino_id'], 'required'],
            [['method_id', 'casino_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'method_id' => 'Method ID',
            'casino_id' => 'Casino ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMethod()
    {
        return $this->hasOne(MethodsPayment::className(), ['id' => 'method_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCasino()
    {
        return $this->hasOne(Casino::className(), ['id' => 'casino_id']);
    }
}
