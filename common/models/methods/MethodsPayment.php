<?php

namespace common\models\methods;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use common\components\behaviors\ArImagesBehavior;
use common\components\behaviors\ArManyToManyBehavior;
use common\components\behaviors\ArMetaTagsBehavior;
use common\models\images\Images;
use common\models\casino\Casino;
use common\models\currencies\Currencies;
use common\models\methods\MethodsCategories;
use common\models\methods\MethodsPayment2Casino;
use common\models\methods\MethodsPayment2Categories;
use common\models\methods\MethodsPayment2Currencies;
use common\traits\ImagesTrait;

/**
 * This is the model class for table "methods_payment".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $how_to_use
 * @property string $url_site
 * @property double $commission
 * @property string $terms_payment
 * @property string $slug
 *
 * @property integer $categories_ids
 *
 * @property string $meta_title
 * @property string $meta_h1
 * @property string $meta_keywords
 * @property string $meta_description
 *
 * @property MethodsCategories $category
 * @property MethodsPayment2casino[] $methodsPayment2casinos
 * @property Casino[] $casinos
 * @property MethodsPayment2currencies[] $methodsPayment2currencies
 */
class MethodsPayment extends \yii\db\ActiveRecord
{
    use ImagesTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'methods_payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'categories_ids'], 'required'],
            [['description', 'how_to_use'], 'string'],
            [['title', 'commission'], 'string', 'max' => 128],
            [['url_site', 'terms_payment', 'slug'], 'string', 'max' => 256],
            [['enabled'], 'boolean'],

            [['meta_title', 'meta_h1', 'meta_keywords', 'meta_description'], 'string', 'max' => 256],

            ['slug', 'required'],
            [['title', 'slug'], 'unique'],

            [['imagesAlt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'             => Yii::t('app/b', 'ID'),
            'title'          => Yii::t('app/b', 'Title'),
            'description'    => Yii::t('app/b', 'Description'),
            'how_to_use'     => Yii::t('app/b', 'Methods payment how to use'),
            'categories_ids' => Yii::t('app/b', 'Methods payment category'),
            'url_site'       => Yii::t('app/b', 'Methods payment url'),
            'commission'     => Yii::t('app/b', 'Methods payment commission'),
            'terms_payment'  => Yii::t('app/b', 'Methods payment terms payment'),
            'slug'           => Yii::t('app/b', 'Slug'),
            'logo'           => Yii::t('app/b', 'Logo'),
            'enabled'        => Yii::t('app/b', 'Show page'),
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => ArManyToManyBehavior::class,
                'intermediaries' => [MethodsPayment2Categories::class],
                'attributes' => ['categories_ids'],
                'table_a_fkey' => 'method_id',
                'table_b_fkeys' => ['category_id'],
            ],
            [
                'class' => ArImagesBehavior::class,
                'model' => $this,
                'attributesOne' => ['logo', 'logo_mini'],
            ],
            // [
            //     'class' => SluggableBehavior::class,
            //     'attribute' => 'title',
            // ],
            [
                'class' => ArMetaTagsBehavior::class,
            ],
            [
                'class' => \yii\behaviors\TimestampBehavior::class,
                'createdAtAttribute' => 'created_time',
                'updatedAtAttribute' => 'updated_time',
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(MethodsCategories::class, ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getCategories_ids() {
        return $this->hasMany(MethodsCategories::class, ['id' => 'category_id'])->viaTable('methods_payment2categories', ['method_id' => 'id']);
    }

    public function setCategories_ids($categories_ids) {
        $this->categories_ids = $categories_ids;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMethodsPayment2casinos()
    {
        return $this->hasMany(MethodsPayment2Casino::class, ['method_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCasinos()
    {
        return $this->hasMany(Casino::class, ['id' => 'casino_id'])->viaTable('methods_payment2casino', ['method_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMethodsPayment2currencies()
    {
        return $this->hasMany(MethodsPayment2Currencies::class, ['method_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencies()
    {
        return $this->hasMany(Currencies::class, ['id' => 'currency_id'])->viaTable('methods_payment2currencies', ['method_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories() {
        return $this->hasMany(MethodsCategories::class, ['id' => 'category_id'])->viaTable('methods_payment2categories', ['method_id' => 'id']);
    }

    public static function getForList() {
        return ArrayHelper::map(MethodsPayment::find()->orderBy('title')->all(), 'id', 'title');
    }

    public function getUrl($absolute = false) {
        return Url::to('/payment/' . $this->slug, $absolute);
    }

    public function getCurrencies_site() {
        $currencies = MethodsPayment2Currencies::find()->where(['method_id' => $this->id])->all();
        $currencies = array_map(function ($item) {
            return $item->currency->iso;
        }, $currencies);
        return join(' / ', $currencies);
    }

    public function getOutput_site() {
        $currencies = MethodsPayment2Currencies::find()->where(['method_id' => $this->id])->all();
        $currencies = array_map(function ($item) {
            return $item->output . ' ' . $item->currency->iso;
        }, $currencies);
        return join(' / ', $currencies);
    }

    public function getCategories_site() {
        $categories = $this->hasMany(MethodsCategories::class, ['id' => 'category_id'])->viaTable('methods_payment2categories', ['method_id' => 'id'])->all();
        $categories = array_map(function ($item) {
            return $item->title;
        }, $categories);
        return join(' / ', $categories);
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->description = str_ireplace('&amp;', '&', $this->description);
            $this->how_to_use = str_ireplace('&amp;', '&', $this->how_to_use);
            return true;
        }
        return false;
    }

    public function search() {
        $query = MethodsPayment::find();

        $query->andFilterWhere(['ilike', 'title', $this->title]);

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['title' => SORT_ASC],
            ],
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
    }

    public function getCasino_site() {
        $query = $this->hasMany(Casino::class, ['id' => 'casino_id'])->where(['enabled' => true])->via('methodsPayment2casinos');
        $count = $query->count();
        return $query->offset(rand(1, $count) - 4)->limit(4)->all();
    }

    /**
     * @return ActiveQuery
     */
    public function getCasino_query() {
        return $this->hasMany(Casino::class, ['t.id' => 'casino_id'])->from(['t' => Casino::tableName()])->where(['t.enabled' => true])->via('methodsPayment2casinos');
    }

    /**
     * @return ActiveQuery
     */
    public function getLogo()
    {
        return $this->hasOne(Images::class, ['model_id' => 'id'])->where(['model_schema' => $this::tableName(), 'attribute' => 'logo']);
    }

    /**
     * @return Images
     */
    public function getImageLogo() {
        $query = $this->getLogo();
        if ($query->count()) {
            /** @var Images $logo */
            $logo = $query->one();
            return $logo;
        }

        return new Images();
    }

}