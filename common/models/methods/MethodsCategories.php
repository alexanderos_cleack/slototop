<?php

namespace common\models\methods;

use Yii;
use yii\helpers\ArrayHelper;

use common\models\methods\MethodsPayment;

/**
 * This is the model class for table "methods_categories".
 *
 * @property integer $id
 * @property string $title
 *
 * @property MethodsPayment[] $methodsPayments
 */
class MethodsCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'methods_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/b', 'ID'),
            'title' => Yii::t('app/b', 'Title'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMethodsPayments()
    {
        return $this->hasMany(MethodsPayment::className(), ['category_id' => 'id']);
    }

    public static function getForList() {
        return ArrayHelper::map(MethodsCategories::find()->asArray()->orderBy('title')->all(), 'id', 'title');
    }

}
