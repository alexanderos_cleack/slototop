<?php

namespace common\models\methods;

use Yii;

/**
 * This is the model class for table "methods_output2casino".
 *
 * @property integer $casino_id
 * @property integer $method_id
 *
 * @property Casino $casino
 * @property MethodsPayment $method
 */
class MethodsOutput2Casino extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'methods_output2casino';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['casino_id', 'method_id'], 'required'],
            [['casino_id', 'method_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'casino_id' => Yii::t('app/b', 'Casino ID'),
            'method_id' => Yii::t('app/b', 'Method ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCasino()
    {
        return $this->hasOne(Casino::className(), ['id' => 'casino_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMethod()
    {
        return $this->hasOne(MethodsPayment::className(), ['id' => 'method_id']);
    }
}
