<?php

namespace common\models\methods;

use Yii;

use common\models\currencies\Currencies;
use common\models\methods\MethodsPayment;

/**
 * This is the model class for table "methods_payment2currencies".
 *
 * @property integer $method_id
 * @property integer $currency_id
 * @property double $output
 *
 * @property Currencies $currency
 * @property MethodsPayment $method
 */
class MethodsPayment2Currencies extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'methods_payment2currencies';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['method_id', 'currency_id'], 'required'],
            [['method_id', 'currency_id'], 'integer'],
            [['output'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'method_id' => Yii::t('app/b', 'Method ID'),
            'currency_id' => Yii::t('app/b', 'Currency ID'),
            'output' => Yii::t('app/b', 'Methods payment output'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currencies::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMethod()
    {
        return $this->hasOne(MethodsPayment::className(), ['id' => 'method_id']);
    }
}
