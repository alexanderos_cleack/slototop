<?php

namespace common\models\methods;

use Yii;

/**
 * This is the model class for table "methods_payment2categories".
 *
 * @property integer $method_id
 * @property integer $category_id
 *
 * @property MethodsCategories $category
 * @property MethodsPayment $method
 */
class MethodsPayment2Categories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'methods_payment2categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['method_id', 'category_id'], 'required'],
            [['method_id', 'category_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'method_id' => Yii::t('app/b', 'Method ID'),
            'category_id' => Yii::t('app/b', 'Category ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(MethodsCategories::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMethod()
    {
        return $this->hasOne(MethodsPayment::className(), ['id' => 'method_id']);
    }
}
