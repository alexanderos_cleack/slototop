<?php

namespace common\models\methods;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

use common\components\behaviors\ArImagesBehavior;

/**
 * This is the model class for table "methods_output".
 *
 * @property integer $id
 * @property string $title
 *
 * @property MethodsOutput2casino[] $methodsOutput2casinos
 * @property Casino[] $casinos
 */
class MethodsOutput extends \yii\db\ActiveRecord {

    private $logo;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'methods_output';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 128]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id'    => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'logo'  => Yii::t('app', 'Logo'),
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => ArImagesBehavior::className(),
                'model' => $this,
                'attributesOne' => ['logo'],
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMethodsOutput2casinos() {
        return $this->hasMany(MethodsOutput2casino::className(), ['method_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCasinos() {
        return $this->hasMany(Casino::className(), ['id' => 'casino_id'])->viaTable('methods_output2casino', ['method_id' => 'id']);
    }

    public static function getForList() {
        return ArrayHelper::map(MethodsOutput::find()->orderBy('title')->all(), 'id', 'title');
    }

    /**
     * @return \yii\data\ActiveDataProvider
     */
    public function search() {
        $query = MethodsOutput::find();
        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
    }
}
