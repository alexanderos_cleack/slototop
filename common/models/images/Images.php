<?php

namespace common\models\images;

use Yii;

/**
 * This is the model class for table "images".
 *
 * @property integer $id
 * @property string $model_schema
 * @property integer $model_id
 * @property integer $attribute
 * @property string $filename
 * @property string $alt
 */
class Images extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'images';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['model_schema', 'model_id', 'attribute', 'filename'], 'required'],
            [['model_id'], 'integer'],
            [['model_schema', 'attribute', 'alt'], 'string', 'max' => 128],
            [['filename'], 'string', 'max' => 256]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'model_schema' => 'Model Schema',
            'model_id' => 'Model ID',
            'attribute' => 'Attribute',
            'filename' => 'Filename',
            'alt' => 'Alt',
        ];
    }

    public function beforeDelete() {
        if (parent::beforeDelete()) {
            $filename = Yii::getAlias('@uploads') . DIRECTORY_SEPARATOR . $this->filename;
            if (file_exists($filename)) {
                unlink($filename);
                Yii::info("Удаление файла '{$filename}' | ID - " . $this->id, 'debug.files');
            }
            return true;
        }
        return false;
    }

    public function getUrl() {
        return Yii::getAlias('@uploads_url') . '/' . $this->filename;
    }

    public function getFileInfo() {
        $filename = Yii::getAlias('@uploads') . DIRECTORY_SEPARATOR . $this->filename;
        if (file_exists($filename)) {
            $imagesize = getimagesize($filename);
            return [
                'width' => $imagesize[0],
                'height' => $imagesize[1],
                'mime' => $imagesize['mime'],
            ];
        }

        return [
            'width' => 0,
            'height' => 0,
            'mime' => null,
        ];
    }

}
