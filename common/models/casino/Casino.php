<?php

namespace common\models\casino;

use common\components\behaviors\ArSameBehavior;
use common\traits\ImagesTrait;
use common\traits\SameTrait;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use common\components\behaviors\ArImagesBehavior;
use common\components\behaviors\ArManyToManyBehavior;
use common\components\behaviors\ArMetaTagsBehavior;
use common\components\behaviors\ArRatingBehavior;
use common\models\images\Images;
use common\models\bonuses\Bonuses2Casino;
use common\models\bonuses\Bonuses;
use common\models\bonuses\BonusesTypes;
use common\models\countries\Countries2Casino;
use common\models\countries\Countries;
use common\models\currencies\Currencies2Casino;
use common\models\currencies\Currencies;
use common\models\languages\Languages2Casino;
use common\models\languages\Languages;
use common\models\licenses\Licenses;
use common\models\methods\MethodsOutput2Casino;
use common\models\methods\MethodsOutput;
use common\models\methods\MethodsPayment2Casino;
use common\models\methods\MethodsPayment;
use common\models\slots\Slots2Vendors;
use common\models\slots\Slots;
use common\models\vendors\Vendors2Casino;
use common\models\vendors\Vendors;
use common\models\vendors\Vendors2Platforms;
use common\models\vendors\VendorsPlatforms;

/**
 * This is the model class for table "casino".
 *
 * @property integer $id
 * @property string $title
 * @property string $payout
 * @property string $ref_url
 * @property string $video_viewer_url
 * @property integer $year_established
 * @property string $terms_output
 * @property integer $amount_games
 * @property integer $blacklist
 * @property boolean $new
 * @property string $new_to_date
 * @property integer $license_id
 * @property integer $rating_users
 * @property integer $rating_admins
 * @property string $description
 * @property string $short_description
 * @property string $casino_url
 * @property string $casino_redirect_url
 * @property string $slug
 *
 * @property string $meta_title
 * @property string $meta_h1
 * @property string $meta_keywords
 * @property string $meta_description
 *
 * @property Bonuses[] $bonuses
 * @property Licenses $license
 * @property Currencies[] $currencies
 * @property Languages[] $languages
 * @property MethodsOutput[] $methods_output
 * @property MethodsPayment[] $methods_payment
 *
 * @method float getRatingAll
 */
class Casino extends \yii\db\ActiveRecord {

    use ImagesTrait, SameTrait;

    public $rating_all;

    private $_currencies = null;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'casino';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title', 'description'], 'required'],
            [['year_established', 'license_id', 'rating_users', 'rating_admins'], 'integer'],
            [['rating_users', 'rating_admins'], 'default', 'value' => 0],
            [['description', 'description_view', 'description_registration', 'description_games', 'description_additional'], 'string'],
            [['blacklist', 'actively', 'new', 'chat', 'main_page'], 'boolean'],
            [['new_to_date'], 'date', 'format' => 'yyyy-MM-dd'],
            [['title'], 'string', 'max' => 128],
            [['phone'], 'string', 'max' => 64],
            [['amount_games'], 'string', 'max' => 32],
            [['payout'], 'string', 'max' => 10],
            [['ref_url', 'terms_output', 'email', 'short_description',
                'casino_url', 'video_viewer_url', 'casino_redirect_url'], 'string', 'max' => 256],
            [['email'], 'email'],
            [['logo'], 'file'],
            [['enabled'], 'boolean'],
            [['screens', 'screens_view', 'screens_registration', 'screens_games'], 'file', 'maxFiles' => 10],
            [['bonuses', 'languages', 'methods_payment', 'methods_output', 'currencies', 'vendors', 'countries'], 'safe'],

            ['slug', 'required'],
            [['title', 'slug'], 'unique'],

            [['meta_title', 'meta_h1', 'meta_keywords', 'meta_description'], 'string', 'max' => 256],

            [['imagesAlt', 'same'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'actively'                 => Yii::t('app', 'Actively'),
            'amount_games'             => Yii::t('app', 'Amount games'),
            'blacklist'                => Yii::t('app', 'Blacklist'),
            'bonuses'                  => Yii::t('app', 'Bonuses'),
            'casino_url'               => Yii::t('app', 'Casino url'),
            'chat'                     => Yii::t('app/b', 'Casino chat'),
            'countries'                => Yii::t('app', 'Countries'),
            'currencies'               => Yii::t('app', 'Currencies'),
            'description'              => Yii::t('app', 'Description'),
            'description_additional'   => Yii::t('app/b', 'Casino description_additional'),
            'description_games'        => Yii::t('app/b', 'Casino description_games'),
            'description_registration' => Yii::t('app/b', 'Casino description_registration'),
            'description_view'         => Yii::t('app/b', 'Casino description_view'),
            'enabled'                  => Yii::t('app/b', 'Show page'),
            'id'                       => 'ID',
            'languages'                => Yii::t('app', 'Languages'),
            'license_id'               => Yii::t('app', 'License'),
            'logo'                     => Yii::t('app', 'Logo'),
            'methods_output'           => Yii::t('app', 'MethodsOutput'),
            'methods_payment'          => Yii::t('app', 'MethodsPayment'),
            'new'                      => Yii::t('app', 'New'),
            'new_to_date'              => Yii::t('app', 'New to date'),
            'payout'                   => Yii::t('app', 'Payout'),
            'phone'                    => Yii::t('app/b', 'Casino phone'),
            'rating_admins'            => Yii::t('app/b', 'Rating admins'),
            'rating_users'             => Yii::t('app/b', 'Rating users'),
            'rating_all'               => Yii::t('app/b', 'Rating'),
            'ref_url'                  => Yii::t('app', 'Ref url'),
            'screens'                  => Yii::t('app', 'Screens'),
            'screens_games'            => Yii::t('app/b', 'Casino screens_games'),
            'screens_registration'     => Yii::t('app/b', 'Casino screens_registration'),
            'screens_view'             => Yii::t('app/b', 'Casino screens_view'),
            'short_description'        => Yii::t('app/b', 'Casino short_description'),
            'terms_output'             => Yii::t('app', 'Terms output'),
            'title'                    => Yii::t('app', 'Title'),
            'vendors'                  => Yii::t('app/b', 'Casino vendors'),
            'video_viewer_url'         => Yii::t('app', 'Video viewer url'),
            'year_established'         => Yii::t('app', 'Year established'),
            'main_page'                => Yii::t('app/b', 'main_page'),
            'slug'                     => Yii::t('app/b', 'Url'),
            'casino_redirect_url'      => Yii::t('app/b', 'Редирект на казино'),

            'meta_title'       => Yii::t('app', 'Meta title'),
            'meta_keywords'    => Yii::t('app', 'Meta keywords'),
            'meta_description' => Yii::t('app', 'Meta description'),
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => ArManyToManyBehavior::class,
                'intermediaries' => [Bonuses2Casino::class, MethodsOutput2Casino::class, MethodsPayment2Casino::class, Languages2Casino::class, Vendors2Casino::class, Countries2Casino::class/*, Currencies2Casino::class*/],
                'attributes' => ['bonuses', 'methods_output', 'methods_payment', 'languages', 'vendors', 'countries'/*, 'currencies'*/],
                'table_a_fkey' => 'casino_id',
                'table_b_fkeys' => ['bonus_id', 'method_id', 'method_id', 'language_id', 'vendor_id', 'country_id'/*, 'currency_id'*/],
            ],
            [
                'class' => ArImagesBehavior::class,
                'model' => $this,
                'attributesOne' => ['logo'],
                'attributesMany' => ['screens', 'screens_view', 'screens_registration', 'screens_games'],
            ],
            // [
            //     'class' => SluggableBehavior::class,
            //     'attribute' => 'title',
            // ],
            [
                'class' => ArMetaTagsBehavior::class,
            ],
            [
                'class' => ArSameBehavior::class,
            ],
            [
                'class' => ArRatingBehavior::class,
            ],
            [
                'class' => \yii\behaviors\TimestampBehavior::class,
                'createdAtAttribute' => 'created_time',
                'updatedAtAttribute' => 'updated_time',
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLicense() {
        return $this->hasOne(Licenses::class, ['id' => 'license_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMethods_output() {
        return $this->hasMany(MethodsPayment::class, ['id' => 'method_id'])->viaTable(MethodsOutput2Casino::tableName(), ['casino_id' => 'id']);
    }

    public function setMethods_output($methods) {
        $this->methods_output = $methods;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMethods_payment() {
        return $this->hasMany(MethodsPayment::class, ['id' => 'method_id'])->viaTable(MethodsPayment2Casino::tableName(), ['casino_id' => 'id']);
    }

    public function setMethods_payment($methods) {
        $this->methods_payment = $methods;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonuses() {
        return $this->hasMany(Bonuses::class, ['id' => 'bonus_id'])->viaTable(Bonuses2Casino::tableName(), ['casino_id' => 'id']);
    }

    public function setBonuses($bonuses) {
        $this->bonuses = $bonuses;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonuses_query() {
        return $this->hasMany(Bonuses::class, ['id' => 'bonus_id'])->where(['enabled' => true])->viaTable(Bonuses2Casino::tableName(), ['casino_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonusesTypes_site() {
        return $this->hasMany(BonusesTypes::class, ['id' => 'bonus_type_id'])->via('bonuses_query')->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencies() {
        return $this->hasMany(Currencies::class, ['id' => 'currency_id'])->viaTable('currencies2casino', ['casino_id' => 'id']);
    }

    public function setCurrencies($currencies) {
        $this->currencies = $currencies;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages() {
        return $this->hasMany(Languages::class, ['id' => 'language_id'])->viaTable(Languages2Casino::tableName(), ['casino_id' => 'id']);
    }

    public function setLanguages($languages) {
        $this->languages = $languages;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountries() {
        return $this->hasMany(Countries::class, ['id' => 'country_id'])->viaTable(Countries2Casino::tableName(), ['casino_id' => 'id']);
    }

    public function setCountries($countries) {
        $this->countries = $countries;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendors() {
        return $this->hasMany(Vendors::class, ['id' => 'vendor_id'])->viaTable(Vendors2Casino::tableName(), ['casino_id' => 'id']);
    }

    public function setVendors($vendors) {
        $this->vendors = $vendors;
    }

    public function getVendors2casino_platforms() {
        return $this->hasMany(Vendors2Casino::class, ['casino_id' => 'id']);//->from(['vendors2casino_platforms' => 'vendors2casino']);
    }

    public function getVendors2platforms() {
        // return $this->hasMany(Vendors2Platforms::class, ['vendor_id' => 'vendor_id'])->via('vendors2casino_platforms');
        // return $this->hasMany(Vendors2Platforms::class, ['vendor_id' => 'vendor_id'])->via('vendors2casino');
        return $this->hasMany(Vendors2Platforms::class, ['vendor_id' => 'vendor_id'])->viaTable(Vendors2Casino::tableName(), ['casino_id' => 'id']);
    }

    public function getPlatforms() {
        return $this->hasMany(VendorsPlatforms::class, ['id' => 'platform_id'])->via('vendors2platforms');
    }

    public function getPlatforms_site() {
        return $this->hasMany(VendorsPlatforms::class, ['id' => 'platform_id'])->via('vendors2platforms')->orderBy('vendors_platforms.title')->all();
    }

    public function search() {
        $query = Casino::find();

        $query->select(['*', '(rating_users + rating_admins) as rating_all']);

        $query->andFilterWhere(['main_page' => $this->main_page]);
        $query->andFilterWhere(['ilike', 'title', $this->title]);

        return new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['title' => SORT_ASC],
                'attributes' => [
                    'id',
                    'title' => [
                        'asc' => ['title' => SORT_ASC],
                        'desc' => ['title' => SORT_DESC]
                    ],
                    'main_page' => [
                        'asc' => ['main_page' => SORT_ASC],
                        'desc' => ['main_page' => SORT_DESC]
                    ],
                    'rating_users' => [
                        'asc' => ['rating_users' => SORT_ASC],
                        'desc' => ['rating_users' => SORT_DESC]
                    ],
                    'rating_admins' => [
                        'asc' => ['rating_admins' => SORT_ASC],
                        'desc' => ['rating_admins' => SORT_DESC]
                    ],
                    'rating_all' => [
                        'asc' => ['(rating_admins + rating_users)' => SORT_ASC],
                        'desc' => ['(rating_admins + rating_users)' => SORT_DESC]
                    ],
                ],
            ],
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
    }

    public static function getForList() {
        return ArrayHelper::map(Casino::find()->orderBy('title')->all(), 'id', 'title');
    }

    public function getUrl($absolute = false) {
        return Url::to('/casino/' . $this->slug, $absolute);
    }

    public function getCurrencies_site() {
        return $this->getCurrencies()->all();
        // if (!$this->_currencies) {
        //     $this->_currencies = Currencies2Casino::find()->where(['casino_id' => $this->id])->all();
        // }
        // $currencies = array_map(function ($item) {
        //     return "{$item->currency->title} ({$item->currency->iso})";
        // }, $this->_currencies);
        // return join(' / ', $currencies);
    }

    public function getMinDeposit_site() {
        if (!$this->_currencies) {
            $this->_currencies = Currencies2Casino::find()->where(['casino_id' => $this->id])->all();
        }
        $currencies = [];
        foreach ($this->_currencies as $currency) {
            if ($currency->min_deposit) {
                $currencies[] = "{$currency->min_deposit} {$currency->currency->iso}";
            }
        }
        return join(' / ', $currencies);
    }

    public function getMinOutput_site() {
        if (!$this->_currencies) {
            $this->_currencies = Currencies2Casino::find()->where(['casino_id' => $this->id])->all();
        }
        $currencies = [];
        foreach ($this->_currencies as $currency) {
            if ($currency->min_output) {
                $currencies[] = "{$currency->min_output} {$currency->currency->iso}";
            }
        }
        return join(' / ', $currencies);
    }

    public function getCountries_site() {
        return $this->getCountries()->orderBy('title')->all();
    }

    public function getLanguages_site() {
        return $this->getLanguages()->all();
    }

    public function getVendors_site() {
        $_vendors = null;

        $vendors = $this->getVendors()->orderBy('title')->andWhere(['enabled' => true])->all();
        $_vendors = $vendors;

        $f = function (&$_vendors, $vendor) use (&$f) {
            $vendors = $vendor->getVendors()->orderBy('title')->where(['enabled' => true])->all();
            $_vendors = \yii\helpers\ArrayHelper::merge($_vendors, $vendors);
            foreach ($vendors as $vendor) {
                $f($_vendors, $vendor);
            }
        };

        foreach ($vendors as $vendor) {
            $f($_vendors, $vendor);
        }

        return $_vendors;
    }

    public function getMethodsPayment_site() {
        return $this->getMethods_payment()->orderBy('title')->andWhere('enabled')->all();
    }

    public function getMethodsOutput_site() {
        return $this->getMethods_output()->orderBy('title')->andWhere('enabled')->all();
    }

    public function getBonuses_site() {
        $query = $this->hasMany(Bonuses::class, ['id' => 'bonus_id'])->where(['enabled' => true])->viaTable(Bonuses2Casino::tableName(), ['casino_id' => 'id']);
        $count = $query->count();
        return $query->offset(rand(1, $count) - 4)->limit(4)->all();
    }

    public function getVendors2casino() {
        return $this->hasMany(Vendors2Casino::class, ['casino_id' => 'id']);
    }

    public function getCasino2vendors() {
        return $this->hasMany(Vendors2Casino::class, ['vendor_id' => 'vendor_id'])->via('vendors2casino');
    }

    public function getSlots2vendors() {
        return $this->hasMany(Slots2Vendors::class, ['vendor_id' => 'vendor_id'])->via('vendors2casino');
    }

    /**
     * @return ActiveQuery
     */
    public function getSlots() {
        return $this->hasMany(Slots::class, ['id' => 'slot_id'])->via('slots2vendors');
    }

    public function getPopularGames_site() {
        $query = $this->hasMany(Slots::class, ['id' => 'slot_id'])->where(['enabled' => true])->via('slots2vendors');
        $count = $query->count();
        return $query->offset(rand(1, $count) - 4)->orderBy('rating_users')->limit(4)->all();
    }

    /**
     * @return ActiveQuery
     */
    public function getPopularGames_query() {
        return $this->hasMany(Slots::class, ['id' => 'slot_id'])->from(['t' => Slots::tableName()])->where(['t.enabled' => true])->via('slots2vendors');
    }

    public function getCasinoSimilar_site() {
        $query = $this->hasMany(Casino::class, ['id' => 'casino_id'])->where(['enabled' => true])->andWhere(['!=', 'id', $this->id])->via('casino2vendors');
        $count = $query->count();
        return  $query->offset(rand(1, $count) - 4)->limit(4)->all();
    }

    /**
     * @return ActiveQuery
     */
    public function getCasinoSimilar_query() {
        $query = $this->getSame();
        $query->andWhere(['t.enabled' => true]);
        if ($query->count()) {
            return $query;
        }

        return $this->hasMany(Casino::class, ['id' => 'casino_id'])->from(['t' => $this::tableName()])->where(['t.enabled' => true])->andWhere(['!=', 't.id', $this->id])->via('casino2vendors');
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->description = str_ireplace('&amp;', '&', $this->description);
            $this->description_view = str_ireplace('&amp;', '&', $this->description_view);
            $this->description_games = str_ireplace('&amp;', '&', $this->description_games);
            $this->description_registration = str_ireplace('&amp;', '&', $this->description_registration);
            $this->description_additional = str_ireplace('&amp;', '&', $this->description_additional);
            return true;
        }
        return false;
    }

    public function getAmountGames_site() {
        return $this->hasMany(Slots::class, ['id' => 'slot_id'])->where(['enabled' => true])->via('slots2vendors')->count();
    }

    /**
     * @return string
     */
    public function getRedirectUrl() {
        return $this->ref_url ?
            '//' . $this->ref_url :
            ($this->casino_redirect_url ?
                Url::to(['/casino/casino/redirect', 'slug' => $this->slug]) :
                '//' . $this->casino_url);
    }

    /**
     * @return string
     */
    public function getTitlePreview() {
        return str_replace(['Казино '], '', $this->title);
    }

    /**
     * @return ActiveQuery
     */
    public function getLogo()
    {
        return $this->hasOne(Images::class, ['model_id' => 'id'])->where(['model_schema' => $this::tableName(), 'attribute' => 'logo']);
    }

    /**
     * @return Images
     */
    public function getImageLogo() {
        $query = $this->getLogo();
        if ($query->count()) {
            /** @var Images $logo */
            $logo = $query->one();
            return $logo;
        }

        return new Images();
    }
}
