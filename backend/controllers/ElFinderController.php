<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use zxbodya\yii2\elfinder\ConnectorAction;

class ElFinderController extends \app\components\BController {

    public function actions() {
	return [
            'connector' => [
                'class' => ConnectorAction::className(),
                'settings' => [
                    'root' => Yii::getAlias('@uploads'),
                    'URL' => Yii::getAlias('@uploads_url') . '/',
                    'rootAlias' => 'Home',
                    'mimeDetect' => 'none'
                ]
            ],
        ];
    } 

}