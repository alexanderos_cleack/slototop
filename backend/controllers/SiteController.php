<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;

use app\models\LoginForm;

class SiteController extends \app\components\BController {

    public $layout = 'default';

    public function behaviors() {
        return [
            [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'images'],
                        'roles' => ['root'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['login'],
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex() {
        return $this->render('index');
    }

    public function actionLogin() {
        $this->layout = 'main';

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(Yii::$app->user->returnUrl);
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout() {
        Yii::$app->user->logout();
        return $this->redirect(Url::to(['/']));
    }

    public function actionImages() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'text/plan');

        $images = \common\models\images\Images::find()->asArray()->orderBy(['filename' => SORT_ASC])->all();
        foreach ($images as $image) {
            if (!file_exists(Yii::getAlias('@uploads') . $image['filename'])) {
                echo $image['model_schema'] . ' ' . $image['model_id'] . ' ' . $image['attribute'] . ' ' . $image['filename'] . "\n";
            }
        }
    }

}

?>