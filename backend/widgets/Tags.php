<?php

namespace app\widgets;

use yii\base\Widget;
use yii\db\ActiveRecord;
use yii\widgets\ActiveForm;

/**
 * Class Tags
 * @package app\widgets
 *
 * @property ActiveRecord $model
 * @property ActiveForm $form
 */
class Tags extends Widget
{
    public $model;
    public $form;

    public function run()
    {
        $model = $this->model;
        $form = $this->form;

        return $this->render('tags', [
            'model' => $model,
            'form' => $form,
        ]);
    }
}