<?php

namespace app\widgets;

use Yii;
use yii\base\Widget;

class SlugRefresh extends Widget {

    public $model;

    public function run() {
        $model = $this->model;
        $model_name = strtolower(array_slice(explode('\\', $model::className()), -1)[0]);
        return $this->render('slug-refresh/index', [
            'model' => $model,
            'id_title' => sprintf('#%s-title', $model_name),
            'id_slug' => sprintf('#%s-slug', $model_name),
        ]);
    }

}

?>