<?php

namespace app\widgets;

use Yii;
use yii\base\Widget;

class MetaTags extends Widget {

    public $model;
    public $form;

    public function run() {
        return $this->render('meta_tags', [
            'model' => $this->model,
            'form' => $this->form,
        ]);
    }

}

?>