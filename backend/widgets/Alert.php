<?php

namespace app\widgets;

use Yii;
use yii\base\Widget;

class Alert extends Widget {

    const ERROR   = 'alert-danger';
    const DANGER  = 'alert-danger';
    const SUCCESS = 'alert-success';
    const INFO    = 'alert-info';
    const WARNING = 'alert-warning';

    public function run() {
        foreach (Yii::$app->session->getAllFlashes() as $class => $messages) {
            foreach ($messages as $message) {
                if (is_array($message)) {
                    foreach ($message as $_message) {
                        $this->showMessage($_message, $class);
                    }
                } else {
                    $this->showMessage($message, $class);
                }
            }
        }
    }

    private function showMessage($message, $class) {
        if ($message) echo \yii\bootstrap\Alert::widget(['body' => $message, 'options' => ['class' => $class]]);
    }

}

?>