<?php
/**
 * @var \yii\web\View $this
 * @var \yii\widgets\ActiveForm $form
 * @var \common\models\slots\Slots $model
 */
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <?= Yii::t('app', 'Теги') ?>
    </div>
    <div class="panel-body">
        <?= $form->field($model, 'tags')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\tags\Tags::find()->where(['model_schema' => $model::tableName()])->all(), 'id', 'title'), [
            'multiple' => true,
            'size' => 15,
        ])->label(Yii::t('app/b', 'Теги')); ?>
        <?= $form->field($model, 'tagsTitles')->hint(Yii::t('app/b', 'Формат: назвние тега через запятую.'))->label(Yii::t('app/b', 'Новые теги')) ?>
    </div>
</div>
