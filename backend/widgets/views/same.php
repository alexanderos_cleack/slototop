<?php
/**
 * @var \yii\web\View $this
 * @var \app\widgets\Same $context
 */
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <?= Yii::t('app/b', 'Похожие') ?>
    </div>
    <div class="panel-body">
        <?= $context->form->field($context->model, 'same')->dropDownList($context->getItems(), [
            'multiple' => true,
            'size' => 15,
        ])->label(Yii::t('app/b', 'Похожие')); ?>
    </div>
</div>
