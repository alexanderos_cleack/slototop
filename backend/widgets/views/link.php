<?php

use yii\helpers\Url;

?>

<ul class="pagination">
    <?php if ($prev): ?>
        <li class="next"><a href="<?= Url::to(['update', 'id' => $prev['id']]) ?>" title="<?= $prev['title'] ?>">«</a></li>
    <?php else: ?>
        <li class="next <?php if (!$prev) echo 'disabled' ?>"><span>«</span></li>
    <?php endif; ?>

    <?php if ($next): ?>
        <li class="next"><a href="<?= Url::to(['update', 'id' => $next['id']]) ?>" title="<?= $next['title'] ?>">»</a></li>
    <?php else: ?>
        <li class="next <?php if (!$next) echo 'disabled' ?>"><span>»</span></li>
    <?php endif; ?>
</ul>