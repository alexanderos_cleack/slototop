<?php

/**
 * @var  $from yii\bootstrap\ActiveForm;
 * @var  $this yii\web\View;
 * @var  $model common\models\metatags\MetaTags;
 */

?>

<div class="panel panel-default">
    <div class="panel-heading">
        <?= Yii::t('app', 'Meta Tags') ?>
    </div>
    <div class="panel-body">
        <?= $form->field($model, 'meta_title') ?>
        <?= $form->field($model, 'meta_h1') ?>
        <?= $form->field($model, 'meta_keywords') ?>
        <?= $form->field($model, 'meta_description') ?>
    </div>
</div>