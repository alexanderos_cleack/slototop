<div class="form-group">
    <label>Slug</label>
    <div class="input-group">
        <?= \yii\helpers\Html::activeTextInput($model, 'slug', ['class' => 'form-control']) ?>
        <span class="input-group-btn">
            <button id="slug-refresh" class="btn btn-default" type="button"><?= \yii\bootstrap\Html::icon('refresh') ?></button>
        </span>
    </div>
</div>

<script>
    $(document).ready(function () {
        slugRefresh('<?= $id_title ?>', '<?= $id_slug ?>');
    });

    function slugRefresh(id_title, id_slug) {
        $('#slug-refresh').click(function () {
            val = ((_val = $(id_slug).val()) && _val.length > 0) ? _val : (((_val = $(id_title).val()) && _val.length > 0) ? _val : null);

            if (val) {
                $.ajax({
                    url: '<?= join('/', array_slice(explode('/', Yii::$app->request->url), 0, 3)) . '/slugRefresh' ?>',
                    type: 'post',
                    data: {slug: val},
                })
                .done(function (data) {
                    $(id_slug).val(data.slug);
                })
            }

        })
    }
</script>