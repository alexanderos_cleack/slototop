<?php
/**
 * @var \yii\web\View $this
 * @var \yii\widgets\ActiveForm $form
 * @var \yii\db\ActiveRecord $parentModel
 * @var \common\models\images\Images $imageModel
 * @var string $tabular
 * @var bool $delete
 */
?>

<?php if ($delete): ?>
    <?= \yii\helpers\Html::a(\yii\helpers\Html::img($imageModel->getUrl(), ['width' => '100px']),
        \yii\helpers\Url::to(['imageDelete', 'id' => $imageModel->id]),
        ['title' => Yii::t('app/f', 'Удалить')]) ?>
<?php else: ?>
    <?= \yii\helpers\Html::img($imageModel->getUrl(), ['width' => '100px']); ?>
<?php endif; ?>

<?= $form->field($parentModel, "{$tabular}imagesAlt[{$imageModel->id}]"); ?>