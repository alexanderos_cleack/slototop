<?php

namespace app\widgets;

use yii\base\Widget;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/**
 * Class Same
 * @package app\widgets
 *
 * @property ActiveRecord $model
 * @property ActiveForm $form
 */
class Same extends Widget
{
    public $model;
    public $form;

    public function run()
    {
        return $this->render('same', [
            'context' => $this,
        ]);
    }

    /**
     * @return array
     */
    public function getItems()
    {
        return ArrayHelper::map($this->model::find()
            ->select(['id', 'title'])
            ->where(['!=', 'id', $this->model->id])
            ->orderBy('title asc')->asArray()->all(), 'id', 'title');
    }

}