<?php

namespace app\widgets;

use common\models\images\Images;

use yii\base\Widget;
use yii\db\ActiveRecord;
use yii\widgets\ActiveForm;

/**
 * Class ImagesAlt
 * @package app\widgets
 *
 * @property ActiveForm
 * @property ActiveRecord $parentModel
 * @property Images $imageModel
 * @property int $tabular
 * @property bool $delete
 */
class ImagesAlt extends Widget
{
    public $form;
    public $parentModel;
    public $imageModel;
    public $tabular;
    public $delete = false;

    public function run()
    {
        if ($this->imageModel instanceof Images) {
            return $this->render('imagesAlt', [
                'form' => $this->form,
                'parentModel' => $this->parentModel,
                'imageModel' => $this->imageModel,
                'tabular' => is_numeric($this->tabular) ? sprintf('[%s]', $this->tabular) : null,
                'delete' => $this->delete,
            ]);
        }
    }

}