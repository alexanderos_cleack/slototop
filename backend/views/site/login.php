<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

use app\assets\LoginAsset;

/**
 * @var yii\web\View $this
 * @var yii\bootstrap\ActiveForm $form
 * @var \common\models\LoginForm $model
 */

$this->title = Yii::t('app','Auth');
$this->context->breadcrumbs[] = $this->title;

LoginAsset::register($this);

?>

<div class="row">
	<div class="col-lg-offset-4 col-lg-4 well well-sm">
		<h1><?= Html::encode($this->title) ?></h1>
		<?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
			<?= $form->field($model, 'username') ?>
			<?= $form->field($model, 'password')->passwordInput() ?>
			<div class="form-group">
				<?= Html::submitButton(Yii::t('app', 'Sign in'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
			</div>
		<?php ActiveForm::end(); ?>
	</div>
</div>

