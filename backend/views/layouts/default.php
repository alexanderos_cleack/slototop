<?php

use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use yii\bootstrap\Dropdown;
use yii\widgets\Breadcrumbs;

use app\assets\AppAsset;
use app\widgets\Alert;


AppAsset::register($this);

$this->beginContent('@app/views/layouts/main.php');

NavBar::begin([
    'options' => [
        'class' => 'navbar navbar-inverse navbar-fixed-top',
    ],
    'brandLabel' => Yii::$app->name,
    'brandUrl' => '/',
]);

echo Nav::widget([
    'encodeLabels' => false,
    'options' => [
        'class' => 'nav navbar-nav navbar-right',
    ],
    'items' => [
        // [
        //  'label' => 'Dropdown',
        //  'items' => [
        //      ['label' => 'Level 1 - Dropdown A', 'url' => '#'],
        //      '<li class="divider"></li>',
        //      '<li class="dropdown-header">Dropdown Header</li>',
        //      ['label' => 'Level 1 - Dropdown B', 'url' => '#'],
        //  ],
        // ],
        // [
        //     'label' => '<li>' . Yii::t('app', 'Admins') . '</li>',
        //     'url' => ['/admins/admins-items/index'],
        // ],
        [
         'label' =>  Yii::$app->glyphs->iuser(Yii::$app->user->identity->login),
         'items' => [
             // '<li class="divider"></li>',
             ['label' => '<li>'. Yii::$app->glyphs->ioff(Yii::t('app', 'Logout')) .'</li>', 'url' => ['/logout']],
         ],
        ],
    ],
]);

NavBar::end();

?>

<div class="container">
    <div class="row">
        <div class="col-md-2 sidebar">
            <?= Nav::widget([
                'options' => ['class' => 'nav nav-pills my-affix'],
                'items' => [
                    ['label' => Yii::t('app', 'Licenses'), 'url' => '#',
                        'items' => Dropdown::widget([
                            'options' => ['class' => 'dropdown-menu my-dropdown-menu'],
                            'items' => [
                                '<li class="dropdown-header">' . Yii::t('app', 'Licenses') .'</li>',
                                ['label' => Yii::t('app', 'List'), 'url' => ['/licenses/licenses-items/index']],
                                ['label' => Yii::t('app', 'Create'), 'url' => ['/licenses/licenses-items/create']],
                            ],
                        ]),
                    ],

                    ['label' => Yii::t('app', 'Languages'), 'url' => '#',
                        'items' => Dropdown::widget([
                            'options' => ['class' => 'dropdown-menu my-dropdown-menu'],
                            'items' => [
                                '<li class="dropdown-header">' . Yii::t('app', 'Languages') .'</li>',
                                ['label' => Yii::t('app', 'List'), 'url' => ['/languages/languages-items/index']],
                                ['label' => Yii::t('app', 'Create'), 'url' => ['/languages/languages-items/create']],
                            ],
                        ]),
                    ],

                    ['label' => Yii::t('app', 'Countries'), 'url' => '#',
                        'items' => Dropdown::widget([
                            'options' => ['class' => 'dropdown-menu my-dropdown-menu'],
                            'items' => [
                                '<li class="dropdown-header">' . Yii::t('app', 'Countries') .'</li>',
                                ['label' => Yii::t('app', 'List'), 'url' => ['/countries/countries-items/index']],
                                ['label' => Yii::t('app', 'Create'), 'url' => ['/countries/countries-items/create']],
                            ],
                        ]),
                    ],

                    ['label' => Yii::t('app', 'Currencies'), 'url' => '#',
                        'items' => Dropdown::widget([
                            'options' => ['class' => 'dropdown-menu my-dropdown-menu'],
                            'items' => [
                                '<li class="dropdown-header">' . Yii::t('app', 'Currencies') .'</li>',
                                ['label' => Yii::t('app', 'List'), 'url' => ['/currencies/currencies-items/index']],
                                ['label' => Yii::t('app', 'Create'), 'url' => ['/currencies/currencies-items/create']],
                            ],
                        ]),
                    ],

                    ['label' => Yii::t('app', 'Casino'), 'url' => '#',
                        'items' => Dropdown::widget([
                            'options' => ['class' => 'dropdown-menu my-dropdown-menu'],
                            'items' => [
                                '<li class="dropdown-header">' . Yii::t('app', 'Casino') .'</li>',
                                ['label' => Yii::t('app', 'List'), 'url' => ['/casino/casino-items/index']],
                                ['label' => Yii::t('app', 'Create'), 'url' => ['/casino/casino-items/create']],
                            ],
                        ]),
                    ],

                    ['label' => Yii::t('app', 'Vendors'), 'url' => '#',
                        'items' => Dropdown::widget([
                            'options' => ['class' => 'dropdown-menu my-dropdown-menu'],
                            'items' => [
                                '<li class="dropdown-header">' . Yii::t('app', 'Vendors') .'</li>',
                                ['label' => Yii::t('app', 'List'), 'url' => ['/vendors/vendors-items/index']],
                                ['label' => Yii::t('app', 'Create'), 'url' => ['/vendors/vendors-items/create']],
                                '<li class="divider"></li>',
                                '<li class="dropdown-header">' . Yii::t('app', 'VendorsPlatforms') .'</li>',
                                ['label' => Yii::t('app', 'List'), 'url' => ['/vendors/vendors-platforms/index']],
                                ['label' => Yii::t('app', 'Create'), 'url' => ['/vendors/vendors-platforms/create']],
                            ],
                        ]),
                    ],

                    ['label' => Yii::t('app', 'Slots'), 'url' => '#',
                        'items' => Dropdown::widget([
                            'options' => ['class' => 'dropdown-menu my-dropdown-menu'],
                            'items' => [
                                '<li class="dropdown-header">' . Yii::t('app', 'Slots') .'</li>',
                                ['label' => Yii::t('app', 'List'), 'url' => ['/slots/slots-items/index']],
                                ['label' => Yii::t('app', 'Create'), 'url' => ['/slots/slots-items/create']],
                                ['label' => Yii::t('app', 'DemoGames'), 'url' => ['/slots/demo-games-items/index']],
                                '<li class="divider"></li>',
                                '<li class="dropdown-header">' . Yii::t('app', 'SlotsCategories') .'</li>',
                                ['label' => Yii::t('app', 'List'), 'url' => ['/slots/slots-categories/index']],
                                ['label' => Yii::t('app', 'Create'), 'url' => ['/slots/slots-categories/create']],
                            ],
                        ]),
                    ],

                    ['label' => Yii::t('app', 'Bonuses'), 'url' => '#',
                        'items' => Dropdown::widget([
                            'options' => ['class' => 'dropdown-menu my-dropdown-menu'],
                            'items' => [
                                '<li class="dropdown-header">' . Yii::t('app', 'Bonuses') .'</li>',
                                ['label' => Yii::t('app', 'List'), 'url' => ['/bonuses/bonuses-items/index']],
                                ['label' => Yii::t('app', 'Create'), 'url' => ['/bonuses/bonuses-items/create']],
                                '<li class="divider"></li>',
                                '<li class="dropdown-header">' . Yii::t('app', 'BonusesTypes') .'</li>',
                                ['label' => Yii::t('app', 'List'), 'url' => ['/bonuses/bonuses-types/index']],
                                ['label' => Yii::t('app', 'Create'), 'url' => ['/bonuses/bonuses-types/create']],
                            ],
                        ]),
                    ],

                    ['label' => Yii::t('app/b', 'Payment system'), 'url' => '#',
                        'items' => Dropdown::widget([
                            'options' => ['class' => 'dropdown-menu my-dropdown-menu'],
                            'items' => [
                                '<li class="dropdown-header">' . Yii::t('app/b', 'Payment system') .'</li>',
                                ['label' => Yii::t('app', 'List'), 'url' => ['/methods/methods-payment-items/index']],
                                ['label' => Yii::t('app', 'Create'), 'url' => ['/methods/methods-payment-items/create']],
                            ],
                        ]),
                    ],

                    ['label' => Yii::t('app/b', 'Pages'), 'url' => '#',
                        'items' => Dropdown::widget([
                            'options' => ['class' => 'dropdown-menu my-dropdown-menu'],
                            'items' => [
                                '<li class="dropdown-header">' . Yii::t('app/b', 'Pages') .'</li>',
                                ['label' => Yii::t('app', 'List'), 'url' => ['/pages/default/index']],
                                ['label' => Yii::t('app', 'Create'), 'url' => ['/pages/default/create']],
                            ],
                        ]),
                    ],

                    ['label' => Yii::t('app/b', 'SEO'), 'url' => '#',
                        'items' => Dropdown::widget([
                            'options' => ['class' => 'dropdown-menu my-dropdown-menu'],
                            'items' => [
                                '<li class="dropdown-header">' . Yii::t('app/b', 'Pages filters') .'</li>',
                                ['label' => Yii::t('app', 'List'), 'url' => ['/seo/seo-filters/index']],
                                ['label' => Yii::t('app', 'Create'), 'url' => ['/seo/seo-filters/create']],
                                '<li class="divider"></li>',
                                '<li class="dropdown-header">' . Yii::t('app/b', 'Pages sorts') .'</li>',
                                ['label' => Yii::t('app', 'List'), 'url' => ['/seo/seo-sorts/index']],
                                ['label' => Yii::t('app', 'Create'), 'url' => ['/seo/seo-sorts/create']],
                                '<li class="divider"></li>',
                                '<li class="dropdown-header">' . Yii::t('app/b', 'Pages') .'</li>',
                                ['label' => Yii::t('app', 'List'), 'url' => ['/seo/seo/index']],
                                ['label' => Yii::t('app', 'Create'), 'url' => ['/seo/seo/create']],
                                '<li class="divider"></li>',
                                '<li class="dropdown-header">' . Yii::t('app/b', 'Casino') .'</li>',
                                ['label' => Yii::t('app', 'List'), 'url' => ['/seo/seo-casino/index']],
                                ['label' => Yii::t('app', 'Create'), 'url' => ['/seo/seo-casino/create']],
                                '<li class="divider"></li>',
                                '<li class="dropdown-header">' . Yii::t('app/b', 'Slots') .'</li>',
                                ['label' => Yii::t('app', 'List'), 'url' => ['/seo/seo-games/index']],
                                ['label' => Yii::t('app', 'Create'), 'url' => ['/seo/seo-games/create']],
                                '<li class="divider"></li>',
                                '<li class="dropdown-header">' . Yii::t('app/b', 'Bonuses') .'</li>',
                                ['label' => Yii::t('app', 'List'), 'url' => ['/seo/seo-bonuses/index']],
                                ['label' => Yii::t('app', 'Create'), 'url' => ['/seo/seo-bonuses/create']],
                                '<li class="divider"></li>',
                                '<li class="dropdown-header">' . Yii::t('app/b', 'Methods') .'</li>',
                                ['label' => Yii::t('app', 'List'), 'url' => ['/seo/seo-methods/index']],
                                ['label' => Yii::t('app', 'Create'), 'url' => ['/seo/seo-methods/create']],
                                '<li class="divider"></li>',
                                '<li class="dropdown-header">' . Yii::t('app/b', 'Vendors') .'</li>',
                                ['label' => Yii::t('app', 'List'), 'url' => ['/seo/seo-vendors/index']],
                                ['label' => Yii::t('app', 'Create'), 'url' => ['/seo/seo-vendors/create']],
                            ],
                        ]),
                    ],

                    ['label' => Yii::t('app/b', 'Теги'), 'url' => '#',
                        'items' => Dropdown::widget([
                            'options' => ['class' => 'dropdown-menu my-dropdown-menu'],
                            'items' => [
                                '<li class="dropdown-header">' . Yii::t('app/b', 'Теги') .'</li>',
                                ['label' => Yii::t('app', 'List'), 'url' => ['/tags/default/index']],
                                ['label' => Yii::t('app', 'Create'), 'url' => ['/tags/default/create']],
                            ],
                        ]),
                    ],

                ],
            ]) ?>
        </div>
        <div class="container">
        <div class="col-md-10">
            <?= Breadcrumbs::widget([
                'homeLink' => [
                    'label' => Yii::t('app', 'Home'),
                    'url' => '/',
                    'template' => '<li><span class="glyphicon glyphicon-home" aria-hidden="true"></span> {link}</li>'
                ],
                'links' => $this->context->breadcrumbs,
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
        </div>
    </div>
</div>

<?php $this->endContent(); ?>