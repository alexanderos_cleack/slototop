<?php

// Config backend

$config = [
    'id' => 'backend_app',
    'basePath' => '@backend',
    'bootstrap' => ['log'],

    'components' => [

        'user' => [
            'identityClass' => 'backend\models\admins\Admins',
            'enableAutoLogin' => true,
        ],

        'request' => [
            'cookieValidationKey' => 'PrxNTMWsb9gbqL9SdFhX',
        ],

        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],

        'urlManager' => [
            'baseUrl' => '/',
            'rules' => [
                'uploads' => 'el-finder/connector',

                '<action:login|logout|test|images|bigscreen>' => 'site/<action>',

                '<module:\w+>/<controller>/<action>/<id:\d+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller>/<action>' => '<module>/<controller>/<action>',
            ],
        ],

        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'info'],
                    'categories' => ['debug.files'],
                    'logFile' => '@runtime/logs/files.log',
                ],
            ],
        ],

        'glyphs' => [
            'class' => \app\components\GlyphsComponent::class,
        ],
    ],

    'modules' => [
        'admins' => [
            'class' => \app\modules\admins\Module::class,
        ],

        'casino' => [
            'class' => \app\modules\casino\Module::class,
        ],

        'vendors' => [
            'class' => \app\modules\vendors\Module::class,
        ],

        'slots' => [
            'class' => \app\modules\slots\Module::class,
        ],

        'bonuses' => [
            'class' => \app\modules\bonuses\Module::class,
        ],

        'methods' => [
            'class' => \app\modules\methods\Module::class,
        ],

        'countries' => [
            'class' => \app\modules\countries\Module::class,
        ],

        'languages' => [
            'class' => \app\modules\languages\Module::class,
        ],

        'currencies' => [
            'class' => \app\modules\currencies\Module::class,
        ],

        'licenses' => [
            'class' => \app\modules\licenses\Module::class,
        ],

        'seo' => [
            'class' => \app\modules\seo\Module::class,
        ],

        'pages' => [
            'class' => \app\modules\pages\Module::class,
        ],

        'tags' => [
            'class' => \app\modules\tags\Module::class,
        ],

    ],

];

return $config;