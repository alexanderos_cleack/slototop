<?php

namespace app\modules\slots;

class Module extends \yii\base\Module {

	public $controllerNamespace = 'app\modules\slots\controllers';
	public $layout = 'main';

	public function init() {
		parent::init();
	}

}
