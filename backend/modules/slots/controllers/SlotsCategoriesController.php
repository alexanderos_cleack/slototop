<?php

namespace app\modules\slots\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

use common\models\slots\SlotsCategories;

class SlotsCategoriesController extends \app\components\BController {

    public $enableCsrfValidation = false;

    public function init() {
        parent::init();
        $this->breadcrumbs[] = ['label' => Yii::t('app', 'SlotsCategories'), 'url' => ['/slots/slots-categories/index']];
    }

    public function actions() {
        return [
            'index'  => 'app\components\crud\IndexAction',
            'create' => 'app\components\crud\CreateAction',
            'view'   => 'app\components\crud\ViewAction',
            'update' => 'app\components\crud\UpdateAction',
            'delete' => 'app\components\crud\DeleteAction',
        ];
    }

    public function createModel() {
        return new SlotsCategories();
    }

    public function loadModel($id) {
        if (($model = SlotsCategories::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
