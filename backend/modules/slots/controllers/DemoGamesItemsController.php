<?php

namespace app\modules\slots\controllers;

use Yii;
use yii\base\DynamicModel;
use yii\helpers\Url;
use yii\web\UploadedFile;

use common\models\demogames\DemoGames;
use common\models\demogames\DemoGamesSearch;
use common\models\demogames\providers\TomHorn;

class DemoGamesItemsController extends \app\components\BController {

    public function init() {
        parent::init();
        $this->breadcrumbs[] = ['label' => Yii::t('app', 'Demo games'), 'url' => ['/slots/demo-games-items/index']];
    }

    public function actions() {
        return [
             'update' => 'app\components\crud\UpdateAction',
        ];
    }

    public function actionIndex() {
        set_time_limit(0);
        $model = new DynamicModel(['provider', 'file']);
        $model->addRule(['provider', 'file'], 'required')->addRule(['provider'], 'string', ['max' => 1000])->addRule(['file'], 'file');

        if ($model->load(Yii::$app->request->post())) {
            try {
                $provider = $model->provider;
                $file = UploadedFile::getInstance($model, 'file');
                if ($file) {
                    $filepath = Yii::getAlias('@runtime') . DIRECTORY_SEPARATOR . $file->name;
                    if ($file->saveAs($filepath)) {
                        $items = file($filepath);
                        $keys = str_getcsv(array_shift($items), ';');
                        foreach ($items as $values) {
                            $values = str_getcsv($values, ';');
                            if ($attributes = TomHorn::parseItem(array_combine($keys, $values))) {
								$attributes['provider'] = $provider;
                                if (!($demo_game = DemoGames::find()->where(['provider' => $attributes['provider'], 'title' => $attributes['title']])->one())) {
                                    $demo_game = new DemoGames;
                                }
                                $demo_game->attributes = $attributes;
                                $demo_game->save();
                            }
                        }
                    }
                }
            } catch (Exception $e) {
                Yii::$app->session->addError(\app\widgets\Alert::ERROR, $e->getMessage());
            }
        }

        $demo_games = new DemoGamesSearch;
        $demo_games->load(Yii::$app->request->queryParams);

        return $this->render('index', [
            'model' => $model,
            'demo_games' => $demo_games,
        ]);
    }

    public function loadModel($id) {
        if (($model = DemoGames::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
