<?php

namespace app\modules\slots\controllers;

use Yii;
use yii\base\Model;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

use common\models\slots\Slots;
use common\models\slots\SlotsCategories;
use common\models\slots\SlotsSearch;
use common\models\symbols\Symbols;

class SlotsItemsController extends \app\components\BController {

    public $enableCsrfValidation = false;

    public function init() {
        parent::init();
        $this->breadcrumbs[] = ['label' => Yii::t('app', 'Slots'), 'url' => ['/slots/slots-items/index']];
    }

    public function actions() {
        return [
            // 'index' => 'app\components\crud\IndexAction',
            // 'create' => 'app\components\crud\CreateAction',
            'view' => 'app\components\crud\ViewAction',
            // 'update' => 'app\components\crud\UpdateAction',
            'delete' => 'app\components\crud\DeleteAction',
            'enable' => 'app\components\crud\EnableAction',
            'disable' => 'app\components\crud\DisableAction',
            'imageDelete' => 'app\components\crud\ImageDeleteAction',

            'slugRefresh' => 'app\components\actions\SlugRefreshAction',
        ];
    }

    public function actionIndex() {
        $model = new SlotsSearch;
        $model->load(Yii::$app->request->queryParams);

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    public function actionCreate() {
        $modelsSymbol = [new Symbols];

        if (Yii::$app->request->isAjax) {
            $category_id = Yii::$app->request->post('category_id');
            $properties = SlotsCategories::findOne($category_id);
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            unset($properties->id);
            unset($properties->title);
            unset($properties->slug);
            return ArrayHelper::toArray($properties);
        } else {

            $model = $this->createModel();

            if ($model->load(Yii::$app->request->post())) {

                $modelsSymbol = \app\models\Model::createMultiple(Symbols::class, Yii::$app->request->post());
                Model::loadMultiple($modelsSymbol, Yii::$app->request->post());

                $transaction = Yii::$app->db->beginTransaction();
                try {
                    if ($model->save()) {
                        foreach ($modelsSymbol as $i => $modelSymbol) {
                            $modelSymbol->tabular = $i;
                            $modelSymbol->slot_id = $model->id;
                            if (!$modelSymbol->save()) {
                                $model->addErrors($modelSymbol->getErrors());
                            }
                        }
                    }

                    if (!$model->hasErrors()) {
                        $transaction->commit();
                        if (Yii::$app->request->post('to_game')) {
                            return $this->redirect($model->getUrl());
                        } else {
                            Yii::$app->session->addFlash(\app\widgets\Alert::SUCCESS, Yii::t('app/b', 'Record added'));
                            return $this->redirect(Url::to(['update', 'id' => $model->id]));
                        }
                    }

                } catch (\Exception $e) {
                    $transaction->rollBack();
                    Yii::$app->session->addFlash(\app\widgets\Alert::ERROR, $e->getMessage());
                }

            } // Request

            return $this->render('create', [
                'model' => $model,
                'modelsSymbol' => count($modelsSymbol) ? $modelsSymbol : [new Symbols],
            ]);

        }
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id);
        $modelsSymbol = $model->symbols;

        if (Yii::$app->request->isAjax) {
            $category_id = Yii::$app->request->post('category_id');
            $properties = SlotsCategories::findOne($category_id);
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            unset($properties->id);
            unset($properties->title);
            return ArrayHelper::toArray($properties);
        } elseif ($model->load(Yii::$app->request->post())) {
            $oldIDs = ArrayHelper::map($modelsSymbol, 'id', 'id');
            $modelsSymbol = \app\models\Model::createMultiple(Symbols::class, Yii::$app->request->post());
            Model::loadMultiple($modelsSymbol, Yii::$app->request->post());

            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelsSymbol, 'id', 'id')));

            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($model->save()) {
                    if (!empty($deletedIDs)) {
                        // Symbols::deleteAll(['id' => $deletedIDs]);
                        foreach ($deletedIDs as $deleted_id) {
                            $symbol = Symbols::find()->where(['id' => $deleted_id])->one();
                            if ($symbol) $symbol->delete();
                        }
                    }
                    foreach ($modelsSymbol as $i => $modelSymbol) {
                        $modelSymbol->tabular = $i;
                        $modelSymbol->slot_id = $model->id;
                        if (!$modelSymbol->save()) {
                            $model->addErrors($modelsSymbol->getErrors());
                        }
                    }
                }

                if (!$model->hasErrors()) {
                    $transaction->commit();
                    if (Yii::$app->request->post('to_game')) {
                        return $this->redirect($model->getUrl());
                    } else {
                        Yii::$app->session->addFlash(\app\widgets\Alert::SUCCESS, Yii::t('app/b', 'Record updated'));
                        return $this->redirect(Url::to(['update', 'id' => $model->id]));
                    }
                }

            } catch (\Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->addFlash(\app\widgets\Alert::ERROR, $e->getMessage());
            }
        } // Request

        return $this->render('update', [
            'model' => $model,
            'modelsSymbol' => count($modelsSymbol) ? $modelsSymbol : [new Symbols],
        ]);

    }

    public function createModel() {
        return new Slots;
    }

    /**
     * @param $id
     * @return Slots
     * @throws NotFoundHttpException
     */
    public function loadModel($id) {
        if (($model = Slots::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
        }
    }

}
