<?php 

use yii\bootstrap\Nav;

$this->beginContent('@app/views/layouts/default.php');

echo Nav::widget([
        'options' => ['class' =>'nav nav-pills my-nav'],
        'items' => [
            ['label' => Yii::t('app', 'SlotsList'), 'url' => ['/slots/slots-items/index']],
            ['label' => Yii::t('app', 'SlotsCreate'), 'url' => ['/slots/slots-items/create']],
            ['label' => Yii::t('app', 'SlotsCategoriesList'), 'url' => ['/slots/slots-categories/index']],
            ['label' => Yii::t('app', 'SlotsCategoriesCreate'), 'url' => ['/slots/slots-categories/create']],
            ['label' => Yii::t('app', 'DemoGames'), 'url' => ['/slots/demo-games-items/index']],
        ],
    ]);
echo $content;
$this->endContent();

?>