
<?php

use yii\widgets\DetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\vendors\Vendors */

?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'title',
        'wild_symbol',
        'skatter',
        'bonus_game',
        'free_spins',
        'doubling_game',
        'auto_play',
    ],
]) ?>