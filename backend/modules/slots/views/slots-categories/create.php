<?php

/* @var $this yii\web\View */
/* @var $model common\models\slots\SlotsCategories */

$this->title = Yii::t('app', 'SlotsCategoriesCreate');
$this->context->breadcrumbs[] = $this->title;

?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>
