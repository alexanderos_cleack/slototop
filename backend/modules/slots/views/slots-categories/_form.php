<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

use common\models\slots\SlotsCategories;

/* @var $this yii\web\View */ 
/* @var $model common\models\slots\SlotsCategories */
/* @var $form ActiveForm */

?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

  <div class="well well-sm">

    <?= $form->field($model, 'title') ?>
    <?= $form->field($model, 'logo')->fileInput() ?>
    <?= \app\widgets\ImagesAlt::widget(['form' => $form, 'parentModel' => $model, 'imageModel' => $model->logo]) ?>

    <?= $form->field($model, 'logo_mini')->fileInput() ?>
    <?= \app\widgets\ImagesAlt::widget(['form' => $form, 'parentModel' => $model, 'imageModel' => $model->logo_mini]) ?>

    <?= $form->field($model, 'filter')->dropDownList(SlotsCategories::getFiltersForList(), ['prompt' => Yii::t('app', 'Select')]) ?>
    <?= $form->field($model, 'auto_play')->checkbox() ?>
    <?= $form->field($model, 'bonus_game')->checkbox() ?>
    <?= $form->field($model, 'free_spins')->checkbox() ?>
    <?= $form->field($model, 'version')->checkbox() ?>
    <?= $form->field($model, 'video_viewer_url')->checkbox() ?>
    <?= $form->field($model, 'jackpot')->checkbox() ?>
    <?= $form->field($model, 'joker')->checkbox() ?>
    <?= $form->field($model, 'wild_symbol')->checkbox() ?>
    <?= $form->field($model, 'zero')->checkbox() ?>
    <?= $form->field($model, 'doubling_games')->checkbox() ?>
    <?= $form->field($model, 'amount_reels')->checkbox() ?>
    <?= $form->field($model, 'amount_wheels')->checkbox() ?>
    <?= $form->field($model, 'amount_combinations')->checkbox() ?>
    <?= $form->field($model, 'amount_lines')->checkbox() ?>
    <?= $form->field($model, 'amount_hands')->checkbox() ?>
    <?= $form->field($model, 'max_boxes')->checkbox() ?>
    <?= $form->field($model, 'win_max')->checkbox() ?>
    <?= $form->field($model, 'bet_max')->checkbox() ?>
    <?= $form->field($model, 'bet_max_hand')->checkbox() ?>
    <?= $form->field($model, 'bet_max_number')->checkbox() ?>
    <?= $form->field($model, 'min_boxes')->checkbox() ?>
    <?= $form->field($model, 'min_combination')->checkbox() ?>
    <?= $form->field($model, 'bet_min')->checkbox() ?>
    <?= $form->field($model, 'bet_min_hand')->checkbox() ?>
    <?= $form->field($model, 'bet_min_number')->checkbox() ?>
    <?= $form->field($model, 'retake')->checkbox() ?>
    <?= $form->field($model, 'risk_game')->checkbox() ?>
    <?= $form->field($model, 'surrender')->checkbox() ?>
    <?= $form->field($model, 'skatter')->checkbox() ?>
    <?= $form->field($model, 'special_combination')->checkbox() ?>
    <?= $form->field($model, 'splite')->checkbox() ?>
    <?= $form->field($model, 'bet_more_less')->checkbox() ?>
    <?= $form->field($model, 'bet_dozen')->checkbox() ?>
    <?= $form->field($model, 'bet_line')->checkbox() ?>
    <?= $form->field($model, 'bet_color')->checkbox() ?>
    <?= $form->field($model, 'bet_even_odd')->checkbox() ?>
    <?= $form->field($model, 'insurance')->checkbox() ?>
    <?= $form->field($model, 'kind_of_cards')->checkbox() ?>
    <?= $form->field($model, 'kind_of_lottery')->checkbox() ?>
    <?= $form->field($model, 'kind_of_poker')->checkbox() ?>
    <?= $form->field($model, 'kind_of_roulette')->checkbox() ?>
    <?= $form->field($model, 'doubling_game')->checkbox() ?>
    <?= $form->field($model, 'triplication_game')->checkbox() ?>
    <?= $form->field($model, 'numeric_series')->checkbox() ?>
  </div>

  <div class="well well-sm">
    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
  </div>

<?php ActiveForm::end(); ?>
