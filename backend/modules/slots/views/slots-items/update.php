<?php

/* @var $this yii\web\View */
/* @var $model common\models\slots\SlotsCategories */

$this->title = Yii::t('app', 'Update');

?>

<?= $this->render('_form', [
    'model' => $model,
    'modelsSymbol' => $modelsSymbol,
]) ?>
