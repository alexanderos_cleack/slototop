<?php

use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\widgets\Pjax;

use common\models\vendors\Vendors;
use common\models\slots\SlotsCategories;

/* @var $this yii\web\View */
/* @var $model common\models\slots\Slots */

$this->title = Yii::t('app', 'SlotsList');
$this->context->breadcrumbs[] = $this->title;

?>

<?php

echo Html::beginForm();

    echo GridView::widget([
        'dataProvider' => $model->search(),
        'filterModel' => $model,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',

            [
               'attribute' => 'vendor',
               'filter' => Vendors::getForList(),
               'value' => function ($model, $key, $index, $column) {
                    return ($_vendor = $model->vendor) ? $_vendor->title : null;
               },
            ],

            [
               'attribute' => 'category_id',
               'filter' => SlotsCategories::getForList(),
               'value' => function ($model, $key, $index, $column) {
                    return $model->category->title;
               },
            ],

            [
                'attribute' => 'main_page',
                'format' => 'boolean',
                'filter' => false,
            ],

            [
                'attribute' => 'rating_users',
                'filter' => false,
                'value' => function ($model, $key, $index, $column) {
                    return $model->rating_users . ' %';
                },
            ],

            [
                'attribute' => 'rating_admins',
                'filter' => false,
                'value' => function ($model, $key, $index, $column) {
                    return $model->rating_admins . ' %';
                },
            ],

            [
                'attribute' => 'rating_all',
                'filter' => false,
                'value' => function ($model, $key, $index, $column) {
                    return $model->rating_all / 2 . ' %';
                },
            ],

            [
                'attribute' => 'updated_time',
                'filter' => false,
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{play} {view} {update} {enable} {disable} {delete}',
                'buttons' => [
                    'play' => function ($url, $model, $key) {
                        return Html::a(Html::icon('picture'), \backend\helpers\UrlHelpers::toFrontend($model->url), ['target' => '_blank', 'title' => Yii::t('app', 'Go to game')]);
                    },
                    'enable' => function ($url, $model, $key) {
                        return $model->enabled ? Html::a(Html::icon('off'), Url::to(['disable', 'id' => $model->id]), ['title' => Yii::t('app', 'Disable'), 'class' => 'btn btn-xs btn-primary']) : '';
                    },
                    'disable' => function ($url, $model, $key) {
                        return !$model->enabled ? Html::a(Html::icon('off'), Url::to(['enable', 'id' => $model->id]), ['title' => Yii::t('app', 'Enable'), 'class' => 'btn btn-xs btn-warning']) : '';
                    },
                ],
            ],
        ],
    ]);

Html::endForm();

?>
