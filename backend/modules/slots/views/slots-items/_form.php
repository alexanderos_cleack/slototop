<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;

use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;

use app\widgets\MetaTags;
use common\models\vendors\Vendors;
use common\models\vendors\VendorsPlatforms;
use common\models\slots\SlotsCategories;
use common\models\demogames\DemoGames;

/**
 * @var \yii\web\View $this
 * @var \common\models\slots\Slots $model
 * @var ActiveForm $form
 * @var \common\models\symbols\Symbols[] $modelsSymbol
 */
?>

<?php if ($model->hasErrors()) Yii::$app->session->addFlash(\app\widgets\Alert::ERROR, Html::errorSummary($model)); ?>

<?php $form = ActiveForm::begin(['id' => 'form-slots-items', 'options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="well well-sm">

        <?= $form->field($model, 'title') ?>
        <?= $form->field($model, 'another_titles') ?>

        <?= \app\widgets\SlugRefresh::widget(['model' => $model]) ?>

        <?= $form->field($model, 'logo')->fileInput() ?>
        <?= \app\widgets\ImagesAlt::widget(['form' => $form, 'parentModel' => $model, 'imageModel' => $model->logo]) ?>

        <?= $form->field($model, 'category_id')->dropdownList(SlotsCategories::getForList(), ['prompt' => Yii::t('app', 'Select')]) ?>

        <?= $form->field($model, 'enabled')->checkbox() ?>
        <?= $form->field($model, 'main_page')->checkbox() ?>

        <?php if($model->category): ?>
            <?= $form->field($model, 'auto_play', ['options' => ['style' => $model->category->auto_play ? 'display: block;' : 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'bonus_game', ['options' => ['style' => $model->category->bonus_game ? 'display: block;' : 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'free_spins', ['options' => ['style' => $model->category->free_spins ? 'display: block;' : 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'wild_symbol', ['options' => ['style' => $model->category->wild_symbol ? 'display: block;' : 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'doubling_games', ['options' => ['style' => $model->category->doubling_games ? 'display: block;' : 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'risk_game', ['options' => ['style' => $model->category->risk_game ? 'display: block;' : 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'surrender', ['options' => ['style' => $model->category->surrender ? 'display: block;' : 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'skatter', ['options' => ['style' => $model->category->skatter ? 'display: block;' : 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'splite', ['options' => ['style' => $model->category->splite ? 'display: block;' : 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'bet_more_less', ['options' => ['style' => $model->category->bet_more_less ? 'display: block;' : 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'bet_dozen', ['options' => ['style' => $model->category->bet_dozen ? 'display: block;' : 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'bet_line', ['options' => ['style' => $model->category->bet_line ? 'display: block;' : 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'bet_color', ['options' => ['style' => $model->category->bet_color ? 'display: block;' : 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'bet_even_odd', ['options' => ['style' => $model->category->bet_even_odd ? 'display: block;' : 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'insurance', ['options' => ['style' => $model->category->insurance ? 'display: block;' : 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'doubling_game', ['options' => ['style' => $model->category->doubling_game ? 'display: block;' : 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'triplication_game', ['options' => ['style' => $model->category->triplication_game ? 'display: block;' : 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'jackpot', ['options' => ['style' => $model->category->jackpot ? 'display: block;' : 'display: none;']])->dropdownList([1 => Yii::t('app', 'Yes'), 2 => Yii::t('app', 'No'), 3 => Yii::t('app', 'Progressive')], ['prompt' => Yii::t('app', 'Select') ]) ?>
            <?= $form->field($model, 'version', ['options' => ['style' => $model->category->version ? 'display: block;' : 'display: none;']]) ?>
            <?= $form->field($model, 'video_viewer_url', ['options' => ['style' => $model->category->video_viewer_url ? 'display: block;' : 'display: none;']]) ?>
            <?= $form->field($model, 'joker', ['options' => ['style' => $model->category->joker ? 'display: block;' : 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'zero', ['options' => ['style' => $model->category->zero ? 'display: block;' : 'display: none;']])->dropdownList([1 => Yii::t('app', 'Yes'), 2 => Yii::t('app', 'No'), 3 => Yii::t('app', 'Doubling game')], ['prompt' => Yii::t('app', 'Select') ]) ?>
            <?= $form->field($model, 'amount_reels', ['options' => ['style' => $model->category->amount_reels ? 'display: block;' : 'display: none;']]) ?>
            <?= $form->field($model, 'amount_wheels', ['options' => ['style' => $model->category->amount_wheels ? 'display: block;' : 'display: none;']]) ?>
            <?= $form->field($model, 'amount_combinations', ['options' => ['style' => $model->category->amount_lines ? 'display: block;' : 'display: none;']]) ?>
            <?= $form->field($model, 'amount_lines', ['options' => ['style' => $model->category->amount_lines ? 'display: block;' : 'display: none;']]) ?>
            <?= $form->field($model, 'amount_hands', ['options' => ['style' => $model->category->amount_hands ? 'display: block;' : 'display: none;']]) ?>
            <?= $form->field($model, 'max_boxes', ['options' => ['style' => $model->category->max_boxes ? 'display: block;' : 'display: none;']]) ?>
            <?= $form->field($model, 'win_max', ['options' => ['style' => $model->category->win_max ? 'display: block;' : 'display: none;']]) ?>
            <?= $form->field($model, 'bet_max_hand', ['options' => ['style' => $model->category->bet_max_hand ? 'display: block;' : 'display: none;']]) ?>
            <?= $form->field($model, 'bet_max', ['options' => ['style' => $model->category->bet_max ? 'display: block;' : 'display: none;']]) ?>
            <?= $form->field($model, 'bet_max_number', ['options' => ['style' => $model->category->bet_max_number ? 'display: block;' : 'display: none;']]) ?>
            <?= $form->field($model, 'min_boxes', ['options' => ['style' => $model->category->min_boxes ? 'display: block;' : 'display: none;']]) ?>
            <?= $form->field($model, 'min_combination', ['options' => ['style' => $model->category->min_combination ? 'display: block;' : 'display: none;']]) ?>
            <?= $form->field($model, 'bet_min_hand', ['options' => ['style' => $model->category->bet_min_hand ? 'display: block;' : 'display: none;']]) ?>
            <?= $form->field($model, 'bet_min', ['options' => ['style' => $model->category->bet_min ? 'display: block;' : 'display: none;']]) ?>
            <?= $form->field($model, 'bet_min_number', ['options' => ['style' => $model->category->bet_min_number ? 'display: block;' : 'display: none;']]) ?>
            <?= $form->field($model, 'special_combination', ['options' => ['style' => $model->category->special_combination ? 'display: block;' : 'display: none;']]) ?>
            <?= $form->field($model, 'kind_of_cards', ['options' => ['style' => $model->category->kind_of_cards ? 'display: block;' : 'display: none;']]) ?>
            <?= $form->field($model, 'kind_of_lottery', ['options' => ['style' => $model->category->kind_of_lottery ? 'display: block;' : 'display: none;']]) ?>
            <?= $form->field($model, 'kind_of_poker', ['options' => ['style' => $model->category->kind_of_poker ? 'display: block;' : 'display: none;']]) ?>
            <?= $form->field($model, 'kind_of_roulette', ['options' => ['style' => $model->category->kind_of_roulette ? 'display: block;' : 'display: none;']]) ?>
            <?= $form->field($model, 'retake', ['options' => ['style' => $model->category->retake ? 'display: block;' : 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'numeric_series', ['options' => ['style' => $model->category->numeric_series ? 'display: block;' : 'display: none;']]) ?>
        <?php else: ?>
            <?= $form->field($model, 'auto_play', ['options' => ['style' => 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'bonus_game', ['options' => ['style' => 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'free_spins', ['options' => ['style' => 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'wild_symbol', ['options' => ['style' => 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'doubling_games', ['options' => ['style' => 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'retake', ['options' => ['style' => 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'risk_game', ['options' => ['style' => 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'surrender', ['options' => ['style' => 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'skatter', ['options' => ['style' => 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'splite', ['options' => ['style' => 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'bet_more_less', ['options' => ['style' => 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'bet_dozen', ['options' => ['style' => 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'bet_line', ['options' => ['style' => 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'bet_color', ['options' => ['style' => 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'bet_even_odd', ['options' => ['style' => 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'insurance', ['options' => ['style' => 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'doubling_game', ['options' => ['style' => 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'triplication_game', ['options' => ['style' => 'display: none;']])->checkBox() ?>
            <?= $form->field($model, 'jackpot', ['options' => ['style' => 'display: none;']])->dropdownList([1 => Yii::t('app', 'Yes'), 2 => Yii::t('app', 'No'), 3 => Yii::t('app', 'Progressive')], ['prompt' => Yii::t('app', 'Select') ]) ?>
            <?= $form->field($model, 'version', ['options' => ['style' => 'display: none;']]) ?>
            <?= $form->field($model, 'video_viewer_url', ['options' => ['style' => 'display: none;']]) ?>
            <?= $form->field($model, 'joker', ['options' => ['style' => 'display: none;']])->dropdownList([1 => Yii::t('app', 'Yes'), 2 => Yii::t('app', 'No')], ['prompt' => Yii::t('app', 'Select') ]) ?>
            <?= $form->field($model, 'zero', ['options' => ['style' => 'display: none;']])->dropdownList([1 => Yii::t('app', 'Yes'), 2 => Yii::t('app', 'No'), 3 => Yii::t('app', 'Doubling game')], ['prompt' => Yii::t('app', 'Select') ]) ?>
            <?= $form->field($model, 'amount_reels', ['options' => ['style' => 'display: none;']]) ?>
            <?= $form->field($model, 'amount_wheels', ['options' => ['style' => 'display: none;']]) ?>
            <?= $form->field($model, 'amount_combinations', ['options' => ['style' => 'display: none;']]) ?>
            <?= $form->field($model, 'amount_lines', ['options' => ['style' => 'display: none;']]) ?>
            <?= $form->field($model, 'amount_hands', ['options' => ['style' => 'display: none;']]) ?>
            <?= $form->field($model, 'max_boxes', ['options' => ['style' => 'display: none;']]) ?>
            <?= $form->field($model, 'win_max', ['options' => ['style' => 'display: none;']]) ?>
            <?= $form->field($model, 'bet_max_hand', ['options' => ['style' => 'display: none;']]) ?>
            <?= $form->field($model, 'bet_max', ['options' => ['style' => 'display: none;']]) ?>
            <?= $form->field($model, 'bet_max_number', ['options' => ['style' => 'display: none;']]) ?>
            <?= $form->field($model, 'min_boxes', ['options' => ['style' => 'display: none;']]) ?>
            <?= $form->field($model, 'min_combination', ['options' => ['style' => 'display: none;']]) ?>
            <?= $form->field($model, 'bet_min_hand', ['options' => ['style' => 'display: none;']]) ?>
            <?= $form->field($model, 'bet_min', ['options' => ['style' => 'display: none;']]) ?>
            <?= $form->field($model, 'bet_min_number', ['options' => ['style' => 'display: none;']]) ?>
            <?= $form->field($model, 'special_combination', ['options' => ['style' => 'display: none;']]) ?>
            <?= $form->field($model, 'kind_of_cards', ['options' => ['style' => 'display: none;']]) ?>
            <?= $form->field($model, 'kind_of_lottery', ['options' => ['style' => 'display: none;']]) ?>
            <?= $form->field($model, 'kind_of_poker', ['options' => ['style' => 'display: none;']]) ?>
            <?= $form->field($model, 'kind_of_roulette', ['options' => ['style' => 'display: none;']]) ?>
            <?= $form->field($model, 'numeric_series', ['options' => ['style' => 'display: none;']]) ?>
        <?php endif; ?>

        <?= $form->field($model, 'demo_game_url') ?>
        <?= $form->field($model, 'video_interesting_url') ?>
        <?= $form->field($model, 'payout') ?>
        <?= $form->field($model, 'demo_game_id')->dropdownList(DemoGames::getForList(), ['prompt' => Yii::t('app', 'Select')]) ?>
        <?= $form->field($model, 'vendors')->dropdownList(Vendors::getForList(), ['prompt' => Yii::t('app', 'Select')]) ?>
        <?= $form->field($model, 'rating_users') ?>
        <?= $form->field($model, 'rating_admins') ?>
        <?= $form->field($model, 'new')->checkBox() ?>
        <?= $form->field($model, 'new_to_date')->widget(DatePicker::className(),['dateFormat' => 'yyyy-MM-dd']) ?>

        <?= $form->field($model, 'bigscreen')->fileInput() ?>
        <?php if ($model->bigscreen): ?>
            <div class="screens">
                <?= \app\widgets\ImagesAlt::widget(['form' => $form, 'parentModel' => $model, 'imageModel' => $model->bigscreen, 'delete' => true]) ?>
            </div>
        <?php endif; ?>

        <?= $form->field($model, 'screens[]')->fileInput(['multiple' => true]) ?>
        <?php if ($model->screens): ?>
            <div class="screens">
            <?php foreach ($model->screens as $screen): ?>
                <?= \app\widgets\ImagesAlt::widget(['form' => $form, 'parentModel' => $model, 'imageModel' => $screen, 'delete' => true]) ?>
            <?php endforeach; ?>
            </div>
        <?php endif; ?>

        <?= $form->field($model, 'description')->widget(TinyMce::className(), [
            'language' => 'ru',
            // 'settings' => [
            //     'plugins' => [
            //             "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
            //             "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            //             "table contextmenu directionality emoticons template textcolor paste fullpage textcolor colorpicker textpattern"
            //     ],

            //     'toolbar1' => "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            //     'toolbar2' => "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | insertdatetime preview | forecolor backcolor",
            //     'toolbar3' => "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | spellchecker | visualchars visualblocks nonbreaking template pagebreak restoredraft",

            //     'menubar' => false,
            //     'toolbar_items_size' => 'small',

            //     'spellchecker_language' => 'ru',
            //     'spellchecker_languages' => "Russian=ru,Ukrainian=uk,English=en",
            //     'spellchecker_word_separator_chars' => '\\s!"#$%&()*+,./:;<=>?@[\]^_{|}\xa7\xa9\xab\xae\xb1\xb6\xb7\xb8\xbb\xbc\xbd\xbe\u00bf\xd7\xf7\xa4\u201d\u201c',
            // ],
            // 'spellcheckerUrl' => 'http://speller.yandex.net/services/tinyspell',
            'fileManager' => [
                'class' => TinyMceElFinder::className(),
                'connectorRoute' => '/uploads',
            ],
        ]) ?>

    </div>

    <?= \app\widgets\Tags::widget([
        'model' => $model,
        'form' => $form,
    ]) ?>

    <?= MetaTags::widget([
        'model' => $model,
        'form' => $form,
    ]) ?>

    <div class="dynamicform_wrapper">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?= Yii::t('app', 'Symbols') ?>
                <button type="button" class="pull-right add-item btn btn-success btn-xs"><i class="fa fa-plus"></i> <?= Yii::t('app', 'Add symbol') ?></button>
            </div>
            <div class="panel-body">
                <?php \app\components\DynamicFormWidget::begin([
                    'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                    'widgetBody' => '.container-items', // required: css class selector
                    'widgetItem' => '.item', // required: css class
                    'limit' => 999, // the maximum times, an element can be cloned (default 999)
                    'min' => 0, // 0 or 1 (default 1)
                    'insertButton' => '.add-item', // css class
                    'deleteButton' => '.remove-item', // css class
                    'model' => $modelsSymbol[0],
                    'formId' => 'form-slots-items',
                    'insertPosition' => 'top',
                    'formFields' => [
                        'multi',
                        'description',
                        'logo',
                    ],
                ]); ?>
                    <div class="container-items">
                        <?php foreach($modelsSymbol as $i => $modelSymbol): ?>
                             <div class="item panel panel-default">
                                <div class="panel-heading">
                                    <?= Yii::t('app', 'Symbol') ?>
                                    <button type="button" class="pull-right remove-item btn btn-danger btn-xs"><i class="fa fa-minus"></i> <?= Yii::t('app', 'Delete symbol') ?></button>
                                </div>
                                <div class="panel-body">
                                    <?php if (!$modelSymbol->isNewRecord): ?>
                                        <?= Html::activeHiddenInput($modelSymbol, "[{$i}]id") ?>
                                    <?php endif; ?>
                                    <?= $form->field($modelSymbol, "[{$i}]multi") ?>
                                    <?= $form->field($modelSymbol, "[{$i}]description") ?>
                                    <?= $form->field($modelSymbol, "[{$i}]logo")->fileInput() ?>
                                    <?php
                                        if ($modelSymbol->logo) {
                                            echo $modelSymbol->logo->filename . '<br>';
                                            echo \app\widgets\ImagesAlt::widget(['form' => $form, 'parentModel' => $modelSymbol, 'imageModel' => $modelSymbol->logo, 'tabular' => $i]);
                                        }
                                    ?>
                                </div>
                             </div>
                        <?php endforeach; ?>
                    </div>
                <?php \app\components\DynamicFormWidget::end(); ?>
            </div>
        </div>
    </div>

    <?= \app\widgets\Link::widget([
        'model' => $model,
    ]) ?>

    <div class="well well-sm">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app/b', 'Create') : Yii::t('app/b', 'Update'), ['class' => 'btn btn-primary']) ?>
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app/b', 'Create to game') : Yii::t('app/b', 'Update to game'), ['class' => 'btn btn-primary', 'name' => 'to_game', 'value' => 'to_game', 'formtarget' => '_blank']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
    </div>

<?php ActiveForm::end(); ?>

<script>
    $(document).ready(function () {
        $('#slots-category_id').change(function () {
            $.ajax({
                url: "<?= Url::to(['create']) ?>",
                method: "post",
                data: {category_id: $('#form-slots-items').find('#slots-category_id').val()},
            }).done(function (data) {
                for (var prop in data) {
                    var selector_css = '.field-slots-' + prop;
                    var selector_id_text = '#slots-' + prop + '[type=text]';
                    var selector_id_checkbox = '#slots-' + prop + '[type=checkbox]';
                    console.log(selector_css);
                    console.log(selector_id_text);
                    console.log(selector_id_checkbox);
                    if (data[prop]) {
                        $(selector_css).css('display', 'block');
                        // if ($(selector_id_checkbox).length) $(selector_id_checkbox).prop('checked', true);
                    } else {
                        $(selector_css).css('display', 'none'); 
                        if ($(selector_id_text).length) $(selector_id_text).val('');
                        if ($(selector_id_checkbox).length) $(selector_id_checkbox).prop('checked', false);
                    }
                }
            });
        });

        $('div.screens').click(function (e) {
            e.preventDefault();
            $.ajax({
                url: $(e.target).parent().attr('href'),
                method: "get",
            }).done(function (data) {
                if (data.action == 'success') {
                    $(e.target).hide();
                }
            });
        });

    });
</script>