<?php

/* @var $this yii\web\View */
/* @var $model common\models\demogames\DemoGames */

$this->title = Yii::t('app', 'Update');

?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>
