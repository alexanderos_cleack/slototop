<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\grid\GridView;

use common\models\demogames\Provider;

$this->title = Yii::t('app', 'DemoGames');

?>

<?php if ($model->hasErrors()) Yii::$app->session->addFlash(\app\widgets\Alert::ERROR, Html::errorSummary($model)); ?>

<h2><?= Yii::t('app', 'Load of demo games') ?></h2>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="well well-sm">
        <?= $form->field($model, 'provider')->label(Yii::t('app', 'Vendor'))->dropdownList(Provider::getProviders(), ['prompt' => Yii::t('app', 'Select')]) ?>
        <?= $form->field($model, 'file')->label(Yii::t('app', 'File'))->fileInput() ?>
    </div>
    <div class="well well-sm">
        <?= Html::submitButton(Yii::t('app', 'Load'), ['class' => 'btn btn-primary']) ?>
    </div>
<?php ActiveForm::end(); ?>

<h2><?= Yii::t('app', 'List of demo games') ?></h2>

<?= GridView::widget([
    'dataProvider' => $demo_games->search(),
    'filterModel' => $demo_games,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'title',
        
        [
            'attribute' => 'provider',
            'filter' => Provider::getProvidersForFilter(),
        ],

        [
            'attribute' => 'options',
            'value' => function () { return '...'; },
            'contentOptions' => [
                'style' => 'width: 100px;',
            ],
        ],

        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view} {update}',
            'buttons' => [
                'view' => function ($url, $model, $key) {
                    return Html::a(Html::icon('play'), $model->url, ['target' => '_blank', 'title' => Yii::t('app', 'Go to game')]);
                },
            ],
        ],
    ],
]); ?>