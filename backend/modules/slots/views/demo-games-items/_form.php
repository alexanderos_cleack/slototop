<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\demogames\DemoGames */
/* @var $form ActiveForm */

?>

<?php if ($model->hasErrors()) Yii::$app->session->addFlash(\app\widgets\Alert::ERROR, Html::errorSummary($model)); ?>

<?php $form = ActiveForm::begin(['id' => 'form-demogames-items', 'options' => ['enctype' => 'multipart/form-data']]); ?>

<div class="well well-sm">

    <?= $form->field($model, 'title') ?>
    <?= $form->field($model, 'provider') ?>
    <?= $form->field($model, 'options')->textarea(['rows' => 10]) ?>
    <?= $form->field($model, 'url')->textarea(['rows' => 10]) ?>
    <?= $form->field($model, 'object')->textarea(['rows' => 10]) ?>

</div>

<div class="well well-sm">
    <?= Html::submitButton($model->isNewRecord ? Yii::t('app/b', 'Create') : Yii::t('app/b', 'Update'), ['class' => 'btn btn-primary']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
</div>

<?php ActiveForm::end(); ?>