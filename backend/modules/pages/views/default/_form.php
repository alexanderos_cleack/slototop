<?php
/**
 * @var \yii\web\View $this
 * @var \common\models\pages\Pages $model
 */
?>

<?php if ($model->hasErrors()) Yii::$app->session->addFlash(\app\widgets\Alert::ERROR, \yii\bootstrap\Html::errorSummary($model)); ?>

<?php $form = \yii\bootstrap\ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

<div class="well well-sm">

    <?php echo $form->field($model, 'title') ?>

    <?php echo \app\widgets\SlugRefresh::widget(['model' => $model]) ?>

    <?= $form->field($model, 'description')->widget(\zxbodya\yii2\tinymce\TinyMce::className(), [
        'language' => 'ru',
        'fileManager' => [
            'class' => \zxbodya\yii2\elfinder\TinyMceElFinder::className(),
            'connectorRoute' => '/uploads',
        ],
    ]); ?>
</div>

<?php echo \app\widgets\MetaTags::widget([
    'model' => $model,
    'form' => $form,
]); ?>

<div class="well well-sm">
    <?php echo \yii\bootstrap\Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => 'btn btn-primary']); ?>
</div>

<?php \yii\bootstrap\ActiveForm::end(); ?>
