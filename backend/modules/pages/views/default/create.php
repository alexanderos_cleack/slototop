<?php
/**
 * @var \yii\web\View $this
 * @var \common\models\pages\Pages $model
 */

$this->title = Yii::t('app', 'PagesCreate');
$this->context->breadcrumbs[] = $this->title;
?>

<?php echo $this->render('_form', [
    'model' => $model,
]); ?>
