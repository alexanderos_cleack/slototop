<?php

use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/**
 * @var \backend\modules\pages\controllers\DefaultController $this
 * @var \common\models\pages\Pages $model
 */

$this->title = Yii::t('app', 'PagesList');
$this->context->breadcrumbs[] = $this->title;
?>

<?php

echo Html::beginForm();

echo GridView::widget([
    'dataProvider' => $model->search(),
    'filterModel' => $model,
    'columns' => [
        'id',
        'title',

        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{show} {view} {update} {enable} {disable} {delete}',
            'buttons' => [
                'show' => function ($url, $model, $key) {
                    return Html::a(Html::icon('picture'), \backend\helpers\UrlHelpers::toFrontend($model->url), ['target' => '_blank', 'title' => Yii::t('app/b', 'Go to bonus')]);
                },
                'enable' => function ($url, $model, $key) {
                    return $model->enabled ? Html::a(Html::icon('off'), Url::to(['disable', 'id' => $model->id]), ['title' => Yii::t('app', 'Disable'), 'class' => 'btn btn-xs btn-primary']) : '';
                },
                'disable' => function ($url, $model, $key) {
                    return !$model->enabled ? Html::a(Html::icon('off'), Url::to(['enable', 'id' => $model->id]), ['title' => Yii::t('app', 'Enable'), 'class' => 'btn btn-xs btn-warning']) : '';
                },
            ],
        ],
    ],
]);

Html::endForm();
