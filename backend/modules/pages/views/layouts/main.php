<?php

use yii\bootstrap\Nav;

$this->beginContent('@app/views/layouts/default.php');

echo Nav::widget([
    'options' => ['class' => 'nav nav-pills my-nav'],
    'items' => [
        ['label' => Yii::t('app', 'PagesList'), 'url' => ['/pages/default/index']],
        ['label' => Yii::t('app', 'PagesCreate'), 'url' => ['/pages/default/create']],
    ],
]);

echo $content;
$this->endContent();

?>