<?php

namespace app\modules\pages;

/**
 * pages module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\pages\controllers';

    public $layout = 'main';

}
