<?php

namespace backend\modules\pages\controllers;

use common\models\pages\Pages;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `pages` module
 */
class DefaultController extends \app\components\BController
{
    public $enableCsrfValidation = false;

    public function init()
    {
        parent::init();
        $this->breadcrumbs[] = ['label' => Yii::t('app', 'Casino'), 'url' => ['/casino/casino-items/index']];
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'index' => 'app\components\crud\IndexAction',
            'create' => 'app\components\crud\CreateAction',
            'view' => 'app\components\crud\ViewAction',
            'update' => 'app\components\crud\UpdateAction',
            'enable' => 'app\components\crud\EnableAction',
            'disable' => 'app\components\crud\DisableAction',
            'delete' => 'app\components\crud\DeleteAction',

            'slugRefresh' => 'app\components\actions\SlugRefreshAction',
        ];
    }

    /**
     * @return Pages
     */
    public function createModel()
    {
        return new Pages();
    }

    /**
     * @param $id
     * @return Pages
     * @throws NotFoundHttpException
     */
    public function loadModel($id)
    {
        if (($model = Pages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
