<?php 

use yii\bootstrap\Nav;

$this->beginContent('@app/views/layouts/default.php');

echo Nav::widget([
        'options' => ['class' =>'nav nav-pills my-nav'],
        'items' => [
            ['label' => Yii::t('app/b', 'Payment system list'), 'url' => ['/methods/methods-payment-items/index']],
            ['label' => Yii::t('app/b', 'Payment system add'), 'url' => ['/methods/methods-payment-items/create']],
        ],
    ]);
echo $content;
$this->endContent();

?>