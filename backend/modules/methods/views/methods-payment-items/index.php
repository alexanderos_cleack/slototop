<?php

use yii\grid\GridView;
use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $model common\models\methods\MethodsPayment */

$this->title = Yii::t('app/b', 'Payment system list');
$this->context->breadcrumbs[] = $this->title;

?>

<?php

echo Html::beginForm();

    echo GridView::widget([
        'dataProvider' => $model->search(),
        'filterModel' => $model,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{show} {view} {update} {delete}',
                'buttons' => [
                    'show' => function ($url, $model, $key) {
                        return Html::a(Html::icon('picture'), \backend\helpers\UrlHelpers::toFrontend($model->url), ['target' => '_blank', 'title' => Yii::t('app', 'Go to method')]);
                    }
                ],
            ],
        ],
    ]);

Html::endForm();

?>
