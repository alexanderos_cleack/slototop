<?php

use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;

use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;

use app\widgets\MetaTags;
use common\models\methods\MethodsCategories;
use common\models\currencies\Currencies;

/* @var $this yii\web\View */
/* @var $model common\models\methods\MethodsPayment */
/* @var $form ActiveForm */

?>

<?php if ($model->hasErrors()) Yii::$app->session->addFlash(\app\widgets\Alert::ERROR, Html::errorSummary($model)); ?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="well well-sm">
        <?= $form->field($model, 'title') ?>

        <?= \app\widgets\SlugRefresh::widget(['model' => $model]) ?>

        <?= $form->field($model, 'logo')->fileInput() ?>
        <?= \app\widgets\ImagesAlt::widget(['form' => $form, 'parentModel' => $model, 'imageModel' => $model->logo]) ?>

        <?= $form->field($model, 'logo_mini')->fileInput() ?>
        <?= \app\widgets\ImagesAlt::widget(['form' => $form, 'parentModel' => $model, 'imageModel' => $model->logo_mini]) ?>

        <?= $form->field($model, 'categories_ids')->dropdownList(MethodsCategories::getForList(), [/*'prompt' => Yii::t('app', 'Select'),*/ 'multiple' => 'multiple']) ?>
        <?= $form->field($model, 'terms_payment') ?>
        <?= $form->field($model, 'url_site') ?>
        <?= $form->field($model, 'commission') ?>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <?= Yii::t('app/b', 'Currencies') ?>
        </div>
        <div class="panel-body">
            <?php foreach ($methods_payment2currencies as $i => $methods_payment2currency): ?>
                <div class="item panel panel-default">
                    <div class="panel-heading">
                        <?= $methods_payment2currency->currency->title; ?>
                    </div>
                    <div class="panel-body">
                        <?php if (!$methods_payment2currency->isNewRecord): ?>
                            <?= Html::activeHiddenInput($methods_payment2currency, "[{$i}]method_id") ?>
                        <?php endif; ?>
                        <?= Html::activeHiddenInput($methods_payment2currency, "[{$i}]currency_id") ?>
                        <?= $form->field($methods_payment2currency, "[{$i}]output") ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

    <div class="well well-sm">
        <?= $form->field($model, 'enabled')->checkbox() ?>
        <?= $form->field($model, 'description')->widget(TinyMce::className(), [
            'language' => 'ru',
            'fileManager' => [
                'class' => TinyMceElFinder::className(),
                'connectorRoute' => '/uploads',
            ],
        ]) ?>
    </div>

    <div class="well well-sm">
        <?= $form->field($model, 'how_to_use')->widget(TinyMce::className(), [
            'language' => 'ru',
            'fileManager' => [
                'class' => TinyMceElFinder::className(),
                'connectorRoute' => '/uploads',
            ],
        ]) ?>
    </div>

    <?= MetaTags::widget([
        'model' => $model,
        'form' => $form,
    ]) ?>

    <?= \app\widgets\Link::widget([
        'model' => $model,
    ]) ?>

    <div class="well well-sm">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app/b', 'Create to game') : Yii::t('app/b', 'Update to game'), ['class' => 'btn btn-primary', 'name' => 'to_game', 'value' => 'to_game', 'formtarget' => '_blank']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
    </div>

<?php ActiveForm::end(); ?>
