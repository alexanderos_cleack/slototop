<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\methods\MethodsPayments */

?>

<?= DetailView::widget([
	'model' => $model,
	'attributes' => [
		'title',
	],
]) ?>