<?php

/* @var $this yii\web\View */
/* @var $model common\models\methods\MethodsPayments */

$this->title = Yii::t('app/b', 'Payment system add');
$this->context->breadcrumbs[] = $this->title;

?>

<?= $this->render('_form', [
    'model' => $model,
    'methods_payment2currencies' => $methods_payment2currencies,
]) ?>
