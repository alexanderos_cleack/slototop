<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\methods\MethodsOutput */

$this->title = Yii::t('app', 'MethodsOutputList');
$this->context->breadcrumbs[] = $this->title;

?>

<?php

echo Html::beginForm();

    echo GridView::widget([
        'dataProvider' => $model->search(),
        'filterModel' => $model,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);

Html::endForm();

?>
