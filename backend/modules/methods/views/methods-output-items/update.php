<?php

/* @var $this yii\web\View */
/* @var $model common\models\methods\MethodsOutput */

$this->title = Yii::t('app', 'MethodsOutputUpdate');

?>

<?= $this->render('_form', [
	'model' => $model,
]) ?>
