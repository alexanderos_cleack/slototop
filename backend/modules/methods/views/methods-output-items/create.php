<?php

/* @var $this yii\web\View */
/* @var $model common\models\methods\MethodsOutput */

$this->title = Yii::t('app', 'MethodsOutputCreate');
$this->context->breadcrumbs[] = $this->title;

?>

<?= $this->render('_form', [
	'model' => $model,
]) ?>
