<?php

namespace app\modules\methods\controllers;

use Yii;
use yii\base\Model;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

use common\models\currencies\Currencies;
use common\models\methods\MethodsPayment2Currencies;
use common\models\methods\MethodsPayment;

class MethodsPaymentItemsController extends \app\components\BController {

    public $enableCsrfValidation = false;

    public function init() {
        parent::init();
        $this->breadcrumbs[] = ['label' => Yii::t('app/b', 'Payment system'), 'url' => ['/methods/methods-payment-items/index']];
    }

    public function actions() {
        return [
            'index' => 'app\components\crud\IndexAction',
            // 'create' => 'app\components\crud\CreateAction',
            'view' => 'app\components\crud\ViewAction',
            // 'update' => 'app\components\crud\UpdateAction',
            'delete' => 'app\components\crud\DeleteAction',

            'slugRefresh' => 'app\components\actions\SlugRefreshAction',
        ];
    }

    public function actionCreate() {
        $model = $this->createModel();

        $methods_payment2currencies = [];
        foreach (Currencies::find()->orderBy(['title' => SORT_ASC])->all() as $currency) {
            $methods_payment2currency = new MethodsPayment2Currencies;
            $methods_payment2currency->currency_id = $currency->id;
            $methods_payment2currencies[] = $methods_payment2currency;
        }

        if ($model->load(Yii::$app->request->post())) {
            Model::loadMultiple($methods_payment2currencies, Yii::$app->request->post());
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($model->save()) {
                    foreach ($methods_payment2currencies as $methods_payment2currency) {
                        if ($methods_payment2currency->output) {
                            $methods_payment2currency->method_id = $model->id;
                            if ($methods_payment2currency->save()) {
                                $model->addErrors($methods_payment2currency->getErrors());
                            }
                        }
                    }
                }

                if (!$model->hasErrors()) {
                    Yii::$app->session->addFlash(\app\widgets\Alert::SUCCESS, Yii::t('app/b', 'Record added'));
                    $transaction->commit();
                    if (Yii::$app->request->post('to_game')) {
                        return $this->redirect($model->getUrl());
                    } else {
                        return $this->redirect(Url::to(['update', 'id' => $model->id]));
                    }
                }
            } catch (\Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->addFlash(\app\widgets\Alert::ERROR, $e->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $model,
            'methods_payment2currencies' => $methods_payment2currencies,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        $methods_payment2currencies = [];
        foreach (Currencies::find()->orderBy(['title' => SORT_ASC])->all() as $currency) {
            $methods_payment2currency = MethodsPayment2Currencies::find()->where([
                'method_id' => $model->id,
                'currency_id' => $currency->id,
            ])->one() ? : new MethodsPayment2Currencies;
            $methods_payment2currency->currency_id = $currency->id;
            $methods_payment2currencies[] = $methods_payment2currency;
        }

        if ($model->load(Yii::$app->request->post())) {
            Model::loadMultiple($methods_payment2currencies, Yii::$app->request->post());
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($model->save()) {
                    foreach ($methods_payment2currencies as $methods_payment2currency) {
                        if ($methods_payment2currency->output) {
                            $methods_payment2currency->method_id = $model->id;
                            if (!$methods_payment2currency->save()) {
                                $model->addErrors($methods_payment2currency->getErrors());
                            }
                        } elseif (!$methods_payment2currency->isNewRecord) {
                            $methods_payment2currency->delete();
                        }
                    }
                }

                if (!$model->hasErrors()) {
                    Yii::$app->session->addFlash(\app\widgets\Alert::SUCCESS, Yii::t('app/b', 'Record updated'));
                    $transaction->commit();
                    if (Yii::$app->request->post('to_game')) {
                        return $this->redirect($model->getUrl());
                    } else {
                        return $this->redirect(Url::to(['update', 'id' => $model->id]));
                    }
                }
            } catch (\Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->addFlash(\app\widgets\Alert::ERROR, $e->getMessage());
            }
        }

        return $this->render('update', [
            'model' => $model,
            'methods_payment2currencies' => $methods_payment2currencies,
        ]);
    }

    public function createModel() {
        return new MethodsPayment();
    }

    public function loadModel($id) {
        if (($model = MethodsPayment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('yii', 'The requested page does not exist.'));
        }
    }

}
