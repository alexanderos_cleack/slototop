<?php

namespace app\modules\methods\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

use common\models\methods\MethodsOutput;

class MethodsOutputItemsController extends \app\components\BController {

	public function init() {
		parent::init();
		$this->breadcrumbs[] = ['label' => Yii::t('app', 'MethodsOutput'), 'url' => ['/methods/methods-output-items/index']];
	}

	public function actions() {
		return [
			'index' => 'app\components\crud\IndexAction',
			'create' => 'app\components\crud\CreateAction',
			'view' => 'app\components\crud\ViewAction',
			'update' => 'app\components\crud\UpdateAction',
			'delete' => 'app\components\crud\DeleteAction',
		];
	}

	public function createModel() {
		return new MethodsOutput();
	}

	public function loadModel($id) {
		if (($model = MethodsOutput::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

}
