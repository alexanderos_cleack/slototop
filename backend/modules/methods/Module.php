<?php

namespace app\modules\methods;

class Module extends \yii\base\Module {

	public $controllerNamespace = 'app\modules\methods\controllers';
	public $layout = 'main';

	public function init() {
		parent::init();
	}

}
