<?php

use yii\bootstrap\Html;
use yii\grid\GridView;

/**
 * @var \backend\modules\tags\controllers\DefaultController $this
 * @var \common\models\tags\Tags $model
 */

$this->title = Yii::t('app', 'Список');
$this->context->breadcrumbs[] = $this->title;
?>

<?php

echo Html::beginForm();

echo GridView::widget([
    'dataProvider' => $model->search(),
    'filterModel' => $model,
    'columns' => [
        'id',
        [
            'attribute' => 'model_schema',
            'filter' => \common\models\tags\Tags::getPagesForList(),
        ],
        'title',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
        ],
    ],
]);

Html::endForm();
