<?php
/**
 * @var \yii\web\View $this
 * @var \common\models\tags\Tags $model
 */
?>

<?php if ($model->hasErrors()) Yii::$app->session->addFlash(\app\widgets\Alert::ERROR, \yii\bootstrap\Html::errorSummary($model)); ?>

<?php $form = \yii\bootstrap\ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

<div class="well well-sm">

    <?php echo $form->field($model, 'model_schema')->dropDownList(\common\models\tags\Tags::getPagesForList(), [
            'prompt' => Yii::t('app/b', 'Select'),
    ]) ?>
    <?php echo $form->field($model, 'title') ?>

</div>

<div class="well well-sm">
    <?php echo \yii\bootstrap\Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => 'btn btn-primary']); ?>
</div>

<?php \yii\bootstrap\ActiveForm::end(); ?>
