<?php
/**
 * @var \yii\web\View $this
 * @var \common\models\tags\Tags $model
 */

$this->title = Yii::t('app', 'Создать');
$this->context->breadcrumbs[] = $this->title;
?>

<?php echo $this->render('_form', [
    'model' => $model,
]); ?>
