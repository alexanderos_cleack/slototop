<?php

use yii\bootstrap\Nav;

$this->beginContent('@app/views/layouts/default.php');

echo Nav::widget([
    'options' => ['class' => 'nav nav-pills my-nav'],
    'items' => [
        ['label' => Yii::t('app', 'Список'), 'url' => ['/tags/default/index']],
        ['label' => Yii::t('app', 'Создать'), 'url' => ['/tags/default/create']],
    ],
]);

echo $content;
$this->endContent();

?>