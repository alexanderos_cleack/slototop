<?php

namespace app\modules\tags;

/**
 * pages module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\tags\controllers';

    public $layout = 'main';

}
