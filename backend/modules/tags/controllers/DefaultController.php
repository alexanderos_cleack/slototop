<?php

namespace backend\modules\tags\controllers;

use Yii;
use yii\web\NotFoundHttpException;

use common\models\tags\Tags;

/**
 * Default controller for the `tags` module
 */
class DefaultController extends \app\components\BController
{
    public $enableCsrfValidation = false;

    public function init()
    {
        parent::init();
        $this->breadcrumbs[] = ['label' => Yii::t('app', 'Теги'), 'url' => ['/tags/tags-items/index']];
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'index' => 'app\components\crud\IndexAction',
            'create' => 'app\components\crud\CreateAction',
            'view' => 'app\components\crud\ViewAction',
            'update' => 'app\components\crud\UpdateAction',
            'delete' => 'app\components\crud\DeleteAction',
        ];
    }

    /**
     * @return Tags
     */
    public function createModel()
    {
        return new Tags();
    }

    /**
     * @param $id
     * @return Tags
     * @throws NotFoundHttpException
     */
    public function loadModel($id)
    {
        if (($model = Tags::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
