<?php

namespace app\modules\licenses;

class Module extends \yii\base\Module {

    public $controllerNamespace = 'app\modules\licenses\controllers';
    public $layout = 'main';

    public function init() {
        parent::init();
    }

}
