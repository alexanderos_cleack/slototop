<?php 

use yii\bootstrap\Nav;

$this->beginContent('@app/views/layouts/default.php');

echo Nav::widget([
        'options' => ['class' =>'nav nav-pills my-nav'],
        'items' => [
            ['label' => Yii::t('app', 'LicensesList'), 'url' => ['/licenses/licenses-items/index']],
            ['label' => Yii::t('app', 'LicensesCreate'), 'url' => ['/licenses/licenses-items/create']],
        ],
    ]);

echo $content;
$this->endContent();

?>