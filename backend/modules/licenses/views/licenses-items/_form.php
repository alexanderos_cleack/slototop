<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

use common\models\licenses\Licenses;

/* @var $this yii\web\View */ 
/* @var $model common\models\licenses\Licenses */
/* @var $form ActiveForm */

?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="well well-sm">

        <?= $form->field($model, 'title') ?>

    </div>

    <div class="well well-sm">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
    </div>

<?php ActiveForm::end(); ?>
