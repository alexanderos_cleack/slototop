<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\licenses\Licenses */

?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'title',
    ],
]) ?>