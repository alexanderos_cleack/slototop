<?php

/* @var $this yii\web\View */
/* @var $model common\models\licenses\Licenses */

$this->title = Yii::t('app', 'LicensesCreate');
$this->context->breadcrumbs[] = $this->title;

?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>
