<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

use zxbodya\yii2\tinymce\TinyMce;
use zxbodya\yii2\elfinder\TinyMceElFinder;

use app\widgets\MetaTags;
use common\models\bonuses\BonusesTypes;
use common\models\casino\Casino;
use common\models\countries\Countries;
use common\models\currencies\Currencies;
use common\models\vendors\Vendors;


/* @var $this yii\web\View */ 
/* @var $model common\models\bonuses\Bonuses */
/* @var $form ActiveForm */

?>

<?php if ($model->hasErrors()) Yii::$app->session->addFlash(\app\widgets\Alert::ERROR, Html::errorSummary($model)); ?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="well well-sm">

        <?= $form->field($model, 'title') ?>

        <?= \app\widgets\SlugRefresh::widget(['model' => $model]) ?>

        <?= $form->field($model, 'logo')->fileInput() ?>
        <?= \app\widgets\ImagesAlt::widget(['form' => $form, 'parentModel' => $model, 'imageModel' => $model->logo]) ?>

        <?= $form->field($model, 'enabled')->checkbox() ?>
        <?= $form->field($model, 'main_page')->checkbox() ?>

        <?= $form->field($model, 'bonus_type_id')->dropdownList(BonusesTypes::getForList(), ['prompt' => Yii::t('app', 'Select')]) ?>

    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <?= Yii::t('app/b', 'Currencies') ?>
        </div>
        <div class="panel-body">
            <?php foreach ($bonuses2currencies as $i => $bonuses2currency): ?>
                <div class="item panel panel-default">
                    <div class="panel-heading">
                        <?= $bonuses2currency->currency->title; ?>
                    </div>
                    <div class="panel-body">
                        <?php if (!$bonuses2currency->isNewRecord): ?>
                            <?= Html::activeHiddenInput($bonuses2currency, "[{$i}]bonus_id") ?>
                        <?php endif; ?>
                        <?= Html::activeHiddenInput($bonuses2currency, "[{$i}]currency_id") ?>
                        <?= $form->field($bonuses2currency, "[{$i}]max_sum") ?>
                        <?= $form->field($bonuses2currency, "[{$i}]max_payout") ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

    </div>

    <div class="well well-sm">

        <?= $form->field($model, 'countries')->dropdownList(Countries::getForList(), ['multiple' => true, 'size' => 10]) ?>
        <?= $form->field($model, 'vendors')->dropdownList(Vendors::getForList(), ['multiple' => true, 'size' => 10]) ?>
        <?= $form->field($model, 'casino')->dropdownList(Casino::getForList(), ['prompt' => Yii::t('app/b', 'Select')]) ?>
        <?= $form->field($model, 'bonus_code') ?>
        <?= $form->field($model, 'size_of_bonus') ?>
        <?= $form->field($model, 'value_on_picture') ?>
        <?= $form->field($model, 'wager') ?>
        <?= $form->field($model, 'amount_uses')->hint('<small>Если не установлено то количество использований не ограничено</small>') ?>
        <?= $form->field($model, 'rating_users') ?>
        <?= $form->field($model, 'rating_admins') ?>
        <?= $form->field($model, 'url_bonus') ?>
        <?= $form->field($model, 'description')->widget(TinyMce::class, [
            'language' => 'ru',
            'fileManager' => [
                'class' => TinyMceElFinder::class,
                'connectorRoute' => '/uploads',
            ],
        ]) ?>
        <?= $form->field($model, 'conditions')->widget(TinyMce::class, [
            'language' => 'ru',
            'fileManager' => [
                'class' => TinyMceElFinder::class,
                'connectorRoute' => '/uploads',
            ],
        ]) ?>
    </div>

    <?= \app\widgets\Same::widget([
        'model' => $model,
        'form' => $form,
    ]) ?>

    <?= MetaTags::widget([
        'model' => $model,
        'form' => $form,
    ]) ?>

    <?= \app\widgets\Link::widget([
        'model' => $model,
    ]) ?>

    <div class="well well-sm">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app/b', 'Create to game') : Yii::t('app/b', 'Update to game'), ['class' => 'btn btn-primary', 'name' => 'to_game', 'value' => 'to_game', 'formtarget' => '_blank']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
    </div>

<?php ActiveForm::end(); ?>