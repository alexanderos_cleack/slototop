<?php

use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\helpers\Url;

use common\models\casino\Casino;
use common\models\bonuses\BonusesTypes;

/* @var $this yii\web\View */
/* @var $model common\models\bonuses\Bonuses */

$this->title = Yii::t('app', 'BonusesList');
$this->context->breadcrumbs[] = $this->title;

?>

<?php

echo Html::beginForm();

    echo GridView::widget([
        'dataProvider' => $model->search(),
        'filterModel' => $model,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',

            [
               'attribute' => 'bonus_type_id',
               'filter' => BonusesTypes::getForList(),
               'value' => function ($model, $key, $index, $column) {
                    return ($model->bonusType) ? $model->bonusType->title : null;
               },
            ],

            [
                'attribute' => 'casino_search',
                'filter' => Casino::getForList(),
                'value' => function ($model) {
                    return ($model->casino) ? $model->casino->title: null;
                },
            ],

            [
                'attribute' => 'main_page',
                'format' => 'boolean',
                'filter' => false,
            ],

            [
                'attribute' => 'rating_users',
                'filter' => false,
                'value' => function ($model, $key, $index, $column) {
                    return $model->rating_users . ' %';
                },
            ],

            [
                'attribute' => 'rating_admins',
                'filter' => false,
                'value' => function ($model, $key, $index, $column) {
                    return $model->rating_admins . ' %';
                },
            ],

            [
                'attribute' => 'rating_all',
                'filter' => false,
                'value' => function ($model, $key, $index, $column) {
                    return $model->rating_all / 2 . ' %';
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{show} {view} {update} {enable} {disable} {delete}',
                'buttons' => [
                    'show' => function ($url, $model, $key) {
                        return Html::a(Html::icon('picture'), \backend\helpers\UrlHelpers::toFrontend($model->url), ['target' => '_blank', 'title' => Yii::t('app/b', 'Go to bonus')]);
                    },
                    'enable' => function ($url, $model, $key) {
                        return $model->enabled ? Html::a(Html::icon('off'), Url::to(['disable', 'id' => $model->id]), ['title' => Yii::t('app', 'Disable'), 'class' => 'btn btn-xs btn-primary']) : '';
                    },
                    'disable' => function ($url, $model, $key) {
                        return !$model->enabled ? Html::a(Html::icon('off'), Url::to(['enable', 'id' => $model->id]), ['title' => Yii::t('app', 'Enable'), 'class' => 'btn btn-xs btn-warning']) : '';
                    },
                ],
            ],
        ],
    ]);

Html::endForm();

?>
