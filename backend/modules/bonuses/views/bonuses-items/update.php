<?php

/* @var $this yii\web\View */

$this->title = Yii::t('app', 'BonusesUpdate');
$this->context->breadcrumbs[] = $this->title;

?>

<?= $this->render('_form', [
    'model' => $model,
    'bonuses2currencies' => $bonuses2currencies,
]) ?>
