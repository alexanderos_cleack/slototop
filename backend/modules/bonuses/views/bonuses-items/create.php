<?php

/* @var $this yii\web\View */
/* @var $model common\models\bonuses\Bonuses */
/* @var $model common\models\bonuses\Bonuses2Currencies */

$this->title = Yii::t('app', 'BonusesCreate');
$this->context->breadcrumbs[] = $this->title;

?>

<?= $this->render('_form', [
    'model' => $model,
    'bonuses2currencies' => $bonuses2currencies,
]) ?>
