<?php

/* @var $this yii\web\View */
/* @var $model common\models\vendors\Vendors */

$this->title = Yii::t('app', 'BonusesTypesUpdate');
$this->context->breadcrumbs[] = $this->title;

?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>
