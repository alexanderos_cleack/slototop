<?php 

/**
 * @var $this yii\web\View
 */

use yii\bootstrap\Nav;

$this->beginContent('@app/views/layouts/default.php');

echo Nav::widget([
		'options' => ['class' =>'nav nav-pills my-nav'],
		'items' => [
			['label' => Yii::t('app', 'BonusesList'), 'url' => ['/bonuses/bonuses-items/index']],
			['label' => Yii::t('app', 'BonusesCreate'), 'url' => ['/bonuses/bonuses-items/create']],
			['label' => Yii::t('app', 'BonusesTypesList'), 'url' => ['/bonuses/bonuses-types/index']],
			['label' => Yii::t('app', 'BonusesTypesCreate'), 'url' => ['/bonuses/bonuses-types/create']],
		],
	]);
echo $content;

$this->endContent();

?>