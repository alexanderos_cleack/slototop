<?php

namespace app\modules\bonuses\controllers;

use Yii;
use yii\base\Model;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

use common\models\bonuses\Bonuses2Currencies;
use common\models\bonuses\Bonuses;
use common\models\currencies\Currencies;

class BonusesItemsController extends \app\components\BController {

    public $enableCsrfValidation = false;

    public function init() {
        parent::init();
        $this->breadcrumbs[] = ['label' => Yii::t('app', 'Bonuses'), 'url' => ['/bonuses/bonuses-items/index']];
    }

    public function actions() {
        return [
            'index' => 'app\components\crud\IndexAction',
            // 'create' => 'app\components\crud\CreateAction',
            'view' => 'app\components\crud\ViewAction',
            // 'update' => 'app\components\crud\UpdateAction',
            'enable' => 'app\components\crud\EnableAction',
            'disable' => 'app\components\crud\DisableAction',
            'delete' => 'app\components\crud\DeleteAction',

            'slugRefresh' => 'app\components\actions\SlugRefreshAction',
        ];
    }

    public function actionCreate() {
        $model = $this->createModel();

        $bonuses2currencies = [];
        foreach (Currencies::find()->orderBy(['title' => SORT_ASC])->all() as $currency) {
            $bonuses2currency = new Bonuses2Currencies;
            $bonuses2currency->currency_id = $currency->id;
            $bonuses2currencies[] = $bonuses2currency;
        }

        if ($model->load(Yii::$app->request->post())) {
            Model::loadMultiple($bonuses2currencies, Yii::$app->request->post());
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($model->save()) {
                    foreach ($bonuses2currencies as $bonuses2currency) {
                        if ($bonuses2currency->max_sum || $bonuses2currency->max_payout) {
                            $bonuses2currency->bonus_id = $model->id;
                            if ($bonuses2currency->save()) {
                                $model->addErrors($bonuses2currency->getErrors());
                            }
                        }
                    }
                }

                if (!$model->hasErrors()) {
                    Yii::$app->session->addFlash(\app\widgets\Alert::SUCCESS, Yii::t('app/b', 'Record added'));
                    $transaction->commit();
                    if (Yii::$app->request->post('to_game')) {
                        return $this->redirect($model->getUrl());
                    } else {
                        return $this->redirect(Url::to(['update', 'id' => $model->id]));
                    }
                }
            } catch (\Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->addFlash(\app\widgets\Alert::ERROR, $e->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $model,
            'bonuses2currencies' => $bonuses2currencies,
        ]);

    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        $bonuses2currencies = [];
        foreach (Currencies::find()->orderBy(['title' => SORT_ASC])->all() as $currency) {
            $bonuses2currency = Bonuses2Currencies::find()->where([
                'bonus_id' => $model->id,
                'currency_id' => $currency->id,
            ])->one() ? : new Bonuses2Currencies;
            $bonuses2currency->currency_id = $currency->id;
            $bonuses2currencies[] = $bonuses2currency;
        }

        if ($model->load(Yii::$app->request->post())) {
            Model::loadMultiple($bonuses2currencies, Yii::$app->request->post());
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($model->save()) {
                    foreach ($bonuses2currencies as $bonuses2currency) {
                        if ($bonuses2currency->max_sum || $bonuses2currency->max_payout) {
                            $bonuses2currency->bonus_id = $model->id;
                            if ($bonuses2currency->save()) {
                                $model->addErrors($bonuses2currency->getErrors());
                            }
                        } elseif (!$bonuses2currency->isNewRecord) {
                            $bonuses2currency->delete();
                        }
                    }
                }

                if (!$model->hasErrors()) {
                    Yii::$app->session->addFlash(\app\widgets\Alert::SUCCESS, Yii::t('app/b', 'Record updated'));
                    $transaction->commit();
                    if (Yii::$app->request->post('to_game')) {
                        return $this->redirect($model->getUrl());
                    } else {
                        return $this->redirect(Url::to(['update', 'id' => $model->id]));
                    }
                }
            } catch (\Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->addFlash(\app\widgets\Alert::ERROR, $e->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $model,
            'bonuses2currencies' => $bonuses2currencies,
        ]);

    }

    public function createModel() {
        return new Bonuses();
    }

    public function loadModel($id) {
        if (($model = Bonuses::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
