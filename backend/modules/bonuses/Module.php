<?php

namespace app\modules\bonuses;

class Module extends \yii\base\Module {

	public $controllerNamespace = 'app\modules\bonuses\controllers';
	public $layout = 'main';

	public function init() {
		parent::init();
	}

}
