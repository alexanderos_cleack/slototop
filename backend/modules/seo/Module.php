<?php

namespace app\modules\seo;

class Module extends \yii\base\Module {

    public $controllerNamespace = 'app\modules\seo\controllers';
    public $layout = 'main';

    public function init() {
        parent::init();
    }

}
