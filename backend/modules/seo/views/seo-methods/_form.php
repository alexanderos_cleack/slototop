<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\helpers\Url;

use zxbodya\yii2\elfinder\ElFinderInput;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use zxbodya\yii2\tinymce\TinyMce;

use app\widgets\MetaTags;
use common\models\seotext\SeoTextMethods;
use common\models\methods\MethodsPayment;

/* @var $this yii\web\View */ 
/* @var $model common\models\seotext\SeoText */
/* @var $form ActiveForm */

?>

<?php if ($model->hasErrors()) Yii::$app->session->addFlash(\app\widgets\Alert::ERROR, Html::errorSummary($model)); ?>

<?php $form = ActiveForm::begin(); ?>

    <div class="well well-sm">

        <?= $form->field($model, 'type')->dropdownList(SeoTextMethods::getTypesForList(), ['prompt' => Yii::t('app/b', 'Select')]) ?>
        <?= $form->field($model, 'model_id')->dropdownList(MethodsPayment::getForList(), ['prompt' => Yii::t('app/b', 'Select')]) ?>
        <?= \yii\helpers\Html::activeHiddenInput($model, 'model_schema', ['value' => MethodsPayment::tableName()]) ?>
        <?= $form->field($model, 'description')->widget(TinyMce::className(), [
            'language' => 'ru',
            'fileManager' => [
                'class' => TinyMceElFinder::className(),
                'connectorRoute' => '/uploads',
            ],
        ]) ?>
    </div>

    <?= MetaTags::widget([
        'model' => $model,
        'form' => $form,
    ]) ?>

    <div class="well well-sm">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
    </div>

<?php ActiveForm::end(); ?>
