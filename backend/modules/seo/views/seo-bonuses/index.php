<?php

use yii\grid\GridView;
use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $model common\models\seotext\SeoText */

$this->title = Yii::t('app/b', 'SeoTextList');
$this->context->breadcrumbs[] = $this->title;

?>

<?php

echo Html::beginForm();

    echo GridView::widget([
        'dataProvider' => $model->search(),
        'filterModel' => $model,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'type',
                    'value' => function ($model, $key, $index, $column) {
                        return $model->getTypeLabel();
                    },
                ],
                [
                    'attribute' => 'model_id',
                    'value' => function ($model, $key, $index, $column) {
                        return $model->bonus->title;
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}',
                ],
        ],
    ]);

Html::endForm();

?>
