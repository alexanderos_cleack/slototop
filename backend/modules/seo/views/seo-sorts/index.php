<?php

use yii\grid\GridView;
use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $model common\models\seotext\SeoText */

$this->title = Yii::t('app/b', 'SeoTextList');
$this->context->breadcrumbs[] = $this->title;

?>

<?php

$model->scenario = 'search';

echo Html::beginForm();

    echo GridView::widget([
        'dataProvider' => $model->search(),
        'filterModel' => $model,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'page',
                    'filter' => \common\models\seotext\SeoSortsRequests::getSortsPagesForList(),
                ],
                'title',
                [
                    'attribute' => 'url',
                    'value' => function () {
                        return '...';
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update} {delete}',
                    'buttons' => [
                        'view' => function ($url, $model, $key) {
                            // return Html::a(Html::icon('picture'), $model->url, ['target' => '_blank', 'title' => Yii::t('app/b', 'View')]);
                        },
                    ],
                ],
        ],
    ]);

Html::endForm();

?>
