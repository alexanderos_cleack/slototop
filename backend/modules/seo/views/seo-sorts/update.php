<?php

/* @var $this yii\web\View */
/* @var $model common\models\seotext\SeoText */

$this->title = Yii::t('app/b', 'SeoTextUpdate');
$this->context->breadcrumbs[] = $this->title;

?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>
