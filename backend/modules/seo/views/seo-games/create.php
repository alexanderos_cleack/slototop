<?php

/* @var $this yii\web\View */
/* @var $model common\models\seotext\SeoText */

$this->title = Yii::t('app/b', 'SeoTextCreate');
$this->context->breadcrumbs[] = $this->title;

?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>
