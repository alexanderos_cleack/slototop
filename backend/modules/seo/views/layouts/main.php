<?php 

use yii\bootstrap\Nav;

$this->beginContent('@app/views/layouts/default.php');

echo Nav::widget([
        'options' => ['class' =>'nav nav-pills my-nav'],
        'items' => [
            ['label' => Yii::t('app/b', 'SeoTextList'), 'url' => ['index']],
            ['label' => Yii::t('app/b', 'SeoTextCreate'), 'url' => ['create']],
        ],
    ]);

echo $content;
$this->endContent();

?>