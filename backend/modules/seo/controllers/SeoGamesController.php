<?php

namespace app\modules\seo\controllers;

use Yii;
use yii\web\Controller;

use common\models\seotext\SeoTextGames;

class SeoGamesController extends \app\components\BController {

    public $enableCsrfValidation = false;

    public function init() {
        parent::init();
        $this->breadcrumbs[] = ['label' => Yii::t('app', 'Seo'), 'url' => ['/seo/seo/index']];
    }

    public function actions() {
        return [
            'index'  => 'app\components\crud\IndexAction',
            'create' => 'app\components\crud\CreateAction',
            'view'   => 'app\components\crud\ViewAction',
            'update' => 'app\components\crud\UpdateAction',
            'delete' => 'app\components\crud\DeleteAction',
        ];
    }

    public function createModel() {
        return new SeoTextGames;
    }

    public function loadModel($id) {
        if (($model = SeoTextGames::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
