<?php

namespace app\modules\seo\controllers;

use Yii;
use yii\web\Controller;

use common\models\seotext\SeoFiltersRequests;

class SeoFiltersController extends \app\components\BController {

    public $enableCsrfValidation = false;

    public function init() {
        parent::init();
        $this->breadcrumbs[] = ['label' => Yii::t('app', 'Seo'), 'url' => ['/seo/seo/index']];
    }

    public function actions() {
        return [
            'index'  => 'app\components\crud\IndexAction',
            // 'create' => 'app\components\crud\CreateAction',
            'view'   => 'app\components\crud\ViewAction',
            'update' => 'app\components\crud\UpdateAction',
            'delete' => 'app\components\crud\DeleteAction',

            'slugRefresh' => 'app\components\actions\SlugRefreshAction',
        ];
    }

    public function actionCreate() {}

    public function createModel() {
        return new SeoFiltersRequests();
    }

    public function loadModel($id) {
        if (($model = SeoFiltersRequests::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
