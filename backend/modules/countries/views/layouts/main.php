<?php 

use yii\bootstrap\Nav;

$this->beginContent('@app/views/layouts/default.php');

echo Nav::widget([
		'options' => ['class' =>'nav nav-pills my-nav'],
		'items' => [
			['label' => Yii::t('app', 'CountriesList'), 'url' => ['/countries/countries-items/index']],
			['label' => Yii::t('app', 'CountriesCreate'), 'url' => ['/countries/countries-items/create']],
		],
	]);
echo $content;
$this->endContent();

?>