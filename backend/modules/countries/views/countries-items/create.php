<?php

/* @var $this yii\web\View */
/* @var $model common\models\countries\Countries */

$this->title = Yii::t('app', 'CountriesCreate');
$this->context->breadcrumbs[] = $this->title;

?>

<?= $this->render('_form', [
	'model' => $model,
]) ?>
