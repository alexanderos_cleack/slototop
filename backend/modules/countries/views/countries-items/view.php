<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\countries\Countries */

?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'title',
        'iso',
    ],
]) ?>