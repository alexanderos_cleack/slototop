<?php

namespace app\modules\countries\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

use common\models\countries\Countries;
use common\models\countries\CountriesSearch;

class CountriesItemsController extends \app\components\BController {

    public $enableCsrfValidation = false;

    public function init() {
        parent::init();
        $this->breadcrumbs[] = ['label' => Yii::t('app', 'Countries'), 'url' => ['/countries/countries-items/index']];
    }

    public function actions() {
        return [
            // 'index' => 'app\components\crud\IndexAction',
            'create' => 'app\components\crud\CreateAction',
            'view' => 'app\components\crud\ViewAction',
            'update' => 'app\components\crud\UpdateAction',
            'delete' => 'app\components\crud\DeleteAction',
        ];
    }

    public function actionIndex() {
        $model = new CountriesSearch;
        $model->load(Yii::$app->request->queryParams);

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    public function createModel() {
        return new Countries();
    }

    public function loadModel($id) {
        if (($model = Countries::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
