<?php

namespace app\modules\countries;

class Module extends \yii\base\Module {

	public $controllerNamespace = 'app\modules\countries\controllers';
	public $layout = 'main';

	public function init() {
		parent::init();
	}

}
