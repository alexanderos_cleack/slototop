<?php

namespace app\modules\currencies;

class Module extends \yii\base\Module {

	public $controllerNamespace = 'app\modules\currencies\controllers';
	public $layout = 'main';

	public function init() {
		parent::init();
	}

}
