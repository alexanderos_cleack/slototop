<?php

/* @var $this yii\web\View */
/* @var $model common\models\currencies\Currencies */

$this->title = Yii::t('app', 'CanguagesUpdate');
$this->context->breadcrumbs[] = $this->title;

?>

<?= $this->render('_form', [
	'model' => $model,
]) ?>
