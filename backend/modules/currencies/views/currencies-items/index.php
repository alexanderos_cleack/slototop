<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\currencies\Currencies */

$this->title = Yii::t('app', 'CurrenciesList');
$this->context->breadcrumbs[] = $this->title;

?>

<?php

echo Html::beginForm();

    echo GridView::widget([
        'dataProvider' => $model->search(),
        'filterModel' => $model,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'iso',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);

Html::endForm();

?>
