<?php

/* @var $this yii\web\View */
/* @var $model common\models\currencies\Currencies */

$this->title = Yii::t('app', 'CurrenciesCreate');
$this->context->breadcrumbs[] = $this->title;

?>

<?= $this->render('_form', [
	'model' => $model,
]) ?>
