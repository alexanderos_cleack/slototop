<?php 

use yii\bootstrap\Nav;

$this->beginContent('@app/views/layouts/default.php');

echo Nav::widget([
		'options' => ['class' =>'nav nav-pills my-nav'],
		'items' => [
			['label' => Yii::t('app', 'CurrenciesList'), 'url' => ['/currencies/currencies-items/index']],
			['label' => Yii::t('app', 'CurrenciesCreate'), 'url' => ['/currencies/currencies-items/create']],
		],
	]);
echo $content;
$this->endContent();

?>