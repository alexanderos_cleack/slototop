<?php

namespace app\modules\currencies\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

use common\models\currencies\Currencies;

class CurrenciesItemsController extends \app\components\BController {

	public function init() {
		parent::init();
		$this->breadcrumbs[] = ['label' => Yii::t('app', 'Currencies'), 'url' => ['/currencies/currencies-items/index']];
	}

	public function actions() {
		return [
			'index' => 'app\components\crud\IndexAction',
			'create' => 'app\components\crud\CreateAction',
			'view' => 'app\components\crud\ViewAction',
			'update' => 'app\components\crud\UpdateAction',
			'delete' => 'app\components\crud\DeleteAction',
		];
	}

	public function createModel() {
		return new Currencies();
	}

	public function loadModel($id) {
		if (($model = Currencies::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

}
