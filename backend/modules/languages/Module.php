<?php

namespace app\modules\languages;

class Module extends \yii\base\Module {

	public $controllerNamespace = 'app\modules\languages\controllers';
	public $layout = 'main';

	public function init() {
		parent::init();
	}

}
