<?php 

use yii\bootstrap\Nav;

$this->beginContent('@app/views/layouts/default.php');

echo Nav::widget([
		'options' => ['class' =>'nav nav-pills my-nav'],
		'items' => [
			['label' => Yii::t('app', 'LanguagesList'), 'url' => ['/languages/languages-items/index']],
			['label' => Yii::t('app', 'LanguagesCreate'), 'url' => ['/languages/languages-items/create']],
		],
	]);
echo $content;
$this->endContent();

?>