<?php

/* @var $this yii\web\View */
/* @var $model common\models\languages\Languages */

$this->title = Yii::t('app', 'LanguagesUpdate');
$this->context->breadcrumbs[] = $this->title;

?>

<?= $this->render('_form', [
	'model' => $model,
]) ?>
