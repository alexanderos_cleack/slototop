<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\languages\Languages */

?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'title',
        'iso',
    ],
]) ?>