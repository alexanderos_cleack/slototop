<?php

namespace app\modules\languages\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;

use common\models\languages\Languages;

class LanguagesItemsController extends \app\components\BController {

	public function init() {
		parent::init();
		$this->breadcrumbs[] = ['label' => Yii::t('app', 'Languages'), 'url' => ['/languages/languages-items/index']];
	}

	public function actions() {
		return [
			'index' => 'app\components\crud\IndexAction',
			'create' => 'app\components\crud\CreateAction',
			'view' => 'app\components\crud\ViewAction',
			'update' => 'app\components\crud\UpdateAction',
			'delete' => 'app\components\crud\DeleteAction',
		];
	}

	public function createModel() {
		return new languages();
	}

	public function loadModel($id) {
		if (($model = languages::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

}
