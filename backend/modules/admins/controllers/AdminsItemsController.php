<?php

namespace app\modules\admins\controllers;

use Yii;
use yii\filters\VerbFilter;

use app\models\admins\Admins;

class AdminsItemsController extends \app\components\BController {

	public function init() {
		parent::init();
		$this->breadcrumbs[] = ['label' => Yii::t('app', 'Admins'), 'url' => ['/admins/admins-items/index']];
	}

	public function actions() {
		return [
			'index' => 'app\components\crud\IndexAction',
			'create' => 'app\components\crud\CreateAction',
			'view' => 'app\components\crud\ViewAction',
			'update' => 'app\components\crud\UpdateAction',
			'delete' => 'app\components\crud\DeleteAction',
		];
	}

	public function createModel() {
		return new Admins();
	}

	public function loadModel($id) {
		if (($model = Admins::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

}
