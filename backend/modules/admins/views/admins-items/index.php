<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\admins\Admins */

$this->title = Yii::t('app', 'AdminsList');
$this->context->breadcrumbs[] = $this->title;

?>

<?php

echo Html::beginForm();

	echo GridView::widget([
		'dataProvider' => $model->search(),
		'filterModel' => $model,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

			'login',

			['class' => 'yii\grid\ActionColumn'],
		],
	]);

Html::endForm();

?>
