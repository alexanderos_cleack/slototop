<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use dosamigos\fileupload\FileUpload;
use yii\jui\DatePicker;

use common\models\licenses\Licenses;
use common\models\methods\MethodsOutput;
use common\models\methods\MethodsPayment;
use common\models\bonuses\Bonuses;
use common\models\languages\Languages;

/* @var $this yii\web\View */ 
/* @var $model common\models\casino\Casino */
/* @var $form ActiveForm */

?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

	<div class="well well-sm">

		<?= $form->field($model, 'title') ?>
		<?= $form->field($model, 'license_id', ['inline' => true])->radioList(Licenses::getForList()) ?>
		<?= $form->field($model, 'bonuses', ['inline' => true])->checkboxList(Bonuses::getForList()) ?>
		<?= $form->field($model, 'methods_output', ['inline' => true])->checkboxList(MethodsOutput::getForList()) ?>
		<?= $form->field($model, 'methods_payment', ['inline' => true])->checkboxList(MethodsPayment::getForList()) ?>
		<?= $form->field($model, 'languages', ['inline' => true])->checkboxList(Languages::getForList()) ?>
		<?= $form->field($model, 'actively')->checkBox() ?>
		<?= $form->field($model, 'blacklist')->checkBox() ?>
		<?= $form->field($model, 'new')->checkBox() ?>
		<?= $form->field($model, 'new_to_date')->widget(DatePicker::className(),['dateFormat' => 'yyyy-MM-dd']) ?>
		<?= $form->field($model, 'min_deposit') ?>
		<?= $form->field($model, 'min_output') ?>
		<?= $form->field($model, 'amount_games') ?>
		<?= $form->field($model, 'rating_users') ?>
		<?= $form->field($model, 'rating_admins') ?>
		<?= $form->field($model, 'payout') ?>
		<?= $form->field($model, 'ref_url') ?>
		<?= $form->field($model, 'video_viewer_url') ?>
		<?= $form->field($model, 'terms_output') ?>
		<?= $form->field($model, 'year_established') ?>
		<?= $form->field($model, 'logo')->fileInput() ?>
		<?= $form->field($model, 'screens[]')->fileInput(['multiple' => true]) ?>
		<?= $form->field($model, 'description')->widget(CKEditor::className(), [
			'options' => ['rows' => 6],
			'preset' => 'full'
		]) ?>

	</div>

	<div class="well well-sm">
		<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
		<?= Html::a(Yii::t('app', 'Cancel'), ['index', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
	</div>

<?php ActiveForm::end(); ?>
