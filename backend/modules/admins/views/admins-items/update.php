<?php

/* @var $this yii\web\View */
/* @var $model common\models\casino\Casino */

$this->title = Yii::t('app', 'CasinoUpdate');
$this->context->breadcrumbs[] = $this->title;

?>

<?= $this->render('_form', [
	'model' => $model,
]) ?>
