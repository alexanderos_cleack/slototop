<?php 

use yii\bootstrap\Nav;

$this->beginContent('@app/views/layouts/default.php');

echo Nav::widget([
		'options' => ['class' =>'nav nav-pills my-nav'],
		'items' => [
			['label' => Yii::t('app', 'CasinoList'), 'url' => ['/casino/casino-items/index']],
			['label' => Yii::t('app', 'CasinoCreate'), 'url' => ['/casino/casino-items/create']],
		],
	]);

echo $content;
$this->endContent();

?>