<?php

namespace app\modules\admins;

class Module extends \yii\base\Module {

    public $controllerNamespace = 'app\modules\admins\controllers';
    public $layout = 'main';

    public function init() {
        parent::init();
    }

}
