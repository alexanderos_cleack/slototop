<?php

namespace app\modules\casino\controllers;

use Yii;
use yii\base\Model;
use yii\filters\VerbFilter;
use yii\helpers\Url;

use common\models\casino\Casino;
use common\models\currencies\Currencies;
use common\models\currencies\Currencies2Casino;

class CasinoItemsController extends \app\components\BController {

    public $enableCsrfValidation = false;

    public function init() {
        parent::init();
        $this->breadcrumbs[] = ['label' => Yii::t('app', 'Casino'), 'url' => ['/casino/casino-items/index']];
    }

    public function actions() {
        return [
            'index'  => 'app\components\crud\IndexAction',
            // 'create' => 'app\components\crud\CreateAction',
            'view'   => 'app\components\crud\ViewAction',
            // 'update' => 'app\components\crud\UpdateAction',
            'delete' => 'app\components\crud\DeleteAction',
            'enable' => 'app\components\crud\EnableAction',
            'disable' => 'app\components\crud\DisableAction',
            'imageDelete' => 'app\components\crud\ImageDeleteAction',

            'slugRefresh' => 'app\components\actions\SlugRefreshAction',
        ];
    }

    public function actionCreate() {
        $model = $this->createModel();

        $currencies2casino = [];
        foreach (Currencies::find()->orderBy(['title' => SORT_ASC])->all() as $currency) {
            $currency2casino = new Currencies2Casino;
            $currency2casino->currency_id = $currency->id;
            $currencies2casino[] = $currency2casino;
        }

        if ($model->load(Yii::$app->request->post())) {
            Model::loadMultiple($currencies2casino, Yii::$app->request->post());
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($model->save()) {
                    foreach ($currencies2casino as $currency2casino) {
                        if ($currency2casino->min_deposit || $currency2casino->min_output) {
                            $currency2casino->casino_id = $model->id;
                            if ($currency2casino->save()) {
                                $model->addErrors($currency2casino->getErrors());
                            }
                        }
                    }
                }

                if (!$model->hasErrors()) {
                    Yii::$app->session->addFlash(\app\widgets\Alert::SUCCESS, Yii::t('app/b', 'Record added'));
                    $transaction->commit();
                    if (Yii::$app->request->post('to_game')) {
                        return $this->redirect($model->getUrl());
                    } else {
                        return $this->redirect(Url::to(['update', 'id' => $model->id]));
                    }
                }
            } catch (\Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->addFlash(\app\widgets\Alert::ERROR, $e->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $model,
            'currencies2casino' => $currencies2casino,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        $currencies2casino = [];
        foreach (Currencies::find()->orderBy(['title' => SORT_ASC])->all() as $currency) {
            $currency2casino = Currencies2Casino::find()->where([
                'casino_id' => $model->id,
                'currency_id' => $currency->id,
            ])->one() ? : new Currencies2Casino;
            $currency2casino->currency_id = $currency->id;
            $currencies2casino[] = $currency2casino;
        }

        if ($model->load(Yii::$app->request->post())) {
            Model::loadMultiple($currencies2casino, Yii::$app->request->post());
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($model->save()) {
                    foreach ($currencies2casino as $currency2casino) {
                        if ($currency2casino->min_deposit || $currency2casino->min_output) {
                            $currency2casino->casino_id = $model->id;
                            if ($currency2casino->save()) {
                                $model->addErrors($currency2casino->getErrors());
                            }
                        } elseif (!$currency2casino->isNewRecord) {
                            $currency2casino->delete();
                        }
                    }
                }

                if (!$model->hasErrors()) {
                    Yii::$app->session->addFlash(\app\widgets\Alert::SUCCESS, Yii::t('app/b', 'Record updated'));
                    $transaction->commit();
                    if (Yii::$app->request->post('to_game')) {
                        return $this->redirect($model->getUrl());
                    } else {
                        return $this->redirect(Url::to(['update', 'id' => $model->id]));
                    }
                }
            } catch (\Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->addFlash(\app\widgets\Alert::ERROR, $e->getMessage());
            }
        }

        return $this->render('update', [
            'model' => $model,
            'currencies2casino' => $currencies2casino,
        ]);
    }

    public function createModel() {
        return new Casino();
    }

    public function loadModel($id) {
        if (($model = Casino::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
