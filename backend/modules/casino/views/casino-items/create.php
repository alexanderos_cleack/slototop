<?php

/* @var $this yii\web\View */
/* @var $model common\models\casino\Casino */

$this->title = Yii::t('app', 'CasinoCreate');
$this->context->breadcrumbs[] = $this->title;

?>

<?= $this->render('_form', [
    'model' => $model,
    'currencies2casino' => $currencies2casino,
]) ?>
