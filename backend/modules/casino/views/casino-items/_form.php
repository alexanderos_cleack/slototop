<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\helpers\Url;

use zxbodya\yii2\elfinder\ElFinderInput;
use zxbodya\yii2\elfinder\TinyMceElFinder;
use zxbodya\yii2\tinymce\TinyMce;

use app\widgets\MetaTags;
use common\models\bonuses\Bonuses;
use common\models\countries\Countries;
use common\models\currencies\Currencies2Casino;
use common\models\currencies\Currencies;
use common\models\languages\Languages;
use common\models\licenses\Licenses;
use common\models\methods\MethodsOutput;
use common\models\methods\MethodsPayment;
use common\models\vendors\Vendors;

/* @var $this yii\web\View */ 
/* @var $model common\models\casino\Casino */
/* @var $form ActiveForm */

?>

<?php if ($model->hasErrors()) Yii::$app->session->addFlash(\app\widgets\Alert::ERROR, Html::errorSummary($model)); ?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="well well-sm">

        <?= $form->field($model, 'title') ?>

        <?= \app\widgets\SlugRefresh::widget(['model' => $model]) ?>

        <?= $form->field($model, 'logo')->fileInput() ?>
        <?= \app\widgets\ImagesAlt::widget(['form' => $form, 'parentModel' => $model, 'imageModel' => $model->logo]) ?>

        <?= $form->field($model, 'casino_url') ?>
        <?= $form->field($model, 'casino_redirect_url') ?>

        <?= $form->field($model, 'enabled')->checkbox() ?>
        <?= $form->field($model, 'main_page')->checkbox() ?>

        <?= $form->field($model, 'actively')->checkBox() ?>
        <?= $form->field($model, 'blacklist')->checkBox() ?>
        <?= $form->field($model, 'new')->checkBox() ?>
        <?= $form->field($model, 'new_to_date')->widget(DatePicker::class,['dateFormat' => 'yyyy-MM-dd']) ?>

        <?= $form->field($model, 'bonuses')->dropdownList(Bonuses::getForList(), ['multiple' => true, 'size' => 10]) ?>
        <?= $form->field($model, 'license_id')->dropdownList(Licenses::getForList(), ['prompt' => Yii::t('app', 'Select')]) ?>
        <?= $form->field($model, 'methods_output')->dropdownList(MethodsPayment::getForList(), ['multiple' => true, 'size' => 10]) ?>
        <?= $form->field($model, 'methods_payment')->dropdownList(MethodsPayment::getForList(), ['multiple' => true, 'size' => 10]) ?>
        <?= $form->field($model, 'countries')->dropdownList(Countries::getForList(), ['multiple' => true, 'size' => 10]) ?>
        <?= $form->field($model, 'languages')->dropdownList(Languages::getForList(), ['multiple' => true, 'size' => 10]) ?>
        <?= $form->field($model, 'vendors')->dropdownList(Vendors::getForList(), ['multiple' => true, 'size' => 10]) ?>

        <?= $form->field($model, 'video_viewer_url') ?>
        <?= $form->field($model, 'payout') ?>
        <?= $form->field($model, 'year_established') ?>
        <?= $form->field($model, 'amount_games') ?>
        <?= $form->field($model, 'rating_admins') ?>
        <?= $form->field($model, 'rating_users') ?>
        <?= $form->field($model, 'ref_url') ?>
        <?= $form->field($model, 'terms_output') ?>
        <?= $form->field($model, 'email') ?>
        <?= $form->field($model, 'phone') ?>
        <?= $form->field($model, 'chat')->checkBox() ?>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <?= Yii::t('app/b', 'Currencies') ?>
        </div>
        <div class="panel-body">
            <?php foreach ($currencies2casino as $i => $currency2casino): ?>
                <div class="item panel panel-default">
                    <div class="panel-heading">
                        <?= $currency2casino->currency->title; ?>
                    </div>
                    <div class="panel-body">
                        <?php if (!$currency2casino->isNewRecord): ?>
                            <?= Html::activeHiddenInput($currency2casino, "[{$i}]casino_id") ?>
                        <?php endif; ?>
                        <?= Html::activeHiddenInput($currency2casino, "[{$i}]currency_id") ?>
                        <?= $form->field($currency2casino, "[{$i}]min_deposit") ?>
                        <?= $form->field($currency2casino, "[{$i}]min_output") ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

    <div class="well well-sm">
        <?= $form->field($model, 'short_description')->textarea(['rows' => 5]) ?>
        <?= $form->field($model, 'screens[]')->fileInput(['multiple' => true]) ?>
        <?php if ($model->screens): ?>
            <div class="screens">
            <?php foreach ($model->screens as $screen): ?>
                <?= \app\widgets\ImagesAlt::widget(['form' => $form, 'parentModel' => $model, 'imageModel' => $screen, 'delete' => true]) ?>
            <?php endforeach; ?>
            </div>
        <?php endif; ?>
        <?= $form->field($model, 'description')->widget(TinyMce::class, [
            'language' => 'ru',
            'fileManager' => [
                'class' => TinyMceElFinder::class,
                'connectorRoute' => '/uploads',
            ],
        ]) ?>
    </div>

    <div class="well well-sm">
        <?= $form->field($model, 'screens_view[]')->fileInput(['multiple' => true]) ?>
        <?php if ($model->screens_view): ?>
            <div class="screens">
            <?php foreach ($model->screens_view as $screen): ?>
                <?= \app\widgets\ImagesAlt::widget(['form' => $form, 'parentModel' => $model, 'imageModel' => $screen, 'delete' => true]) ?>
            <?php endforeach; ?>
            </div>
        <?php endif; ?>
        <?= $form->field($model, 'description_view')->widget(TinyMce::class, [
            'language' => 'ru',
            'fileManager' => [
                'class' => TinyMceElFinder::class,
                'connectorRoute' => '/uploads',
            ],
        ]) ?>
    </div>

    <div class="well well-sm">
        <?= $form->field($model, 'screens_registration[]')->fileInput(['multiple' => true]) ?>
        <?php if ($model->screens_registration): ?>
            <div class="screens">
            <?php foreach ($model->screens_registration as $screen): ?>
                <?= \app\widgets\ImagesAlt::widget(['form' => $form, 'parentModel' => $model, 'imageModel' => $screen, 'delete' => true]) ?>
            <?php endforeach; ?>
            </div>
        <?php endif; ?>
        <?= $form->field($model, 'description_registration')->widget(TinyMce::class, [
            'language' => 'ru',
            'fileManager' => [
                'class' => TinyMceElFinder::class,
                'connectorRoute' => '/uploads',
            ],
        ]) ?>
    </div>

    <div class="well well-sm">
        <?= $form->field($model, 'screens_games[]')->fileInput(['multiple' => true]) ?>
        <?php if ($model->screens_games): ?>
            <div class="screens">
            <?php foreach ($model->screens_games as $screen): ?>
                <?= \app\widgets\ImagesAlt::widget(['form' => $form, 'parentModel' => $model, 'imageModel' => $screen, 'delete' => true]) ?>
            <?php endforeach; ?>
            </div>
        <?php endif; ?>
        <?= $form->field($model, 'description_games')->widget(TinyMce::class, [
            'language' => 'ru',
            'fileManager' => [
                'class' => TinyMceElFinder::class,
                'connectorRoute' => '/uploads',
            ],
        ]) ?>
    </div>

    <div class="well well-sm">
        <?= $form->field($model, 'description_additional')->widget(TinyMce::class, [
            'language' => 'ru',
            'fileManager' => [
                'class' => TinyMceElFinder::class,
                'connectorRoute' => '/uploads',
            ],
        ]) ?>
    </div>

    <?= \app\widgets\Same::widget([
        'model' => $model,
        'form' => $form,
    ]) ?>

    <?= MetaTags::widget([
        'model' => $model,
        'form' => $form,
    ]) ?>

    <?= \app\widgets\Link::widget([
        'model' => $model,
    ]) ?>

    <div class="well well-sm">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app/b', 'Create to game') : Yii::t('app/b', 'Update to game'), ['class' => 'btn btn-primary', 'name' => 'to_game', 'value' => 'to_game', 'formtarget' => '_blank']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
    </div>

<?php ActiveForm::end(); ?>

<script>
    $(document).ready(function () {
        $('div.screens').click(function (e) {
            e.preventDefault();
            $.ajax({
                url: $(e.target).parent().attr('href'),
                method: "get",
            }).done(function (data) {
                if (data.action == 'success') {
                    $(e.target).hide();
                }
            });
        });
    });

</script>
