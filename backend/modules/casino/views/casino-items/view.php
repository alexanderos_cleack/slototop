<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\casino\Casino */

?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'title',
        'payout',
        'description'
    ],
]) ?>