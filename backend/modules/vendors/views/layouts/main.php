<?php 

use yii\bootstrap\Nav;

$this->beginContent('@app/views/layouts/default.php');

echo Nav::widget([
		'options' => ['class' =>'nav nav-pills my-nav'],
		'items' => [
			['label' => Yii::t('app', 'VendorsList'), 'url' => ['/vendors/vendors-items/index']],
			['label' => Yii::t('app', 'VendorsCreate'), 'url' => ['/vendors/vendors-items/create']],
			['label' => Yii::t('app', 'VendorsPlatformsList'), 'url' => ['/vendors/vendors-platforms/index']],
			['label' => Yii::t('app', 'VendorsPlatformsCreate'), 'url' => ['/vendors/vendors-platforms/create']],
		],
	]);
echo $content;
$this->endContent();

?>