<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

use zxbodya\yii2\elfinder\TinyMceElFinder;
use zxbodya\yii2\tinymce\TinyMce;

use app\widgets\MetaTags;
use common\models\countries\Countries;
use common\models\languages\Languages;
use common\models\vendors\VendorsPlatforms;
use common\models\vendors\Vendors;

/* @var $this yii\web\View */
/* @var $model common\models\vendors\Vendors */
/* @var $form ActiveForm */

?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="well well-sm">
        <?= $form->field($model, 'title') ?>

        <?= \app\widgets\SlugRefresh::widget(['model' => $model]) ?>

        <?= $form->field($model, 'logo')->fileInput() ?>
        <?= \app\widgets\ImagesAlt::widget(['form' => $form, 'parentModel' => $model, 'imageModel' => $model->logo]) ?>

        <?= $form->field($model, 'logo_mini')->fileInput() ?>
        <?= \app\widgets\ImagesAlt::widget(['form' => $form, 'parentModel' => $model, 'imageModel' => $model->logo_mini]) ?>

        <?= $form->field($model, 'logo_casino')->fileInput() ?>
        <?= \app\widgets\ImagesAlt::widget(['form' => $form, 'parentModel' => $model, 'imageModel' => $model->logo_casino]) ?>

        <?= $form->field($model, 'enabled')->checkbox() ?>
        <?= $form->field($model, 'main_page')->checkbox() ?>

        <?= $form->field($model, 'payout') ?>
        <?= $form->field($model, 'year_established') ?>
        <?= $form->field($model, 'amount_games') ?>
        <?= $form->field($model, 'vendors')->dropdownList(Vendors::getForList(), ['multiple' => true, 'size' => 10]) ?>
        <?= $form->field($model, 'platforms')->dropdownList(VendorsPlatforms::getVendorsForList(), ['multiple' => true, 'size' => 10]) ?>
        <?= $form->field($model, 'languages')->dropdownList(Languages::getForList(), ['multiple' => true, 'size' => 10]) ?>
        <?= $form->field($model, 'country_id')->dropdownList(Countries::getForList(), ['prompt' => Yii::t('app', 'Select')]) ?>
        <?= $form->field($model, 'screens[]')->fileInput(['multiple' => true]) ?>
        <?php if ($model->screens): ?>
            <div id="screens">
            <?php foreach ($model->screens as $screen): ?>
                <?= \app\widgets\ImagesAlt::widget(['form' => $form, 'parentModel' => $model, 'imageModel' => $screen, 'delete' => true]) ?>
            <?php endforeach; ?>
            </div>
        <?php endif; ?>
        <?= $form->field($model, 'description')->widget(TinyMce::class, [
            'language' => 'ru',
            'fileManager' => [
                'class' => TinyMceElFinder::class,
                'connectorRoute' => '/uploads',
            ],
        ]) ?>
    </div>

    <?= MetaTags::widget([
        'model' => $model,
        'form' => $form,
    ]) ?>

    <?= \app\widgets\Link::widget([
        'model' => $model,
    ]) ?>

    <div class="well well-sm">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
    </div>

<?php ActiveForm::end(); ?>
