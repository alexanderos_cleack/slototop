<?php

use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\vendors\Vendors */

$this->title = Yii::t('app', 'VendorsList');
$this->context->breadcrumbs[] = $this->title;

?>

<?php

echo Html::beginForm();

    echo GridView::widget([
        'dataProvider' => $model->search(),
        'filterModel' => $model,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',

            [
                'attribute' => 'main_page',
                'format' => 'boolean',
                'filter' => false,
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{show} {view} {update} {enable} {disable} {delete}',
                'buttons' => [
                    'show' => function ($url, $model, $key) {
                        return Html::a(Html::icon('picture'), \backend\helpers\UrlHelpers::toFrontend($model->url), ['target' => '_blank', 'title' => Yii::t('app', 'Go to vendor')]);
                    },
                    'enable' => function ($url, $model, $key) {
                        return $model->enabled ? Html::a(Html::icon('off'), Url::to(['disable', 'id' => $model->id]), ['title' => Yii::t('app', 'Disable'), 'class' => 'btn btn-xs btn-primary']) : '';
                    },
                    'disable' => function ($url, $model, $key) {
                        return !$model->enabled ? Html::a(Html::icon('off'), Url::to(['enable', 'id' => $model->id]), ['title' => Yii::t('app', 'Enable'), 'class' => 'btn btn-xs btn-warning']) : '';
                    },
                ],
            ],
        ],
    ]);

Html::endForm();

?>
