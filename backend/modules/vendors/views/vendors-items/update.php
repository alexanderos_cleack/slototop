<?php

/* @var $this yii\web\View */
/* @var $model common\models\vendors\Vendors */

$this->title = Yii::t('app', 'VendorsUpdate');

?>

<?= $this->render('_form', [
	'model' => $model,
]) ?>
