<?php

/* @var $this yii\web\View */
/* @var $model common\models\vendors\VendorsPlatforms */

$this->title = Yii::t('app', 'VendorsPlatformsCreate');
$this->context->breadcrumbs[] = $this->title;

?>

<?= $this->render('_form', [
	'model' => $model,
]) ?>
