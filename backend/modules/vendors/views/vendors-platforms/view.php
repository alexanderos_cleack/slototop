<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\vendors\VendorsPlatforms */

?>

<?= DetailView::widget([
	'model' => $model,
	'attributes' => [
		'title',
	],
]) ?>