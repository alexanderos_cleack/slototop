<?php

namespace app\modules\vendors\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

use common\models\vendors\VendorsPlatforms;

class VendorsPlatformsController extends \app\components\BController {

	public function init() {
		parent::init();
		$this->breadcrumbs[] = ['label' => Yii::t('app', 'VendorsPlatforms'), 'url' => ['/vendors/vendors-platforms/index']];
	}

	public function actions() {
		return [
			'index' => 'app\components\crud\IndexAction',
			'create' => 'app\components\crud\CreateAction',
			'view' => 'app\components\crud\ViewAction',
			'update' => 'app\components\crud\UpdateAction',
			'delete' => 'app\components\crud\DeleteAction',
		];
	}

	public function createModel() {
		return new VendorsPlatforms();
	}

	public function loadModel($id) {
		if (($model = VendorsPlatforms::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

}
