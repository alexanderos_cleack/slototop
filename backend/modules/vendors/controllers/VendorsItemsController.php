<?php

namespace app\modules\vendors\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

use common\models\vendors\Vendors;

class VendorsItemsController extends \app\components\BController {

    public $enableCsrfValidation = false;

    public function init() {
        parent::init();
        $this->breadcrumbs[] = ['label' => Yii::t('app', 'Vendors'), 'url' => ['/vendors/vendors-items/index']];
    }

    public function actions() {
        return [
            'index'  => 'app\components\crud\IndexAction',
            'create' => 'app\components\crud\CreateAction',
            'view'   => 'app\components\crud\ViewAction',
            'update' => 'app\components\crud\UpdateAction',
            'delete' => 'app\components\crud\DeleteAction',
            'enable' => 'app\components\crud\EnableAction',
            'disable' => 'app\components\crud\DisableAction',
            'imageDelete' => 'app\components\crud\ImageDeleteAction',

            'slugRefresh' => 'app\components\actions\SlugRefreshAction',
        ];
    }

    public function createModel() {
        return new Vendors();
    }

    public function loadModel($id) {
        if (($model = Vendors::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
        }
    }

}
