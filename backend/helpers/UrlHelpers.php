<?php

namespace backend\helpers;

/**
 * Class UrlHelpers
 * @package backend\helpers
 */
class UrlHelpers
{
    /**
     * @param string $url
     * @return string
     */
    public static function toFrontend($url)
    {
        return '//' . self::getFrontendDomain() . $url;
    }

    /**
     * @return string
     */
    public static function getFrontendDomain() {
        return str_replace('admin.', '', \Yii::$app->request->hostName);
    }
}