<?php

namespace backend\models\admins;

use Yii;
use yii\base\NotSupportedException;
use yii\base\Security;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "admins".
 *
 * @property integer $id
 * @property string $login
 * @property string $password
 * @property string $auth_key
 * @property string $role
 * @property boolean $enabled
 * @property string $created_at
 * @property string $updated_at
 */
class Admins extends ActiveRecord implements IdentityInterface {

    public static function tableName() {
        return 'admins';
    }

    public function rules() {
        return [
            [['login', 'password_hash', 'auth_key'], 'required'],
            [['enabled'], 'boolean'],
            [['created_at', 'updated_at'], 'safe'],
            [['login', 'password_hash'], 'string', 'max' => 255],
            [['auth_key', 'role'], 'string', 'max' => 32],
            [['login'], 'unique']
        ];
    }

    public function attributeLabels() {
        return [
            'id'         => Yii::t('app', 'ID'),
            'login'      => Yii::t('app', 'Login'),
            'password'   => Yii::t('app', 'Password'),
            'role'       => Yii::t('app', 'Role'),
            'enabled'    => Yii::t('app', 'Enabled'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function behaviors() {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    public static function findIdentity($id) {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        throw new NotSupportedException("'findIdentityByAccessToken' not supported");
    }

    public function getId() {
        return $this->id;
    }

    public function getAuthKey() {
        // throw new NotSupportedException("'getAuthKey' not supported");
        return $this->auth_key;
    }

    public function validateAuthKey($auth_key) {
        // throw new NotSupportedException("'validateAuthKey' not supported");
        return $this->getAuthKey() === $auth_key;
    }

    public static function findByLogin($login) {
        return static::findOne(['login' => $login]);
    }

    public function validatePassword($password) {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    public function getLogin() {
        return $this->login;
    }

    public function search() {
        $query = Admins::find();
        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
    }

}
