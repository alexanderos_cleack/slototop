<?php

namespace app\models;

use Yii;
use yii\base\Model;

use backend\models\admins\Admins;

/**
 * Login form
 */
class LoginForm extends Model {
    public $username;
    public $password;
    public $remember_me = true;

    private $_user = false;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['username', 'password'], 'required'],
            ['remember_me', 'boolean'],
            ['password', 'validatePassword'],
        ];
    }

    public function attributeLabels() {
        return [
            'username'    => Yii::t('app', 'Login'),
            'password'    => Yii::t('app', 'Password'),
            'remember_me' => Yii::t('app', 'Remember me'),
        ];
    }

    public function validatePassword() {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError('password', 'Incorrect username or password.');
            }
        }
    }

    public function login() {
        if ($this->validate()) {
            $this->_user->touch('updated_at');
            return Yii::$app->user->login($this->_user, $this->remember_me ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    public function getUser() {
        if (!$this->_user) {
            $this->_user = Admins::findByLogin($this->username);
        }
        return $this->_user;
    }

}
