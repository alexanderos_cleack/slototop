<?php

namespace app\models;

use Yii;

class Model extends \yii\base\Model {

    public static function createMultiple($modelName, $data) {
        $models = [];
        $chunks = explode('\\', $modelName);
        $_modelName = end($chunks);
        if (isset($data[$_modelName])) {
            foreach ($data[$_modelName] as $model) {
                $models[] = (isset($model['id']) && $model['id'] > 0 && $_model = $modelName::findOne($model['id'])) ? $_model : new $modelName;
            }
        }
        return $models;
    }

}