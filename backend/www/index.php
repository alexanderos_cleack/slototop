<?php

if ($_SERVER['SERVER_ADDR'] == '88.198.240.53') {
    error_reporting(E_ALL);
    defined('YII_DEBUG') or define('YII_DEBUG', true);
    defined('YII_ENV') or define('YII_ENV', 'dev');
} else {
    defined('YII_DEBUG') or define('YII_DEBUG', false);
    defined('YII_ENV') or define('YII_ENV', 'prod');
}

// register Composer autoloader
require(__DIR__ . '/../../common/vendor/autoload.php');

// include Yii class file
require(__DIR__ . '/../../common/vendor/yiisoft/yii2/Yii.php');

// load application configuration
$config_common = require(__DIR__ . '/../../common/config/main.php');
$config_app = require(__DIR__ . '/../config/main.php');
$config = yii\helpers\ArrayHelper::merge($config_common, $config_app);

// create, configure and run application
(new yii\web\Application($config))->run();