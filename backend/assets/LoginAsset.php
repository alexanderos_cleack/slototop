<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class LoginAsset
 * @package app\assets
 */
class LoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '/';

    public $css = [
        'css/login.css'
    ];

    public $js = [
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
