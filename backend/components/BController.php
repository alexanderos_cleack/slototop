<?php

namespace app\components;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Class BController
 * @package app\components
 */
class BController extends Controller
{
    public $breadcrumbs = [];

    public function init()
    {
        parent::init();
        $this->breadcrumbs['homeLink'] = [
            'label' => Yii::t('app', 'Adminka'),
            'url' => Yii::$app->urlManager->baseUrl,
            'template' => '<li><span class="glyphicon glyphicon-king" aria-hidden="true"></span> {link}</li>',
        ];
    }

    public function behaviors()
    {
        return [
            'AccessControl' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

}