<?php

namespace app\components;

use Yii;
use yii\base\Component;

/**
 * Class RbacComponent
 * @package app\components
 */
class RbacComponent extends Component
{
    public $userClass;

    private $_auth;

    public function init()
    {
        parent::init();
        $this->_auth = Yii::$app->authManager;
    }

    /**
     * [addChild description]
     * @param string $parent [description]
     * @param string $child [description]
     * @return [type]        [description]
     */
    public function addChild($parent, $child)
    {
    }

    /**
     * [addRole description]
     * @param string $name [description]
     */
    public function addRole($name)
    {
        $this->_auth->createRole($name);
    }

    /**
     * [assignRole description]
     * @param  string $role [description]
     * @param  integer $user_id [description]
     * @return [type]           [description]
     */
    public function assignRole($role, $user_id)
    {
        if (($role = $this->_auth->getRole($role)) === null) throw new Exception('Role doesn\'t exist');

        $reflaction_method = new \ReflectionMethod($this->userClass, 'findOne');
        if (!$reflaction_method->invoke(null, $user_id)) throw new Exception('User doesn\'t exist');


        $this->_auth->assign($role, $user_id);
    }

    /**
     * [createRole description]
     * @param  string $name [description]
     * @return [type]       [description]
     */
    private function createRole($name)
    {
        $this->_auth->createRole($name);
    }

}