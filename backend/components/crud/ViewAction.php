<?php

namespace app\components\crud;

use yii\base\Action;

/**
 * Class ViewAction
 * @package app\components\crud
 */
class ViewAction extends Action
{
    public function run($id)
    {
        return $this->controller->render('view', [
            'model' => $this->controller->loadModel($id),
        ]);
    }

}