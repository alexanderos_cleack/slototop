<?php

namespace app\components\crud;

use Yii;
use yii\base\Action;

use common\models\images\Images;

class ImageDeleteAction extends Action
{
    public function run($id)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $image = Images::findOne($id);
        if ($image->delete()) return ['action' => 'success'];
        return ['action' => 'fail'];
    }

}