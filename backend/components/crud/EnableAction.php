<?php

namespace app\components\crud;

use yii\base\Action;
use yii\helpers\Url;

class EnableAction extends Action
{
    public function run($id)
    {
        $model = $this->controller->loadModel($id);
        $model::updateAll(['enabled' => true], ['id' => $id]);
        $this->controller->redirect(Url::to(['index']));
    }

}