<?php

namespace app\components\crud;

use Yii;
use yii\base\Action;
use yii\helpers\Url;

/**
 * Class CreateAction
 * @package app\components\crud
 */
class CreateAction extends Action
{
    public function run()
    {
        $model = $this->controller->createModel();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash(\app\widgets\Alert::SUCCESS, Yii::t('app/b', 'Record added'));
            return $this->controller->redirect(Url::to(['update', 'id' => $model->id]));
        } else {
            return $this->controller->render('create', [
                'model' => $model,
            ]);
        }
    }

}