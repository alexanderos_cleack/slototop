<?php

namespace app\components\crud;

use Yii;
use yii\base\Action;
use yii\helpers\Url;

/**
 * Class UpdateAction
 * @package app\components\crud
 */
class UpdateAction extends Action
{
    public function run($id)
    {
        $model = $this->controller->loadModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $model->save();
                $transaction->commit();
                Yii::$app->session->addFlash(\app\widgets\Alert::SUCCESS, Yii::t('app/b', 'Record updated'));
                return $this->controller->redirect(Url::to(['update', 'id' => $model->id]));
            } catch (\Exception $e) {
                $transaction->rollBack();
                Yii::$app->session->addFlash(\app\widgets\Alert::ERROR, $e->getMessage());
                return $this->controller->render('update', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->controller->render('update', [
                'model' => $model,
            ]);
        }
    }

}