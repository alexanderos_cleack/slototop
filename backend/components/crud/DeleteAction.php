<?php

namespace app\components\crud;

use Yii;
use yii\base\Action;

/**
 * Class DeleteAction
 * @package app\components\crud
 */
class DeleteAction extends Action
{
    public function run($id)
    {
        $this->controller->loadModel($id)->delete();
        Yii::$app->session->addFlash(\app\widgets\Alert::SUCCESS, Yii::t('app/b', 'Record deleted'));
        return $this->controller->redirect(['index']);
    }

}