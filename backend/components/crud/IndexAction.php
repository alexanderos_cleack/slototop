<?php

namespace app\components\crud;

use Yii;
use yii\base\Action;

/**
 * Class IndexAction
 * @package app\components\crud
 */
class IndexAction extends Action
{
    public function run()
    {
        $model = $this->controller->createModel();
        $model->load(Yii::$app->request->queryParams);

        return $this->controller->render('index', [
            'model' => $model,
        ]);
    }

}