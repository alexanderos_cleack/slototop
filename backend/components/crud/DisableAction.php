<?php

namespace app\components\crud;

use Yii;
use yii\base\Action;
use yii\helpers\Url;

/**
 * Class DisableAction
 * @package app\components\crud
 */
class DisableAction extends Action
{
    public function run($id)
    {
        $model = $this->controller->loadModel($id);
        $model::updateAll(['enabled' => false], ['id' => $id]);
        $this->controller->redirect(Url::to(['index']));
    }

}