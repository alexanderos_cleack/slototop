<?php

namespace app\components;

use yii\base\Component;

/**
 * Class GlyphsComponent
 * @package app\components
 */
class GlyphsComponent extends Component
{
    private function base($name, $text)
    {
        return '<span class="glyphicon glyphicon-' . $name . '" aria-hidden="true"></span> ' . $text;
    }

    public function iuser($text)
    {
        return $this->base('user', $text);
    }

    public function ioff($text)
    {
        return $this->base('off', $text);
    }

}