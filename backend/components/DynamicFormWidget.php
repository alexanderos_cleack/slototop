<?php

namespace app\components;

use yii\web\View;
use wbraganca\dynamicform\DynamicFormAsset;

/**
 * Class DynamicFormWidget
 * @package app\components
 */
class DynamicFormWidget extends \wbraganca\dynamicform\DynamicFormWidget
{
    /**
     * Registers the needed assets.
     *
     * @param View $view The View object
     */
    public function registerAssets($view)
    {
        DynamicFormAsset::register($view);

        $hashVar = $this->getHashVarName();

        // add a click handler for the clone button
        $js = 'jQuery("#' . $this->formId . '").on("click", "' . $this->insertButton . '", function(e) {' . "\n";
        $js .= "    e.preventDefault();\n";
        $js .= '    jQuery(".' . $this->widgetContainer . '").triggerHandler("beforeInsert", [jQuery(this)]);' . "\n";
        $js .= '    jQuery(".' . $this->widgetContainer . '").yiiDynamicForm("addItem", ' . $hashVar . ", e, jQuery(this));\n";
        $js .= "});\n";
        $view->registerJs($js, $view::POS_READY);

        // add a click handler for the remove button
        $js = 'jQuery("#' . $this->formId . '").on("click", "' . $this->deleteButton . '", function(e) {' . "\n";
        $js .= "    e.preventDefault();\n";
        $js .= '    jQuery(".' . $this->widgetContainer . '").yiiDynamicForm("deleteItem", ' . $hashVar . ", e, jQuery(this));\n";
        $js .= "});\n";
        $view->registerJs($js, $view::POS_READY);

        $js = 'setTimeout(function () {jQuery("#' . $this->formId . '").yiiDynamicForm(' . $hashVar . ');}, 1000);' . "\n";
        $view->registerJs($js, $view::POS_LOAD);
    }
}