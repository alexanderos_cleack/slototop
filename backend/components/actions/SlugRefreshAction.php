<?php

namespace app\components\actions;

use Yii;
use yii\base\Action;

/**
 * Class SlugRefreshAction
 * @package app\components\actions
 */
class SlugRefreshAction extends Action
{
    public function run()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['slug' => \yii\helpers\Inflector::slug(Yii::$app->request->post('slug'))];
    }

}