<?php

use yii\helpers\Url;

/**
 * @var \common\models\casino\Casino[] $casino
 * @var \common\models\bonuses\Bonuses[] $bonuses
 * @var \common\models\slots\Slots[] $games
 */
?>

<?php

    if ($seo_main) {
        $this->title = $seo_main->meta_title;
        $this->registerMetaTag(['name' => 'keywords', 'content' => $seo_main->meta_keywords]);
        $this->registerMetaTag(['name' => 'description', 'content' => $seo_main->meta_description]);
    }

?>

<div class="float-left">
    <div class="main-slider">
        <ul class="bxslider">
          <li>
            <div class="slider-text">
                <span class="text-slider-h"><span>1000</span> гривен<br />от казино Кинг</span>
                <span class="slider-text-sp">Первое украинское онлайн казино предлагает<br />бездепозитный бонус 1000 гривен для граждан Украины</span>
                <a data-href="//sloto.top/casino/casino-slotoking" class="btn-green">Читать обзор</a>
            </div>
            <img src="/img/slider/king.jpg" alt="Бонус от Слотокинг">
          </li>
          <li>
            <div class="slider-text">
                <span class="text-slider-h"><span>3000</span> рублей<br />от клуба Вулкан</span>
                <span class="slider-text-sp"><br>VulkanOriginals предлагает всем новым игрокам<br>бездепозитный бонус в размере 3000 российских рублей</span>
                <a data-href="//sloto.top/casino/casino-vulkan-original" class="btn-green">Читать обзор</a>
            </div>
            <img src="/img/slider/vulkan.jpg" alt="Вулкан бездепозитный бонус">
          </li>
		  <li>
            <div class="slider-text">
                <span class="text-slider-h">Бездеп <span>$20</span><br />от казино NetGame</span>
                <span class="slider-text-sp"><br>Шикарный бонус от самого честного казино СНГ - NetGameCasino<br>Удобный скачиваемый клиент для Windows и Android</span>
                <a data-href="//sloto.top/casino/casino-vip-net-game" class="btn-green">Читать обзор</a>
            </div>
            <img src="/img/slider/netgame.jpg" alt="20 долларов бездепа от казино НетГейм">
          </li>
		  <li>
            <div class="slider-text">
                <span class="text-slider-h"><span>Турниры</span> на слотах<br />в казино ReelEmperor</span>
                <span class="slider-text-sp"><br>Обходи других игроков в таблице рейтингов<br>и выигрывай сумасшедшие деньги в азартных турнирах!</span>
                <a href="http://sloto.top/casino/casino-reel-emperor" class="btn-green">Читать обзор</a>
            </div>
            <img src="/img/slider/reel.jpg" alt="20 долларов бездепа от казино НетГейм">
          </li>
        </ul>
    </div>

    <div class="block-all">
        <div class="bl-red-title">
            <span class="bl-img"></span>
            <div class="bl-t-title">
                <span>КАЗИНО</span>
                <a href="<?= Url::to(['/casino/casino/index']) ?>">ВСЕ КАЗИНО ></a>
            </div>

            <div class="clear"></div>
        </div>

        <div class="games">
            <?php foreach($casino as $item): ?>
                <?= \app\modules\casino\widgets\CasinoItem::widget(['model' => $item]) ?>
            <?php endforeach; ?>
            <div class="clear"></div>
        </div>

        <?php if ($seo = \common\models\seotext\SeoText::getSeoMainCasino()): ?>
        <div class="block-all">
            <div class="roll-up-block roll-up-reverse">
                <span class="roll-up-block-arrow"></span>
                <span>Свернуть описание</span>
            </div>
            <div class="roll-block">
                <?= $seo->description ?>
            </div>
        </div>
        <?php endif ?>

    </div>

    <div class="block-all">
        <div class="bl-red-title">
            <span class="bl-img"></span>
            <div class="bl-t-title">
                <span>БОНУСЫ</span>
                <a href="<?= Url::to(['/bonuses/bonuses/index']) ?>">ВСЕ БОНУСЫ ></a>
            </div>

            <div class="clear"></div>
        </div>

        <div class="games">
            <?php foreach($bonuses as $item): ?>
                <?= \app\modules\bonuses\widgets\BonusItem::widget(['model' => $item]) ?>
            <?php endforeach; ?>

            <div class="clear"></div>
        </div>

        <?php if ($seo = \common\models\seotext\SeoText::getSeoMainBonuses()): ?>
        <div class="block-all">
            <div class="roll-up-block roll-up-reverse">
                <span class="roll-up-block-arrow"></span>
                <span>Развернуть описание</span>
            </div>
            <div class="roll-block" style="display: none;">
                <?= $seo->description ?>
            </div>
        </div>
        <?php endif ?>

    </div>

    <div class="block-all">
        <div class="bl-red-title">
            <span class="bl-img"></span>
            <div class="bl-t-title">
                <span>ИГРЫ</span>
                <a href="<?= Url::to(['/games/games/index']) ?>">ВСЕ ИГРЫ ></a>
            </div>

            <div class="clear"></div>
        </div>

        <div class="games">
            <?php foreach($games as $item): ?>
                <?= \app\modules\games\widgets\GameItem::widget(['model' => $item]) ?>
            <?php endforeach; ?>

            <div class="clear"></div>
        </div>

        <?php if ($seo = \common\models\seotext\SeoText::getSeoMainGames()): ?>
        <div class="block-all">
            <div class="roll-up-block roll-up-reverse">
                <span class="roll-up-block-arrow"></span>
                <span>Развернуть описание</span>
            </div>
            <div class="roll-block" style="display: none;">
                <?= $seo->description ?>
            </div>
        </div>
        <?php endif ?>

    </div>


    <div class="line"></div>
    <?php if ($seo_main): ?>
        <?php echo $seo_main->description; ?>
    <?php endif; ?>

</div>

<?php $this->beginBlock('widgets'); ?>
    <div>
        <?php echo \app\widgets\NewCasino::widget() ?>
        <?php echo \app\widgets\NewBonuses::widget() ?>
        <?php echo \app\widgets\NewGames::widget() ?>
    </div>
<?php $this->endBlock(); ?>