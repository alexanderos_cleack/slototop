<div class="float-left">
    <div class="block-all">
        <div class="bl-red-title">
            <span class="bl-img"></span>
            <div class="bl-t-title">
                <span>ошибка 404</span>
            </div>

            <div class="clear"></div>
        </div>
    </div>

    <div class="block-all chr-casino-block">
        <div class="contacts-block">
            <div class="float-left">
                <img src="/img/404.png" alt="">
            </div>

            <div class="float-right">
                <div class="chr-other-title">Извините, страница не найдена</div>
                <p>Возможно, эта страница была удалена либо допущена ошибка в адресе. Вы можете воспользоваться поиском или просмотреть категории.</p>
                <a class="btn-green" href="/">На главную!</a>
            </div>

            <div class="clear"></div>
        </div>
    </div>
</div>