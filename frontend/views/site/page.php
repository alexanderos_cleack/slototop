<?php
/**
 * @var \yii\base\View $this
 * @var \common\models\pages\Pages $modelPage
 * @var
 */

$this->title = $modelPage->meta_title;
$this->registerMetaTag(['name' => 'keywords', 'content' => $modelPage->meta_keywords]);
$this->registerMetaTag(['name' => 'description', 'content' => $modelPage->meta_description]);
?>

<div class="float-left contact-page">
    <div class="block-all">
        <div class="bl-red-title">
            <span class="bl-img"></span>
            <div class="bl-t-title">
                <span><?php echo $modelPage->title; ?></span>
            </div>

            <div class="clear"></div>
        </div>
    </div>

    <div class="block-all">
        <?php echo $modelPage->description; ?>
    </div>
</div>
