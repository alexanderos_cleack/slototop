<?php

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \common\models\seotext\SeoText $seo
 */

?>

<?php
    if ($seo) {
        $this->title = $seo->meta_title;
        $this->registerMetaTag(['name' => 'keywords', 'content' => $seo->meta_keywords]);
        $this->registerMetaTag(['name' => 'description', 'content' => $seo->meta_description]);
    }
?>

<div class="float-left">
    <div class="block-all">
        <div class="bl-red-title">
            <span class="bl-img"></span>
            <div class="bl-t-title">
                <span><?= $seo ? $seo->meta_h1 : Yii::t('app/f', 'Search') ?></span>
            </div>
            <div class="clear"></div>
        </div>

        <div class="games">
            <?php $form = \yii\widgets\ActiveForm::begin(['action' => Url::to(['/site/search']), 'method' => 'get']); ?>
            <div class="form-group">
                <?php echo $form->field(new \app\models\BaseSearch, 'search')->label(false) ?>
            </div>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app/f', 'ПОИСК'), ['class' => 'btn btn-green']) ?>
            </div>
            <?php \yii\widgets\ActiveForm::end(); ?>
        </div>
    </div>

    <div class="line"></div>
    <?php if ($seo): ?>
        <?php echo $seo->description; ?>
    <?php endif; ?>
</div>