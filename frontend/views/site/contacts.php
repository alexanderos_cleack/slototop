<?php

/**
 * @var  $this yii\web\View
 */

?>

<?php

    \app\assets\FormstylerAsset::register($this);

    if ($seo) {
        $this->title = $seo->meta_title;
        $this->registerMetaTag(['name' => 'keywords', 'content' => $seo->meta_keywords]);
        $this->registerMetaTag(['name' => 'description', 'content' => $seo->meta_description]);
    }

?>

<div class="float-left contact-page">
    <div class="block-all">
        <div class="bl-red-title">
            <span class="bl-img bl-img-mail"></span>
            <div class="bl-t-title">
                <span>напишите нам!</span>
            </div>

            <div class="clear"></div>
        </div>
    </div>  

    <div class="block-all grey-border-bottom bonus-page">
        <div class="contacts-select">
            <div class="float-left-c">
                Выберите цель обращения:
            </div>

            <div class="float-right-c">
                <div class="jq-selectbox jqselect" style="display: inline-block; position: relative; z-index: 100;"><select style="margin: 0px; padding: 0px; position: absolute; left: 0px; top: 0px; width: 100%; height: 100%; opacity: 0;">
                     <option selected="">Написать письмо</option>
                     <option value="x2">Написать письмо</option>
                     <option value="x3">Написать письмо</option>
                     <option value="x4">Написать письмо</option>
                </select><div class="jq-selectbox__select" style="position: relative"><div class="jq-selectbox__select-text">Написать письмо</div><div class="jq-selectbox__trigger"><div class="jq-selectbox__trigger-arrow"></div></div></div><div class="jq-selectbox__dropdown" style="position: absolute; left: 0px; display: none; height: auto; bottom: auto; top: 50px;"><div class="jq-selectbox__search" style="display: none;"><input type="search" autocomplete="off" placeholder="Поиск..."></div><div class="jq-selectbox__not-found" style="display: block;">Совпадений не найдено</div><ul style="position: relative; list-style: none; overflow-y: auto; overflow-x: hidden; max-height: 322px;"><li class="sel selected">Написать письмо</li><li>Написать письмо</li><li>Написать письмо</li><li>Написать письмо</li></ul></div></div>
            </div>

            <div class="clear"></div>
        </div>
        <div class="contacts-form">
            <form method="post">
                <div class="label cf-theme">
                    <input type="text" placeholder="Тема">
                </div>

                <div class="label cf-mail">
                    <input type="text" placeholder="Ваш email">
                </div>

                <div class="clear"></div>

                <div class="label cf-post">
                    <textarea placeholder="Сообщение"></textarea>
                </div>

                <div class="label cf-button">
                    <span>Напишите нам и мы постараемся ответить Вам как можно скорее!</span>
                    <button class="btn-red">ОТПРАВИТЬ</button>

                    <div class="clear"></div>
                </div>
            </form>
        </div>
    </div>

    <div class="block-all">
        <p>С помощью этой формы Вы можете <a href="#">написать нам письмо</a>, <a href="#">прислать нам обзор своего казино</a>, <a href="#">заказать обзор казино</a>, <a href="#">заказать рекламу</a> на нашем сайте. Мы ждем Ваших обращений!</p>
    </div>

    <div class="block-all">
        <div class="bl-red-title">
            <span class="bl-img bl-img-map"></span>
            <div class="bl-t-title">
                <span>Карта</span>
            </div>

            <div class="clear"></div>
        </div>
    </div>

    <div class="block-all contacts-map">   
       
<style>
    #map-canvas {
        height: 450px;
        width:100%;
        margin: 0px;
        padding: 0px
      }
</style>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBEe435oCcblO3iiAzMJub8IIhyD2JSeVA&callback=initMap"
  type="text/javascript"></script>
<div id="map-canvas"></div> 

<script src="https:/maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
    <script>
    var map;
    function initialize() {

    var myLatlng = new google.maps.LatLng(50.469756, 30.508464);

  var mapOptions = {
    zoom: 15,
    center: new google.maps.LatLng(50.469756, 30.508464)
  };
  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

  var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: 'SlotoTop',
  });



}

google.maps.event.addDomListener(window, 'load', initialize);

    </script>


    </div>
</div>

<div class="float-right contact-page">
    <div class="block-sidebar sidebar-banner">
        <img src="./img/contacts/banner.png" alt="">
    </div>

    <div class="block-sidebar">
        <div class="bl-red-title">
            <span class="bl-img bl-map"></span>
            <div class="bl-t-title">
                <span>Наши контакты</span>
            </div>

            <div class="clear"></div>
        </div>

        <div class="block-casino">
            <!-- <span class="ct-sidebar-text">Адрес:</span> -->
        </div>

        <div class="block-casino">
            <span class="ct-sidebar-text">Email:</span>
            Andrey@sloto.top
        </div>

        <div class="block-casino">
            <span class="ct-sidebar-text">Мы в соцсетях:</span> 
            <img src="./img/contacts/soc.png" alt=""> 
        </div>
    </div>


    <div class="block-sidebar">
        <div class="bl-red-title">
            <span class="bl-img bl-team"></span>
            <div class="bl-t-title">
                <span>КОМАНДА</span>
            </div>

            <div class="clear"></div>
        </div>

        <div class="block-casino">
            <div class="float-left">
                <img src="./img/contacts/photo.png" alt="">
            </div>
            <div class="float-right">
                <span><b>Андрей Журов</b></span>
                <p>главный редактор <br> Гуру в сфере гэмблинга.</p>
				<p>04071, Украина, г. Киев, ул. Введенская, 15</p>
                
            </div>

            <div class="clear"></div>
        </div>
    </div>
</div>

<div class="clear"></div>