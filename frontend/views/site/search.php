<?php
/**
 * @var yii\web\View $this
 * @var common\models\casino\Casino[] $casino
 * @var \common\models\slots\Slots[] $games
 * @var \common\models\bonuses\Bonuses[] $bonuses
 * @var \common\models\vendors\Vendors[] $vendors
 * @var \common\models\methods\MethodsPayment[] $payments
 */
?>

<?php

    $this->registerMetaTag(['name' => 'robots', 'content' => 'noindex,nofollow']);

?>

<div class="float-left">
    <div class="block-all">
        <div class="bl-red-title">
            <span class="bl-img"></span>
            <div class="bl-t-title">
                <span><?= Yii::t('app/f', 'Search') ?></span>
                <div class="bl-all-count"><?= Yii::t('app/f', 'Total found: {count}', ['count' => $count]) ?></div>

                <div class="bl-sort">
                    <a href="#" class="bl-sort-active"></a>
                    <a href="#"></a>
                </div>
            </div>

            <div class="clear"></div>
        </div>


        <div class="games">
            <?php foreach ($casino as $item): ?>
                <?= \app\modules\casino\widgets\CasinoItem::widget(['model' => $item]) ?>
            <?php endforeach; ?>

            <?php foreach ($games as $item): ?>
                <?= \app\modules\games\widgets\GameItem::widget(['model' => $item]) ?>
            <?php endforeach; ?>

            <?php foreach ($bonuses as $item): ?>
                <?= \app\modules\bonuses\widgets\BonusItem::widget(['model' => $item]) ?>
            <?php endforeach; ?>

            <?php foreach ($vendors as $item): ?>
                <?= \app\modules\vendors\widgets\VendorItem::widget(['model' => $item]) ?>
            <?php endforeach; ?>

            <?php foreach ($payments as $item): ?>
                <?= \app\modules\methods\widgets\MethodPaymentItem::widget(['mdoel' => $item]) ?>
            <?php endforeach; ?>

            <div class="clear"></div>
        </div>

    </div>
</div>