<?php

use yii\helpers\Html;

/**
 * @var \yii\web\View $this
 * @var string $content
 */

\app\assets\CasinoRedirect::register($this);

$this->beginPage()
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <title><?php echo $this->title ? Html::encode($this->title) : Yii::$app->name ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?= $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>

    <!-- particles.js container -->
    <div id="particles-js"></div>
    <!-- stats - count particles -->

    <?= $content; ?>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>