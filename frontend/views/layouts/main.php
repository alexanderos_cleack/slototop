<?php

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var string $content
 * @var \app\models\ABaseSort $sort
 */
?>

<?php \app\assets\AppAsset::register($this); ?>
<?php \app\assets\FormstylerAsset::register($this); ?>

<?php $this->beginPage()?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <title><?php echo $this->title ? Html::encode($this->title) : Yii::$app->name ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="yandex-verification" content="7dc13cd524c7c7ba">
    <?= Html::csrfMetaTags() ?>
    <?= $this->head() ?>
</head>
<body>
    <?php if ($_SERVER['SERVER_ADDR'] !== '127.0.0.1'): ?>
        <script type="text/javascript">window.onload = new function(){var hd = document.getElementsByTagName('head').item(0);var js = document.createElement('script'); js.setAttribute('language', 'javascript'); js.setAttribute('src', '//certify.gpwa.org/script/sloto.top/'); hd.appendChild(js); return false;}</script>
    <?php endif; ?>
    <?php $this->beginBody() ?>

    <div id="wrapper">
        <div class="left-overflow"></div>
        <div id="content">

            <?php if (\app\helpers\AdminHelper::isAllowed()): ?>
                <?php if (isset($this->blocks['admin'])): ?>
                    <?php \app\assets\AdminAsset::register($this); ?>
                    <div class="admin">
                        <?= $this->blocks['admin'] ?>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <!-- HEADER -->
            <div id="header">
                <a data-href="/" class="logo">
                    <img src="/img/logo.png" alt="Slototop logo">
                </a>

                <?= \app\widgets\Menu::widget() ?>

                <?php if (false): ?>
                <div class="select-country">
                    <div class="select-country-text">
                        <img src="/img/header/select.png" alt="">
                        <span><?= Yii::t('app/f', 'Ваша страна') ?>:</span>
                    </div>

                    <select>
                         <option selected>Украина</option>
                         <option value="x2">USA</option>
                         <option value="x3">Canada</option>
                         <option value="x4">Russia</option>
                    </select>
                </div>
                <?php endif; ?>

                <div class="form-hide">
                    <span class="mobile-menu-btn">
                        <span class="bar bar-1"></span>
                        <span class="bar bar-2"></span>
                        <span class="bar bar-3"></span>
                    </span>
                </div>

                <div class="clear"></div>
            </div>

            <div id="sidebar-left">
                <ul class="sd-left">
                    <li>
                        <span class="sd-tab">
                            <span>
                                <img src="/img/left/ico3.png" alt=""> <br>
                                <i><?= Yii::t('app/f', 'Поиск') ?></i>
                            </span>
                        </span>
                    </li>

                    <li>
                        <?php if (isset($this->params['active_sort'])): ?>
                        <span class="sd-tab">
                            <span>
                                <img src="/img/left/ico4.png" alt=""> <br>
                                <i><?= Yii::t('app/f', 'Сортировка') ?></i>
                            </span>
                        </span>
                        <?php endif; ?>
                    </li>

                    <li>
                        <?php if (isset($this->params['active_filter'])): ?>
                        <span class="sd-tab">
                            <span>
                                <img src="/img/left/ico5.png" alt=""> <br>
                                <i><?= Yii::t('app/f', 'Фильтр') ?></i>
                            </span>
                        </span>
                        <?php endif; ?>
                    </li>

                </ul>

                <ul class="sd-left sd-left-bottom">
                    <li>
                        <span class="sd-tab-no-active">
                            <span>
                                <img src="/img/left/ico-b.png" alt="Slototop mini logo">
                            </span>
                        </span>
                    </li>
                </ul>
            </div>

            <div class="show-sidebar">
                <div class="show-sidebar-in">
                    <div class="sb-in-filters">
                        <div class="sb-title">
                            Поиск:
                        </div>

                        <?php $form = \yii\widgets\ActiveForm::begin(['action' => Url::to(['/site/search']), 'method' => 'get']); ?>
                            <div class="label">
                                <span class="sb-label-title">Поиск:</span>
                                <?php echo $form->field(new \app\models\BaseSearch, 'search')->label(false) ?>
                            </div>

                            <div class="label">
                                <?= Html::submitButton(Yii::t('app/f', 'ПОИСК'), ['class' => 'btn-green']) ?>
                            </div>
                        <?php \yii\widgets\ActiveForm::end(); ?>
                    </div>

                    <div class="sb-in-filters">
                        <?php if (isset($this->params['active_sort'])): ?>
                            <?php $sort = $this->params['sort']; ?>
                            <div class="sb-title">
                                <?= Yii::t('app/f', 'СОРТИРОВАТЬ ПО') ?>:
                            </div>

                            <?php if (isset($this->params['active_sort_rating'])): ?>
                                <div class="label">
                                    <span class="sort-title"><?= Yii::t('app/f', 'По популярности') ?>:</span>
                                    <ul class="sort-on sort-pop">
                                        <li><a href="<?= $sort->getSortRatingAscUrl(); ?>"><span class="<?php echo $sort->isRatingAsc() ? 'sort-stars-active-top' : 'sort-stars-no-top'; ?>"></span></a></li>
                                        <li><a href="<?= $sort->getSortRatingDescUrl(); ?>"><span class="<?php echo $sort->isRatingDesc() ? 'sort-stars-active-bot' : 'sort-stars-no-bot'; ?>"></span></a></li>
                                    </ul>
                                </div>
                            <?php endif; ?>

                            <?php if (isset($this->params['active_sort_new'])): ?>
                                <div class="label">
                                    <span class="sort-title"><?= Yii::t('app/f', 'По новизне') ?>:</span>
                                    <ul class="sort-on sort-new">
                                        <li><a href="<?= $sort->getSortNewAscUrl(); ?>"><span class="<?php echo $sort->isNewAsc() ? 'sort-new-active-top' : 'sort-new-no-top'; ?>"></span></a></li>
                                        <li><a href="<?= $sort->getSortNewDescUrl(); ?>"><span class="<?php echo $sort->isNewDesc() ? 'sort-new-active-bot' : 'sort-new-no-bot'; ?>"></span></a></li>
                                    </ul>
                                </div>
                            <?php endif; ?>

                            <?php if (isset($this->params['active_sort_title'])): ?>
                                <div class="label">
                                    <span class="sort-title"><?= Yii::t('app/f', 'По алфавиту') ?>:</span>
                                    <ul class="sort-on sort-alph">
                                        <li><a href="<?= $sort->getSortTitleAscUrl(); ?>"><span class="<?php echo $sort->isTitleAsc() ? 'sort-alph-active-top' : 'sort-alph-no-top'; ?>"></span></a></li>
                                        <li><a href="<?= $sort->getSortTitleDescUrl(); ?>"><span class="<?php echo $sort->isTitleDesc() ? 'sort-alph-active-bot' : 'sort-alph-no-bot'; ?>"></span></a></li>
                                    </ul>
                                </div>
                            <?php endif; ?>

                        <?php endif; ?>
                    </div>

                    <?php if (isset($this->blocks['filter'])): ?>
                        <?= $this->blocks['filter'] ?>
                    <?php endif; ?>

                </div>

                <div class="clear"></div>
            </div>


            <div id="main-content">

                <?php if (isset($this->blocks['seo'])): ?>
                    <div id='seoTextWidget'>
                        <?= $this->blocks['seo'] ?>
                    </div>
                <?php endif; ?>

                <div class="center">
                    <?= $content ?>

                    <div class="float-right">
                        <?php echo \app\widgets\Banners::widget() ?>
                        <?php if (isset($this->blocks['widgets'])): ?>
                            <?= $this->blocks['widgets'] ?>
                        <?php endif; ?>
                    </div>

                    <div class="clear"></div>
                </div>
            </div>
        </div>

        <div class="page-buffer"></div>
    </div>

    <div id="footer">
        <div class="footer-center">
            <ul>
                <li><a href="<?= Url::to(['/casino/casino/index']) ?>" <?php echo $this->context->id == 'casino' ? 'class="active"' : '' ?>><?= Yii::t('app/f', 'Casino') ?></a></li>
                <li><a href="<?= Url::to(['/games/games/index']) ?>" <?php echo $this->context->id == 'games' ? 'class="active"' : '' ?>><?= Yii::t('app/f', 'Games') ?></a></li>
                <li><a href="<?= Url::to(['/bonuses/bonuses/index']) ?>" <?php echo $this->context->id == 'bonuses' ? 'class="active"' : '' ?>><?= Yii::t('app/f', 'Bonuses') ?></a></li>
                <li><a href="<?= Url::to(['/vendors/vendors/index']) ?>" <?php echo $this->context->id == 'vendors' ? 'class="active"' : '' ?>><?= Yii::t('app/f', 'Vendors') ?></a></li>
                <li><a href="<?= Url::to(['/methods/payments/index']) ?>" <?php echo $this->context->id == 'payments' ? 'class="active"' : '' ?>><?= Yii::t('app/f', 'Payment system') ?></a></li>
                <li><a href="<?= Url::to(['/contacts']) ?>"><?= Yii::t('app/f', 'Contacts') ?></a></li>
            </ul>
            <div class="footer-copyright">
                <table>
                    <tr align="center">
                        <td><?php echo date('Y') ?> - SlotoTOP<br><a href="//certify.gpwa.org/verify/sloto.top/" onclick="return GPWAVerificationPopup(this)" id="GPWASeal"><img src="//certify.gpwa.org/seal/sloto.top/"  onError="this.width=0; this.height=0;"  border="0" alt="GPWA Seal of Approval" /></a></td>
                        <td><a href="//www.dmca.com/Protection/Status.aspx?ID=0ea4733a-db2f-4451-9461-3dab7f8e424d" title="DMCA.com Protection Status" class="dmca-badge"> <img src="//images.dmca.com/Badges/DMCA_badge_grn_60w.png?ID=0ea4733a-db2f-4451-9461-3dab7f8e424d" alt="DMCA.com Protection Status"></a> <script src="//images.dmca.com/Badges/DMCABadgeHelper.min.js"> </script></td>
                    </tr>
                </table>
            </div>

            <div class="clear"></div>
        </div>
    </div>

    <?php $this->endBody() ?>

    <div id="scrollup">
        <img alt="Прокрутить вверх" src="/img/up-arrow.png" style="">
    </div>

    <script type="text/javascript">
      jQuery( document ).ready(function() {
        jQuery('#scrollup img').mouseover( function(){
            jQuery( this ).animate({opacity: 0.65},100);
        }).mouseout( function(){
            jQuery( this ).animate({opacity: 1},100);
        }).click( function(){
            window.scroll(0 ,0);
            return false;
        });

        jQuery(window).scroll(function(){
            if ( jQuery(document).scrollTop() > 0 ) {
                jQuery('#scrollup').fadeIn('fast');
            } else {
                jQuery('#scrollup').fadeOut('fast');
            }
        });
    });
    </script>

    <?php if ($_SERVER['SERVER_ADDR'] !== '127.0.0.1'): ?>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-83912563-1', 'auto');
            ga('send', 'pageview');
        </script>

        <script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>
    <?php endif; ?>

</body>
</html>
<?php $this->endPage() ?>