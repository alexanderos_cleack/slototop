<?php

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Class InitAsset
 * @package app\assets
 */
class InitAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '/';

    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD,
    ];

    public $js = [
        'js/jquery-2.1.3.min.js',
    ];

    public $depends = [
        YiiAsset::class,
    ];

}
