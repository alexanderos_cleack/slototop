<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class AppAsset
 * @package app\assets
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';

    public $baseUrl = '/';

    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD,
    ];

    public $css = [
        'css/styles.css',
        'css/responsive.css?v=1.00',
        'css/slider.css',
        'css/custom.css?v=1.00',
        'css/new_changes.css?v=1.01',
    ];

    public $js = [
        'js/main.js?v=1.00',
        'js/slide.js',
        'js/custom.js?v=1.01',
    ];

    public $depends = [
        InitAsset::class,
    ];

}