<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class RatingAsset
 * @package app\assets
 */
class RatingAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '/';

    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD,
    ];

    public $css = [
        'css/jquery.rating.css',
    ];

    public $js = [
        'js/jquery.rating-2.0.min.js',
        'js/rating.js',
    ];

    public $depends = [
        AppAsset::class,
    ];

}
