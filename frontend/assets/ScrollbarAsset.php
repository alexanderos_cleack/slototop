<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class ScrollbarAsset
 * @package app\assets
 */
class ScrollbarAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '/';

    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD,
    ];

    public $css = [
        'css/jquery.mCustomScrollbar.css',
    ];

    public $js = [
        'js/jquery.mCustomScrollbar.concat.min.js',
        'js/scrollbar.js',
    ];

    public $depends = [
        AppAsset::class,
    ];

}
