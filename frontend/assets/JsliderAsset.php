<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class JsliderAsset
 * @package app\assets
 */
class JsliderAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '/';

    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD,
    ];

    public $css = [
        '/css/jslider.css',
    ];

    public $js = [
        '/js/jshashtable-2.1_src.js',
        '/js/jquery.numberformatter-1.2.3.js',
        '/js/tmpl.js',
        '/js/jquery.dependClass-0.1.js',
        '/js/draggable-0.1.js',
        '/js/jquery.slider.js',
    ];

    public $depends = [
        AppAsset::class,
    ];

}