<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class AppAsset
 * @package app\assets
 */
class CasinoRedirect extends AssetBundle
{
    public $basePath = '@webroot';

    public $baseUrl = '/';

    public $jsOptions = [
        'position' => \yii\web\View::POS_END,
    ];

    public $css = [
        'css/casinoRedirect.css?v=1.00'
    ];

    public $js = [
        'js/particles.js?V=1.00',
        'js/load.js?V=1.00',
    ];

    public $depends = [
        InitAsset::class,
    ];

}