<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class PowerangeAsset
 * @package app\assets
 */
class PowerangeAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '/';

    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD,
    ];

    public $css = [
        'css/plugin.powerange.css',
    ];

    public $js = [
        'js/plugin.powerange.js',
    ];

    public $depends = [
        AppAsset::class,
    ];

}