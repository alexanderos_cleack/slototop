<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class FormstylerAsset
 * @package app\assets
 */
class FormstylerAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '/';

    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD,
    ];

    public $css = [
        'css/jquery.formstyler.css',
    ];

    public $js = [
        'js/jquery.formstyler.min.js',
    ];

    public $depends = [
        AppAsset::class,
    ];

}