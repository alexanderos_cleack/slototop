<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class AdminAsset
 * @package app\assets
 */
class AdminAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '/';

    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD,
    ];

    public $css = [
        'css/admin.css',
    ];

    public $depends = [
        AppAsset::class,
    ];

}