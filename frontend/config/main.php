<?php

// Config frontend

$config = [
    'id' => 'frontend_app',
    'basePath' => '@frontend',

    'catchAll' => [
        // 'site/offline',
    ],

    'homeUrl' => '/',

    'components' => [
        'user' => [
            'identityClass' => \backend\models\admins\Admins::class,
            'enableAutoLogin' => true,
        ],

        'request' => [
            'cookieValidationKey' => 'aupRf2pZsEzrZckeyMuc',
        ],

        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'baseUrl' => '/',
            'suffix' => '/',
            'normalizer' => [
                'class' => \yii\web\UrlNormalizer::class,
                'action' => \yii\web\UrlNormalizer::ACTION_REDIRECT_TEMPORARY,
            ],
            'rules' => [
                ['pattern' => 'sitemap.xml', 'route' => 'sitemap/default/index', 'suffix' => false],
                ['pattern' => 'sitemap-home.xml', 'route' => 'sitemap/default/home', 'suffix' => false],
                ['pattern' => 'sitemap-casino.xml', 'route' => 'sitemap/default/casino', 'suffix' => false],
                ['pattern' => 'sitemap-games.xml', 'route' => 'sitemap/default/games', 'suffix' => false],
                ['pattern' => 'sitemap-bonuses.xml', 'route' => 'sitemap/default/bonuses', 'suffix' => false],
                ['pattern' => 'sitemap-vendors.xml', 'route' => 'sitemap/default/vendors', 'suffix' => false],
                ['pattern' => 'sitemap-payments.xml', 'route' => 'sitemap/default/payments', 'suffix' => false],

                ['pattern' => 'sitemap-filter-casino.xml', 'route' => 'sitemap/default/filter-casino', 'suffix' => false],
                ['pattern' => 'sitemap-filter-games.xml', 'route' => 'sitemap/default/filter-games', 'suffix' => false],
                ['pattern' => 'sitemap-filter-bonuses.xml', 'route' => 'sitemap/default/filter-bonuses', 'suffix' => false],
                ['pattern' => 'sitemap-filter-vendors.xml', 'route' => 'sitemap/default/filter-vendors', 'suffix' => false],

                'test/<action:[\w-]+>' => 'developer/<action>',

                'contacts' => 'site/contacts',
                'search' => 'site/search',
                'gpwa' => 'site/gpwa',

                'casino' => 'casino/casino/index',
                'casino/items' => 'casino/casino/items',
                'casino/filters' => 'casino/casino/filters',
                'rating/casino/<slug:[\w-]+>' => 'casino/casino/rating',
                'casino/game/<game:[\w-]+>' => 'casino/casino/game',
                'casino/same/<casino:[\w-]+>' => 'casino/casino/same',
                'casino/payment/<method:[\w-]+>' => 'casino/casino/payment',
                'casino/popular/vendor/<vendor:[\w-]+>' => 'casino/casino/vendor-popular-casino',
                'casino/filter/<slug:[\w-]+>' => 'casino/casino/filter',
                'casino/sort/<slug:[\w-]+>' => 'casino/casino/sort',
                'casino/r/<slug:[\w-]+>' => 'casino/casino/redirect',
                'casino/<slug:[\w-]+>' => 'casino/casino/show',

                'games' => 'games/games/index',
                'games/items' => 'games/games/items',
                'games/filters' => 'games/games/filters',
                'rating/game/<slug:[\w-]+>' => 'games/games/rating',
                'games/same/<slug:[\w-]+>' => 'games/games/same',
                'games/popular/<casino:[\w-]+>' => 'games/games/casino',
                'games/filter/<slug:[\w-]+>' => 'games/games/filter',
                'games/sort/<slug:[\w-]+>' => 'games/games/sort',
                'games/<vendor:[\w-]+>' => 'games/games/vendor',
                'game/<slug:[\w-]+>' => 'games/games/show',

                'vendors' => 'vendors/vendors/index',
                'vendors/items' => 'vendors/vendors/items',
                'vendors/filters' => 'vendors/vendors/filters',
                'vendors/filter/<slug:[\w-]+>' => 'vendors/vendors/filter',
                'vendors/sort/<slug:[\w-]+>' => 'vendors/vendors/sort',
                'vendor/<slug:[\w-]+>' => 'vendors/vendors/show',

                'bonuses' => 'bonuses/bonuses/index',
                'bonuses/items' => 'bonuses/bonuses/items',
                'bonuses/filters' => 'bonuses/bonuses/filters',
                'bonuses/casino/vendor/<vendor:[\w-]+>' => 'bonuses/bonuses/vendor-bonuses-casino',
                'bonuses/casino/<casino:[\w-]+>' => 'bonuses/bonuses/casino',
                'bonuses/same/<bonus:[\w-]+>' => 'bonuses/bonuses/same',
                'bonuses/filter/<slug:[\w-]+>' => 'bonuses/bonuses/filter',
                'bonuses/sort/<slug:[\w-]+>' => 'bonuses/bonuses/sort',
                'bonus/<slug:[\w-]+>' => 'bonuses/bonuses/show',

                'payments' => 'methods/payments/index',
                'payments/items' => 'methods/payments/items',
                'payments/filter/<slug:[\w-]+>' => 'methods/payments/filter',
                'payments/sort/<slug:[\w-]+>' => 'methods/payments/sort',
                'payment/<slug:[\w-]+>' => 'methods/payments/show',

                '<slug:[\w-]+>' => 'site/page',
            ],
        ],

        'assetManager' => [
            'baseUrl' => '/assets',
//            'assetMap' => [
//                'jquery.js' => '/js/jquery.js',
//                'yii.js' => '/js/yii.js',
//                'yii.activeForm.js' => '/js/yii.activeForm.js',
//                'yii.validation.js' => '/js/yii.validation.js',
//                'custom.css' => '/css/mini/custom.css',
//                'jquery.formstyler.css' => '/css/mini/jquery.formstyler.css',
//                'responsive.css' => '/css/mini/responsive.css',
//                'slider.css' => '/css/mini/slider.css',
//                'styles.css' => '/css/mini/styles.css',
//           ],
        ],

    ],

    'modules' => [
        'casino' => [
            'class' => \app\modules\casino\Module::class,
        ],
        'games' => [
            'class' => \app\modules\games\Module::class,
        ],
        'bonuses' => [
            'class' => \app\modules\bonuses\Module::class,
        ],
        'vendors' => [
            'class' => \app\modules\vendors\Module::class,
        ],
        'methods' => [
            'class' => \app\modules\methods\Module::class,
        ],
        'sitemap' => [
            'class' => \app\modules\sitemap\Module::class,
        ],
    ],

];

return $config;