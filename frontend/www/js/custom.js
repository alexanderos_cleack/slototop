$(document).ready(function () {
    $('[data-href]').on('click', function () {
        var href = $(this).data('href'),
            target = $(this).attr('target');

        open(href, target || '_self');
    });
});