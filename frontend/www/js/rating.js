function rating(selector, controller, slug) {
    $(selector).rating({
        fx: 'float',
        image: '/img/rating/stars.png',
        loader: '/img/rating/ajax-loader.gif',
        minimal: 1,
        url: '/rating/' + controller + '/' + slug,
        readOnly: $(selector + ' input').val() ? true : false,
    });
}

