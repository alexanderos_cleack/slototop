$(document).ready(function() {

    setTimeout(function() {
        $(".sb-in-filters").css("display", "none");
    }, 1);

    $(".select-country select, .contacts-select select, .sb-in-filters select, input[type='checkbox'], input[type='radio']").styler({
        selectSearch: true
    });

    $('.bxslider').bxSlider({
        auto: true
    });

    $("span.sd-tab").click(function() {
        $(".sd-left li span.sd-tab").not(this).removeClass("sd-active");
        if($(this).hasClass("sd-active")) {
            $(this).removeClass("sd-active");
            $(".show-sidebar").removeClass("show-this-sd");
        } else {
            $(this).addClass("sd-active");
            $(".show-sidebar").addClass("show-this-sd");
        }

        $(".show-sidebar-in .sb-in-filters").removeClass("filters-show");
        $(".show-sidebar-in .sb-in-filters").eq($(this).parent().index()).addClass("filters-show");
    });

    $(".sd-inactive").each(function() {
        $(this).unbind("click");
    });

    $(".form-hide").click(function() {
        $(this).toggleClass("menu-open");

        if($(this).hasClass("menu-open")) {
            $("#header ul.main-menu").slideDown(300);
        } else {
            $("#header ul.main-menu").slideUp(300);
        }
    });

    $(".roll-up-block").click(function() {
        $(this).toggleClass("roll-up-reverse");

        if($(this).hasClass("roll-up-reverse")) {
            $(this).find("span:last-child").text("Развернуть описание");
        } else {
            $(this).find("span:last-child").text("Свернуть описание");
        }

        $(this).next().slideToggle(300);
    });


    /* Sort games (start) */

    $(".bl-sort a").click(function(e) {
        e.preventDefault();

        $(".bl-sort a").removeClass("bl-sort-active");
        $(this).addClass("bl-sort-active");
    });

    /* Sort games (end) */

    /* Powerange (start) */

    // Callback.
    /*var clbk = document.querySelector('.js-callback');
    var initClbk = new Powerange(clbk, {
        callback: displayValue,
        start: 20,
        max: 100,
        klass: "test"
    });

    function displayValue() {
    	document.getElementById('js-display-callback').innerHTML = clbk.value + "%";
    	$("#js-display-callback").css("left", $("." + this.klass).find(".range-handle").css("left"));
    }*/

    /* Powerange (end) */

    /* Sort btns (start) */

    $(".label .sort-on span.sort-in").click(function() {
        $(this).parent().parent().find("span").removeClass("sort-active");
        $(this).addClass("sort-active");
    });

    /* Sort btns (end) */


    /* Select checkbox (start) */

    $(".dropdown-ch a").on('click', function () {
        $(".dropdown-ch ul").slideToggle(0);
    });

    $(".dropdown-ch ul li a").on('click', function () {
        $(".dropdown-ch ul").hide();
    });

    function getSelectedValue(id) {
        return $("#" + id).find("a span.value").html();
    }

    $(document).bind('click', function(e) {
        var $clicked = $(e.target);
        if (!$clicked.parents().hasClass("dropdown-ch")) $(".dropdown-ch ul").hide();
    });

    $('.mutliSelect .jq-checkbox').on('click', function() {
        var title2 = $(this).find('input[type="checkbox"]').val(),
            title = title2 + ",";

        if ($(this).hasClass('checked')) {
            console.log(title);
            $(".multiSel").show();
            var html = '<span title="' + title + '">' + title + '</span>';
            $('.multiSel').append(html);
            $(".hida").hide();
        } else {
            $('span[title="' + title + '"]').remove();
            var ret = $(".hida");
            $('.dropdown-ch a').append(ret);
        }
    });

    /* Select checkbox (end) */

    $(".hover-show-list .menu-games").click(function() {
        if($(window).width() <= 1250) {
            $(this).next("ul").slideToggle(200);
            return false;
        }
    });

});