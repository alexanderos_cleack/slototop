<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL ?>
<?php echo '<?xml-stylesheet type="text/xsl" href="' . Yii::$app->request->hostInfo . '/xsl/sitemap-index.xsl?ver=4.5"?>' . PHP_EOL ?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <?php foreach($categories as $category): ?>
    <sitemap>
        <loc><?= $category ?></loc>
    </sitemap>
    <?php endforeach; ?>
</sitemapindex>
