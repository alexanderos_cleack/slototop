<?php

use yii\helpers\Url;

?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL ?>
<?php echo '<?xml-stylesheet type="text/xsl" href="' . Yii::$app->request->hostInfo . '/xsl/sitemap.xsl?ver=4.5"?>' . PHP_EOL ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <?php foreach($items as $item): ?>
    <url>
        <loc><?= Url::to([$item_url, 'slug' => $item['slug']], true) ?></loc>
    </url>
    <?php endforeach; ?>
</urlset>