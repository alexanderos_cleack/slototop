<?php

namespace app\modules\sitemap\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;

use common\models\bonuses\Bonuses;
use common\models\casino\Casino;
use common\models\methods\MethodsPayment;
use common\models\seotext\SeoFiltersRequests;
use common\models\slots\Slots;
use common\models\vendors\Vendors;

/**
 * Class DefaultController
 * @package app\modules\sitemap\controllers
 */
class DefaultController extends Controller
{
    public function init()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/xml');
    }

    public function actionIndex()
    {

        $slots = Slots::find()->select(['title', 'slug'])->where(['enabled' => 'true'])->asArray()->all();

        $categories = [
            Url::to(['/sitemap/default/home'], true),
            Url::to(['/sitemap/default/casino'], true),
            Url::to(['/sitemap/default/games'], true),
            Url::to(['/sitemap/default/bonuses'], true),
            Url::to(['/sitemap/default/vendors'], true),
            Url::to(['/sitemap/default/payments'], true),
            Url::to(['/sitemap/default/filter-casino'], true),
            Url::to(['/sitemap/default/filter-games'], true),
            Url::to(['/sitemap/default/filter-bonuses'], true),
            Url::to(['/sitemap/default/filter-vendors'], true),
        ];

        return $this->render('index', [
            'categories' => $categories,
        ]);
    }

    public function actionHome()
    {
        return $this->render('home');
    }

    public function actionCasino()
    {
        $items = Casino::find()->where(['enabled' => true])->orderBy(['title' => SORT_ASC])->asArray()->all();

        return $this->render('category', [
            'items' => $items,
            'item_url' => '/casino/casino/show',
        ]);
    }

    public function actionGames()
    {
        $items = Slots::find()->where(['enabled' => true])->orderBy(['title' => SORT_ASC])->asArray()->all();

        return $this->render('category', [
            'items' => $items,
            'item_url' => '/games/games/show',
        ]);
    }

    public function actionBonuses()
    {
        $items = Bonuses::find()->where(['enabled' => true])->orderBy(['title' => SORT_ASC])->asArray()->all();

        return $this->render('category', [
            'items' => $items,
            'item_url' => '/bonuses/bonuses/show',
        ]);
    }

    public function actionVendors()
    {
        $items = Vendors::find()->where(['enabled' => true])->orderBy(['title' => SORT_ASC])->asArray()->all();

        return $this->render('category', [
            'items' => $items,
            'item_url' => '/vendors/vendors/show',
        ]);
    }

    public function actionPayments()
    {
        $items = MethodsPayment::find()->where(['enabled' => true])->orderBy(['title' => SORT_ASC])->asArray()->all();

        return $this->render('category', [
            'items' => $items,
            'item_url' => '/methods/payments/show',
        ]);
    }

    public function actionFilterCasino()
    {
        $items = SeoFiltersRequests::find()->where(['page' => SeoFiltersRequests::PAGE_CASINO])->andWhere(['!=', 'slug', ''])->asArray()->all();

        return $this->render('category', [
            'items' => $items,
            'item_url' => '/casino/casino/filter',
        ]);
    }

    public function actionFilterGames()
    {
        $items = SeoFiltersRequests::find()->where(['page' => SeoFiltersRequests::PAGE_GAMES])->andWhere(['!=', 'slug', ''])->asArray()->all();

        return $this->render('category', [
            'items' => $items,
            'item_url' => '/games/games/filter',
        ]);
    }

    public function actionFilterBonuses()
    {
        $items = SeoFiltersRequests::find()->where(['page' => SeoFiltersRequests::PAGE_BONUSES])->andWhere(['!=', 'slug', ''])->asArray()->all();

        return $this->render('category', [
            'items' => $items,
            'item_url' => '/bonuses/bonuses/filter',
        ]);
    }

    public function actionFilterVendors()
    {
        $items = SeoFiltersRequests::find()->where(['page' => SeoFiltersRequests::PAGE_VENDORS])->andWhere(['!=', 'slug', ''])->asArray()->all();

        return $this->render('category', [
            'items' => $items,
            'item_url' => '/vendors/vendors/filter',
        ]);
    }

}