<?php

namespace app\modules\sitemap;

/**
 * Class Module
 * @package app\modules\sitemap
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\sitemap\controllers';
    public $layout = false;

    public function init()
    {
        parent::init();
    }

}
