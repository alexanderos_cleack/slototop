<?php

namespace app\modules\vendors\models;

use Yii;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use common\models\seotext\SeoFiltersRequests;

use app\models\BaseFilters;

/**
 * Class Filters
 * @package app\modules\vendors\models
 *
 * @property integer $search_country
 * @property integer $search_language
 */
class Filters extends BaseFilters
{
    public $search_country;
    public $search_language;

    public function getCountriesForList()
    {
        $query = clone $this->query;

        $query->distinct()->select(['countries.id as id', 'countries.title as title', 't.country_id'])->innerJoinWith([
            'country' => function (ActiveQuery $query) {
                $query->select(['id', 'title']);
            }
        ])->asArray()->orderBy('title');

        return ArrayHelper::map($query->all(), 'id', 'title');
    }

    public function getLanguagesForList()
    {
        $query = clone $this->query;

        $query->distinct()->select(['languages.id as id', 'languages.title as title', 't.country_id'])->innerJoinWith([
            'languages' => function (ActiveQuery $query) {
                $query->select(['id', 'title']);
            }
        ])->asArray()->orderBy('title');

        return ArrayHelper::map($query->all(), 'id', 'title');
    }

    public static function set($query, $attributes = [])
    {
        $model = new Filters;
        $model->query = $query;

        if ($model->load(Yii::$app->request->post()) || $model->load(Yii::$app->request->get())) {
            if ($model->validate()) {
                $model->isValidate = true;
                self::search($query, $model);
                if ($seo = \common\models\seotext\SeoFiltersRequests::getSeoVendorsByFilter($model)) {
                    if ($seo->slug) {
                        Yii::$app->getResponse()->redirect(Url::to(['/vendors/vendors/filter', 'slug' => $seo->slug]))->send();
                    }
                }
                \common\models\seotext\SeoFiltersRequests::addFilterVendors($model);
            }
        } elseif ($attributes) {
            $model->attributes = $attributes;
            if ($model->validate()) {
                self::search($query, $model);
                if ($seo = \common\models\seotext\SeoFiltersRequests::getSeoVendorsByFilter($model)) {
                    $model->seo = $seo;
                }
            }
        }

        return $model;
    }

    /**
     * @param ActiveQuery $query
     * @param \app\modules\vendors\models\Filters $model
     */
    public static function search($query, $model)
    {
        if ($model->search_language) {
            $query->innerJoinWith('languages');
            $query->andFilterWhere([
                'languages.id' => $model->search_language,
            ]);
        }

        $query->andFilterWhere([
            't.country_id' => $model->search_country,
        ]);
    }

    public function rules()
    {
        return [
            [['search_country', 'search_language'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'search_country' => 'Страна',
            'search_language' => 'Поддерживаемые языки',
        ];
    }

    /**
     * @param  integer $id
     * @return string
     */
    public static function getSearchCountryUrl($id)
    {
        $sql = "SELECT *
        FROM seo_filters_requests
        WHERE filter::JSON->>'search_country' = :search_country::TEXT
          AND page = :page
	  AND count_elements = 1";
        $sql = Yii::$app->db->createCommand($sql)
            ->bindValue(':page', SeoFiltersRequests::PAGE_VENDORS)
            ->bindValue(':search_country', $id)
            ->rawSql;
        $seo_filter = SeoFiltersRequests::findBySql($sql)->one();
        if ($seo_filter) {
            if ($seo_filter->slug) {
                return Url::to(['/vendors/vendors/filter', 'slug' => $seo_filter->slug]);
            }
            return $seo_filter->url;
        }
        return self::getUrl('search_country', $id);
    }

    /**
     * @param  integer $id
     * @return string
     */
    public static function getSearchLanguageUrl($id)
    {
        $sql = "SELECT *
        FROM seo_filters_requests
        WHERE filter::JSON->>'search_language' = :search_language::TEXT
          AND page = :page
          AND count_elements = 1";
        $sql = Yii::$app->db->createCommand($sql)
            ->bindValue(':page', SeoFiltersRequests::PAGE_VENDORS)
            ->bindValue(':search_language', $id)
            ->rawSql;
        $seo_filter = SeoFiltersRequests::findBySql($sql)->one();
        if ($seo_filter) {
            if ($seo_filter->slug) {
                return Url::to(['/vendors/vendors/filter', 'slug' => $seo_filter->slug]);
            }
            return $seo_filter->url;
        }
        return self::getUrl('search_language', $id);
    }


    protected static function getUrl($attribute, $search, $many = false)
    {
        $attribute = $many ? "Filters[{$attribute}][]" : "Filters[{$attribute}]";
        return Url::to(['/vendors/vendors/index', $attribute => $search]);
    }

}