<?php

namespace app\modules\vendors\widgets;

use yii\base\Widget;

use common\models\vendors\Vendors;

/**
 * Class VendorItem
 * @package app\modules\vendors\widgets
 *
 * @property Vendors $model
 */
class VendorItem extends Widget
{
    public $model;

    public function run()
    {
        return $this->render('vendorItem', [
            'model' => $this->model,
        ]);
    }

}