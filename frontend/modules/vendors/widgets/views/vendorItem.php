<?php
/**
 * @var \yii\base\View $view
 * @var \common\models\vendors\Vendors $model
 */
?>

<div class="game">
    <div class="game-in">
        <span class="game-title"><?= $model->title ?></span>
        <div class="game-img">
            <?= \app\helpers\ImageHelper::image($model->getImageLogo(), $model->title) ?>
            <a href="<?= \app\helpers\UrlHelpers::vendor($model->slug) ?>"
               title="<?= $model->title ?>" class="btn-red"><?= Yii::t('app/f', 'ПЕРЕЙТИ') ?></a>
        </div>
        <span class="game-stars gm-untitle"></span>
    </div>
</div>
