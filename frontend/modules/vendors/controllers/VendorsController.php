<?php

namespace app\modules\vendors\controllers;

use Yii;
use yii\data\Pagination;
use yii\web\Controller;

use app\helpers\BreadcrumbsHelper;
use app\helpers\UrlHelpers;
use app\modules\vendors\models\Filters;
use app\modules\vendors\models\Sort;
use app\models\vendors\Vendors;
use app\modules\vendors\widgets\VendorItem;

/**
 * Class VendorsController
 * @package app\modules\vendors\controllers
 */
class VendorsController extends Controller
{
    public $enableCsrfValidation = false;

    public function actionIndex()
    {
        BreadcrumbsHelper::add(Yii::t('app/f', 'Производители'));

        $query = Vendors::find()
            ->select(['t.id', 't.title', 't.slug', 't.enabled'])
            ->from(['t' => Vendors::tableName()])
            ->where(['t.enabled' => true]);

        $filters = Filters::set($query);

        $pagination = $this->getPagination($query);

        $query->with(['logo']);

        $sort = Sort::set($query);

        $items = $this->getItems($query, $pagination);

        $seo = \common\models\seotext\SeoText::getSeoVendors();

        $this->saveQuery($query, $pagination);

        return $this->render('index', [
            'items' => $items,
            'count' => $pagination->totalCount,
            'pagination' => $pagination,
            'filters' => $filters,
            'sort' => $sort,
            'seo' => $seo,
        ]);
    }

    public function actionShow($slug)
    {
        $query = \app\models\vendors\Vendors::find()->where(['slug' => $slug/*, 'enabled' => true*/]);
        if (!$query->count()) throw new \yii\web\NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        $query->with(['screens', 'logo']);

        /** @var Vendors $vendor */
        $vendor = $query->one();

        $sort = new Sort;

        BreadcrumbsHelper::add(Yii::t('app/f', 'Производители'), UrlHelpers::vendors());
        BreadcrumbsHelper::add($vendor->title);

        return $this->render('show', [
            'vendor' => $vendor,
            'sort' => $sort,
        ]);
    }

    public function actionFilter($slug)
    {
        $seo = \common\models\seotext\SeoFiltersRequests::getSeoVendors($slug);
        if (!$seo) throw new \yii\web\NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        $model = \app\models\vendors\Vendors::class;
        $query = $model::find()->select(['t.id', 't.title', 't.slug', 't.enabled'])->from(['t' => $model::tableName()])->where(['t.enabled' => true]);

        $filters = Filters::set($query, json_decode($seo->filter, true));

        $pagination = $this->getPagination($query);

        $query->with(['logo']);

        $sort = Sort::set($query, ['slug' => $seo->slug], null, $seo);

        $items = $this->getItems($query, $pagination);

        $this->saveQuery($query, $pagination);

        BreadcrumbsHelper::add(Yii::t('app/f', 'Производители'), UrlHelpers::vendors());
        BreadcrumbsHelper::add($seo->meta_h1);

        return $this->render('index', [
            'items' => $items,
            'count' => $pagination->totalCount,
            'pagination' => $pagination,
            'filters' => $filters,
            'sort' => $sort,
            'seo' => $seo,
        ]);
    }

    public function actionSort($slug)
    {
        $seo_sort = \common\models\seotext\SeoSortsRequests::getSeoVendors($slug);
        if (!$seo_sort) throw new \yii\web\NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        $model = \app\models\vendors\Vendors::class;
        $query = $model::find()->select(['t.id', 't.title', 't.slug', 't.enabled'])->from(['t' => $model::tableName()])->where(['t.enabled' => true]);

        if ($seo_sort->filter) {
            $filters = Filters::set($query, json_decode($seo_sort->filter->filter, true));
        } else {
            $filters = Filters::set($query);
        }

        $pagination = $this->getPagination($query);

        $query->with(['logo']);

        if ($seo_sort->filter) {
            $sort = Sort::set($query, ['slug' => $seo_sort->filter->slug], json_decode($seo_sort->sort, true), $seo_sort->filter);
        } else {
            $sort = Sort::set($query, [], json_decode($seo_sort->sort, true));
        }

        $items = $this->getItems($query, $pagination);

        $this->saveQuery($query, $pagination);

        return $this->render('index', [
            'items' => $items,
            'count' => $pagination->totalCount,
            'pagination' => $pagination,
            'filters' => $filters,
            'sort' => $sort,
            'seo' => $seo_sort,
        ]);
    }

    public function actionFilters()
    {
        if (Yii::$app->request->isAjax) {
            $query = \common\models\vendors\Vendors::find()
                ->select(['t.id', 't.title'])
                ->from(['t' => \common\models\vendors\Vendors::tableName()])
                ->where(['t.enabled' => true]);

            $filters = new Filters();
            $filters->query = $query;

            if ($filters->load(Yii::$app->request->post()) && $filters->validate()) {
                Filters::search($query, $filters);

                return $this->renderPartial('filters/form', [
                    'filters' => $filters,
                ]);
            }
        }

        throw new \yii\web\BadRequestHttpException();
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function actionItems()
    {
        $session = Yii::$app->session;
        if ($session->has('vendors')) {
            list($query, $pagination) = array_values($session->get('vendors'));
            $pagination->page = Yii::$app->request->get('page');

            $items = [];
            foreach ($query->offset($pagination->offset)->limit($pagination->limit)->each() as $item) {
                $items[] = VendorItem::widget(['model' => $item]);
            };

            Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
            return join('', $items);
        }
    }

    /**
     * @param  \yii\db\ActiveQuery $query
     */
    protected function saveQuery(\yii\db\ActiveQuery $query, \yii\data\Pagination $pagination)
    {
        $session = Yii::$app->session;
        $session->set('vendors', [
            'query' => $query,
            'pagination' => $pagination,
        ]);
    }

    /**
     * @param  \yii\db\ActiveQuery $query
     * @return \yii\data\Pagination
     */
    protected function getPagination(\yii\db\ActiveQuery $query)
    {
        $count_query = clone $query;
        $count = $count_query->count();
        return new Pagination(['totalCount' => $count, 'pageSize' => 52]);
    }

    /**
     * @param  \yii\db\ActiveQuery $query
     * @param  \yii\data\Pagination $pagination
     * @return \common\models\casino\Casino[]
     */
    protected function getItems(\yii\db\ActiveQuery $query, \yii\data\Pagination $pagination)
    {
        $offset_query = clone $query;
        return $offset_query->offset($pagination->offset)->limit($pagination->limit)->all();
    }

}