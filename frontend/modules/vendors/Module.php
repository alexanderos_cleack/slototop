<?php

namespace app\modules\vendors;

/**
 * Class Module
 * @package app\modules\vendors
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\vendors\controllers';
    public $layout = 'main';

    public function init()
    {
        parent::init();
    }

}
