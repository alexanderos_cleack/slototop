<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var \app\modules\vendors\models\Filters $model
 */
?>

<?php

$model = isset($filters) ? $filters : new \app\modules\casino\models\Filters();

?>

<div class="sb-in-filters">
    <div class="sb-title">
        <?= Yii::t('app/f', 'Фильтровать по:') ?>
    </div>

    <?php $form = ActiveForm::begin(['id' => 'filters', 'method' => 'post', 'action' => \app\helpers\UrlHelpers::vendors()]); ?>
        <div id="ajax-filter">
            <?php echo $this->context->renderPartial('filters/form', [
                'filters' => $model,
            ]); ?>
        </div>
    <?php ActiveForm::end(); ?>

</div>