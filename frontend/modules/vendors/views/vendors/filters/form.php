<?php
/**
 * @var \yii\web\View $this
 * @var \app\modules\vendors\models\Filters $filters
 */
?>

<div class="label">
    <span class="sb-label-title"><?= $filters->getAttributeLabel('search_country') ?>:</span>
    <?= \yii\helpers\Html::activeDropdownList($filters, 'search_country', $filters->getCountriesForList(), ['prompt' => Yii::t('app/f', 'Filter select')/*, 'multiple' => true, 'separator' => '<div style="height: 5px;">&nbsp;</div>'*/]) ?>
</div>

<div class="label">
    <span class="sb-label-title"><?= $filters->getAttributeLabel('search_language') ?>:</span>
    <?= \yii\helpers\Html::activeDropdownList($filters, 'search_language', $filters->getLanguagesForList(), ['prompt' => Yii::t('app/f', 'Filter select')/*, 'multiple' => true, 'separator' => '<div style="height: 5px;">&nbsp;</div>'*/]) ?>
</div>

<div class="label">
    <?= \yii\helpers\Html::submitButton('Подобрать!', ['class' => 'btn-green']) ?>
</div>

<script>
    $('select').on('change', function () {
        $.ajax({
            'type': 'post',
            'url': '<?php echo \yii\helpers\Url::to(['/vendors/vendors/filters/']); ?>',
            'data': $('#filters').serialize()
        }).done(function (data) {
            $('#ajax-filter').html(data);
            $('input, select').trigger('refresh');
            $('input, select').styler();
            $(".jq-selectbox__dropdown ul").addClass('mCustomScrollbar').mCustomScrollbar({
                theme: "minimal"
            });
        });
    });
</script>
