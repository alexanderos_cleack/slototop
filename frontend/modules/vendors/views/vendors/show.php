<?php 

use yii\helpers\Url;

/**
 * @var  $this yii\web\View
 * @var  $game common\models\vendors\Vendors
 */

?>

<?php

    if ($vendor->meta_title) $this->title = $vendor->meta_title;
    if ($vendor->meta_keywords) $this->registerMetaTag(['name' => 'keywords', 'content' => $vendor->meta_keywords]);
    if ($vendor->meta_description) $this->registerMetaTag(['name' => 'description', 'content' => $vendor->meta_description]);

    if (!$vendor->enabled) $this->registerMetaTag(['name' => 'robots', 'content' => 'noindex,nofollow']);

?>

<?php echo \app\widgets\Ogp::widget([
    'article_section' => Yii::t('app/f', 'Vendors'),
    'image' => $vendor->logo,
    'model' => $vendor,
    'url' => Url::to(['/vendors/vendors/show', 'slug' => $vendor->slug], true)
]) ?>

<?php echo app\widgets\SchemaArticle::widget([
    'model' => $vendor,
    'add_content' => [
        \app\widgets\SchemaPerson::widget(['model' => $vendor]),
        \app\widgets\SchemaOrganization::widget(['model' => $vendor]),
        \app\widgets\SchemaAggregateRating::widget(['model' => $vendor]),
        \app\widgets\SchemaImageObject::widget(['image' => $vendor->logo]),
    ],
]) ?>

<div class="float-left pay-method-open">
    <?= \app\widgets\Breadcrumbs::widget(); ?>
    <div class="block-all">
        <div class="bl-red-title">
            <span class="bl-img bl-img-platform"></span>
            <div class="bl-t-title">
                <span><h1><?= $vendor->title ?></h1></span>
                <a href="<?= Yii::$app->request->referrer ?>"><?= Yii::t('app/f', 'назад к результатам >') ?></a>
            </div>

            <div class="clear"></div>
        </div>
    </div>  

    <div class="block-all grey-border-bottom bonus-page block-game-bg">
        <div class="game-type-left">
            <div class="game-type game-type-img">
                <?= \app\helpers\ImageHelper::image($vendor->getImageLogo(), $vendor->title) ?>
            </div>
            <div class="game-type casino-page">
                <?php if ($vendor->country): ?>
                    <div>Страна: <span class="text-blue"><a href="<?= \app\modules\vendors\models\Filters::getSearchCountryUrl($vendor->country->id) ?>"><?= $vendor->country->title ?></a></span></div>
                <?php endif; ?>
                <div><?= Yii::t('app/f', 'Год основания:') ?> <span class="text-blue"><?= $vendor->year_established ?></span></div>

                <?php if ($languages = $vendor->languages): ?>
                    <div><?= Yii::t('app/f', 'Поддерживаемые языки:') ?> <span class="text-blue">
                        <?php foreach ($languages as $language): ?>
                            <a href="<?= \app\modules\vendors\models\Filters::getSearchLanguageUrl($language->id) ?>"><?= $language->title ?></a>
                        <?php endforeach; ?>
                    </span></div>
                <?php endif; ?>

            </div>

            <div class="clear"></div>
        </div>

        <div class="game-type-right">
            <div class="game-type game-type-rate-casino">
                Количество игр: <br>
                <span><?= $vendor->amount_games ?></span>
                На сайте: <br>
                <span><?= $vendor->getAmountGames_site() ?></span>
                <div class="line"></div>
                <span class="gt-c-span">
                    <?= \app\helpers\ImageHelper::image('/img/casino/info.png') ?>
                    <?= Yii::t('app/f', 'Рейтинг Slototop') ?>
                </span>
                <span class="gt-c-span">
                    <span><?= (int)$vendor->rating_admins ?></span>
                    /100
                </span>
            </div>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>  

    <div class="block-all chr-casino-block">
        <div class="bl-red-title-normal">
            <span><?= Yii::t('app/f', 'О компании') ?></span>

            <div style="float: right"><?= \app\widgets\SNShare::widget() ?></div>

            <div class="clear"></div>
        </div>

        <?= $vendor->description ?>

        <?php if ($vendor->screens): ?>
            <div class="game-small-pr">
                <?php foreach ($vendor->screens as $screen): ?>
                    <?= \app\helpers\ImageHelper::image($screen) ?>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>

    </div>

    <div class="line"></div>

    <?php if ($items = $vendor->getGames_site()): ?>
        <div class="block-all chr-casino-block">
            <div class="red-line"></div>
            <div class="chr-other-title"><?= Yii::t('app/f', 'Игры') ?> <?= $vendor->title ?></div>
        </div>

        <div class="games">
            <?php foreach ($vendor->getGames_site() as $item): ?>
                <div class="game">
                    <div class="game-in">
                        <span class="game-title"><?= $item->title ?></span>
                        <?= \app\helpers\ImageHelper::image($item->logo) ?>
                        <span class="game-stars gm-untitle"><?= $item->vendor->title ?></span>
                        <a href="<?= Url::to(['/games/games/show', 'slug' => $item->slug]) ?>" class="btn-red" target="_blank"><?= Yii::t('app/f', 'ПЕРЕЙТИ') ?></a>
                    </div>
                </div>
            <?php endforeach; ?>
            <div class="clear"></div>
            <div style="text-align: right;">
                <a href="<?= \app\modules\games\models\Filters::getSearchVendorUrl($vendor->id) ?>"><?= Yii::t('app/f', 'Все игры с этим производителем') ?></a>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($items = $vendor->getCasino_site()): ?>
        <div class="block-all chr-casino-block">
            <div class="red-line"></div>
            <div class="chr-other-title"><?= Yii::t('app/f', 'Казино, работающие с') ?> <?= $vendor->title ?></div>
        </div>

        <div class="games">
            <?php foreach ($items as $item): ?>
                <div class="game">
                    <div class="game-in">
                        <span class="game-title"><?= $item->title ?></span>
                        <?= \app\helpers\ImageHelper::image($item->logo) ?>
                        <span class="game-stars gm-untitle"><span>95</span>/100</span>
                        <a href="<?= Url::to(['/casino/casino/show', 'slug' => $item->slug]) ?>" class="btn-red" target="_blank"><?= Yii::t('app/f', 'ПЕРЕЙТИ') ?></a>
                    </div>
                </div>
            <?php endforeach; ?>
            <div class="clear"></div>
            <div style="text-align: right;">
                <a href="<?= \app\modules\casino\models\Filters::getSearchVendorUrl($vendor->id) ?>"><?= Yii::t('app/f', 'Все казино с этим производителем') ?></a>
            </div>
        </div>
    <?php endif; ?>

</div>

<?php $this->beginBlock('widgets'); ?>
    <div class="fr-right-color">
        <?php echo \app\widgets\VendorNewGames::widget(['vendor' => $vendor]) ?>
        <?php echo \app\widgets\VendorBonusesCasino::widget(['vendor' => $vendor]) ?>
        <?php echo \app\widgets\VendorPopularCasino::widget(['vendor' => $vendor]) ?>
    </div>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('admin'); ?>
    <a href="<?php echo \app\helpers\AdminHelper::getVendorsItemsUpdateUrl($vendor->id); ?>" target="_blank"><?= Yii::t('app/f', 'Редактировать') ?></a>&nbsp;|
    <?= \app\widgets\Link::widget(['model' => $vendor]) ?>
<?php $this->endBlock(); ?>