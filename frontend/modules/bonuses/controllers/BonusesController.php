<?php

namespace app\modules\bonuses\controllers;

use Yii;
use yii\data\Pagination;
use yii\web\Controller;

use common\models\bonuses\Bonuses;
use common\models\casino\Casino;

use app\helpers\BreadcrumbsHelper;
use app\helpers\UrlHelpers;
use app\modules\bonuses\models\Filters;
use app\modules\bonuses\models\Sort;
use app\modules\bonuses\widgets\BonusItem;

/**
 * Class BonusesController
 * @package app\modules\bonuses\controllers
 */
class BonusesController extends Controller
{
    public $enableCsrfValidation = false;

    public function actionIndex()
    {
        BreadcrumbsHelper::add(Yii::t('app/f', 'Бонусы казино'));

        $query = Bonuses::find()
            ->select(['t.id', 't.title', 't.slug', 't.bonus_type_id', 't.value_on_picture', 't.enabled'])
            ->from(['t' => Bonuses::tableName()])
            ->where(['t.enabled' => true]);

        $filters = Filters::set($query);

        $pagination = $this->getPagination($query);

        $query->with(['casino.logo', 'bonusType']);

        $sort = Sort::set($query);

        $items = $this->getItems($query, $pagination);

        $seo = \common\models\seotext\SeoText::getSeoBonuses();

        $this->saveQuery($query, $pagination);

        return $this->render('index', [
            'items' => $items,
            'count' => $pagination->totalCount,
            'pagination' => $pagination,
            'filters' => $filters,
            'sort' => $sort,
            'seo' => $seo,
        ]);
    }

    public function actionShow($slug)
    {
        /** @var Bonuses $bonus */
        $bonus = Bonuses::find()->where(['slug' => $slug/*, 'enabled' => true*/])->one();
        if (!$bonus) throw new \yii\web\NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        BreadcrumbsHelper::add(Yii::t('app/f', 'Бонусы казино'), UrlHelpers::bonuses());
        BreadcrumbsHelper::add($bonus->title);

        return $this->render('show', [
            'bonus' => $bonus,
        ]);
    }

    public function actionCasino($casino)
    {
        $casino = Casino::find()->where(['slug' => $casino])->one();
        if (!$casino) throw new \yii\web\NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        $query = $casino->getBonuses_query();
        $query->from(['t' => Bonuses::tableName()])->andWhere(['t.enabled' => true]);

        $filters = Filters::set($query);

        $pagination = $this->getPagination($query);

        $sort = Sort::set($query, ['casino' => $casino->slug]);

        $items = $this->getItems($query, $pagination);

        $seo = \common\models\seotext\SeoText::getSeoBonusesCasino($casino);

        $this->saveQuery($query, $pagination);

        return $this->render('index', [
            'items' => $items,
            'count' => $pagination->totalCount,
            'pagination' => $pagination,
            'filters' => $filters,
            'sort' => $sort,
            'seo' => $seo,
        ]);
    }

    public function actionSame($bonus)
    {
        $bonus = Bonuses::find()->where(['slug' => $bonus])->one();
        if (!$bonus) throw new \yii\web\NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        $query = $bonus->getBonusesSimilar_query();
        $query->andWhere(['t.enabled' => true]);

        $filters = Filters::set($query);

        $pagination = $this->getPagination($query);

        $sort = Sort::set($query, ['bonus' => $bonus->slug]);

        $items = $this->getItems($query, $pagination);

        $seo = \common\models\seotext\SeoText::getSeoBonusesSame($bonus);

        $this->saveQuery($query, $pagination);

        return $this->render('index', [
            'items' => $items,
            'count' => $pagination->totalCount,
            'pagination' => $pagination,
            'filters' => $filters,
            'sort' => $sort,
            'seo' => $seo,
        ]);
    }

    public function actionVendorBonusesCasino($vendor)
    {
        $vendor = \app\models\vendors\Vendors::find()->where(['slug' => $vendor])->one();
        if (!$vendor) throw new \yii\web\NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        $query = $vendor->getVendorsBonusesCasino();
        $query->andWhere(['t.enabled' => true]);

        $filters = Filters::set($query);

        $pagination = $this->getPagination($query);

        $sort = Sort::set($query, ['vendor' => $vendor->slug]);

        $items = $this->getItems($query, $pagination);

        $seo = \common\models\seotext\SeoText::getSeoVendorBonusesCasino($vendor);

        $this->saveQuery($query, $pagination);

        return $this->render('index', [
            'items' => $items,
            'count' => $pagination->totalCount,
            'pagination' => $pagination,
            'filters' => $filters,
            'sort' => $sort,
            'seo' => $seo,
        ]);
    }

    public function actionFilter($slug)
    {
        $seo = \common\models\seotext\SeoFiltersRequests::getSeoBonuses($slug);
        if (!$seo) throw new \yii\web\NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        $model = \app\models\bonuses\Bonuses::class;
        $query = $model::find()->select(['t.id', 't.title', 't.slug', 't.bonus_type_id', 't.value_on_picture', 't.enabled'])->from(['t' => $model::tableName()])->where(['t.enabled' => true]);

        $filters = Filters::set($query, json_decode($seo->filter, true));

        $pagination = $this->getPagination($query);

        $query->with(['casino.logo', 'bonusType']);

        $sort = Sort::set($query, ['slug' => $seo->slug], null, $seo);

        $items = $this->getItems($query, $pagination);

        $this->saveQuery($query, $pagination);

        BreadcrumbsHelper::add(Yii::t('app/f', 'Бонусы казино'), UrlHelpers::bonuses());
        BreadcrumbsHelper::add($seo->meta_h1);

        return $this->render('index', [
            'items' => $items,
            'count' => $pagination->totalCount,
            'pagination' => $pagination,
            'filters' => $filters,
            'sort' => $sort,
            'seo' => $seo,
        ]);
    }

    public function actionSort($slug)
    {
        $seo_sort = \common\models\seotext\SeoSortsRequests::getSeoBonuses($slug);
        if (!$seo_sort) throw new \yii\web\NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        $model = \app\models\bonuses\Bonuses::class;
        $query = $model::find()->select(['t.id', 't.title', 't.slug', 't.bonus_type_id', 't.value_on_picture', 't.enabled'])->from(['t' => $model::tableName()])->where(['t.enabled' => true]);

        if ($seo_sort->filter) {
            $filters = Filters::set($query, json_decode($seo_sort->filter->filter, true));
        } else {
            $filters = Filters::set($query);
        }

        $pagination = $this->getPagination($query);

        $query->with(['casino.logo', 'bonusType']);

        if ($seo_sort->filter) {
            $sort = Sort::set($query, ['slug' => $seo_sort->filter->slug], json_decode($seo_sort->sort, true), $seo_sort->filter);
        } else {
            $sort = Sort::set($query, [], json_decode($seo_sort->sort, true));
        }

        $items = $this->getItems($query, $pagination);

        $this->saveQuery($query, $pagination);

        return $this->render('index', [
            'items' => $items,
            'count' => $pagination->totalCount,
            'pagination' => $pagination,
            'filters' => $filters,
            'sort' => $sort,
            'seo' => $seo_sort,
        ]);
    }

    public function actionFilters()
    {
        if (Yii::$app->request->isAjax) {
            $query = Bonuses::find()
                ->select(['t.id', 't.title'])
                ->from(['t' => Bonuses::tableName()])
                ->where(['t.enabled' => true]);

            $filters = new Filters();
            $filters->query = $query;

            if ($filters->load(Yii::$app->request->post()) && $filters->validate()) {
                Filters::search($query, $filters);

                return $this->renderPartial('filters/form', [
                    'filters' => $filters,
                ]);
            }
        }

        throw new \yii\web\BadRequestHttpException();
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function actionItems()
    {
        $session = Yii::$app->session;
        if ($session->has('bonuses')) {
            list($query, $pagination) = array_values($session->get('bonuses'));
            $pagination->page = Yii::$app->request->get('page');

            $items = [];
            foreach ($query->offset($pagination->offset)->limit($pagination->limit)->each() as $item) {
                $items[] = BonusItem::widget(['model' =>  $item]);
            };

            Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
            return join('', $items);
        }
    }

    /**
     * @param  \yii\db\ActiveQuery $query
     */
    protected function saveQuery(\yii\db\ActiveQuery $query, \yii\data\Pagination $pagination)
    {
        $session = Yii::$app->session;
        $session->set('bonuses', [
            'query' => $query,
            'pagination' => $pagination,
        ]);
    }

    /**
     * @param  \yii\db\ActiveQuery $query
     * @return \yii\data\Pagination
     */
    protected function getPagination(\yii\db\ActiveQuery $query)
    {
        $count_query = clone $query;
        $count = $count_query->count();
        return new Pagination(['totalCount' => $count, 'pageSize' => 52]);
    }

    /**
     * @param  \yii\db\ActiveQuery $query
     * @param  \yii\data\Pagination $pagination
     * @return \common\models\casino\Casino[]
     */
    protected function getItems(\yii\db\ActiveQuery $query, \yii\data\Pagination $pagination)
    {
        $offset_query = clone $query;
        return $offset_query->offset($pagination->offset)->limit($pagination->limit)->all();
    }

}