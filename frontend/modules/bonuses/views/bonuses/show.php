<?php

use yii\helpers\Html;
use yii\helpers\Url;

use app\modules\bonuses\models\Filters;

/**
 * @var yii\web\View $this
 * @var common\models\bonuses\Bonuses $bonus
 */
?>

<?php

    // $this->title = $bonus->title;
    if ($bonus->meta_title) $this->title = $bonus->meta_title;
    if ($bonus->meta_keywords) $this->registerMetaTag(['name' => 'keywords', 'content' => $bonus->meta_keywords]);
    if ($bonus->meta_description) $this->registerMetaTag(['name' => 'description', 'content' => $bonus->meta_description]);

    if (!$bonus->enabled) $this->registerMetaTag(['name' => 'robots', 'content' => 'noindex,nofollow']);

?>

<?php echo \app\widgets\Ogp::widget([
    'article_section' => Yii::t('app/f', 'Bonuses'),
    'image' => $bonus->casino->logo,
    'model' => $bonus,
    'url' => Url::to(['/bonuses/bonuses/show', 'slug' => $bonus->slug], true)
]) ?>

<?php echo app\widgets\SchemaArticle::widget([
    'model' => $bonus,
    'add_content' => [
        \app\widgets\SchemaPerson::widget(['model' => $bonus]),
        \app\widgets\SchemaOrganization::widget(['model' => $bonus]),
        \app\widgets\SchemaAggregateRating::widget(['model' => $bonus]),
        \app\widgets\SchemaImageObject::widget(['image' => $bonus->casino->logo]),
    ],
]) ?>

<div class="float-left">
    <?= \app\widgets\Breadcrumbs::widget(); ?>
    <div class="block-all">
        <div class="bl-red-title">
            <span class="bl-img bl-img-cherry"></span>
            <div class="bl-t-title">
                <span><h1><?= $bonus->title ?></h1></span>
                <a href="<?= Yii::$app->request->referrer ?>"><?= Yii::t('app/f', 'назад к результатам >') ?></a>
            </div>

            <div class="clear"></div>
        </div>
    </div>
    <div class="block-all grey-border-bottom bonus-page block-game-bg">
        <div class="game-type-left">
            <div class="game-type game-type-img">
                <?= \app\helpers\ImageHelper::image($bonus->casino->getImageLogo(), $bonus->logo ? $bonus->logo->alt : $bonus->title) ?>
            </div>

            <div class="game-type casino-page">
                <div><?= Yii::t('app/f', 'Казино:') ?> <span class="text-blue"><a href="<?= \app\helpers\UrlHelpers::casino($bonus->casino->slug) ?>" rel="nofollow" target="_blank"><?= $bonus->casino->getTitlePreview() ?></a></span></div>
                <div><?= Yii::t('app/f', 'Тип бонуса:') ?> <span class="text-blue"><?= Html::a($bonus->bonusType->title, Filters::getSearchTypeUrl($bonus->bonusType->id)) ?></span></div>
                <?php if ($bonus->wager): ?>
                    <div><?= Yii::t('app/f', 'Вейджер:') ?> <span class="text-blue"><?= $bonus->wager ?></span></div>
                <?php endif; ?>
                <div><?= Yii::t('app/f', 'Количество использований:') ?> <span class="text-blue"><?= $bonus->amount_uses ? : Yii::t('app/f','не ограничено') ?></span></div>
            </div>

            <div class="clear"></div>
        </div>

        <div class="game-type-right">
            <div class="game-type game-type-casino">
                <?= \app\helpers\ImageHelper::image($bonus->bonusType->logo_mini) ?>
            </div>
            <div class="game-type game-type-rate-casino">
                <?php if ($bonus->size_of_bonus): ?>
                    <span class="gt-c-span"><?= Yii::t('app/f', 'Размер бонуса:') ?></span>
                    <br>
                    <span class="text-blue"><?= $bonus->size_of_bonus ?></span>
                    <br>
                <?php else: ?>
                    <br>
                    <br>
                <?php endif; ?>
                <a class="btn-red" data-href="<?= $bonus->casino->getRedirectUrl(); ?>" rel="nofollow" target="_blank"><?= Yii::t('app/f', 'ПОЛУЧИТЬ') ?></a>
            </div>
            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>  

    <div class="block-all grey-border-bottom chr-casino-block">
        <?php if ($countries = $bonus->getCountries_site()): ?>
            <div class="table-game-col">
                <div class="table-game-row">
                    <i class="icon-chr chr1"></i>
                    <span><?= Yii::t('app/f', 'Доступен для игроков из') ?></span>
                </div>
                <div class="table-game-row flags-list">
                    <span class="text-blue">
                        <?php $items = []; ?>
                        <?php foreach ($countries as $item): ?>
                            <?php $items[] = Html::a($item->logo_mini ? \app\helpers\ImageHelper::image($item->logo_mini, null, ['title' => $item->title])  : $item->title, Filters::getSearchCountryUrl($item->id)); ?>
                        <?php endforeach; ?>
                        <?= join(' ', $items) ?>
                    </span>
                </div>
            </div>
        <?php endif; ?>

        <?php if ($max_sum = $bonus->getMaxSum_site()): ?>
            <div class="table-game-col">
                <div class="table-game-row">
                    <i class="icon-chr chr2"></i>
                    <span><?= Yii::t('app/f', 'Макс. сумма бонуса') ?></span>
                </div>
                <div class="table-game-row">
                    <span class="text-blue"><?= $max_sum ?></span>
                </div>
            </div>
        <?php endif; ?>

        <?php if ($max_payout = $bonus->getMaxPayout_site()): ?>
            <div class="table-game-col">
                <div class="table-game-row">
                    <i class="icon-chr chr2"></i>
                    <span><?= Yii::t('app/f', 'Макс. выплата') ?></span>
                </div>
                <div class="table-game-row">
                    <span class="text-blue"><?= $bonus->getMaxPayout_site() ?></span>
                </div>
            </div>
        <?php endif; ?>

        <div class="clear"></div>
    </div>

    <div class="block-all chr-casino-block">
        <div class="bl-red-title-normal">
            <span><?= Yii::t('app/f', 'Описание') ?></span>

            <div style="float: right"><?= \app\widgets\SNShare::widget() ?></div>

            <div class="clear"></div>
        </div>

        <?= $bonus->description ?>
        <div class="red-line"></div>
        <div class="chr-other-title"><?= Yii::t('app/f', 'Условия бонуса') ?></div>
        <?= $bonus->conditions ?>
    </div>

</div>

<?php $this->beginBlock('widgets'); ?>
    <div class="fr-right-color">
        <?= \app\widgets\BonusesCasino::widget(['bonus' => $bonus]) ?>
        <?= \app\widgets\BonusesSame::widget(['bonus' => $bonus]) ?>
    </div>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('admin'); ?>
    <a href="<?php echo \app\helpers\AdminHelper::getBonusesItemsUpdateUrl($bonus->id); ?>" target="_blank"><?= Yii::t('app/f', 'Редактировать') ?></a>&nbsp;|
    <?= \app\widgets\Link::widget(['model' => $bonus]) ?>
<?php $this->endBlock(); ?>