<?php
/**
 * @var \yii\web\View $this
 * @var \app\modules\bonuses\models\Filters $filters
 */
?>

<div class="label">
    <span class="sb-label-title"><?= $filters->getAttributeLabel('search_type') ?>:</span>
    <?= \yii\helpers\Html::activeDropdownList($filters, 'search_type', $filters->getBonusesTypesForList(), ['prompt' => Yii::t('app/f', 'Filter select')]) ?>
</div>

<div class="label">
    <span class="sb-label-title"><?= $filters->getAttributeLabel('search_currency') ?>:</span>
    <?= \yii\helpers\Html::activeDropdownList($filters, 'search_currency', $filters->getCurrenciesForList(), ['prompt' => Yii::t('app/f', 'Filter select')]) ?>
</div>

<div class="label">
    <?= \yii\helpers\Html::submitButton('Подобрать!', ['class' => 'btn-green']) ?>
</div>

<script>
    $('select').on('change', function () {
        $.ajax({
            'type': 'post',
            'url': '<?php echo \yii\helpers\Url::to(['/bonuses/bonuses/filters/']); ?>',
            'data': $('#filters').serialize()
        }).done(function (data) {
            $('#ajax-filter').html(data);
            $('input, select').trigger('refresh');
            $('input, select').styler();
            $(".jq-selectbox__dropdown ul").addClass('mCustomScrollbar').mCustomScrollbar({
                theme: "minimal"
            });
        });
    });
</script>
