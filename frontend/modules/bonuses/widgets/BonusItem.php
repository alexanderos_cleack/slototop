<?php

namespace app\modules\bonuses\widgets;

use yii\base\Widget;

use common\models\bonuses\Bonuses;

/**
 * Class BonusItem
 * @package app\modules\bonuses\widgets
 *
 * @property Bonuses $model
 */
class BonusItem extends Widget
{
    public $model;

    public function run()
    {
        return $this->render('bonusItem', [
            'model' => $this->model,
        ]);
    }

}