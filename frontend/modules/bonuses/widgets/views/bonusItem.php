<?php
/**
 * @var \yii\web\View $this
 * @var \common\models\bonuses\Bonuses $model
 */
?>

<div class="game">
    <div class="game-in">
        <span class="game-title"><?= $model->title ?></span>
        <div class="game-img">
            <?= \app\helpers\ImageHelper::image($model->casino->getImageLogo(), $model->logo ? $model->logo->alt : $model->title) ?>
            <a href="<?= \app\helpers\UrlHelpers::bonus($model->slug) ?>"
               title="<?= $model->title ?>" class="btn-red"><?= Yii::t('app/f', 'ПЕРЕЙТИ') ?></a>
        </div>
        <div class="bn-percent"><?= $model->value_on_picture ?></div>
        <span class="gm-untitle"><?= $model->bonusType->title ?></span>
    </div>
</div>
