<?php

namespace app\modules\bonuses\models;

use \common\models\seotext\SeoSortsRequests;

use app\helpers\UrlHelpers;
use app\models\ABaseSort;

/**
 * Class Sort
 * @package app\modules\bonuses\models
 */
class Sort extends ABaseSort
{
    public function defaultSort()
    {
        $this->setTitle()->asc();
        $this->checkTitle();
    }

    /**
     * @return Sort
     */
    public static function createModel()
    {
        return new Sort;
    }

    /**
     * @param  Sort $sort
     * @return SeoSortsRequests
     */
    public function getSeoBySort($sort)
    {
        return SeoSortsRequests::getSeoBonusesBySort($sort);
    }

    /**
     * @param Sort $sort
     * @param integer $filter_id
     */
    public function addSort($sort, $filter_id = null)
    {
        SeoSortsRequests::addSortBonuses($sort, $filter_id);
    }

    /**
     * @param string $slug
     * @return string
     */
    public function getUrlSort($slug)
    {
        return UrlHelpers::bonusesSort($slug);
    }

    /**
     * @return string
     */
    public function getUrlFilter()
    {
        return '/bonuses/bonuses/filter';
    }

    /**
     * @return string
     */
    public function getUrlBase()
    {
        return UrlHelpers::bonuses();
    }

}