<?php

namespace app\modules\bonuses\models;

use Yii;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use common\models\seotext\SeoFiltersRequests;

use app\models\BaseFilters;

/**
 * Class Filters
 * @package app\modules\bonuses\models
 *
 * @property integer $search_country
 * @property integer $search_currency
 * @property integer $search_type
 * @property integer $search_use
 * @property integer $search_wager
 */
class Filters extends BaseFilters
{
    public $search_country;
    public $search_currency;
    public $search_type;
    public $search_use;
    public $search_wager;


    public function getBonusesTypesForList() {
        $query = clone $this->query;

        $query->distinct()->select(['bonuses_types.id as id', 'bonuses_types.title as title', 'bonus_type_id'])->joinWith([
            'bonusType' => function (ActiveQuery $query) {
                $query->select(['id', 'title']);
            }
        ])->asArray()->orderBy('title');

        return ArrayHelper::map($query->all(), 'id', 'title');
    }

    public function getCurrenciesForList() {
        $query = clone $this->query;

        $query->distinct()->select(['currencies.id as id', 'currencies.title as title', 'bonus_type_id'])->innerJoinWith([
            'currencies' => function (ActiveQuery $query) {
                $query->select(['id', 'title']);
            }
        ])->asArray()->orderBy('title');

        return ArrayHelper::map($query->all(), 'id', 'title');
    }

    public static function set($query, $attributes = [])
    {
        $model = new Filters;
        $model->query = $query;

        if ($model->load(Yii::$app->request->post()) || $model->load(Yii::$app->request->get())) {
            if ($model->validate()) {
                $model->isValidate = true;
                self::search($query, $model);
                if ($seo = \common\models\seotext\SeoFiltersRequests::getSeoBonusesByFilter($model)) {
                    if ($seo->slug) {
                        Yii::$app->getResponse()->redirect(Url::to(['/bonuses/bonuses/filter', 'slug' => $seo->slug]))->send();
                    }
                }
                \common\models\seotext\SeoFiltersRequests::addFilterBonuses($model);
            }
        } elseif ($attributes) {
            $model->attributes = $attributes;
            if ($model->validate()) {
                self::search($query, $model);
                if ($seo = \common\models\seotext\SeoFiltersRequests::getSeoBonusesByFilter($model)) {
                    $model->seo = $seo;
                }
            }
        }

        return $model;
    }

    /**
     * @param ActiveQuery $query
     * @param Filters $model
     */
    public static function search($query, $model)
    {
        $query->andFilterWhere(['t.bonus_type_id' => $model->search_type]);

        if ($model->search_currency) {
            $query->innerJoinWith('currencies');
            $query->andFilterWhere(['currencies.id' => $model->search_currency]);
        }

        if ($model->search_country) {
            $query->joinWith('countries');
            $query->andFilterWhere(['countries.id' => $model->search_country]);
        }

        $query->andFilterWhere(['COALESCE(t.amount_uses, 0)' => $model->search_use]);
        $query->andFilterWhere(['t.wager' => $model->search_wager]);
    }

    public function rules()
    {
        return [
            [['search_currency', 'search_type', 'search_country'], 'integer'],
            [['search_use', 'search_wager'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'search_type' => 'Тип бонуса',
            'search_currency' => 'Валюта',
        ];
    }


    /**
     * @param  integer $id
     * @return string
     */
    public static function getSearchTypeUrl($id)
    {
        $sql = "SELECT *
        FROM seo_filters_requests
        WHERE filter::JSON->>'search_type' = :search_type::TEXT
          AND page = :page
          AND count_elements = 1";
        $sql = Yii::$app->db->createCommand($sql)
            ->bindValue(':page', SeoFiltersRequests::PAGE_BONUSES)
            ->bindValue(':search_type', $id)
            ->rawSql;
        $seo_filter = SeoFiltersRequests::findBySql($sql)->one();
        if ($seo_filter) {
            if ($seo_filter->slug) {
                return Url::to(['/bonuses/bonuses/filter', 'slug' => $seo_filter->slug]);
            }
            return $seo_filter->url;
        }
        return self::getUrl('search_type', $id);
    }

    /**
     * @param  integer $id
     * @return string
     */
    public static function getSearchCountryUrl($id)
    {
        $sql = "SELECT *
        FROM seo_filters_requests
        WHERE filter::JSON->>'search_country' = :search_country::TEXT
          AND page = :page
          AND count_elements = 1";
        $sql = Yii::$app->db->createCommand($sql)
            ->bindValue(':page', SeoFiltersRequests::PAGE_BONUSES)
            ->bindValue(':search_country', $id)
            ->rawSql;
        $seo_filter = SeoFiltersRequests::findBySql($sql)->one();
        if ($seo_filter) {
            if ($seo_filter->slug) {
                return Url::to(['/bonuses/bonuses/filter', 'slug' => $seo_filter->slug]);
            }
            return $seo_filter->url;
        }
        return self::getUrl('search_country', $id);
    }

    /**
     * @param  integer $id
     * @return string
     */
    public static function getSearchUseUrl($id)
    {
        $sql = "SELECT *
        FROM seo_filters_requests
        WHERE filter::JSON->>'search_use' = :search_use::TEXT
          AND page = :page";
        $sql = Yii::$app->db->createCommand($sql)
            ->bindValue(':page', SeoFiltersRequests::PAGE_BONUSES)
            ->bindValue(':search_use', $id)
            ->rawSql;
        $seo_filter = SeoFiltersRequests::findBySql($sql)->one();
        if ($seo_filter) {
            if ($seo_filter->slug) {
                return Url::to(['/bonuses/bonuses/filter', 'slug' => $seo_filter->slug]);
            }
            return $seo_filter->url;
        }
        return self::getUrl('search_use', $id);
    }

    /**
     * @param  integer $id
     * @return string
     */
    public static function getSearchWagerUrl($id)
    {
        $sql = "SELECT *
        FROM seo_filters_requests
        WHERE filter::JSON->>'search_wager' = :search_wager::TEXT
          AND page = :page";
        $sql = Yii::$app->db->createCommand($sql)
            ->bindValue(':page', SeoFiltersRequests::PAGE_BONUSES)
            ->bindValue(':search_wager', $id)
            ->rawSql;
        $seo_filter = SeoFiltersRequests::findBySql($sql)->one();
        if ($seo_filter) {
            if ($seo_filter->slug) {
                return Url::to(['/bonuses/bonuses/filter', 'slug' => $seo_filter->slug]);
            }
            return $seo_filter->url;
        }
        return self::getUrl('search_wager', $id);
    }


    protected static function getUrl($attribute, $search, $many = false)
    {
        $attribute = $many ? "Filters[{$attribute}][]" : "Filters[{$attribute}]";
        return Url::to(['/bonuses/bonuses/index', $attribute => $search]);
    }

}