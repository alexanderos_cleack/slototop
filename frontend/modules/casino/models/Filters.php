<?php

namespace app\modules\casino\models;

use Yii;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use common\models\seotext\SeoFiltersRequests;

use app\models\BaseFilters;

/**
 * Class Filters
 * @package app\modules\casino\models
 *
 * @property integer $search_bonus
 * @property integer $search_country
 * @property integer $search_currency
 * @property integer $search_language
 * @property integer $search_method
 * @property integer $search_vendor
 * @property integer $search_platform
 * @property integer $search_license
 * @property string $search_terms_output
 * @property SeoFiltersRequests $seo
 */
class Filters extends BaseFilters
{
    public $search_bonus;
    public $search_country;
    public $search_currency;
    public $search_language;
    public $search_method;
    public $search_vendor;
    public $search_platform;
    public $search_license;
    public $search_terms_output;
    public $seo;

    /**
     * @return array
     */
    public function getCountriesForList() {
        $query = clone $this->query;

        $query->joinWith([
            'countries' => function (ActiveQuery $query) {
                $query->select(['id', 'title']);
            }
        ]);

        return ArrayHelper::map($query->select(['countries.id as id', 'countries.title as title'])->orderBy('title')->asArray()->all(), 'id', 'title');
    }

    /**
     * @return array
     */
    public function getCurrenciesForList() {
        $query = clone $this->query;

        $query->joinWith([
            'currencies' => function (ActiveQuery $query) {
                $query->select(['id', 'title']);
            }
        ]);

        return ArrayHelper::map($query->select(['currencies.id as id', 'currencies.title as title'])->orderBy('title')->asArray()->all(), 'id', 'title');
    }

    /**
     * @return array
     */
    public function getMethodsPaymentForList() {
        $query = clone $this->query;

        $query->joinWith([
            'methods_payment' => function (ActiveQuery $query) {
                $query->select(['id', 'title']);
            }
        ]);

        return ArrayHelper::map($query->select(['methods_payment.id as id', 'methods_payment.title as title'])->orderBy('title')->asArray()->all(), 'id', 'title');
    }

    /**
     * @return array
     */
    public function getVendorsForList() {
        $query = clone $this->query;

        $query->joinWith([
            'vendors' => function (ActiveQuery $query) {
                $query->select(['id', 'title']);
            }
        ]);

        return ArrayHelper::map($query->select(['vendors.id as id', 'vendors.title as title'])->orderBy('title')->asArray()->all(), 'id', 'title');
    }

    /**
     * @return array
     */
    public function getLanguagesForList() {
        $query = clone $this->query;

        $query->joinWith([
            'languages' => function (ActiveQuery $query) {
                $query->select(['id', 'title']);
            }
        ]);

        return ArrayHelper::map($query->select(['languages.id as id', 'languages.title as title'])->orderBy('title')->asArray()->all(), 'id', 'title');
    }

    /**
     * @return array
     */
    public function getBonusesTypesForList() {
        $query = clone $this->query;

        $query->innerJoinWith([
            'bonuses.bonusType' => function (ActiveQuery $query) {
                $query->select(['id', 'title']);
            }
        ]);

        return ArrayHelper::map($query->select(['bonuses_types.id as id', 'bonuses_types.title as title'])->orderBy('title')->asArray()->all(), 'id', 'title');
    }

    /**
     * @param ActiveQuery $query
     * @param array $attributes
     * @return Filters
     */
    public static function set($query, $attributes = [])
    {
        $model = new Filters;
        $model->query = $query;

        if ($model->load(Yii::$app->request->post()) || $model->load(Yii::$app->request->get())) {
            if ($model->validate()) {
                $model->isValidate = true;
                self::search($query, $model);
                if ($seo = \common\models\seotext\SeoFiltersRequests::getSeoCasinoByFilter($model)) {
                    if ($seo->slug) {
                        Yii::$app->getResponse()->redirect(Url::to(['/casino/casino/filter', 'slug' => $seo->slug]))->send();
                    }
                }
                \common\models\seotext\SeoFiltersRequests::addFilterCasino($model);
            }
        } elseif ($attributes) {
            $model->attributes = $attributes;
            if ($model->validate()) {
                self::search($query, $model);
                if ($seo = \common\models\seotext\SeoFiltersRequests::getSeoCasinoByFilter($model)) {
                    $model->seo = $seo;
                }
            }
        } else {
            self::search($query, $model);
        }

        return $model;
    }

    /**
     * @param ActiveQuery $query
     * @param Filters $model
     */
    public static function search($query, $model)
    {
        if ($model->search_bonus) {
            $query->innerJoinWith('bonuses');
            $query->andFilterWhere(['bonuses.bonus_type_id' => $model->search_bonus]);
        }

        if ($model->search_country) {
            $query->joinWith('countries');
            $query->andFilterWhere(['countries.id' => $model->search_country]);
        }

        if ($model->search_currency) {
            $query->joinWith('currencies');
            $query->andFilterWhere(['currencies.id' => $model->search_currency]);
        }

        if ($model->search_language) {
            $query->joinWith('languages');
            $query->andFilterWhere(['languages.id' => $model->search_language]);
        }

        if ($model->search_method) {
            $query->joinWith('methods_payment');
            $query->andFilterWhere(['methods_payment.id' => $model->search_method]);
        }

        if ($model->search_vendor) {
            $query->joinWith('vendors');
            $query->andFilterWhere(['vendors.id' => $model->search_vendor]);
        }

        if ($model->search_platform) {
            $query->joinWith('vendors2platforms');
            $query->andFilterWhere(['vendors2platforms.platform_id' => $model->search_platform]);
        }

        if ($model->search_license) {
            $query->andFilterWhere(['license_id' => $model->search_license]);
        }

        if ($model->search_terms_output) {
            $query->andFilterWhere(['terms_output' => $model->search_terms_output]);
        }
    }

    public function rules()
    {
        return [
            [['search_bonus', 'search_country', 'search_currency', 'search_language', 'search_license', 'search_method', 'search_platform', 'search_vendor'], 'integer'],
            [['search_terms_output'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'search_bonus' => 'Бонусы',
            'search_country' => 'Игроки из стран',
            'search_currency' => 'Валюта',
            'search_language' => 'Языки',
            'search_method' => 'Методы оплаты',
            'search_vendor' => 'Платформы',
            'search_terms_ouput' => 'Сроки вывода',
        ];
    }

    /**
     * @param  integer $id
     */
    public static function getSearchLanguageUrl($id)
    {
        $sql = "SELECT *
        FROM seo_filters_requests
        WHERE filter::JSON->>'search_language' = :search_language::TEXT
          AND page = :page
          AND count_elements = 1";
        $sql = Yii::$app->db->createCommand($sql)
            ->bindValue(':page', SeoFiltersRequests::PAGE_CASINO)
            ->bindValue(':search_language', $id)
            ->rawSql;
        $seo_filter = SeoFiltersRequests::findBySql($sql)->one();
        if ($seo_filter) {
            if ($seo_filter->slug) {
                return Url::to(['/casino/casino/filter', 'slug' => $seo_filter->slug]);
            }
            return $seo_filter->url;
        }

        return self::getUrl('search_language', $id);
    }

    /**
     * @param  integer $id
     * @return string
     */
    public static function getSearchCountryUrl($id)
    {
        $sql = "SELECT *
        FROM seo_filters_requests
        WHERE filter::JSON->>'search_country' = :search_country::TEXT
          AND page = :page
          AND count_elements = 1";
        $sql = Yii::$app->db->createCommand($sql)
            ->bindValue(':page', SeoFiltersRequests::PAGE_CASINO)
            ->bindValue(':search_country', $id)
            ->rawSql;
        $seo_filter = SeoFiltersRequests::findBySql($sql)->one();
        if ($seo_filter) {
            if ($seo_filter->slug) {
                return Url::to(['/casino/casino/filter', 'slug' => $seo_filter->slug]);
            }
            return $seo_filter->url;
        }
        return self::getUrl('search_country', $id);
    }

    /**
     * @param  integer $id
     * @return string
     */
    public static function getSearchCurrencyUrl($id)
    {
        $sql = "SELECT *
        FROM seo_filters_requests
        WHERE filter::JSON->>'search_currency' = :search_currency::TEXT
          AND page = :page
          AND count_elements = 1";
        $sql = Yii::$app->db->createCommand($sql)
            ->bindValue(':page', SeoFiltersRequests::PAGE_CASINO)
            ->bindValue(':search_currency', $id)
            ->rawSql;
        $seo_filter = SeoFiltersRequests::findBySql($sql)->one();
        if ($seo_filter) {
            if ($seo_filter->slug) {
                return Url::to(['/casino/casino/filter', 'slug' => $seo_filter->slug]);
            }
            return $seo_filter->url;
        }
        return self::getUrl('search_currency', $id);
    }

    /**
     * @param  integer $id
     * @return string
     */
    public static function getSearchVendorUrl($id)
    {
        $sql = "SELECT *
        FROM seo_filters_requests
        WHERE filter::JSON->>'search_vendor' = :search_vendor::TEXT
          AND page = :page
          AND count_elements = 1";
        $sql = Yii::$app->db->createCommand($sql)
            ->bindValue(':page', SeoFiltersRequests::PAGE_CASINO)
            ->bindValue(':search_vendor', $id)
            ->rawSql;
        $seo_filter = SeoFiltersRequests::findBySql($sql)->one();
        if ($seo_filter) {
            if ($seo_filter->slug) {
                return Url::to(['/casino/casino/filter', 'slug' => $seo_filter->slug]);
            }
            return $seo_filter->url;
        }
        return self::getUrl('search_vendor', $id);
    }

    /**
     * @param  integer $id
     * @return string
     */
    public static function getSearchBonusUrl($id)
    {
        $sql = "SELECT *
        FROM seo_filters_requests
        WHERE filter::JSON->>'search_bonus' = :search_bonus::TEXT
          AND page = :page
          AND count_elements = 1";
        $sql = Yii::$app->db->createCommand($sql)
            ->bindValue(':page', SeoFiltersRequests::PAGE_CASINO)
            ->bindValue(':search_bonus', $id)
            ->rawSql;
        $seo_filter = SeoFiltersRequests::findBySql($sql)->one();
        if ($seo_filter) {
            if ($seo_filter->slug) {
                return Url::to(['/casino/casino/filter', 'slug' => $seo_filter->slug]);
            }
            return $seo_filter->url;
        }
        return self::getUrl('search_bonus', $id);
    }

    /**
     * @param  integer $id
     * @return string
     */
    public static function getSearchPlatformUrl($id)
    {
        $sql = "SELECT *
        FROM seo_filters_requests
        WHERE filter::JSON->>'search_platform' = :search_platform::TEXT
          AND page = :page
          AND count_elements = 1";
        $sql = Yii::$app->db->createCommand($sql)
            ->bindValue(':page', SeoFiltersRequests::PAGE_CASINO)
            ->bindValue(':search_platform', $id)
            ->rawSql;
        $seo_filter = SeoFiltersRequests::findBySql($sql)->one();
        if ($seo_filter) {
            if ($seo_filter->slug) {
                return Url::to(['/casino/casino/filter', 'slug' => $seo_filter->slug]);
            }
            return $seo_filter->url;
        }
        return self::getUrl('search_platform', $id);
    }

    /**
     * @param  integer $id
     * @return string
     */
    public static function getSearchMethodUrl($id)
    {
        $sql = "SELECT *
        FROM seo_filters_requests
        WHERE filter::JSON->>'search_method' = :search_method::TEXT
          AND page = :page
          AND count_elements = 1";
        $sql = Yii::$app->db->createCommand($sql)
            ->bindValue(':page', SeoFiltersRequests::PAGE_CASINO)
            ->bindValue(':search_method', $id)
            ->rawSql;
        $seo_filter = SeoFiltersRequests::findBySql($sql)->one();
        if ($seo_filter) {
            if ($seo_filter->slug) {
                return Url::to(['/casino/casino/filter', 'slug' => $seo_filter->slug]);
            }
            return $seo_filter->url;
        }
        return self::getUrl('search_method', $id);
    }

    /**
     * @param  integer $id
     * @return string
     */
    public static function getSearchLicenseUrl($id)
    {
        $sql = "SELECT *
        FROM seo_filters_requests
        WHERE filter::JSON->>'search_license' = :search_license::TEXT
          AND page = :page
          AND count_elements = 1";
        $sql = Yii::$app->db->createCommand($sql)
            ->bindValue(':page', SeoFiltersRequests::PAGE_CASINO)
            ->bindValue(':search_license', $id)
            ->rawSql;
        $seo_filter = SeoFiltersRequests::findBySql($sql)->one();
        if ($seo_filter) {
            if ($seo_filter->slug) {
                return Url::to(['/casino/casino/filter', 'slug' => $seo_filter->slug]);
            }
            return $seo_filter->url;
        }
        return self::getUrl('search_license', $id);
    }

    /**
     * @param string $termsOutput
     * @return string
     */
    public static function getSearchTermOutputUrl($termsOutput)
    {
        $sql = "SELECT *
        FROM seo_filters_requests
        WHERE filter::JSON->>'search_terms_output' = :search_terms_output::TEXT
          AND page = :page
          AND count_elements = 1";
        $sql = Yii::$app->db->createCommand($sql)
            ->bindValue(':page', SeoFiltersRequests::PAGE_CASINO)
            ->bindValue(':search_terms_output', $termsOutput)
            ->rawSql;
        $seo_filter = SeoFiltersRequests::findBySql($sql)->one();
        if ($seo_filter) {
            if ($seo_filter->slug) {
                return Url::to(['/casino/casino/filter', 'slug' => $seo_filter->slug]);
            }
            return $seo_filter->url;
        }
        return self::getUrl('search_terms_output', $termsOutput);
    }

    protected static function getUrl($attribute, $search, $many = false)
    {
        $attribute = $many ? "Filters[{$attribute}][]" : "Filters[{$attribute}]";
        return Url::to(['/casino/casino/index', $attribute => $search]);
    }

}