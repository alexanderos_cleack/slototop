<?php

namespace app\modules\casino\models;

use \common\models\seotext\SeoSortsRequests;

use app\helpers\UrlHelpers;
use app\models\ABaseSort;

/**
 * Class Sort
 * @package app\modules\casino\models
 */
class Sort extends ABaseSort
{
    public function defaultSort()
    {
        $this->setRating()->desc();
        $this->checkRating();
    }

    /**
     * @return Sort
     */
    public static function createModel()
    {
        return new Sort;
    }

    /**
     * @param  Sort $sort
     * @return SeoSortsRequests
     */
    public function getSeoBySort($sort)
    {
        return SeoSortsRequests::getSeoCasinoBySort($sort);
    }

    /**
     * @param Sort $sort
     * @param integer $filter_id
     */
    public function addSort($sort, $filter_id = null)
    {
        SeoSortsRequests::addSortCasino($sort, $filter_id);
    }

    /**
     * @param string $slug
     * @return string
     */
    public function getUrlSort($slug)
    {
        return UrlHelpers::casinoSort($slug);
    }

    /**
     * @return string
     */
    public function getUrlFilter()
    {
        return '/casino/casino/filter';
    }

    /**
     * @return string
     */
    public function getUrlBase()
    {
        return UrlHelpers::casino();
    }

}