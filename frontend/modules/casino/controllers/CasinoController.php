<?php

namespace app\modules\casino\controllers;

use Yii;
use yii\data\Pagination;
use yii\web\Controller;

use common\models\casino\Casino;
use common\models\methods\MethodsPayment;
use common\models\slots\Slots;
use common\models\vendors\Vendors;

use app\helpers\BreadcrumbsHelper;
use app\modules\casino\models\Filters;
use app\modules\casino\models\Sort;
use app\helpers\UrlHelpers;
use app\modules\casino\widgets\CasinoItem;

/**
 * Class CasinoController
 * @package app\modules\casino\controllers
 */
class CasinoController extends Controller
{
    public $enableCsrfValidation = false;

    public function actions()
    {
        return [
            'rating' => [
                'class' => 'app\components\actions\RatingAction',
                'model' => Casino::class,
            ],
        ];
    }

    public function actionIndex()
    {
        BreadcrumbsHelper::add(Yii::t('app/f', 'Онлайн казино'));

        $query = Casino::find()
            ->select(['t.id', 't.title', 't.slug', 't.rating_users', 't.rating_admins', 't.enabled', '(t.rating_users + t.rating_admins)'])
            ->from(['t' => Casino::tableName()])
            ->where(['t.enabled' => true]);

        $filters = Filters::set($query);

        $pagination = $this->getPagination($query);

        $query->with(['logo']);

        $sort = Sort::set($query);

        $items = $this->getItems($query, $pagination);

        $seo = \common\models\seotext\SeoText::getSeoCasino();

        $this->saveQuery($query, $pagination);

        return $this->render('index', [
            'items' => $items,
            'count' => $pagination->totalCount,
            'pagination' => $pagination,
            'filters' => $filters,
            'sort' => $sort,
            'seo' => $seo,
        ]);
    }

    public function actionShow($slug)
    {
        $query = Casino::find()->where(['slug' => $slug]);
        if (!$query->count()) throw new \yii\web\NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        /** @var Casino $casino */
        $casino = $query->one();

        BreadcrumbsHelper::add(Yii::t('app/f', 'Онлайн казино'), UrlHelpers::casino());
        BreadcrumbsHelper::add($casino->getTitlePreview());

        return $this->render('show', [
            'casino' => $casino,
        ]);
    }

    public function actionGame($game)
    {
        $game = Slots::find()->where(['slug' => $game])->one();
        if (!$game) throw new \yii\web\NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        $query = $game->getCasino_query();

        $filters = Filters::set($query);

        $pagination = $this->getPagination($query);

        $sort = Sort::set($query, ['game' => $game->slug]);

        $items = $this->getItems($query, $pagination);

        $seo = \common\models\seotext\SeoText::getSeoCasinoGame($game);

        $this->saveQuery($query, $pagination);

        return $this->render('index', [
            'items' => $items,
            'count' => $pagination->totalCount,
            'pagination' => $pagination,
            'filters' => $filters,
            'sort' => $sort,
            'seo' => $seo,
        ]);
    }

    public function actionPayment($method)
    {
        $method = MethodsPayment::find()->where(['slug' => $method])->one();
        if (!$method) throw new \yii\web\NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        $query = $method->getCasino_query();

        $filters = Filters::set($query);

        $pagination = $this->getPagination($query);

        $sort = Sort::set($query, ['method' => $method->slug]);

        $items = $this->getItems($query, $pagination);

        $seo = \common\models\seotext\SeoText::getSeoPaymentsCasion($method);

        $this->saveQuery($query, $pagination);

        return $this->render('index', [
            'items' => $items,
            'count' => $pagination->totalCount,
            'pagination' => $pagination,
            'filters' => $filters,
            'sort' => $sort,
            'seo' => $seo,
        ]);

    }

    public function actionSame($casino)
    {
        /** @var Casino $casino */
        $casino = Casino::find()->where(['slug' => $casino])->one();
        if (!$casino) throw new \yii\web\NotFoundHttpException(Yii::t('yii', 'Page not found.'));
        $query = $casino->getCasinoSimilar_query();

        $filters = Filters::set($query);

        $pagination = $this->getPagination($query);

        $sort = Sort::set($query, ['slug' => $casino->slug]);

        $items = $this->getItems($query, $pagination);

        $seo = \common\models\seotext\SeoText::getSeoCasinoSame($casino);

        $this->saveQuery($query, $pagination);

        return $this->render('index', [
            'items' => $items,
            'count' => $pagination->totalCount,
            'pagination' => $pagination,
            'filters' => $filters,
            'sort' => $sort,
            'seo' => $seo,
        ]);

    }

    public function actionVendorPopularCasino($vendor)
    {
        /** @var \app\models\vendors\Vendors $vendor */
        $vendor = \app\models\vendors\Vendors::find()->where(['slug' => $vendor])->one();
        if (!$vendor) throw new \yii\web\NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        $query = $vendor->getVendorsPopularCasino();

        $filters = Filters::set($query);

        $pagination = $this->getPagination($query);

        $sort = Sort::set($query, ['slug' => $vendor->slug]);
        $sort->setRating()->desc();
        $sort->checkRating();

        $items = $this->getItems($query, $pagination);

        $seo = \common\models\seotext\SeoText::getSeoVendorPopularCasino($vendor);

        $this->saveQuery($query, $pagination);

        return $this->render('index', [
            'items' => $items,
            'count' => $pagination->totalCount,
            'pagination' => $pagination,
            'filters' => $filters,
            'sort' => $sort,
            'seo' => $seo,
        ]);
    }

    public function actionFilter($slug)
    {
        $seo = \common\models\seotext\SeoFiltersRequests::getSeoCasino($slug);
        if (!$seo) throw new \yii\web\NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        $model = \app\models\casino\Casino::class;
        $query = $model::find()->distinct()->select(['t.id', 't.title', 't.slug', 't.rating_users', 't.rating_admins', 't.enabled', '(t.rating_users + t.rating_admins)'])->from(['t' => $model::tableName()])->where(['t.enabled' => true]);

        $filters = Filters::set($query, json_decode($seo->filter, true));

        $pagination = $this->getPagination($query);

        $query->with(['logo']);

        $sort = Sort::set($query, ['slug' => $seo->slug], null, $seo);

        $items = $this->getItems($query, $pagination);

        $this->saveQuery($query, $pagination);

        BreadcrumbsHelper::add(Yii::t('app/f', 'Онлайн казино'), UrlHelpers::casino());
        BreadcrumbsHelper::add($seo->meta_h1);

        return $this->render('index', [
            'items' => $items,
            'count' => $pagination->totalCount,
            'pagination' => $pagination,
            'filters' => $filters,
            'sort' => $sort,
            'seo' => $seo,
        ]);
    }

    public function actionSort($slug)
    {
        $seo_sort = \common\models\seotext\SeoSortsRequests::getSeoCasino($slug);
        if (!$seo_sort) throw new \yii\web\NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        $model = \app\models\casino\Casino::class;
        $query = $model::find()->distinct()->select(['t.id', 't.title', 't.slug', 't.rating_users', 't.rating_admins', 't.enabled', '(t.rating_users + t.rating_admins)'])->from(['t' => $model::tableName()])->where(['t.enabled' => true]);

        if ($seo_sort->filter) {
            $filters = Filters::set($query, json_decode($seo_sort->filter->filter, true));
        } else {
            $filters = Filters::set($query);
        }

        $pagination = $this->getPagination($query);

        $query->with(['logo']);

        if ($seo_sort->filter) {
            $sort = Sort::set($query, ['slug' => $seo_sort->filter->slug], json_decode($seo_sort->sort, true), $seo_sort->filter);
        } else {
            $sort = Sort::set($query, [], json_decode($seo_sort->sort, true));
        }

        $items = $this->getItems($query, $pagination);

        $this->saveQuery($query, $pagination);

        return $this->render('index', [
            'items' => $items,
            'count' => $pagination->totalCount,
            'pagination' => $pagination,
            'filters' => $filters,
            'sort' => $sort,
            'seo' => $seo_sort,
        ]);
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function actionItems()
    {
        $session = Yii::$app->session;
        if ($session->has('casino')) {
            list($query, $pagination) = array_values($session->get('casino'));
            $pagination->page = Yii::$app->request->get('page');

            $items = [];
            foreach ($query->offset($pagination->offset)->limit($pagination->limit)->each() as $item) {
                $items[] = CasinoItem::widget(['model' => $item]);
            };

            Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
            return join('', $items);
        }
    }

    /**
     * @param string $slug
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionRedirect($slug) {
        $this->layout = '//casinoRedirect';

        $query = Casino::find()->where(['slug' => $slug]);
        if (!$query->count()) throw new \yii\web\NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        /** @var Casino $casino */
        $casino = $query->one();

        if ($casino->ref_url) {
            Yii::$app->response->redirect($casino->getRedirectUrl())->send();
        } elseif (!$casino->casino_redirect_url) {
            Yii::$app->response->redirect($casino->getRedirectUrl())->send();
        }

        return $this->render('redirect', [
            'casino' => $casino,
        ]);
    }

    public function actionFilters()
    {
        if (Yii::$app->request->isAjax) {
            $query = Casino::find()
                ->select(['t.id', 't.title'])
                ->from(['t' => Casino::tableName()])
                ->where(['t.enabled' => true]);

            $filters = new Filters();
            $filters->query = $query;

            if ($filters->load(Yii::$app->request->post()) && $filters->validate()) {
                Filters::search($query, $filters);

                return $this->renderPartial('filters/form', [
                    'filters' => $filters,
                ]);
            }
        }

        throw new \yii\web\BadRequestHttpException();
    }

    /**
     * @param  \yii\db\ActiveQuery $query
     */
    protected function saveQuery(\yii\db\ActiveQuery $query, \yii\data\Pagination $pagination)
    {
        $session = Yii::$app->session;
        $session->set('casino', [
            'query' => $query,
            'pagination' => $pagination,
        ]);
    }

    /**
     * @param  \yii\db\ActiveQuery $query
     * @return \yii\data\Pagination
     */
    protected function getPagination(\yii\db\ActiveQuery $query)
    {
        $count_query = clone $query;
        $count = $count_query->count();
        return new Pagination(['totalCount' => $count, 'pageSize' => 52]);
    }

    /**
     * @param  \yii\db\ActiveQuery $query
     * @param  \yii\data\Pagination $pagination
     * @return \common\models\casino\Casino[]
     */
    protected function getItems(\yii\db\ActiveQuery $query, \yii\data\Pagination $pagination)
    {
        $offset_query = clone $query;
        return $offset_query->offset($pagination->offset)->limit($pagination->limit)->all();
    }

}