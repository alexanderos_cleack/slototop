<?php 

/**
 * @var $this yii\web\View
 */

$this->beginContent('@app/views/layouts/default.php');

echo $content;

$this->endContent();

?>