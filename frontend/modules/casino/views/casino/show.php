<?php 

use yii\helpers\Html;
use yii\helpers\Url;

use app\modules\casino\models\Filters;

/**
 * @var  $this yii\web\View
 * @var  $casino \common\models\casino\Casino
 */

?>

<?php

    \app\assets\RatingAsset::register($this);

    // $this->title = $casino->title;
    if ($casino->meta_title) $this->title = $casino->meta_title;
    if ($casino->meta_keywords) $this->registerMetaTag(['name' => 'keywords', 'content' => $casino->meta_keywords]);
    if ($casino->meta_description) $this->registerMetaTag(['name' => 'description', 'content' => $casino->meta_description]);

    if (!$casino->enabled) $this->registerMetaTag(['name' => 'robots', 'content' => 'noindex,nofollow']);

?>

<?php echo \app\widgets\Ogp::widget([
    'article_section' => Yii::t('app/f', 'Casino'),
    'image' => $casino->logo,
    'model' => $casino,
    'url' => Url::to(['/casino/casino/show', 'slug' => $casino->slug], true)
]) ?>

<?php echo app\widgets\SchemaArticle::widget([
    'model' => $casino,
    'add_content' => [
        \app\widgets\SchemaPerson::widget(['model' => $casino]),
        \app\widgets\SchemaOrganization::widget(['model' => $casino]),
        \app\widgets\SchemaAggregateRating::widget(['model' => $casino]),
        \app\widgets\SchemaImageObject::widget(['image' => $casino->logo]),
    ],
]) ?>

<div class="float-left">
    <?= \app\widgets\Breadcrumbs::widget(); ?>
    <div class="block-all">
        <div class="bl-red-title">
            <span class="bl-img"></span>
            <div class="bl-t-title">
                <span><h1><?= $casino->title ?></h1></span>
                <a href="<?= Yii::$app->request->referrer ?>"><?= Yii::t('app/f', 'назад к результатам >') ?></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>

    <div class="block-all grey-border-bottom block-game-bg">
        <div class="game-type-left">
            <div class="game-type game-type-img">
                <?= \app\helpers\ImageHelper::image($casino->getImageLogo(), $casino->title) ?>
            </div>
            <div class="game-type casino-page">
                <span><?= Yii::t('app/f', 'Рейтинг посетителей:') ?></span>
                <div class="rating-progress">
                    <div><span style="width: <?= $casino->rating_users ?>%"></span></div>
                    <div><?= $casino->rating_users ?>%</div>
                    <div class="clear"></div>
                </div>
                <span class="span-rate-casino"><?= Yii::t('app/f', 'Год основания:') ?> <span class="text-blue"><?= $casino->year_established ?></span></span> <br>
                <span><?= Yii::t('app/f', 'Адрес сайта:') ?> <span class="text-blue">
                        <a data-href="<?= $casino->getRedirectUrl() ?>" target="_blank"><?= $casino->casino_url ?></a>
                    </span>
                </span>
            </div>

            <div class="clear"></div>
        </div>

        <div class="game-type-right">
            <div class="game-type game-type-casino">
                <span>Ваша оценка:</span>
                <div id="rating">
                    <input type="hidden" name="val" value="<?= $casino->rating ?>">
                    <input type="hidden" name="_csrf" value="<?= Yii::$app->request->csrfToken ?>"> 
                </div><br>
                <span class="span-rate-casino">Кол-во игр: <span class="text-blue"><?= $casino->amount_games ?></span></span>
            </div>
            <div class="game-type game-type-rate-casino">
                <span class="gt-c-span">
                    <?= \app\helpers\ImageHelper::image('/img/casino/info.png') ?><?= Yii::t('app/f', 'Рейтинг Slototop') ?>
                </span>
                <span class="gt-c-span"><span><?= (int)$casino->ratingAll ?></span>/100</span>
                <a class="btn-red" data-href="<?= $casino->getRedirectUrl() ?>" rel="nofollow" target="_blank"><?= Yii::t('app/f', 'ПЕРЕЙТИ') ?></a>
            </div>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <div class="block-all grey-border-bottom chr-casino-block">

        <?php if ($countries = $casino->getCountries_site()): ?>
        <div class="table-game-col">
            <div class="table-game-row">
                <i class="icon-chr chr1"></i>
                <span><?= Yii::t('app/f', 'Принимает игроков') ?></span>
            </div>
            <div class="table-game-row flags-list">
                <span class="text-blue">
                    <?php $items = []; ?>
                    <?php foreach ($countries as $item): ?>
                        <?php $items[] = Html::a($item->logo_mini ? \app\helpers\ImageHelper::image($item->logo_mini, $item->title, ['title' => $item->title]) : $item->title, Filters::getSearchCountryUrl($item->id)); ?>
                    <?php endforeach; ?>
                    <?= join(' ', $items) ?>
                </span>
            </div>
        </div>
        <?php endif; ?>

        <?php if ($currencies = $casino->getCurrencies_site()): ?>
        <div class="table-game-col">
            <div class="table-game-row">
                <i class="icon-chr chr2"></i>
                <span><?= Yii::t('app/f', 'Валюты') ?></span>
            </div>
            <div class="table-game-row">
                <span class="text-blue">
                    <?php $items = []; ?>
                    <?php foreach ($currencies as $item): ?>
                        <?php $items[] = Html::a($item->logo_mini ? \app\helpers\ImageHelper::image($item->logo_mini, null, ['title' => $item->title]) : $item->title, Filters::getSearchCurrencyUrl($item->id)); ?>
                    <?php endforeach; ?>
                    <?= join(' / ', $items) ?>
                </span>
            </div>
        </div>
        <?php endif; ?>

        <?php if ($methods = $casino->getMethodsPayment_site()): ?>
        <div class="table-game-col">
            <div class="table-game-row">
                <i class="icon-chr chr3"></i>
                <span><?= Yii::t('app/f', 'Методы оплаты') ?></span>
            </div>
            <div class="table-game-row pay-method-list">
                <span class="text-blue">
                    <?php $items = []; ?>
                    <?php foreach ($methods as $item): ?>
                        <?php $items[] = Html::a($item->logo_mini ? \app\helpers\ImageHelper::image($item->logo_mini, $item->title, ['title' => $item->title]) : $item->title, Filters::getSearchMethodUrl($item->id)); ?>
                    <?php endforeach; ?>
                    <?= join(' ', $items) ?>
                </span>
            </div>
        </div>
        <?php endif; ?>

        <?php if ($methods = $casino->getMethodsOutput_site()): ?>
        <div class="table-game-col">
            <div class="table-game-row">
                <i class="icon-chr chr8"></i>
                <span><?= Yii::t('app/f', 'Методы выплаты') ?></span>
            </div>
            <div class="table-game-row pay-method-list">
                <span class="text-blue">
                    <?php $items = []; ?>
                    <?php foreach ($methods as $item): ?>
                        <?php $items[] = Html::a($item->logo_mini ? \app\helpers\ImageHelper::image($item->logo_mini, $item->title, ['title' => $item->title]) : $item->title, Filters::getSearchMethodUrl($item->id)); ?>
                    <?php endforeach; ?>
                    <?= join(' ', $items) ?>
                </span>
            </div>
        </div>
        <?php endif; ?>

        <?php if (false): ?>
        <div class="table-game-col">
            <div class="table-game-row">
                <i class="icon-chr chr4"></i>
                <span>Payout</span>
            </div>
            <div class="table-game-row">
                <div class="casino-rate">
                    <span style="width: <?= $casino->payout ?>%"></span>
                </div>
                <div><?= $casino->payout ?>%</div>
                <div class="clear"></div>
            </div>
        </div>
        <?php endif; ?>

        <div class="table-game-col">
            <div class="table-game-row">
                <i class="icon-chr chr5"></i>
                <span><?= Yii::t('app/f', 'Мин. депозит') ?></span>
            </div>
            <div class="table-game-row">
                <span class="text-blue"><?= $casino->getMinDeposit_site() ?></span>
            </div>
        </div>
        <div class="table-game-col">
            <div class="table-game-row">
                <i class="icon-chr chr6"></i>
                <span><?= Yii::t('app/f', 'Мин. выплата') ?></span>
            </div>
            <div class="table-game-row">
                <span class="text-blue"><?= $casino->getMinOutput_site() ?></span>
            </div>
        </div>

        <?php if ($casino->terms_output): ?>
            <div class="table-game-col">
                <div class="table-game-row">
                    <i class="icon-chr chr7"></i>
                    <span><?= Yii::t('app/f', 'Сроки вывода') ?></span>
                </div>
                <div class="table-game-row">
                    <span class="text-blue"><?php echo Html::a($casino->terms_output, Filters::getSearchTermOutputUrl($casino->terms_output)); ?></span>
                </div>
            </div>
        <?php endif; ?>

        <?php if ($vendors = $casino->getVendors_site()): ?>
        <div class="table-game-col">
            <div class="table-game-row">
                <i class="icon-chr chr9"></i>
                <span><?= Yii::t('app/f', 'Платформы') ?></span>
            </div>
            <div class="table-game-row pay-method-list">
                <span class="text-blue">
                    <?php $items = []; ?>
                    <?php foreach ($vendors as $item): ?>
                        <?php $items[] = Html::a($item->logo_casino ? \app\helpers\ImageHelper::image($item->logo_casino, $item->title, ['title' => $item->title]) : $item->title, Filters::getSearchVendorUrl($item->id)); ?>
                    <?php endforeach; ?>
                    <?= join(' ', $items) ?>
                </span>
            </div>
        </div>
        <?php endif; ?>

        <?php if ($languages = $casino->getLanguages_site()): ?>
        <div class="table-game-col">
            <div class="table-game-row">
                <i class="icon-chr chr10"></i>
                <span><?= Yii::t('app/f', 'Язык интерфейса') ?></span>
            </div>
            <div class="table-game-row">
                <span class="text-blue">
                    <?php $items = []; ?>
                    <?php foreach ($languages as $item): ?>
                        <?php $items[] = Html::a($item->logo_mini ? \app\helpers\ImageHelper::image($item->logo_mini, $item->title, ['title' => $item->title]) : $item->title, Filters::getSearchLanguageUrl($item->id)); ?>
                    <?php endforeach; ?>
                    <?= join(' / ', $items) ?>
                </span>
            </div>
        </div>
        <?php endif; ?>

        <?php if ($types = $casino->getBonusesTypes_site()): ?>
        <div class="table-game-col">
            <div class="table-game-row">
                <i class="icon-chr chr11"></i>
                <span><?= Yii::t('app/f', 'Бонусы') ?></span>
            </div>
            <div class="table-game-row">
                <?php $items = []; ?>
                <?php foreach ($types as $item): ?>
                    <?php $items[] = Html::a($item->title, Filters::getSearchBonusUrl($item->id)); ?>
                <?php endforeach; ?>
                <span class="text-blue"><?= join('  ', $items) ?></span>
            </div>
        </div>
        <?php endif; ?>
        
        <?php if ($platforms = $casino->getPlatforms_site()): ?>
        <div class="table-game-col">
            <div class="table-game-row">
                <i class="icon-chr chr12"></i>
                <span><?= Yii::t('app/f', 'Технологии') ?></span>
            </div>
            <div class="table-game-row pay-method-list">
                <?php $items = []; ?>
                <?php foreach ($platforms as $item): ?>
                    <?php $items[] = Html::a($item->logo_mini ? \app\helpers\ImageHelper::image($item->logo_mini, $item->title, ['title' => $item->title]) : $item->title, Filters::getSearchPlatformUrl($item->id)); ?>
                <?php endforeach; ?>
                <span class="text-blue"><?= join(' ', $items) ?></span>
            </div>
        </div>
        <?php endif; ?>

        <div class="table-game-col">
            <div class="table-game-row">
                <i class="icon-chr chr13"></i>
                <span><?= Yii::t('app/f', 'Лицензия') ?></span>
            </div>
            <div class="table-game-row">
                <span class="text-blue"><?php if ($casino->license) echo Html::a($casino->license->title, Filters::getSearchLicenseUrl($casino->license->id)); ?></span>
            </div>
        </div>

        <?php if ($casino->video_viewer_url): ?>
        <div class="table-game-col">
            <div class="table-game-row">
                <i class="icon-chr chr14"></i>
                <span><?= Yii::t('app/f', 'Видеообзор') ?></span>
            </div>
            <div class="table-game-row">
                <span class="text-blue"><?= $casino->video_viewer_url ?></span>
            </div>
        </div>
        <?php endif; ?>

        <?php if ($casino->email): ?>
        <div class="table-game-col">
            <div class="table-game-row">
                <i class="icon-chr chr15"></i>
                <span><?= Yii::t('app/f', 'E-mail') ?></span>
            </div>
            <div class="table-game-row">
                <span class="text-blue"><?= $casino->email ?></span>
            </div>
        </div>
        <?php endif; ?>

        <?php if ($casino->phone): ?>
        <div class="table-game-col">
            <div class="table-game-row">
                <i class="icon-chr chr16"></i>
                <span><?= Yii::t('app/f', 'Телефон') ?></span>
            </div>
            <div class="table-game-row"><?= $casino->phone ?></div>
        </div>
        <?php endif; ?>

        <div class="table-game-col">
            <div class="table-game-row">
                <i class="icon-chr chr17"></i>
                <span><?= Yii::t('app/f', 'Чат') ?></span>
            </div>
            <div class="table-game-row">
                <span class="text-blue"><?php echo $casino->chat ? Yii::t('app', 'Yes') : Yii::t('app', 'No') ?></span>
            </div>
        </div>

        <div class="clear"></div>
    </div>

    <div class="block-all grey-border-bottom chr-casino-block">
        <div class="bl-red-title-normal">
            <span><?= Yii::t('app/f', 'Описание казино') ?></span>

            <div style="float: right"><?= \app\widgets\SNShare::widget() ?></div>

            <div class="clear"></div>
        </div>

        <?php $screen_seq = 1; ?>
        <?= $casino->description ?>
        <?php if ($casino->screens): ?>
            <div class="game-small-pr">
                <?php foreach ($casino->screens as $screen): ?>
                    <?= \app\helpers\ImageHelper::image($screen, $casino->title . ' ' . $screen_seq++) ?>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>

        <div class="red-line"></div>

        <div class="chr-other-title"><?= Yii::t('app/f', 'Внешний вид') ?></div>

        <?= $casino->description_view ?>
        <?php if ($casino->screens_view): ?>
            <div class="game-small-pr">
                <?php foreach ($casino->screens_view as $screen): ?>
                    <?= \app\helpers\ImageHelper::image($screen, $casino->title . ' ' . $screen_seq++) ?>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>



        <div class="red-line"></div>

        <div class="chr-other-title"><?= Yii::t('app/f', 'Регистрация') ?></div>

        <?= $casino->description_registration ?>
        <?php if ($casino->screens_registration): ?>
            <div class="game-small-pr">
                <?php foreach ($casino->screens_registration as $screen): ?>
                    <?= \app\helpers\ImageHelper::image($screen, $casino->title . ' ' . $screen_seq++) ?>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>


        <div class="red-line"></div>

        <div class="chr-other-title"><?= Yii::t('app/f', 'Ассортимент игр казино') ?></div>

        <?= $casino->description_games ?>
        <?php if ($casino->screens_games): ?>
            <div class="game-small-pr">
                <?php foreach ($casino->screens_games as $screen): ?>
                    <?= \app\helpers\ImageHelper::image($screen, $casino->title . ' ' . $screen_seq++) ?>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>

        <?php if ($casino->description_additional): ?>
            <div class="red-line"></div>
            <?= $casino->description_additional ?>
        <?php endif; ?>

    </div>

</div>

<?php $this->beginBlock('widgets'); ?>
    <div class="fr-right-color">
        <?= \app\widgets\CasinoBonuses::widget(['casino' => $casino]) ?>
        <?= \app\widgets\CasinoPopularGames::widget(['casino' => $casino]) ?>
        <?= \app\widgets\CasinoSimilar::widget(['casino' => $casino]) ?>
    </div>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('admin'); ?>
    <a href="<?php echo \app\helpers\AdminHelper::getCasinoItemsUpdateUrl($casino->id); ?>" target="_blank">Редактировать</a>&nbsp;|
    <?= \app\widgets\Link::widget(['model' => $casino]) ?>
<?php $this->endBlock(); ?>

<script>
    rating('#rating', 'casino', '<?= $casino->slug ?>');
</script>