<?php
/**
 * @var \app\modules\casino\models\Filters $filters
 */
?>

<div class="label">
    <span class="sb-label-title"><?= $filters->getAttributeLabel('search_country') ?>:</span>
    <?= \yii\helpers\Html::activeDropdownList($filters, 'search_country', $filters->getCountriesForList(), ['prompt' => Yii::t('app/f', 'Filter select')]) ?>
</div>

<div class="label">
    <span class="sb-label-title"><?= $filters->getAttributeLabel('search_currency') ?>:</span>
    <?= \yii\helpers\Html::activeDropdownList($filters, 'search_currency', $filters->getCurrenciesForList(), ['prompt' => Yii::t('app/f', 'Filter select')]) ?>
</div>

<div class="label">
    <span class="sb-label-title"><?= $filters->getAttributeLabel('search_method') ?>:</span>
    <?= \yii\helpers\Html::activeDropdownList($filters, 'search_method', $filters->getMethodsPaymentForList(), ['prompt' => Yii::t('app/f', 'Filter select')]) ?>
</div>

<div class="label">
    <span class="sb-label-title"><?= $filters->getAttributeLabel('search_vendor') ?>:</span>
    <?= \yii\helpers\Html::activeDropdownList($filters, 'search_vendor', $filters->getVendorsForList(), ['prompt' => Yii::t('app/f', 'Filter select')]) ?>
</div>

<div class="label">
    <span class="sb-label-title"><?= $filters->getAttributeLabel('search_language') ?>:</span>
    <?= \yii\helpers\Html::activeDropdownList($filters, 'search_language', $filters->getLanguagesForList(), ['prompt' => Yii::t('app/f', 'Filter select')]) ?>
</div>

<div class="label">
    <span class="sb-label-title"><?= $filters->getAttributeLabel('search_bonus') ?>:</span>
    <?= \yii\helpers\Html::activeDropdownList($filters, 'search_bonus', $filters->getBonusesTypesForList(), ['prompt' => Yii::t('app/f', 'Filter select')]) ?>
</div>

<div class="label">
    <?= \yii\helpers\Html::submitButton('Подобрать!', ['class' => 'btn-green']) ?>
</div>

<script>
    $('select').on('change', function () {
        $.ajax({
            'type': 'post',
            'url': '<?php echo \yii\helpers\Url::to(['/casino/casino/filters/']); ?>',
            'data': $('#filters').serialize()
        }).done(function (data) {
            $('#ajax-filter').html(data);
            $('input, select').trigger('refresh');
            $('input, select').styler();
            $(".jq-selectbox__dropdown ul").addClass('mCustomScrollbar').mCustomScrollbar({
                theme: "minimal"
            });
        });
    });
</script>