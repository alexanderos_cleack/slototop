<?php
/**
 * @var \yii\web\View $this
 * @var \app\modules\casino\models\Filters $filters
 */
?>

<?php

$model = isset($filters) ? $filters : new \app\modules\casino\models\Filters();

?>

<div class="sb-in-filters">
    <div class="sb-title">
        <?= Yii::t('app/f', 'Фильтровать по:') ?>
    </div>

    <?php $form = \yii\widgets\ActiveForm::begin(['id' => 'filters', 'method' => 'post', 'action' => \app\helpers\UrlHelpers::casino()]); ?>
        <div id="ajax-filter">
            <?php echo $this->context->renderPartial('filters/form', [
                'filters' => $filters,
            ]); ?>
        </div>
    <?php \yii\widgets\ActiveForm::end(); ?>
</div>