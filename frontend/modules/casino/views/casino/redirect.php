<?php
/**
 * @var \app\modules\casino\controllers\CasinoController $this
 * @var \common\models\casino\Casino $casino
 */
?>

<script language='javascript'>
    setTimeout(function () {
        document.location.href = "//<?php echo $casino->casino_redirect_url; ?>";
    }, 1500);
</script>

<div class="loading-page">
    <span><?= Yii::t('app/f' , 'К сожалению, данный сайт недоступен в Вашей стране') ?></span>
    <div>
        <?= \app\helpers\ImageHelper::image($casino->logo) ?>
    </div>
    <span><?= Yii::t('app/f' , 'Переводим Вас в лучшее заведение Вашего региона') ?></span>
    <img src="/img/loader.svg">
</div>