<?php

namespace app\modules\casino;

/**
 * Class Module
 * @package app\modules\casino
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\casino\controllers';
    public $layout = 'main';

    public function init()
    {
        parent::init();
    }

}
