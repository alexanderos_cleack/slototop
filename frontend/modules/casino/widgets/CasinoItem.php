<?php

namespace app\modules\casino\widgets;

use yii\base\Widget;

use common\models\casino\Casino;

/**
 * Class CasinoItem
 * @package app\modules\casino\widgets
 *
 * @property Casino $model
 */
class CasinoItem extends Widget
{
    public $model;

    public function run()
    {
        return $this->render('casinoItem', [
            'model' => $this->model,
        ]);
    }

}