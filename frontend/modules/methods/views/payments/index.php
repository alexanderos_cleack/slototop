<?php
/**
 * @var yii\web\View $this
 * @var \common\models\methods\MethodsPayment[] $items
 * @var \common\models\seotext\SeoText $seo
 * @var \app\modules\casino\models\Sort $sort
 * @var \app\modules\casino\models\Filters $filters
 */
?>

<?php 

    \app\assets\FormstylerAsset::register($this);

    $this->registerJsFile('/js/formstyler.js', ['position' => \yii\web\View::POS_END]);

    if (!$seo || $sort->isValidate) {
        $this->registerMetaTag(['name' => 'robots', 'content' => 'noindex,nofollow']);
    }

    if ($seo) {
        $this->title = $seo->meta_title;
        $this->registerMetaTag(['name' => 'keywords', 'content' => $seo->meta_keywords]);
        $this->registerMetaTag(['name' => 'description', 'content' => $seo->meta_description]);
    }

?>

<div class="float-left">
    <?= \app\widgets\Breadcrumbs::widget(); ?>
    <div class="block-all">
        <div class="bl-red-title">
            <span class="bl-img"></span>
            <div class="bl-t-title">
                <span><?= $seo && $seo->meta_h1 ? $seo->meta_h1 : Yii::t('app/f', 'Payment system') ?></span>
                <?php if (false): ?><div class="bl-all-count"><?= Yii::t('app/f', 'Total found: {count}', ['count' => $count]) ?></div><?php endif; ?>

                <div class="bl-sort">
                    <div><?= Yii::t('app/f', 'Total found: {count}', ['count' => $count]) ?></div>
                    <?php if (false): ?>
                        <a href="#" class="bl-sort-active"></a>
                        <a href="#"></a>
                    <?php endif; ?>
                </div>
            </div>

            <div class="clear"></div>
        </div>

        <div class="games">
            <?php foreach ($items as $item): ?>
                <?= \app\modules\methods\widgets\MethodPaymentItem::widget(['model' => $item]) ?>
            <?php endforeach; ?>

            <div id="game-ajax"></div>

            <div class="clear"></div>
        </div>

        <div align="center">
            <button id="load" class="btn-load"><?= Yii::t('app/f', 'Загрузить') ?></button>
        </div>

        <div class="line"></div>

        <?php if ($seo): ?>
            <?php
                $this->beginBlock('seo');
                echo $seo->description;
                $this->endBlock('seo');
            ?>
            <div id='hereSeoText'></div><script>$('#hereSeoText').html($('#seoTextWidget').html());$('#seoTextWidget').html("");</script>
        <?php endif; ?>
    </div>
</div>

<?php

$this->params['active_sort'] = true;
$this->params['active_sort_title'] = true;
$this->params['active_sort_new'] = true;
$this->params['sort'] = $sort;

?>

<?php $this->beginBlock('widgets'); ?>
    <div>
        <?= \app\widgets\NewCasino::widget() ?>
        <?= \app\widgets\NewBonuses::widget() ?>
        <?= \app\widgets\NewGames::widget() ?>
    </div>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('admin'); ?>
    <?= \app\widgets\AdminSeo::widget(['filter' => $filters, 'sort' => $sort]) ?>
<?php $this->endBlock(); ?>

<script>
    var page = 1;
    $('#load').click(function () {
        $.ajax({
            url: '<?= \app\helpers\UrlHelpers::methodsPaymentsItems() ?>',
            data: {page: page}
        }).done(function(data) {
            if (data) {
                $('#game-ajax').append(data);
                page++;
            } else {
                $('#load').hide();
            }
        });
    });
</script>