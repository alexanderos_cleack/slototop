<?php 

use yii\helpers\Url;

/**
 * @var  $this yii\web\View
 * @var  $method common\models\methods\MethodsPayment
 */

?>

<?php

    if ($method->meta_title) $this->title = $method->meta_title;
    if ($method->meta_keywords) $this->registerMetaTag(['name' => 'keywords', 'content' => $method->meta_keywords]);
    if ($method->meta_description) $this->registerMetaTag(['name' => 'description', 'content' => $method->meta_description]);

    if (!$method->enabled) $this->registerMetaTag(['name' => 'robots', 'content' => 'noindex,nofollow']);

?>

<?php echo \app\widgets\Ogp::widget([
    'article_section' => Yii::t('app/f', 'Payment system'),
    'image' => $method->logo,
    'model' => $method,
    'url' => Url::to(['/methods/payments/show', 'slug' => $method->slug], true)
]) ?>

<?php echo app\widgets\SchemaArticle::widget([
    'model' => $method,
    'add_content' => [
        \app\widgets\SchemaPerson::widget(['model' => $method]),
        \app\widgets\SchemaOrganization::widget(['model' => $method]),
        \app\widgets\SchemaAggregateRating::widget(['model' => $method]),
        \app\widgets\SchemaImageObject::widget(['image' => $method->logo]),
    ],
]) ?>

<div class="float-left pay-method-open">
    <?= \app\widgets\Breadcrumbs::widget(); ?>
    <div class="block-all">
        <div class="bl-red-title">
            <span class="bl-img bl-img-cherry"></span>
            <div class="bl-t-title">
                <span><h1><?= $method->title ?></h1></span>
                <a href="<?= Yii::$app->request->referrer ?>"><?= Yii::t('app/f', 'назад к результатам >') ?></a>
            </div>

            <div class="clear"></div>
        </div>
    </div>

    <div class="block-all grey-border-bottom bonus-page block-game-bg">
        <div class="game-type-left">
            <div class="game-type game-type-img">
                <?= \app\helpers\ImageHelper::image($method->getImageLogo(), $method->title) ?>
            </div>
            <div class="game-type casino-page">

                <?php if ($categories = $method->categories): ?>
                    <div><?= Yii::t('app/f', 'Тип:') ?>
                    <?php foreach ($categories as $category): ?>
                        <span class="text-blue"><a href="<?= \app\modules\methods\models\Filters::getSearchTypeUrl($category->id) ?>"><?= $category->title ?></a></span>
                    <?php endforeach; ?>
                    </div>
                <?php endif; ?>

                <div><?= Yii::t('app/f', 'Официальный сайт:') ?> <span class="text-blue"><?= $method->url_site ?></span></div>

                <?php if ($currencies = $method->currencies): ?>
                    <div><?= Yii::t('app/f', 'Доступные валюты:') ?>
                    <?php foreach ($currencies as $currency): ?>
                        <span class="text-blue"><span class="text-blue"><a href="<?= \app\modules\methods\models\Filters::getSearchCurrencyUrl($currency->id) ?>"><?= $currency->iso ?></a></span></span>
                    <?php endforeach; ?>
                    </div>
                <?php endif; ?>

                <?php if ($output_site = $method->output_site): ?>
                    <div><?= Yii::t('app/f', 'Возможность вывода:') ?> <span class="text-blue"><?= $output_site ?></span></div>
                <?php endif; ?>
            </div>

            <div class="clear"></div>
        </div>

        <div class="game-type-right">
            <div class="game-type game-type-rate-casino">
                <?= Yii::t('app/f', 'Комиссия:') ?> <br>
                <span><?= $method->commission ?></span>
                <div class="line"></div>
                <?= Yii::t('app/f', 'Сроки зачисления:') ?> <br>
                <span><?= $method->terms_payment ?></span>
            </div>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <div class="block-all chr-casino-block">
        <div class="bl-red-title-normal">
            <span><?= Yii::t('app/f', 'Описание') ?></span>

            <div style="float: right"><?= \app\widgets\SNShare::widget() ?></div>

            <div class="clear"></div>
        </div>

        <?= $method->description ?>

    </div>

<!--     <div class="block-all">
        <div class="roll-up-block">
            <span class="roll-up-block-arrow"></span>
            <span>Свернуть описание</span>
        </div>
    </div> -->

    <div class="block-all chr-casino-block">
        <div class="red-line"></div>
        <div class="chr-other-title"><?= Yii::t('app/f', 'Как пользоваться?') ?></div>

        <?= $method->how_to_use ?>

    </div>

<!--     <div class="block-all">
        <div class="roll-up-block">
            <span class="roll-up-block-arrow"></span>
            <span>Свернуть описание</span>
        </div>
    </div> -->

</div>

<?php $this->beginBlock('widgets'); ?>
    <div class="fr-right-color">
        <?= \app\widgets\PayMethodCasino::widget(['method' => $method]) ?>
    </div>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('admin'); ?>
    <a href="/admin/methods/methods-payment-items/update/<?= $method->id; ?>" target="_blank"><?= Yii::t('app/f', 'Редактировать') ?></a>&nbsp;|
    <?= \app\widgets\Link::widget(['model' => $method]) ?>
<?php $this->endBlock(); ?>