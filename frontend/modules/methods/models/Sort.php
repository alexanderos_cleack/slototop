<?php

namespace app\modules\methods\models;

use \common\models\seotext\SeoSortsRequests;

use app\helpers\UrlHelpers;
use app\models\ABaseSort;

/**
 * Class Sort
 * @package app\modules\methods\models
 */
class Sort extends ABaseSort
{
    public function defaultSort()
    {
        $this->setTitle()->asc();
        $this->checkTitle();
    }

    /**
     * @return Sort
     */
    public static function createModel()
    {
        return new Sort;
    }

    /**
     * @param Sort $sort
     * @return SeoSortsRequests
     */
    public function getSeoBySort($sort)
    {
        return SeoSortsRequests::getSeoPaymentsBySort($sort);
    }

    /**
     * @param Sort $sort
     * @param integer $filter_id
     */
    public function addSort($sort, $filter_id = null)
    {
        SeoSortsRequests::addSortPayments($sort, $filter_id);
    }

    /**
     * @param string $slug
     * @return string
     */
    public function getUrlSort($slug)
    {
        return UrlHelpers::methodsPaymentsSort($slug);
    }

    /**
     * @return string
     */
    public function getUrlFilter()
    {
        return '/methods/payments/filter';
    }

    /**
     * @return string
     */
    public function getUrlBase()
    {
        return UrlHelpers::methodsPayments();
    }

    public function checkRating()
    {
    }

}