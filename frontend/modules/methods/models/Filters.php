<?php

namespace app\modules\methods\models;

use Yii;
use yii\helpers\Url;

use common\models\seotext\SeoFiltersRequests;

use app\models\BaseFilters;

/**
 * Class Filters
 * @package app\modules\methods\models
 *
 * @property integer $search_type
 * @property integer $search_currency
 */
class Filters extends BaseFilters
{
    public $search_type;
    public $search_currency;

    public static function set($query, $attributes = [])
    {
        $model = new Filters;
        $model->query = $query;

        if ($model->load(Yii::$app->request->post()) || $model->load(Yii::$app->request->get())) {
            if ($model->validate()) {
                $model->isValidate = true;
                self::search($query, $model);
                if ($seo = \common\models\seotext\SeoFiltersRequests::getSeoMethodsByFilter($model)) {
                    if ($seo->slug) {
                        Yii::$app->getResponse()->redirect(Url::to(['/methods/methods/filter', 'slug' => $seo->slug]))->send();
                    }
                }
                \common\models\seotext\SeoFiltersRequests::addFilterMethods($model);
            }
        } elseif ($attributes) {
            $model->attributes = $attributes;
            if ($model->validate()) {
                self::search($query, $model);
                if ($seo = \common\models\seotext\SeoFiltersRequests::getSeoMethodsByFilter($model)) {
                    $model->seo = $seo;
                }
            }
        }

        return $model;
    }

    public static function search($query, $model)
    {
        if ($model->search_type) {
            $query->joinWith('categories');
            $query->andFilterWhere(['methods_categories.id' => $model->search_type]);
        }

        if ($model->search_currency) {
            $query->joinWith('currencies');
            $query->andFilterWhere(['currencies.id' => $model->search_currency]);
        }
    }

    public function rules()
    {
        return [
            [['search_currency', 'search_type'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'search_type' => 'Тип платежа',
            'search_currency' => 'Валюта',
        ];
    }


    /**
     * @param  integer $id
     * @return string
     */
    public static function getSearchTypeUrl($id)
    {
        $sql = "SELECT *
        FROM seo_filters_requests
        WHERE filter::JSON->>'search_type' = :search_type::TEXT
          AND page = :page
          AND count_elements = 1";
        $sql = Yii::$app->db->createCommand($sql)
            ->bindValue(':page', SeoFiltersRequests::PAGE_METHODS)
            ->bindValue(':search_type', $id)
            ->rawSql;
        $seo_filter = SeoFiltersRequests::findBySql($sql)->one();
        if ($seo_filter) {
            if ($seo_filter->slug) {
                return Url::to(['/methods/payments/filter', 'slug' => $seo_filter->slug]);
            }
            return $seo_filter->url;
        }
        return self::getUrl('search_type', $id);
    }

    /**
     * @param  integer $id
     * @return string
     */
    public static function getSearchCurrencyUrl($id)
    {
        $sql = "SELECT *
        FROM seo_filters_requests
        WHERE filter::JSON->>'search_currency' = :search_currency::TEXT
          AND page = :page
          AND count_elements = 1";
        $sql = Yii::$app->db->createCommand($sql)
            ->bindValue(':page', SeoFiltersRequests::PAGE_METHODS)
            ->bindValue(':search_currency', $id)
            ->rawSql;
        $seo_filter = SeoFiltersRequests::findBySql($sql)->one();
        if ($seo_filter) {
            if ($seo_filter->slug) {
                return Url::to(['/methods/payments/filter', 'slug' => $seo_filter->slug]);
            }
            return $seo_filter->url;
        }
        return self::getUrl('search_currency', $id);
    }

    protected static function getUrl($attribute, $search, $many = false)
    {
        $attribute = $many ? "Filters[{$attribute}][]" : "Filters[{$attribute}]";
        return Url::to(['/methods/payments/index', $attribute => $search]);
    }

}
