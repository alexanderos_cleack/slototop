<?php

namespace app\modules\methods\controllers;

use Yii;
use yii\data\Pagination;
use yii\web\Controller;

use common\models\methods\MethodsPayment;

use app\helpers\BreadcrumbsHelper;
use app\helpers\UrlHelpers;
use app\modules\methods\models\Filters;
use app\modules\methods\models\Sort;
use app\modules\methods\widgets\MethodPaymentItem;

/**
 * Class PaymentsController
 * @package app\modules\methods\controllers
 */
class PaymentsController extends Controller
{
    public $enableCsrfValidation = false;

    public function actionIndex()
    {
        BreadcrumbsHelper::add(Yii::t('app/f', 'Платежные системы'));

        $query = \app\models\methods\MethodsPayment::find()
            ->select(['t.id', 't.title', 't.slug', 't.enabled'])
            ->from(['t' => \app\models\methods\MethodsPayment::tableName()])
            ->where(['enabled' => true]);

        $filters = Filters::set($query);

        $pagination = $this->getPagination($query);

        $query->with(['logo']);

        $sort = Sort::set($query);

        $items = $this->getItems($query, $pagination);

        $seo = \common\models\seotext\SeoText::getSeoPayments();

        $this->saveQuery($query, $pagination);

        return $this->render('index', [
            'items' => $items,
            'count' => $pagination->totalCount,
            'pagination' => $pagination,
            'filters' => $filters,
            'sort' => $sort,
            'seo' => $seo,
        ]);
    }

    public function actionShow($slug)
    {
        /** @var MethodsPayment $method */
        $method = MethodsPayment::find()->where(['slug' => $slug/*, 'enabled' => true*/])->one();
        if (!$method) throw new \yii\web\NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        BreadcrumbsHelper::add(Yii::t('app/f', 'Платежные системы'), UrlHelpers::methodsPayments());
        BreadcrumbsHelper::add($method->title);

        return $this->render('show', [
            'method' => $method,
        ]);
    }

    public function actionFilter($slug)
    {
        $seo = \common\models\seotext\SeoFiltersRequests::getSeoMethods($slug);
        if (!$seo) throw new \yii\web\NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        $model = \app\models\methods\MethodsPayment::class;
        $query = $model::find()->select(['t.id', 't.title', 't.slug', 't.enabled'])->from(['t' => $model::tableName()])->where(['t.enabled' => true]);

        $filters = Filters::set($query, json_decode($seo->filter, true));

        $pagination = $this->getPagination($query);

        $query->with(['logo']);

        $sort = Sort::set($query, ['slug' => $seo->slug], null, $seo);

        $items = $this->getItems($query, $pagination);

        $this->saveQuery($query, $pagination);

        BreadcrumbsHelper::add(Yii::t('app/f', 'Платежные системы'), UrlHelpers::methodsPayments());
        BreadcrumbsHelper::add($seo->meta_h1);

        return $this->render('index', [
            'items' => $items,
            'count' => $pagination->totalCount,
            'pagination' => $pagination,
            'filters' => $filters,
            'sort' => $sort,
            'seo' => $seo,
        ]);
    }

    public function actionSort($slug)
    {
        $seo_sort = \common\models\seotext\SeoSortsRequests::getSeoPayments($slug);
        if (!$seo_sort) throw new \yii\web\NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        $model = \app\models\methods\MethodsPayment::class;
        $query = $model::find()->select(['t.id', 't.title', 't.slug', 't.enabled'])->from(['t' => MethodsPayment::tableName()])->where(['enabled' => true]);

        if ($seo_sort->filter) {
            $filters = Filters::set($query, json_decode($seo_sort->filter->filter, true));
        } else {
            $filters = Filters::set($query);
        }

        $pagination = $this->getPagination($query);

        $query->with(['logo']);

        if ($seo_sort->filter) {
            $sort = Sort::set($query, ['slug' => $seo_sort->filter->slug], json_decode($seo_sort->sort, true), $seo_sort->filter);
        } else {
            $sort = Sort::set($query, [], json_decode($seo_sort->sort, true));
        }

        $items = $this->getItems($query, $pagination);

        $this->saveQuery($query, $pagination);

        return $this->render('index', [
            'items' => $items,
            'count' => $pagination->totalCount,
            'pagination' => $pagination,
            'filters' => $filters,
            'sort' => $sort,
            'seo' => $seo_sort,
        ]);
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function actionItems()
    {
        $session = Yii::$app->session;
        if ($session->has('payments')) {
            list($query, $pagination) = array_values($session->get('payments'));
            $pagination->page = Yii::$app->request->get('page');

            $items = [];
            foreach ($query->offset($pagination->offset)->limit($pagination->limit)->each() as $item) {
                $items[] = MethodPaymentItem::widget(['model' => $item]);
            };

            Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
            return join('', $items);
        }
    }

    /**
     * @param  \yii\db\ActiveQuery $query
     */
    protected function saveQuery(\yii\db\ActiveQuery $query, \yii\data\Pagination $pagination)
    {
        $session = Yii::$app->session;
        $session->set('payments', [
            'query' => $query,
            'pagination' => $pagination,
        ]);
    }

    /**
     * @param  \yii\db\ActiveQuery $query
     * @return \yii\data\Pagination
     */
    protected function getPagination(\yii\db\ActiveQuery $query)
    {
        $count_query = clone $query;
        $count = $count_query->count();
        return new Pagination(['totalCount' => $count, 'pageSize' => 52]);
    }

    /**
     * @param  \yii\db\ActiveQuery $query
     * @param  \yii\data\Pagination $pagination
     * @return \common\models\casino\Casino[]
     */
    protected function getItems(\yii\db\ActiveQuery $query, \yii\data\Pagination $pagination)
    {
        $offset_query = clone $query;
        return $offset_query->offset($pagination->offset)->limit($pagination->limit)->all();
    }

}