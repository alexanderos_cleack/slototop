<?php

namespace app\modules\methods\widgets;

use yii\base\Widget;

use common\models\methods\MethodsPayment;

/**
 * Class MethodPaymentItem
 * @package app\modules\methods\widgets
 *
 * @property MethodsPayment $model
 */
class MethodPaymentItem extends Widget
{
    public $model;

    public function run()
    {
        return $this->render('methodPaymentItem', [
            'model' => $this->model,
        ]);
    }

}