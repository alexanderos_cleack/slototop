<?php
/**
 * @var \yii\web\View $this
 * @var \common\models\methods\MethodsPayment $model
 */
?>

<div class="game">
    <div class="game-in">
        <span class="game-title"><?= $model->title ?></span>
        <div class="game-img">
            <?= \app\helpers\ImageHelper::image($model->getImageLogo(), $model->title) ?>
            <a href="<?= \app\helpers\UrlHelpers::methodPayment($model->slug) ?>"
               title="<?= $model->title ?>" class="btn-red"><?= Yii::t('app/f', 'ПЕРЕЙТИ') ?></a>
        </div>
        <span class="game-stars gm-untitle"></span>
    </div>
</div>
