<?php

namespace app\modules\games\models;

use \common\models\seotext\SeoSortsRequests;

use app\helpers\UrlHelpers;
use app\models\ABaseSort;

/**
 * Class Sort
 * @package app\modules\games\models
 */
class Sort extends ABaseSort
{
    public function defaultSort()
    {
        $this->setRating()->desc();
        $this->checkRating();
    }

    /**
     * @return Sort
     */
    public static function createModel()
    {
        return new Sort;
    }

    /**
     * @param Sort $sort
     * @return SeoSortsRequests
     */
    public function getSeoBySort($sort)
    {
        return SeoSortsRequests::getSeoGamesBySort($sort);
    }

    /**
     * @param Sort $sort
     * @param integer $filter_id
     */
    public function addSort($sort, $filter_id = null)
    {
        SeoSortsRequests::addSortGames($sort, $filter_id);
    }

    /**
     * @param string $slug
     * @return string
     */
    public function getUrlSort($slug)
    {
        return UrlHelpers::gamesSort($slug);
    }

    /**
     * @return string
     */
    public function getUrlFilter()
    {
        return '/games/games/filter';
    }

    /**
     * @return string
     */
    public function getUrlBase()
    {
        return UrlHelpers::games();
    }

    public function checkTitle()
    {
        if ($this->title == 'asc') {
            $this->sortAttributes = [$this->getSortTitle() => SORT_ASC];
            $this->query->orderBy($this->sortAttributes);
        } elseif ($this->title == 'desc') {
            $this->sortAttributes = [$this->getSortTitle() => SORT_DESC];
            $this->query->orderBy($this->sortAttributes);
        }
    }

    public function checkRating()
    {
        if ($this->rating == 'asc') {
            $this->sortAttributes = [
                't.rating_users' => SORT_ASC,
                $this->getSortTitle() => SORT_ASC,
            ];
            $this->query->orderBy($this->sortAttributes);
        } elseif ($this->rating == 'desc') {
            $this->sortAttributes = [
                't.rating_users' => SORT_DESC,
                $this->getSortTitle() => SORT_ASC,
            ];
            $this->query->orderBy($this->sortAttributes);
        }
    }

    protected function getSortTitle() {
        return 'regexp_replace(t.title, \'[а-яА-Я ]*\', \'\')';
    }

}
