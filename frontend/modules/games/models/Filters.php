<?php

namespace app\modules\games\models;

use common\models\slots\Slots;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use common\models\seotext\SeoFiltersRequests;
use common\models\slots\SlotsCategories;

use app\models\BaseFilters;

/**
 * Class Filters
 * @package app\modules\games\models
 *
 * @property integer $search_vendor
 * @property integer $search_category
 * @property integer $search_currency
 * @property integer $search_reels
 * @property integer $search_jackpot
 * @property integer $search_joker
 * @property integer $search_amount_hands
 * @property float $search_bet_max_hand
 * @property float $search_bet_min_hand
 * @property bool $search_wild
 * @property bool $search_scatter
 * @property bool $search_bonus
 * @property bool $search_freespin
 * @property bool $search_double
 * @property bool $search_video
 * @property bool $search_auto
 * @property bool $search_risk
 * @property string $search_bet
 * @property string $search_win
 * @property string $search_roulette_type
 * @property string $search_cards_type
 * @property string $search_lottery_type
 * @property string $search_rating
 * @property string $search_lines
 * @property string $search_kind_of_poker
 * @property string $search_min_combination
 *
 * @property bool $search_bet_color
 * @property bool $search_bet_dozen
 * @property bool $search_bet_even_odd
 * @property bool $search_bet_line
 * @property bool $search_bet_more_less
 * @property bool $search_doubling_game
 * @property bool $search_insurance
 * @property bool $search_retake
 * @property bool $search_splite
 * @property bool $search_surrender
 * @property bool $search_triplication_game
 * @property float $search_bet_max
 * @property float $search_bet_max_number
 * @property float $search_bet_min
 * @property float $search_bet_min_number
 * @property integer $search_amount_combinations
 * @property integer $search_amount_lines
 * @property integer $search_amount_wheels
 * @property integer $search_max_boxes
 * @property integer $search_min_boxes
 * @property integer $search_zero
 * @property string $search_numeric_series
 * @property string $search_special_combination
 * @property string $search_version
 * @property string $search_video_viewer_url
 * @property string $search_win_max
 * @property integer $search_tag
 *
 * @property float $filter_max_amount_lines;
 * @property float $filter_max_win_max;
 * @property float $filter_max_bet_max;
 *
 * @property SlotsCategories $category
 */
class Filters extends BaseFilters
{
    public $search_vendor;
    public $search_category;
    public $search_rating;// = '1;100';
    public $search_lines;// = '1;999999';
    public $search_reels;
    public $search_jackpot;
    public $search_wild;
    public $search_scatter;
    public $search_bonus;
    public $search_freespin;
    public $search_double;
    public $search_video;
    public $search_auto;
    public $search_bet;// = '1;999999';
    public $search_win;// = '1;9999999';
    public $search_currency;
    public $search_roulette_type;
    public $search_cards_type;
    public $search_lottery_type;
    public $search_risk;
    public $search_joker;
    public $search_amount_hands;
    public $search_bet_max_hand;
    public $search_kind_of_poker;
    public $search_bet_min_hand;
    public $search_min_combination;

    public $search_amount_combinations;
    public $search_amount_lines;
    public $search_amount_wheels;
    public $search_bet_color;
    public $search_bet_dozen;
    public $search_bet_even_odd;
    public $search_bet_line;
    public $search_bet_max;
    public $search_bet_max_number;
    public $search_bet_min;
    public $search_bet_min_number;
    public $search_bet_more_less;
    public $search_doubling_game;
    public $search_insurance;
    public $search_max_boxes;
    public $search_min_boxes;
    public $search_numeric_series;
    public $search_retake;
    public $search_slug;
    public $search_special_combination;
    public $search_splite;
    public $search_surrender;
    public $search_triplication_game;
    public $search_version;
    public $search_video_viewer_url;
    public $search_win_max;
    public $search_zero;
    public $search_tag;

    protected $filter_max_amount_lines;
    protected $filter_max_win_max;
    protected $filter_max_bet_max;

    private $_category;

    public function getVendorsForList()
    {
        $query = clone $this->query;

        $query->distinct()->select(['vendors.id as id', 'vendors.title as title', 't.category_id'])
            ->innerJoinWith([
                'vendors' => function (ActiveQuery $query) {
                    $query->select(['id', 'title']);
                }
            ])->orderBy('title')->asArray();

        return ArrayHelper::map($query->all(), 'id', 'title');
    }

    public function getSlotsCategoriesForList()
    {
        $query = clone $this->query;

        $query->distinct()->select(['slots_categories.id as id', 'slots_categories.title as title', 't.category_id'])
            ->innerJoinWith([
                'category' => function (ActiveQuery $query) {
                    $query->select(['id', 'title']);
                }
            ])->orderBy('title')->asArray();

        return ArrayHelper::map($query->all(), 'id', 'title');
    }

    public function getRouletteTypeForList()
    {
        $query = clone $this->query;

        $query->distinct()->select('t.kind_of_roulette')
            ->andWhere(['!=', 't.kind_of_roulette', ''])
            ->orderBy('t.kind_of_roulette');

        return ArrayHelper::map($query->all(), 'kind_of_roulette', 'kind_of_roulette');
    }

    public function getLotteryTypeForList()
    {
        $query = clone $this->query;

        $query->distinct()->select('t.kind_of_lottery')
            ->andwhere(['!=', 't.kind_of_lottery', ''])
            ->orderBy('t.kind_of_lottery');

        return ArrayHelper::map($query->all(), 'kind_of_lottery', 'kind_of_lottery');
    }

    public function getCardsTypeForList()
    {
        $query = clone $this->query;

        $query->distinct()->select('t.kind_of_cards')
            ->andWhere(['!=', 't.kind_of_cards', ''])
            ->orderBy('t.kind_of_cards');

        return ArrayHelper::map($query->all(), 'kind_of_cards', 'kind_of_cards');
    }

    public function getReelsForList()
    {
        $query = clone $this->query;

        $query->distinct()->select('t.amount_reels')
            ->andWhere(['not', ['t.amount_reels' => null]])
            ->orderBy('t.amount_reels');

        return ArrayHelper::map($query->all(), 'amount_reels', 'amount_reels');
    }

    public function getJackpotForList()
    {
        $query = clone $this->query;

        $query->distinct()->select('t.jackpot')
            ->andWhere(['not', ['t.jackpot' => null]])
            ->orderBy('t.jackpot');

        return ArrayHelper::map($query->all(), 'jackpot', 'jackpot');
    }

    public function getWildForList()
    {
        $query = clone $this->query;

        $query->distinct()->select('t.wild_symbol')->orderBy('t.wild_symbol desc');

        return ArrayHelper::map($query->all(), 'wild_symbol', 'wild_symbol');
    }

    public function getScatterForList()
    {
        $query = clone $this->query;

        $query->distinct()->select('t.skatter')->orderBy('t.skatter desc');

        return ArrayHelper::map($query->all(), 'skatter', 'skatter');
    }

    public function getBonusForList()
    {
        $query = clone $this->query;

        $query->distinct()->select('t.bonus_game')->orderBy('t.bonus_game desc');

        return ArrayHelper::map($query->all(), 'bonus_game', 'bonus_game');
    }

    public function getFreespinForList()
    {
        $query = clone $this->query;

        $query->distinct()->select('t.free_spins')->orderBy('t.free_spins desc');

        return ArrayHelper::map($query->all(), 'free_spins', 'free_spins');
    }

    public function getDoubleForList()
    {
        $query = clone $this->query;

        $query->distinct()->select('t.doubling_games')->orderBy('t.doubling_games desc');

        return ArrayHelper::map($query->all(), 'doubling_games', 'doubling_games');
    }

    public function getAutoForList()
    {
        $query = clone $this->query;

        $query->distinct()->select('t.auto_play')->orderBy('t.auto_play desc');

        return ArrayHelper::map($query->all(), 'auto_play', 'auto_play');
    }

    public function getFilterMaxRating()
    {
        return Slots::find()->max('rating_users') ?? 0;

        $query = clone $this->query;

        return $query->max('t.rating_users') ?? 0;
    }

    /**
     * @return float
     */
    public function getFilterMaxAmountLines()
    {
        return Slots::find()->max('amount_lines') ?? 0;

        $query = clone $this->query;

        return $query->max('t.amount_lines') ?? 0;
    }

    /**
     * @return float
     */
    public function getFilterMaxBetMax()
    {
        return Slots::find()->max('bet_max') ?? 0;

        $query = clone $this->query;

        return $query->max('t.bet_max') ?? 0;
    }

    /**
     * @return float
     */
    public function getFilterMaxWinMax()
    {
        return Slots::find()->max('win_max') ?? 0;

        $query = clone $this->query;

        return $query->max('t.win_max') ?? 0;
    }

    public static function set($query, $attributes = [])
    {
        $model = new Filters;
        $model->query = $query;

        if ($model->load(Yii::$app->request->post()) || $model->load(Yii::$app->request->get())) {
            if ($model->validate()) {
                $model->isValidate = true;
                self::search($query, $model);
                if ($seo = \common\models\seotext\SeoFiltersRequests::getSeoGamesByFilter($model)) {
                    if ($seo->slug) {
                        Yii::$app->getResponse()->redirect(Url::to(['/games/games/filter', 'slug' => $seo->slug]))->send();
                    }
                }
                \common\models\seotext\SeoFiltersRequests::addFilterGames($model);
            }
        } elseif ($attributes) {
            $model->attributes = $attributes;
            if ($model->validate()) {
                self::search($query, $model);
                if ($seo = \common\models\seotext\SeoFiltersRequests::getSeoGamesByFilter($model)) {
                    $model->seo = $seo;
                }
            }
        } else {
            self::search($query, $model);
        }

        return $model;
    }

    /**
     * @param ActiveQuery $query
     * @param Filters $model
     */
    public static function search($query, $model)
    {
        if ($model->search_vendor) {
            $query->innerJoinWith('vendor');
            $query->andFilterWhere(['vendors.id' => $model->search_vendor]);
        }

        if ($model->search_category) {
            $query->innerJoinWith('category');
            $query->andFilterWhere(['slots_categories.id' => $model->search_category]);
            $category = SlotsCategories::findOne($model->search_category);
            if ($category) {
                $model->category = $category;
            } else {
                $model->category = null;
            }
        }

        if ($model->search_rating) {
            list($from, $to) = explode(';', $model->search_rating);
            $query->andFilterWhere(['between', 't.rating_users', $from, $to]);
        }/* else {
            $model->search_rating = '0;' . $model->getFilterMaxRating();
        }*/

        if ($model->search_lines) {
            list($from, $to) = explode(';', $model->search_lines);
            $query->andFilterWhere(['between', 'COALESCE(t.amount_lines, 0)', $from, $to]);
        }/* else {
            $model->search_lines = '0;' . $model->getFilterMaxAmountLines();
        }*/


        if ($model->search_bet) {
            list($from, $to) = explode(';', $model->search_bet);
            $query->andFilterWhere(['between', 'COALESCE(t.bet_max, 0)', $from, $to]);
        }/* else {
            $model->search_bet = '0;' . $model->getFilterMaxBetMax();
        }*/


        if ($model->search_win) {
            list($from, $to) = explode(';', $model->search_win);
            $query->andFilterWhere(['between', 'COALESCE(t.win_max, 0)', $from, $to]);
        }/* else {
            $model->search_win = '0;' . $model->getFilterMaxWinMax();
        }*/

        if ($model->search_tag) {
            $query->innerJoinWith('tags2table');
            $query->andFilterWhere(['tags2table.tag_id' => $model->search_tag]);
        }

        $query->andFilterWhere([
            'slots_categories.video_viewer_url' => $model->search_video,
            't.amount_reels' => $model->search_reels,
            't.auto_play' => $model->search_auto,
            't.bonus_game' => $model->search_bonus,
            't.category_id' => $model->search_category,
            't.doubling_games' => $model->search_double,
            't.free_spins' => $model->search_freespin,
            't.jackpot' => $model->search_jackpot,
            't.kind_of_cards' => $model->search_cards_type,
            't.kind_of_roulette' => $model->search_roulette_type,
            't.kind_of_lottery' => $model->search_lottery_type,
            't.skatter' => $model->search_scatter,
            't.wild_symbol' => $model->search_wild,
            't.risk_game' => $model->search_risk,
            't.joker' => $model->search_joker,
            't.amount_hands' => $model->search_amount_hands,
            't.bet_max_hand' => $model->search_bet_max_hand,
            't.kind_of_poker' => $model->search_kind_of_poker,
            't.bet_min_hand' => $model->search_bet_min_hand,
            't.min_combination' => $model->search_min_combination,
            't.bet_color' => $model->search_bet_color,
            't.bet_dozen' => $model->search_bet_dozen,
            't.bet_even_odd' => $model->search_bet_even_odd,
            't.bet_line' => $model->search_bet_line,
            't.bet_more_less' => $model->search_bet_more_less,
            't.doubling_game' => $model->search_doubling_game,
            't.insurance' => $model->search_insurance,
            't.retake' => $model->search_retake,
            't.splite' => $model->search_splite,
            't.surrender' => $model->search_surrender,
            't.triplication_game' => $model->search_triplication_game,
            't.bet_max' => $model->search_bet_max,
            't.bet_max_number' => $model->search_bet_max_number,
            't.bet_min' => $model->search_bet_min,
            't.bet_min_number' => $model->search_bet_min_number,
            't.amount_combinations' => $model->search_amount_combinations,
            't.amount_lines' => $model->search_amount_lines,
            't.amount_wheels' => $model->search_amount_wheels,
            't.max_boxes' => $model->search_max_boxes,
            't.min_boxes' => $model->search_min_boxes,
            't.zero' => $model->search_zero,
            't.numeric_series' => $model->search_numeric_series,
            't.special_combination' => $model->search_special_combination,
            't.version' => $model->search_version,
            't.video_viewer_url' => $model->search_video_viewer_url,
            't.win_max' => $model->search_win_max,
            'vendors.id' => $model->search_vendor,
        ]);
    }

    public function rules()
    {
        return [
            [['search_category', 'search_vendor',
                'search_currency', 'search_amount_hands',
                'search_amount_combinations', 'search_amount_lines', 'search_amount_wheels',
                'search_max_boxes', 'search_min_boxes', 'search_zero', 'search_tag'], 'integer'],

            [['search_reels', 'search_jackpot'], 'each', 'rule' => ['integer']],

            [['search_bet_max_hand', 'search_bet_min_hand', 'search_bet_max',
                'search_bet_max_number', 'search_bet_min', 'search_bet_min_number'], 'number'],

            [['search_wild', 'search_scatter', 'search_bonus',
                'search_freespin', 'search_double', 'search_auto',
                'search_video', 'search_risk', 'search_joker',
                'search_bet_color', 'search_bet_dozen', 'search_bet_even_odd',
                'search_bet_line', 'search_bet_more_less', 'search_doubling_game',
                'search_insurance', 'search_retake', 'search_splite',
                'search_surrender', 'search_triplication_game'], 'boolean'],

            [['search_cards_type', 'search_roulette_type', 'search_lottery_type',
                'search_kind_of_poker', 'search_min_combination', 'search_numeric_series',
                'search_special_combination', 'search_version', 'search_video_viewer_url',
                'search_win_max'], 'string', 'max' => 64],

            [['search_rating', 'search_lines', 'search_bet', 'search_win'], 'match', 'pattern' => '/\d{1,7};\d{1,7}/'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'search_auto' => 'Автоматическая игра',
            'search_bet' => 'Макс. ставка',
            'search_bonus' => 'Бонусная игра',
            'search_category' => 'Категория',
            'search_double' => 'Игра на удвоение',
            'search_freespin' => 'Бонусные вращения',
            'search_jackpot' => 'Джекпот',
            'search_lines' => 'Количество линий',
            'search_rating' => 'Рейтинг пользователей',
            'search_scatter' => 'Скаттер',
            'search_vendor' => 'Производитель',
            'search_video' => 'Видеообзор',
            'search_reels' => 'Количество барабанов',
            'search_wild' => 'Дикий символ',
            'search_win' => 'Макс. выигрыш',
            'search_currency' => 'Валюта',
            'search_cards_type' => 'Тип карт',
            'search_roulette_type' => 'Тип рулетки',
            'search_lottery_type' => 'Тип лотереи',
            'search_risk' => 'Риск игра',
            'search_joker' => 'Джокер',
        ];
    }

    /**
     * @param SlotsCategories $category
     */
    public function setCategory($category)
    {
        $this->_category = $category;
    }

    /**
     * @return SlotsCategories
     */
    public function getCategory()
    {
        return $this->_category;
    }

    /**
     * @param  integer $id
     * @return string
     */
    public static function getSearchCategoryUrl($id)
    {
        $sql = "SELECT *
        FROM seo_filters_requests
        WHERE filter::JSON->>'search_category' = :search_category::TEXT
          AND page = :page
          AND count_elements = 1
          AND slug IS NOT NULL";
        $sql = Yii::$app->db->createCommand($sql)
            ->bindValue(':page', SeoFiltersRequests::PAGE_GAMES)
            ->bindValue(':search_category', $id)
            ->bindValue(':count_elements', 1)
            ->rawSql;
        $seo_filter = SeoFiltersRequests::findBySql($sql)->one();
        if ($seo_filter) {
            if ($seo_filter->slug) {
                return Url::to(['/games/games/filter', 'slug' => $seo_filter->slug]);
            }
            return $seo_filter->url;
        }
        return self::getUrl('search_category', $id);
    }

    /**
     * @param  integer $id
     * @return string
     */
    public static function getSearchVendorUrl($id)
    {
        $sql = "SELECT *
        FROM seo_filters_requests
        WHERE filter::JSON->>'search_vendor' = :search_vendor::TEXT
          AND page = :page
          AND count_elements = 1";
        $sql = Yii::$app->db->createCommand($sql)
            ->bindValue(':page', SeoFiltersRequests::PAGE_GAMES)
            ->bindValue(':search_vendor', $id)
            ->rawSql;
        $seo_filter = SeoFiltersRequests::findBySql($sql)->one();
        if ($seo_filter) {
            if ($seo_filter->slug) {
                return Url::to(['/games/games/filter', 'slug' => $seo_filter->slug]);
            }
            return $seo_filter->url;
        }
        return self::getUrl('search_vendor', $id);
    }

    /**
     * @param string $property
     * @param string $value
     * @return string string
     */
    public static function getSearchPropertyUrl($property, $value)
    {
        $mapping = [
            'amount_hands' => 'search_amount_hands',
            'amount_reels' => 'search_reels',
            'auto_play' => 'search_auto',
            'bet_max_hand' => 'search_bet_max_hand',
            'bonus_game' => 'search_bonus',
            'category_id' => 'search_category',
            'doubling_games' => 'search_double',
            'free_spins' => 'search_freespin',
            'jackpot' => 'search_jackpot',
            'joker' => 'search_joker',
            'kind_of_cards' => 'search_cards_type',
            'kind_of_lottery' => 'search_lottery_type',
            'kind_of_roulette' => 'search_roulette_type',
            'risk_game' => 'search_risk',
            'skatter' => 'search_scatter',
            'wild_symbol' => 'search_wild',
        ];

        $property = isset($mapping[$property]) ? $mapping[$property] : 'search_' . $property;

        $sql = "SELECT *
        FROM seo_filters_requests
        WHERE filter::JSON->>'" . $property . "' = :property
          AND page = :page
          AND count_elements = 1";
        $sql = Yii::$app->db->createCommand($sql)
            ->bindValue(':page', SeoFiltersRequests::PAGE_GAMES)
            ->bindValue(':property', (string)$value)
            ->rawSql;
        $seo_filter = SeoFiltersRequests::findBySql($sql)->one();
        if ($seo_filter) {
            if ($seo_filter->slug) {
                return Url::to(['/games/games/filter', 'slug' => $seo_filter->slug]);
            }
            return $seo_filter->url;
        }
        return self::getUrl($property, $value);
    }

    /**
     * @param  integer $id
     * @return string
     */
    public static function getSearchTagUrl($id) {
        $sql = "SELECT *
        FROM seo_filters_requests
        WHERE filter::JSON->>'search_tag' = :search_tag::TEXT
          AND page = :page
          AND count_elements = 1";
        $sql = Yii::$app->db->createCommand($sql)
            ->bindValue(':page', SeoFiltersRequests::PAGE_GAMES)
            ->bindValue(':search_tag', $id)
            ->rawSql;
        $seo_filter = SeoFiltersRequests::findBySql($sql)->one();
        if ($seo_filter) {
            if ($seo_filter->slug) {
                return Url::to(['/games/games/filter', 'slug' => $seo_filter->slug]);
            }
            return $seo_filter->url;
        }
        return self::getUrl('search_tag', $id);
    }
    /**
     * @param  integer $id
     * @return string
     */
    public static function getSearchVendorTitle($id)
    {
        $sql = "SELECT *
        FROM seo_filters_requests
        WHERE filter::JSON->>'search_vendor' = :search_vendor::TEXT
          AND page = :page
          AND count_elements = 1";
        $sql = Yii::$app->db->createCommand($sql)
            ->bindValue(':page', SeoFiltersRequests::PAGE_GAMES)
            ->bindValue(':search_vendor', $id)
            ->rawSql;
        $seo_filter = SeoFiltersRequests::findBySql($sql)->one();
        if ($seo_filter) {
            return $seo_filter->title;
        }
    }

    /**
     * @return array
     */
    public static function getSlotsCategories()
    {
        $categories_ids = \app\models\slots\Slots::find()->distinct()->select(['category_id'])->where(['enabled' => true])->asArray()->all();
        $categories_ids = array_map(function ($items) {
            return $items['category_id'];
        }, $categories_ids);
        return \common\models\slots\SlotsCategories::find()->select(['id', 'title'])->where(['id' => $categories_ids])->orderBy('title')->asArray()->all();
    }

    protected static function getUrl($attribute, $search)
    {
        $attribute = "Filters[{$attribute}]";
        return Url::to(['/games/games/index', $attribute => $search]);
    }

}
