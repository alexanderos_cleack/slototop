<?php

use yii\helpers\Html;

/**
 * @var \app\modules\games\models\Filters $filters
 */
?>

<div class="label">
    <span class="sb-label-title"><?= $filters->getAttributeLabel('search_risk') ?>:</span>
    <div class="radio-line">
        <?= Html::activeRadioList($filters, 'search_risk', [1 => Yii::t('app/f', 'Yes'), 0 => Yii::t('app/f', 'No')]) ?>
    </div>
</div>

<div class="label">
    <span class="sb-label-title"><?= $filters->getAttributeLabel('search_joker') ?>:</span>
    <div class="radio-line">
        <?= Html::activeRadioList($filters, 'search_joker', [1 => Yii::t('app/f', 'Yes'), 0 => Yii::t('app/f', 'No')]) ?>
    </div>
</div>