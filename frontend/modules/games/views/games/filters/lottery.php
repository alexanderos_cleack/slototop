<?php

use yii\helpers\Html;

/**
 * @var \app\modules\games\models\Filters $filters
 */
?>

<div class="label">
    <span class="sb-label-title"><?= $filters->getAttributeLabel('search_lottery_type') ?>:</span>
    <?= Html::activeDropDownList($filters, 'search_lottery_type', $filters->getLotteryTypeForList(), ['prompt' => Yii::t('app/f', 'Filter select')]) ?>
</div>