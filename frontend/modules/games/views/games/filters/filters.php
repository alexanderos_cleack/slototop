<?php

use yii\widgets\ActiveForm;

/**
 * @var \app\modules\games\models\Filters $filters
 */
?>

<?php

$filters = isset($filters) ? $filters : new \app\modules\games\models\Filters();

?>

<div class="sb-in-filters">
    <div class="sb-title">
        <?= Yii::t('app/f', 'Фильтровать по:') ?>
    </div>

    <?php $form = ActiveForm::begin(['id' => 'filters', 'method' => 'post', 'action' => \app\helpers\UrlHelpers::games()]); ?>
    <div id="ajax-filter">
        <?php echo $this->context->renderPartial('filters/form', [
            'filters' => $filters,
        ]); ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>