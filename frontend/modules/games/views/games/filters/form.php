<?php
/**
 * @var \yii\web\View $this
 * @var \app\modules\games\models\Filters $filters
 */
?>

<?php

\app\assets\FormstylerAsset::register($this);
\app\assets\JsliderAsset::register($this);

$this->registerJsFile('/js/formstyler.js', ['position' => \yii\web\View::POS_END]);

?>

<div class="label">
    <span class="sb-label-title"><?= $filters->getAttributeLabel('search_vendor') ?>:</span>
    <?= \yii\helpers\Html::activeDropDownList($filters, 'search_vendor', $filters->getVendorsForList(), ['prompt' => Yii::t('app/f', 'Filter select')]) ?>
</div>

<div class="label">
    <span class="sb-label-title"><?= $filters->getAttributeLabel('search_category') ?>:</span>
    <?= \yii\helpers\Html::activeDropDownList($filters, 'search_category', $filters->getSlotsCategoriesForList(), ['prompt' => Yii::t('app/f', 'Filter select')]) ?>
</div>

<div id="ajax-filter-category">
    <?php if ($filters->category): ?>
        <?php echo $this->context->renderPartial('filters/' . $filters->category->filter, [
            'filters' => $filters,
        ]); ?>
    <?php endif; ?>
</div>

<div class="label">
    <?= \yii\helpers\Html::submitButton(Yii::t('app/f', 'Подобрать!'), ['class' => 'btn-green']) ?>
</div>

<script>
    $('select').on('change', function () {
        update(this);
    });

    $('[type="checkbox"]').on('change', function () {
        update(this);
    });


    $('[type="radio"]').on('change', function () {
        update(this);
    });

    function update(e) {
        if ($(e).attr('id') == 'filters-search_category' && !$(e).val()) {
            $('#ajax-filter-category').empty();
        }

        $.ajax({
            'type': 'post',
            'url': '<?php echo \yii\helpers\Url::to(['/games/games/filters/']); ?>',
            'data': $('#filters').serialize()
        }).done(function (data) {
            $('#ajax-filter').html(data);
            $('input, select').trigger('refresh');
            $('input, select').styler();
            $(".jq-selectbox__dropdown ul").addClass('mCustomScrollbar').mCustomScrollbar({
                theme: "minimal"
            });
        });
    }
</script>
