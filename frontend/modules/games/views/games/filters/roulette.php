<?php

use yii\helpers\Html;

use common\models\currencies\Currencies;

/**
 * @var \app\modules\games\models\Filters $filters
 */
?>

<div class="label">
    <span class="sb-label-title"><?= $filters->getAttributeLabel('search_roulette_type') ?>:</span>
    <?= Html::activeDropDownList($filters, 'search_roulette_type', $filters->getRouletteTypeForList(), ['prompt' => Yii::t('app/f', 'Filter select')]) ?>
</div>

<!--<div class="label">
    <span class="sb-label-title"><?= $filters->getAttributeLabel('search_currency') ?>:</span>
    <?= Html::activeDropDownList($filters, 'search_currency', Currencies::getForList(), ['prompt' => Yii::t('app/f', 'Filter select')]) ?>
</div> -->