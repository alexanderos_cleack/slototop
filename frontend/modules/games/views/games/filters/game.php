<?php

use yii\helpers\Html;

/**
 * @var \app\modules\games\models\Filters $filters
 */
?>


<?php

/**
 * @param array $items
 * @return array
 */
function radioList($items) {
    $_items = [];

    foreach ($items as $k => $v) {
        if ($k == 1) {
            $_items[$k] = Yii::t('app/f', 'Yes');
        } else {
            $_items[$k] = Yii::t('app/f', 'No');
        }
    }

    return $_items;
}

?>

<div class="label">
    <span class="sb-label-title"><?= $filters->getAttributeLabel('search_reels') ?>:</span>
    <div class="chb-line">
        <?= Html::activeCheckboxList($filters, 'search_reels', $filters->getReelsForList()) ?>
    </div>
</div>

<div class="label">
    <span class="sb-label-title"><?= $filters->getAttributeLabel('search_jackpot') ?>:</span>
    <div class="radio-line">
        <?= Html::activeCheckboxList($filters, 'search_jackpot', $filters->getJackpotForList()) ?>
    </div>
</div>

<div class="label">
    <span class="sb-label-title"><?= $filters->getAttributeLabel('search_wild') ?>:</span>
    <div class="radio-line">
        <?= Html::activeRadioList($filters, 'search_wild', radioList($filters->getWildForList())) ?>
    </div>
</div>

<div class="label">
    <span class="sb-label-title"><?= $filters->getAttributeLabel('search_scatter') ?>:</span>
    <div class="radio-line">
        <?= Html::activeRadioList($filters, 'search_scatter', radioList($filters->getScatterForList())) ?>
    </div>
</div>

<div class="label">
    <span class="sb-label-title"><?= $filters->getAttributeLabel('search_bonus') ?>:</span>
    <div class="radio-line">
        <?= Html::activeRadioList($filters, 'search_bonus', radioList($filters->getBonusForList())) ?>
    </div>
</div>

<div class="label">
    <span class="sb-label-title"><?= $filters->getAttributeLabel('search_freespin') ?>:</span>
    <div class="radio-line">
        <?= Html::activeRadioList($filters, 'search_freespin', radioList($filters->getFreespinForList())) ?>
    </div>
</div>

<div class="label">
    <span class="sb-label-title"><?= $filters->getAttributeLabel('search_double') ?>:</span>
    <div class="radio-line">
        <?= Html::activeRadioList($filters, 'search_double', radioList($filters->getDoubleForList())) ?>
    </div>
</div>

<div class="label">
    <span class="sb-label-title"><?= $filters->getAttributeLabel('search_auto') ?>:</span>
    <div class="radio-line">
        <?= Html::activeRadioList($filters, 'search_auto', radioList($filters->getAutoForList())) ?>
    </div>
</div>