<?php

use yii\helpers\Html;
use yii\helpers\Url;

use app\modules\games\models\Filters;

/**
 * @var yii\web\View $this
 * @var common\models\slots\Slots $game
 * @var \common\models\symbols\Symbols $symbol
 */
?>

<?php

    \app\assets\RatingAsset::register($this);

    // $this->title = $game->title;
    if ($game->meta_title) $this->title = $game->meta_title;
    if ($game->meta_keywords) $this->registerMetaTag(['name' => 'keywords', 'content' => $game->meta_keywords]);
    if ($game->meta_description) $this->registerMetaTag(['name' => 'description', 'content' => $game->meta_description]);

    if (!$game->enabled) $this->registerMetaTag(['name' => 'robots', 'content' => 'noindex,nofollow']);

?>

<?php echo \app\widgets\Ogp::widget([
    'article_section' => $game->category->title,
    'image' => $game->logo,
    'model' => $game,
    'url' => Url::to(['/games/games/show', 'slug' => $game->slug], true)
]) ?>

<?php echo app\widgets\SchemaArticle::widget([
    'model' => $game,
    'add_content' => [
        \app\widgets\SchemaPerson::widget(['model' => $game]),
        \app\widgets\SchemaOrganization::widget(['model' => $game]),
        \app\widgets\SchemaAggregateRating::widget(['model' => $game]),
        \app\widgets\SchemaImageObject::widget(['image' => $game->logo]),
    ],
]) ?>

<?php echo app\widgets\SchemaVideoGame::widget([
    'model' => $game,
    'add_content' => [
        \app\widgets\SchemaAggregateRating::widget(['model' => $game]),
        \app\widgets\SchemaOffer::widget(['model' => $game]),
    ],
]) ?>

<div class="float-left">
    <?= \app\widgets\Breadcrumbs::widget(); ?>
    <div class="block-all">
        <div class="bl-red-title">
            <span class="bl-img"></span>
            <div class="bl-t-title">
                <span><h1><?= $game->title ?></h1></span>
                <a href="<?= \app\helpers\UrlHelpers::referrer() ?>"><?= Yii::t('app/f', 'назад к результатам >') ?></a>
            </div>

            <div class="clear"></div>
        </div>
    </div>

    <div class="block-all grey-border-bottom block-game-bg">
        <div class="game-type-left">
            <div class="game-type game-type-img">
                <?= \app\helpers\ImageHelper::image($game->logo, $game->title) ?>
            </div>
            <div class="game-type">
                <span><?= Yii::t('app/f', 'Рейтинг посетителей:') ?></span>
                <div class="rating-progress">
                    <div><span style="width: <?= $game->rating_users ?>%"></span></div>
                    <div><?= $game->rating_users ?>%</div>
                    <div class="clear"></div>
                </div>

                <span><?= Yii::t('app/f', 'Ваша оценка:') ?></span>
                <div id="rating">
                    <input type="hidden" name="val" value="<?= $game->rating ?>">
                    <input type="hidden" name="_csrf" value="<?= Yii::$app->request->csrfToken ?>">
                </div>
            </div>

            <div class="clear"></div>
        </div>

        <div class="game-type-right">
            <div class="game-type game-type-avtomat">
                <?php if ($game->vendor): ?>
                    <?= Html::a(\app\helpers\ImageHelper::image($game->vendor ? $game->vendor->logo_mini : null, Filters::getSearchVendorTitle($game->vendor->id)),  Filters::getSearchVendorUrl($game->vendor->id)) ?>
                <?php endif; ?>
                <?= \app\helpers\ImageHelper::image($game->category ? $game->category->logo_mini : null, $game->category->title) ?>
                <a href="<?= \app\modules\games\models\Filters::getSearchCategoryUrl($game->category->id) ?>"><?= $game->category->title ?></a>
            </div>
            <div class="game-type game-type-heart">
                <?= \app\helpers\ImageHelper::image('/img/game/heart.png') ?>
                <a href="#"><?= Yii::t('app/f', 'Добавить в любимые') ?></a>
                <span><?= Yii::t('app/f', '(добавило {amount} человек)', [
                    'amount' => 2546
                ]) ?></span>
            </div>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>
    </div>

    <div id="big-game" class="block-all big-game" style="display: none;">
        <?php if (!$game->demoGame): ?>
            <?= \app\helpers\ImageHelper::image($game->bigscreen, 'Bigscreen ' . $game->title) ?>
        <?php endif; ?>
    </div>


    <div class="block-all big-game">
        <?= \app\helpers\ImageHelper::image($game->bigscreen, 'Bigscreen ' . $game->title) ?>
        <?php if($game->demoGame): ?><span class="bg-hover-game"><span><?= Yii::t('app/f', 'Играть') ?></span></span><?php endif; ?>
    </div>

    <div class="block-all under-game-links">
        <?php foreach ($game->getTags()->all() as $tag): ?>
            <a href="<?= Filters::getSearchTagUrl($tag->id) ?>"><?= $tag->title ?></a>
        <?php endforeach; ?>
    </div>

    <div class="block-all grey-border-bottom">
        <div class="table-game">

            <?php foreach($game->getPropertiesSite() as $properties): ?>
                <div class="table-game-col">
                    <?php foreach($properties as $property => $css): ?>
                        <div class="table-game-row">
                            <div class="table-game-row-in">
                                <i class="icon-chr <?= $css ?>"></i>
                                <span><?= $game->getAttributeLabel($property) ?></span>
                            </div>
                            <div class="table-game-row-in">
                                <span>
                                    <?php $text = $game->$property; ?>

                                    <?php if ($property == 'jackpot'): ?>
                                        <?php
                                            $text = [1 => Yii::t('app', 'Yes'), 2 => Yii::t('app', 'No'), 3 => Yii::t('app', 'Progressive')][$game->$property];
                                        ?>
                                    <?php elseif ($property == 'zero'): ?>
                                        <?php
                                        $text = [1 => Yii::t('app', 'Yes'), 2 => Yii::t('app', 'No'), 3 => Yii::t('app', 'Doubling game')][$game->$property];
                                        ?>
                                    <?php elseif ($property == 'joker'): ?>
                                        <?php
                                        $text = [1 => Yii::t('app', 'Yes'), 0 => Yii::t('app', 'No')][$game->$property];
                                        ?>
                                    <?php elseif($property == 'video_viewer_url'): ?>
                                        <a href="<?= $game->$property ?>" class="btn-green" target="_blank">СМОТРЕТЬ</a>
                                    <?php elseif(is_bool($game->$property)): ?>
                                        <?php $text = $game->$property ? Yii::t('app', 'Yes') : Yii::t('app', 'No'); ?>
                                    <?php endif; ?>

                                    <?php echo Html::a($text, Filters::getSearchPropertyUrl($property, $game->$property)); ?>
                                </span>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endforeach; ?>

            <div class="clear"></div>
        </div>
    </div>

    <div class="block-all grey-border-bottom">
        <div class="bl-red-title-normal">
            <span><?= Yii::t('app/f', 'Описание игры') ?></span>

            <div style="float: right"><?= \app\widgets\SNShare::widget() ?></div>

            <div class="clear"></div>
        </div>

        <?= $game->description ?>

        <div class="game-small-pr">
            <?php for ($i = 0; $i < count($game->screens); $i += 1): ?>
                <?= \app\helpers\ImageHelper::image($game->screens[$i], $game->title . ' ' . ($i + 1)) ?>
            <?php endfor; ?>
        </div>

    </div>

    <?php if ($game->another_titles): ?>
        <?php echo $game->another_titles; ?>
    <?php endif; ?>

    <?php if ($symbols = $game->getSymbolsSite()): ?>
        <div class="block-all grey-border-bottom">
            <div class="roll-up-block">
                <span class="roll-up-block-arrow"></span>
                <span><?= Yii::t('app/f', 'Свернуть описание') ?></span>
            </div>

                <div class="roll-block">
                <span class="red-line"></span>
                <div class="roll-title">
                    <?= Yii::t('app/f', 'Символы игрового автомата') ?> <?= str_replace(['Игровой автомат '], '', $game->title) ?>
                </div>

                <?php foreach($symbols as $symbol): ?>

                <div class="roll-blocks">
                    <?php if(isset($symbol[0])): ?>
                    <div class="float-left">
                        <div>
                            <?= \app\helpers\ImageHelper::image($symbol[0]->logo, $game->getTitlePreview() . ' символ ' . $symbol[0]->description) ?>
                            <span>
                                <?php foreach($symbol[0]->getMultiSite()['left'] as $left): ?>
                                    <?= $left ?><br>
                                <?php endforeach; ?>
                            </span>
                            <span>
                                <?php foreach($symbol[0]->getMultiSite()['right'] as $right): ?>
                                    <?= $right ?><br>
                                <?php endforeach; ?>
                            </span>
                        </div>
                        <p><?= $symbol[0]->description ?></p>
                    </div>
                    <?php endif; ?>

                    <?php if(isset($symbol[1])): ?>
                    <div class="float-right">
                        <div>
                            <?= \app\helpers\ImageHelper::image($symbol[1]->logo, $game->getTitlePreview() . ' символ ' . $symbol[1]->description) ?>
                            <span>
                                <?php foreach($symbol[1]->getMultiSite()['left'] as $left): ?>
                                    <?= $left ?><br>
                                <?php endforeach; ?>
                            </span>
                            <span>
                                <?php foreach($symbol[1]->getMultiSite()['right'] as $right): ?>
                                    <?= $right ?><br>
                                <?php endforeach; ?>
                            </span>
                        </div>
                        <p><?= $symbol[1]->description ?></p>
                    </div>
                    <?php endif; ?>

                    <div class="clear"></div>
                </div>

                <?php endforeach; ?>

            </div>
        </div>
    <?php endif; ?>

    <?php if($game->video_viewer_url || $game->video_interesting_url): ?>

    <div class="block-all grey-border-bottom">
        <div class="roll-up-block">
            <span class="roll-up-block-arrow"></span>
            <span><?= Yii::t('app/f', 'Свернуть описание') ?></span>
        </div>

        <div class="roll-block">
            <span class="red-line"></span>
            <div class="roll-title">
                <?= Yii::t('app/f', 'Видеообзоры и интересное видео') ?>
            </div>

            <div class="roll-blocks roll-videos">
                <?php if($game->video_viewer_url): ?>
                    <div class="roll-video-block">
                        <?= $game->video_viewer_url ?>
                    </div>
                <?php endif; ?>

                <?php if($game->video_interesting_url): ?>
                    <div class="roll-video-block">
                        <?= $game->video_interesting_url ?>
                    </div>
                <?php endif; ?>

            </div>
        </div>
    </div>

    <?php endif; ?>
</div>

<?php $this->beginBlock('widgets'); ?>
    <div class="fr-right-color">
        <?= \app\widgets\GamesSame::widget(['game' => $game]) ?>
        <?= \app\widgets\GameCasino::widget(['game' => $game]) ?>
    </div>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('admin'); ?>
    <a href="<?php echo \app\helpers\AdminHelper::getSlotsItemsUpdateUrl($game->id); ?>" target="_blank">Редактировать</a>&nbsp;|
    <?= \app\widgets\Link::widget(['model' => $game]) ?>
<?php $this->endBlock(); ?>

<script>
    rating('#rating', 'game', '<?= $game->slug ?>');
</script>

<?php if ($game->demoGame): ?>
<script>
    $('[class="block-all big-game"]').click(function (e) {
        $(this).css('display', 'none');

        <?php if ($game->demoGame->url): ?>
            var game = $("<iframe>").attr({
                "width": "100%",
                "height": 480,
                "frameborder" : 0,
                "allowfullscreen" : true,
                "src" : "<?php echo str_replace('http://', '//', $game->demoGame->url); ?>"
            });
        <?php else: ?>
            var game = $("<?php echo addcslashes($game->demoGame->object, '"'); ?>");
        <?php endif; ?>

        $('#big-game').append(game).css('display', 'block');
    });
</script>
<?php endif; ?>