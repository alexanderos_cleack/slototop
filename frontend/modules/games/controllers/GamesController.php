<?php

namespace app\modules\games\controllers;

use Yii;
use yii\data\Pagination;
use yii\web\Controller;

use common\models\casino\Casino;
use common\models\slots\Slots;
use common\models\slots\SlotsCategories;

use app\helpers\UrlHelpers;
use app\helpers\BreadcrumbsHelper;
use app\modules\games\models\Filters;
use app\modules\games\models\Sort;
use app\modules\games\widgets\GameItem;

/**
 * Class GamesController
 * @package app\modules\games\controllers
 */
class GamesController extends Controller
{
    public $enableCsrfValidation = false;

    public function actions()
    {
        return [
            'rating' => [
                'class' => 'app\components\actions\RatingAction',
                'model' => Slots::class,
            ],
        ];
    }

    public function actionIndex()
    {
        BreadcrumbsHelper::add(Yii::t('app/f', 'Игровые автоматы'));

        $query = \app\models\slots\Slots::find()
            ->select(['t.id', 't.title', 't.slug', 't.enabled'])
            ->from(['t' => \app\models\slots\Slots::tableName()])
            ->where(['t.enabled' => true]);

        $filters = Filters::set($query);

        $pagination = $this->getPagination($query);

        $query->with(['logo', 'vendor']);

        $sort = Sort::set($query);

        $items = $this->getItems($query, $pagination);

        $seo = \common\models\seotext\SeoText::getSeoGames();

        $this->saveQuery($query, $pagination);

        return $this->render('index', [
            'items' => $items,
            'count' => $pagination->totalCount,
            'pagination' => $pagination,
            'filters' => $filters,
            'sort' => $sort,
            'seo' => $seo,
        ]);
    }

    public function actionShow($slug)
    {
        /** @var Slots $game */
        $game = Slots::find()->where(['slug' => $slug/*, 'enabled' => true*/])->one();

        if (!$game) throw new \yii\web\NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        $properties = SlotsCategories::find()->where(['id' => $game->category_id])->one();

        BreadcrumbsHelper::add(Yii::t('app/f', 'Игровые автоматы'), UrlHelpers::games());
        BreadcrumbsHelper::add($game->getTitlePreview());

        return $this->render('show', [
            'game' => $game,
            'properties' => $properties,
        ]);
    }

    public function actionSame($slug)
    {
        $game = Slots::find()->where(['slug' => $slug])->one();
        if (!$game) throw new \yii\web\NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        $query = $game->getGamesSameQuery();

        $filters = Filters::set($query);

        $pagination = $this->getPagination($query);

        $sort = Sort::set($query, ['slug' => $slug]);

        $items = $this->getItems($query, $pagination);

        $seo = \common\models\seotext\SeoText::getSeoGamesSame($game);

        $this->saveQuery($query, $pagination);

        return $this->render('index', [
            'items' => $items,
            'count' => $pagination->totalCount,
            'pagination' => $pagination,
            'filters' => $filters,
            'sort' => $sort,
            'seo' => $seo,
        ]);
    }

    public function actionCasino($casino)
    {
        /** @var Casino $casino */
        $casino = Casino::find()->where(['slug' => $casino])->one();
        if (!$casino) throw new \yii\web\NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        $query = $casino->getPopularGames_query();

        $filters = Filters::set($query);

        $pagination = $this->getPagination($query);

        $sort = Sort::set($query, ['casino' => $casino->slug]);

        $items = $this->getItems($query, $pagination);

        $seo = \common\models\seotext\SeoText::getSeoGamesPopular($casino);

        $this->saveQuery($query, $pagination);

        return $this->render('index', [
            'items' => $items,
            'count' => $pagination->totalCount,
            'pagination' => $pagination,
            'filters' => $filters,
            'sort' => $sort,
            'seo' => $seo,
        ]);
    }

    public function actionVendor($vendor)
    {
        $vendor = \app\models\vendors\Vendors::find()->where(['slug' => $vendor])->one();
        if (!$vendor) throw new \yii\web\NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        $query = $vendor->getSlots();
        $query->where(['t.enabled' => true]);

        $filters = Filters::set($query);

        $pagination = $this->getPagination($query);

        $query->with(['logo', 'vendor']);

        $sort = Sort::set($query, ['vendor' => $vendor->slug]);

        $items = $this->getItems($query, $pagination);

        $seo = \common\models\seotext\SeoText::getSeoGamesVendor($vendor);

        $this->saveQuery($query, $pagination);

        return $this->render('index', [
            'items' => $items,
            'count' => $pagination->totalCount,
            'pagination' => $pagination,
            'filters' => $filters,
            'sort' => $sort,
            'seo' => $seo,
        ]);
    }

    public function actionVendorNewGames($vendor)
    {
        $vendor = \app\models\vendors\Vendors::find()->where(['slug' => $vendor])->one();
        if (!$vendor) throw new \yii\web\NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        $query = $vendor->getVendorsNewGames();

        $filters = Filters::set($query);

        $pagination = $this->getPagination($query);

        $sort = Sort::set($query, ['vendor' => $vendor->slug]);

        $items = $this->getItems($query, $pagination);

        $seo = \common\models\seotext\SeoText::getSeoGamesPopular($vendor);

        $this->saveQuery($query, $pagination);

        return $this->render('index', [
            'items' => $items,
            'count' => $pagination->totalCount,
            'pagination' => $pagination,
            'filters' => $filters,
            'sort' => $sort,
            'seo' => $seo,
        ]);
    }

    public function actionFilter($slug)
    {
        $seo = \common\models\seotext\SeoFiltersRequests::getSeoGames($slug);
        if (!$seo) throw new \yii\web\NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        $model = \app\models\slots\Slots::class;
        $query = $model::find()->select(['t.id', 't.title', 't.slug', 't.enabled'])->from(['t' => $model::tableName()])->where(['t.enabled' => true]);

        $filters = Filters::set($query, json_decode($seo->filter, true));

        $pagination = $this->getPagination($query);

        $query->with(['logo', 'vendor']);

        $sort = Sort::set($query, ['slug' => $seo->slug], null, $seo);

        $items = $this->getItems($query, $pagination);

        $this->saveQuery($query, $pagination);

        BreadcrumbsHelper::add(Yii::t('app/f', 'Игровые автоматы'), UrlHelpers::games());
        BreadcrumbsHelper::add($seo->meta_h1);

        return $this->render('index', [
            'items' => $items,
            'count' => $pagination->totalCount,
            'pagination' => $pagination,
            'filters' => $filters,
            'sort' => $sort,
            'seo' => $seo,
        ]);
    }

    public function actionSort($slug)
    {
        $seo_sort = \common\models\seotext\SeoSortsRequests::getSeoGames($slug);
        if (!$seo_sort) throw new \yii\web\NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        $model = \app\models\slots\Slots::class;
        $query = $model::find()->select(['t.id', 't.title', 't.slug', 't.enabled'])->from(['t' => $model::tableName()])->where(['t.enabled' => true]);
        $query->with(['logo']);

        if ($seo_sort->filter) {
            $filters = Filters::set($query, json_decode($seo_sort->filter->filter, true));
        } else {
            $filters = Filters::set($query);
        }

        $pagination = $this->getPagination($query);

        if ($seo_sort->filter) {
            $sort = Sort::set($query, ['slug' => $seo_sort->filter->slug], json_decode($seo_sort->sort, true), $seo_sort->filter);
        } else {
            $sort = Sort::set($query, [], json_decode($seo_sort->sort, true));
        }

        $items = $this->getItems($query, $pagination);

        $this->saveQuery($query, $pagination);

        return $this->render('index', [
            'items' => $items,
            'count' => $pagination->totalCount,
            'pagination' => $pagination,
            'filters' => $filters,
            'sort' => $sort,
            'seo' => $seo_sort,
        ]);
    }

    public function actionFilters()
    {
        if (Yii::$app->request->isAjax) {
            $query = Slots::find()
                ->select(['t.id', 't.title'])
                ->from(['t' => Slots::tableName()])
                ->where(['t.enabled' => true]);

            $filters = new Filters();
            $filters->query = $query;

            if ($filters->load(Yii::$app->request->post()) && $filters->validate()) {
                Filters::search($query, $filters);

                return $this->renderPartial('filters/form', [
                    'filters' => $filters,
                ]);
            }
        }

        throw new \yii\web\BadRequestHttpException();
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function actionItems()
    {
        $session = Yii::$app->session;
        if ($session->has('games')) {
            list($query, $pagination) = array_values($session->get('games'));
            $pagination->page = Yii::$app->request->get('page');

            $items = [];
            foreach ($query->offset($pagination->offset)->limit($pagination->limit)->each() as $item) {
                $items[] = GameItem::widget(['model' => $item]);
            };

            Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
            return join('', $items);
        }
    }

    /**
     * @param  \yii\db\ActiveQuery $query
     */
    protected function saveQuery(\yii\db\ActiveQuery $query, \yii\data\Pagination $pagination)
    {
        $session = Yii::$app->session;
        $session->set('games', [
            'query' => $query,
            'pagination' => $pagination,
        ]);
    }

    /**
     * @param  \yii\db\ActiveQuery $query
     * @return \yii\data\Pagination
     */
    protected function getPagination(\yii\db\ActiveQuery $query)
    {
        $count_query = clone $query;
        $count = $count_query->count();
        return new Pagination(['totalCount' => $count, 'pageSize' => 52]);
    }

    /**
     * @param  \yii\db\ActiveQuery $query
     * @param  \yii\data\Pagination $pagination
     * @return \common\models\casino\Casino[]
     */
    protected function getItems(\yii\db\ActiveQuery $query, \yii\data\Pagination $pagination)
    {
        $offset_query = clone $query;
        return $offset_query->offset($pagination->offset)->limit($pagination->limit)->all();
    }

}