<?php

namespace app\modules\games\widgets;

use yii\base\Widget;

use common\models\slots\Slots;

/**
 * Class GameItem
 * @package app\modules\games\widgets
 *
 * @property Slots $model
 */
class GameItem extends Widget
{
    public $model;

    public function run()
    {
        return $this->render('gameItem', [
            'model' => $this->model,
        ]);
    }

}