<?php
/**
 * @var \yii\web\View $this
 * @var \common\models\slots\Slots $model
 */
?>

<div class="game">
    <div class="game-in">
        <span class="game-title"><?= $model->getTitlePreview() ?></span>
        <div class="game-img">
            <?= \app\helpers\ImageHelper::image($model->getImageLogo(), $model->title) ?>
            <a href="<?= \app\helpers\UrlHelpers::game($model->slug) ?>"
               title="<?= $model->title ?>" class="btn-red"><?= Yii::t('app/f', 'ПЕРЕЙТИ') ?></a>
        </div>
        <span class="gm-untitle game-set"><?= $model->vendor->title ?></span>
    </div>
</div>
