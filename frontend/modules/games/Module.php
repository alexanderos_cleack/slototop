<?php

namespace app\modules\games;

/**
 * Class Module
 * @package app\modules\games
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\games\controllers';
    public $layout = 'main';

    public function init()
    {
        parent::init();
    }

}
