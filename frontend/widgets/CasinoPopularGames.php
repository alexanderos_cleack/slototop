<?php

namespace app\widgets;

use yii\base\Widget;

use common\models\casino\Casino;

/**
 * Class CasinoPopularGames
 * @package app\widgets
 *
 * @property Casino $casino
 */
class CasinoPopularGames extends Widget
{
    public $casino;

    public function run()
    {
        $query = $this->casino->getSlots();
        $query->where(['enabled' => true]);
        if ($count = $query->count()) {
            $items = $query->offset(rand(1, $count) - 4)->orderBy('rating_users')->limit(4)->all();
            return $this->render('casino-popular-games/index', [
                'items' => $items,
                'casino' => $this->casino,
            ]);
        }
    }

}