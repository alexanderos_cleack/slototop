<?php
/**
 * @var \yii\web\View $this
 * @var \common\models\casino\Casino[] $items
 * @var \app\modules\casino\models\Sort $sort
 */
?>

<div class="block-sidebar">
    <div class="bl-red-title">
        <span class="bl-img"></span>
        <div class="bl-t-title">
            <span><?= Yii::t('app/f', 'New casino') ?></span>
        </div>

        <div class="clear"></div>
    </div>

    <?php foreach ($items as $item): ?>
        <div class="block-casino">
            <a href="<?= \app\helpers\UrlHelpers::casino($item->slug) ?>">
                <div class="float-left">
                    <?= \app\helpers\ImageHelper::image($item->logo, $item->title) ?>
                </div>
                <div class="float-right">
                    <span><?= $item->title ?></span>
                </div>
            </a>
            <div class="clear"></div>
        </div>
    <?php endforeach; ?>

    <div class="block-casino-all">
        <a href="<?= $sort->getSortNewDescUrl() ?>"><?= Yii::t('app/f', 'All new casino') ?></a>
    </div>
</div>