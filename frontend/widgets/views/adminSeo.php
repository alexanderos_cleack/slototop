<?php
/**
 * @var \yii\web\View $this
 * @var \app\widgets\AdminSeo $context
 */
?>

<?php if ($context->filter && $context->filter->seo): ?>
    <?php if (!$context->filter->seo->isFull()): ?>
        <a href="<?php echo \app\helpers\AdminHelper::getSeoFiltersUpdateUrl($context->filter->seo->id); ?>"
           target="_blank"><?= Yii::t('app/f', 'Add text for filter') ?></a>
    <?php else: ?>
        <a href="<?php echo \app\helpers\AdminHelper::getSeoFiltersUpdateUrl($context->filter->seo->id); ?>"
           target="_blank"><?= Yii::t('app/f', 'Edit text for filter') ?></a>
    <?php endif; ?>
<?php endif; ?>

<?php if ($context->sort && $context->sort->seo): ?>
    <?php if (!$context->sort->seo->isFull()): ?>
        <a href="<?php echo \app\helpers\AdminHelper::getSeoSortsUpdateUrl($context->sort->seo->id); ?>"
           target="_blank"><?= Yii::t('app/f', 'Add text for sort') ?></a>
    <?php else: ?>
        <a href="<?php echo \app\helpers\AdminHelper::getSeoSortsUpdateUrl($context->sort->seo->id); ?>"
           target="_blank"><?= Yii::t('app/f', 'Edit text for sort') ?></a>
    <?php endif; ?>
<?php endif; ?>
