<?php
/**
 * @var \yii\web\View $this
 */
?>

<?= \yii\widgets\Breadcrumbs::widget([
    'homeLink' => [
        'label' => 'Sloto.Top',
        'url' => '/',
    ],
    'links' => \app\helpers\BreadcrumbsHelper::get(),
    'options' => [
        'class' => 'breadcrumbs',
    ],
]); ?>
