<?php

use yii\helpers\Url;

?>

<?php if ($image): ?>
	<?php $image_info = $image->getFileInfo(); ?>
	<div itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
		<meta itemprop="url" content="<?php echo Url::to($image->getUrl(), true); ?>" />
		<meta itemprop="width" content="<?php echo $image_info['width'] ?>" />
		<meta itemprop="height" content="<?php echo $image_info['height'] ?>" />
	</div>
<?php endif; ?>