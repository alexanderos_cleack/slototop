<?php
/**
 * @var \yii\web\View $this
 * @var \common\models\slots\Slots[] $items
 * @var \app\models\vendors\Vendors $vendor
 * @var \app\modules\games\models\Sort $sort
 */
?>

<div class="block-sidebar">
    <div class="bl-red-title">
        <span class="bl-img"></span>
        <div class="bl-t-title">
            <span><?= Yii::t('app/f', 'Vendors new games') ?></span>
        </div>

        <div class="clear"></div>
    </div>

    <?php foreach ($items as $item): ?>
        <div class="block-casino">
            <a href="<?= \app\helpers\UrlHelpers::game($item->slug) ?>">
                <div class="float-left">
                    <?= \app\helpers\ImageHelper::image($item->logo, $item->title) ?>
                </div>
                <div class="float-right">
                    <span><?= $item->title ?></span>
                </div>
            </a>
            <div class="float-right">
                <a href="<?= \app\helpers\UrlHelpers::vendor($item->vendor->slug) ?>" class="sidebar-type-link">
                    <p>
                        <?= \app\helpers\ImageHelper::image('/img/games/set.png') ?> <?= $item->vendor->title ?>
                    </p>
                </a>
            </div>
            <div class="clear"></div>
        </div>
    <?php endforeach; ?>

    <div class="block-casino-all">
        <a href="<?= \common\models\seotext\SeoSortsRequests::getSeoGamesUrlByUrl($sort->getNewDescUrl(\app\modules\games\models\Filters::getSearchVendorUrl($vendor->id))) ?>"><?= Yii::t('app/f', 'All new games vendor') ?></a>
    </div>
</div>