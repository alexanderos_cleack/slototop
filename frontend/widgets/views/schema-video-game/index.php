<div style="display: none;" itemscope itemtype="http://schema.org/VideoGame">
    <meta itemprop="name" content="<?php echo $model->meta_title ?>">
    <meta itemprop="description" content="<?php echo $model->meta_description ?>">
    <meta itemprop="url" content="<?php echo $model->getUrl(true) ?>">
    <meta itemprop="sameAs" content="<?php echo $model->getUrl(true) ?>">

    <p itemprop="genre"><?php echo $model->category->title; ?></p>
    <?php if ($model->vendor): ?>
        <?php foreach ($model->vendor->platforms as $platform): ?>
            <p itemprop="operatingSystem"><?php echo $platform->title ?></p>
        <?php endforeach; ?>
    <?php endif; ?>

    <link itemprop="applicationCategory" href="http://schema.org/GameApplication"/>

    <?php echo join('', $add_content); ?>
</div>