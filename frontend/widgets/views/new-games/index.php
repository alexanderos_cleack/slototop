<?php
/**
 * @var \yii\web\View $this
 * @var \common\models\slots\Slots[] $items []
 * @var \app\modules\games\models\Sort $sort
 */
?>

<div class="block-sidebar">
    <div class="bl-red-title">
        <span class="bl-img"></span>
        <div class="bl-t-title">
            <span><?= Yii::t('app/f', 'New games') ?></span>
        </div>

        <div class="clear"></div>
    </div>

    <?php foreach ($items as $item): ?>
        <div class="block-casino">
            <a href="<?= \app\helpers\UrlHelpers::game($item->slug) ?>">
                <div class="float-left">
                    <?= \app\helpers\ImageHelper::image($item->logo, $item->title) ?>
                </div>
                <div class="float-right">
                    <span><?= $item->title ?></span>
                </div>
            </a>
            <div class="float-right">
                <?php if ($item->vendor): ?>
                    <a href="<?= \app\helpers\UrlHelpers::vendor($item->vendor->slug) ?>" class="sidebar-type-link">
                        <p>
                            <?= \app\helpers\ImageHelper::image('/img/games/set.png') ?> <?= $item->vendor->title ?>
                        </p>
                    </a>
                <?php endif; ?>
            </div>
            <div class="clear"></div>
        </div>
    <?php endforeach; ?>

    <div class="block-casino-all">
        <a href="<?= $sort->getSortNewDescUrl() ?>"><?= Yii::t('app/f', 'All new games') ?></a>
    </div>
</div>