<?php
/**
 * @var \yii\web\View $this
 * @var \common\models\bonuses\Bonuses[] $items
 * @var \common\models\bonuses\Bonuses $bonus
 */
?>

<div class="block-sidebar">
    <div class="bl-red-title">
        <span class="bl-img bl-img-cherry"></span>
        <div class="bl-t-title">
            <span><?= Yii::t('app/f', 'Casino bonuses') ?></span>
        </div>

        <div class="clear"></div>
    </div>

    <?php foreach ($items as $item): ?>
        <div class="block-casino">
            <a href="<?= \app\helpers\UrlHelpers::bonus($item->slug) ?>" target="_blank">
                <div class="float-left">
                    <?= \app\helpers\ImageHelper::image($item->bonusType->logo_mini, $item->title) ?>
                </div>
                <div class="float-right">
                    <span><?= $item->title ?></span>
                    <span class="game-stars gm-untitle"></span>
                </div>
            </a>
            <div class="float-right">
                <a href="<?= app\modules\bonuses\models\Filters::getSearchTypeUrl($item->bonusType->id) ?>"
                   class="sidebar-type-link">
                    <p><?= Yii::t('app/f', 'Type: {type}', ['type' => $item->bonusType->title]) ?></p></a>
            </div>

            <div class="clear"></div>
        </div>
    <?php endforeach; ?>

    <div class="block-casino-all">
        <a href="<?= \yii\helpers\Url::to(['/bonuses/bonuses/casino', 'casino' => $bonus->casino->slug]) ?>"><?= Yii::t('app/f', 'All casino bonuses') ?></a>
    </div>
</div>