<?php

use yii\helpers\Url;

?>

<div style="display: none;" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
	<meta itemprop="bestRating" content="<?php echo $rating['best_rating'] ?>" />
	<meta itemprop="worstRating" content="<?php echo $rating['worst_rating'] ?>" />
	<meta itemprop="ratingValue" content="<?php echo $rating['rating_value'] ?>" />
	<meta itemprop="ratingCount" content="<?php echo $rating['rating_count'] ?>" />
</div>