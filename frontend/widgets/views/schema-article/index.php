<?php

use yii\helpers\Url;

?>

<div style="display: none;" itemscope itemtype="http://schema.org/Article">
	<span itemprop="mainEntityOfPage"><?php echo $model->meta_title ?></span>
	<meta itemprop="headline" content="<?php echo $model->meta_title ?>" />
	<meta itemprop="description" content="<?php echo $model->meta_description ?>" />
	<meta itemprop="datePublished" content="<?php echo $model->created_time ?>" />
	<meta itemprop="dateModified" content="<?php echo $model->updated_time ?>" />
	<meta itemprop="url" content="<?php echo $model->getUrl(true); ?>" />

	<?php echo join('', $add_content); ?>
</div>
