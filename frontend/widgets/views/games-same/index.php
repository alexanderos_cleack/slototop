<?php
/**
 * @var \yii\web\View $this
 * @var \common\models\slots\Slots[] $games
 * @var \common\models\slots\Slots $game
 */
?>

<div class="block-sidebar">
    <div class="bl-red-title">
        <span class="bl-img bl-img-cherry"></span>
        <div class="bl-t-title">
            <span><?= Yii::t('app/f', 'Similar games') ?></span>
        </div>

        <div class="clear"></div>
    </div>

    <?php foreach ($games as $item): ?>
        <div class="block-casino">
            <a href="<?= \app\helpers\UrlHelpers::game($item->slug) ?>">
                <div class="float-left">
                    <?= \app\helpers\ImageHelper::image($item->logo, $item->title) ?>
                </div>
                <div class="float-right">
                    <span><?= $item->title ?></span>
                </div>
                <div class="float-right">
                    <a href="<?= \app\helpers\UrlHelpers::vendor($item->vendor->slug) ?>" class="sidebar-type-link">
                        <p>
                            <?= \app\helpers\ImageHelper::image('/img/games/set.png') ?> <?= $item->vendor->title ?>
                        </p>
                    </a>
                </div>
            </a>

            <div class="clear"></div>
        </div>
    <?php endforeach; ?>

    <div class="block-casino-all">
        <a href="<?= \yii\helpers\Url::to(['/games/games/same', 'slug' => $game->slug]) ?>"
           target="_blank"><?= Yii::t('app/f', 'All similar games') ?></a>
    </div>
</div>