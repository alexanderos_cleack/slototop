<?php
/**
 * @var \yii\web\View $this
 * @var \app\widgets\Menu $context
 */
?>

<ul class="main-menu">
    <li class="separate"></li>
    <li><a href="<?= \yii\helpers\Url::to(['/casino/casino/index']) ?>"
           class="menu-casino <?php echo Yii::$app->controller instanceof \app\modules\casino\controllers\CasinoController ? 'active' : '' ?>"><span></span><span><?= Yii::t('app/f', 'Casino') ?></span></a>
    </li>

    <li class="separate"></li>
    <li class="hover-show-list">
        <a href="<?= \yii\helpers\Url::to(['/games/games/index']) ?>"
           class="menu-games <?php echo Yii::$app->controller instanceof \app\modules\games\controllers\GamesController ? 'active' : '' ?>"><span></span><span><?= Yii::t('app/f', 'Games') ?></span></a>
        <ul <?= $context->isSlotsCategoriesActive() ? 'style="display:block"' : null ?>>
            <?php foreach (\app\modules\games\models\Filters::getSlotsCategories() as $item): ?>
                <li>
                    <a class="<?= \app\modules\games\models\Filters::getSearchCategoryUrl($item['id']) == '/' . Yii::$app->request->pathInfo ? 'active' : null ?>"
                       href="<?= \app\modules\games\models\Filters::getSearchCategoryUrl($item['id']) ?>"><?= $item['title'] ?></a>
                </li>
            <?php endforeach; ?>
        </ul>
    </li>

    <li class="separate"></li>
    <li><a href="<?= \yii\helpers\Url::to(['/bonuses/bonuses/index']) ?>"
           class="menu-bonuses <?php echo Yii::$app->controller instanceof \app\modules\bonuses\controllers\BonusesController ? 'active' : '' ?>"><span></span><span><?= Yii::t('app/f', 'Bonuses') ?></span></a>
    </li>

    <li class="separate"></li>
    <li><a href="<?= \yii\helpers\Url::to(['/vendors/vendors/index']) ?>"
           class="menu-platforms <?php echo Yii::$app->controller instanceof \app\modules\vendors\controllers\VendorsController ? 'active' : '' ?>"><span></span><span><?= Yii::t('app/f', 'Vendors') ?></span></a>
    </li>

    <li class="separate"></li>
    <li><a data-href="<?= \yii\helpers\Url::to(['/methods/payments/index']) ?>"
           class="menu-payments <?php echo Yii::$app->controller instanceof \app\modules\methods\controllers\PaymentsController ? 'active' : '' ?>"><span></span><span><?= Yii::t('app/f', 'Payment system') ?></span></a>
    </li>
    <li class="separate"></li>
</ul>
