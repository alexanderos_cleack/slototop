<?php

use yii\helpers\Url;

?>

<?php if ($prev): ?>
<a href="<?= Url::to(['show', 'slug' => $prev['slug']]) ?>" title="<?= $prev['title'] ?>">Предыдущая</a>
<?php else: ?>
    <span style="color: #aaa">Предыдущая</span>
<?php endif; ?>

<?php if ($next): ?>
<a href="<?= Url::to(['show', 'slug' => $next['slug']]) ?>" title="<?= $next['title'] ?>">Следущая</a>
<?php else: ?>
    <span style="color: #aaa">Следущая</span>
<?php endif; ?>