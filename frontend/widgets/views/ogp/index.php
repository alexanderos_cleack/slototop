<?php

use yii\helpers\Url;

$this->registerMetaTag(['prefix' => 'og: http://ogp.me/ns#', 'property' => 'og:type', 'content' => 'article']);
$this->registerMetaTag(['prefix' => 'og: http://ogp.me/ns#', 'property' => 'og:title', 'content' => $model->meta_title]);
$this->registerMetaTag(['prefix' => 'og: http://ogp.me/ns#', 'property' => 'og:description', 'content' => $model->meta_description]);
$this->registerMetaTag(['prefix' => 'og: http://ogp.me/ns#', 'property' => 'og:url', 'content' => $url]);

if ($image) {
	$image_info = $image->getFileInfo();
	$this->registerMetaTag(['prefix' => 'og: http://ogp.me/ns#', 'property' => 'og:image', 'content' => Url::to($image->getUrl(), true)]);
	$this->registerMetaTag(['prefix' => 'og: http://ogp.me/ns#', 'property' => 'og:image:type', 'content' => $image_info['mime']]);
	$this->registerMetaTag(['prefix' => 'og: http://ogp.me/ns#', 'property' => 'og:image:width', 'content' => $image_info['width']]);
	$this->registerMetaTag(['prefix' => 'og: http://ogp.me/ns#', 'property' => 'og:image:height', 'content' => $image_info['height']]);
}

$this->registerMetaTag(['prefix' => 'article: http://ogp.me/ns/article#', 'property' => 'article:published_time', 'content' => (new DateTime($model->created_time))->format('Y-m-d')]);
$this->registerMetaTag(['prefix' => 'article: http://ogp.me/ns/article#', 'property' => 'article:modified_time', 'content' => (new DateTime($model->updated_time))->format('Y-m-d')]);
$this->registerMetaTag(['prefix' => 'article: http://ogp.me/ns/article#', 'property' => 'article:section', 'content' => ucfirst($article_section)]);

$this->registerMetaTag(['prefix' => 'article: http://ogp.me/ns/article#', 'property' => 'og:site_name', 'content' => $seo_main->meta_title]);