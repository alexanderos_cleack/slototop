<?php

namespace app\widgets;

use yii\base\Widget;

use common\models\casino\Casino;

/**
 * Class CasinoBonuses
 * @package app\widgets
 *
 * @property Casino $casino
 */
class CasinoBonuses extends Widget
{
    public $casino;

    public function run()
    {
        $query = $this->casino->getBonuses();
        $query->where(['enabled' => true]);
        if ($count = $query->count()) {
            $items = $query->offset(rand(1, $count) - 4)->limit(4)->all();
            return $this->render('casino-bonuses/index', [
                'items' => $items,
                'casino' => $this->casino,
            ]);
        }
    }

}