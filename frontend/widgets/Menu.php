<?php

namespace app\widgets;

use yii\base\Widget;

use app\modules\games\models\Filters;

/**
 * Class Menu
 * @package app\widgets
 */
class Menu extends Widget
{
    public function run()
    {
        return $this->render('menu/header', [
            'context' => $this,
        ]);
    }

    /**
     * @return bool
     */
    public function isSlotsCategoriesActive()
    {
        foreach (Filters::getSlotsCategories() as $item) {
            if (Filters::getSearchCategoryUrl($item['id']) == '/' . \Yii::$app->request->pathInfo) {
                return true;
            }
        }

        return false;
    }

}