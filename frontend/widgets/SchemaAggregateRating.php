<?php

namespace app\widgets;

use yii\base\Widget;

class SchemaAggregateRating extends Widget {

    public $model;

    public function run() {
        $rating = \common\models\rating\Rating::getRatingInfo($this->model);

        return $this->render('schema-aggregate-rating/index', [
            'model' => $this->model,
            'rating' => $rating,
        ]);
    }

}