<?php

namespace app\widgets;

use yii\base\Widget;

class SchemaVideoGame extends Widget {

    public $model;
    public $add_content;

    public function run() {
        return $this->render('schema-video-game/index', [
            'model' => $this->model,
            'add_content' => $this->add_content,
        ]);
    }

}