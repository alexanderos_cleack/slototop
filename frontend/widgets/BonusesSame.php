<?php

namespace app\widgets;

use yii\base\Widget;

use common\models\bonuses\Bonuses;

/**
 * Class BonusesSame
 * @package app\widgets
 *
 * @property Bonuses $bonus
 */
class BonusesSame extends Widget
{
    public $bonus;

    public function run()
    {
        $query = $this->bonus->getBonusesSimilar_query();
        if ($count = $query->count()) {
            $items = $query->offset(rand(1, $count) - 4)->limit(4)->orderBy('rating_users')->all();
            return $this->render('bonuses-same/index', [
                'items' => $items,
                'bonus' => $this->bonus,
            ]);
        }
    }

}