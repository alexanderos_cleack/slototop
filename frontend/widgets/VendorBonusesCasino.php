<?php

namespace app\widgets;

use yii\base\Widget;

use app\models\vendors\Vendors;

/**
 * Class VendorBonusesCasino
 * @package app\widgets
 *
 * @property Vendors $vendor
 */
class VendorBonusesCasino extends Widget
{
    public $vendor;

    public function run()
    {
        $query = $this->vendor->getVendorsBonusesCasino();
        $query->limit(4);
        if ($query->count()) {
            $bonuses = $query->all();
            return $this->render('vendor-bonuses-casino/index', [
                'items' => $bonuses,
                'vendor' => $this->vendor,
            ]);
        }
    }

}