<?php

namespace app\widgets;

use yii\base\Widget;

use app\models\vendors\Vendors;

/**
 * Class VendorPopularCasino
 * @package app\widgets
 *
 * @property Vendors $vendor
 */
class VendorPopularCasino extends Widget
{
    public $vendor;

    public function run()
    {
        $query = $this->vendor->getVendorsPopularCasino();
        $query->limit(4);
        if ($query->count()) {
            $casino = $query->all();
            return $this->render('vendor-popular-casino/index', [
                'items' => $casino,
                'vendor' => $this->vendor,
            ]);
        }
    }

}