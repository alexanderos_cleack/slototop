<?php

namespace app\widgets;

use Yii;
use yii\base\Widget;
use yii\widgets\LinkPager as LP;

class LinkPager extends Widget {

    public $pagination;

    public function run() {

        if (Yii::$app->request->get('page')) {
            $this->view->registerMetaTag(['name' => 'robots', 'content' => 'noindex,nofollow']);
        }

        echo LP::widget([
            'pagination' => $this->pagination,
            'activePageCssClass' => 'main-nav-current',
            'nextPageLabel' => '>',
            'prevPageLabel' => '<',
            'options' => [
                'class' => 'main-nav',
            ],
        ]);
    }

}