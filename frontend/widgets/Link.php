<?php

namespace app\widgets;

use Yii;
use yii\base\Widget;

class Link extends Widget {

    public $model;

    public function run() {
        $class = $this->model->className();

        $prev = false;
        $current = false;
        $next = false;

        foreach ($class::find()->asArray()->select(['id', 'title', 'slug'])->where(['enabled' => true])->orderBy(['title' => SORT_ASC])->each() as $item) {
            if ($current) {
                $next = $item;
                break;
            }

            if ($item['id'] != $this->model->id) {
                $prev = $item;
            } else {
                $current = true;
            }
        }

        if (!$this->model->isNewRecord) return $this->render('link', [
            'prev' => $prev,
            'next' => $next,
        ]);
    }

}

?>