<?php

namespace app\widgets;

use yii\base\Widget;

use common\models\casino\Casino;

/**
 * Class CasinoSimilar
 * @package app\widgets
 *
 * @property Casino $casino
 */
class CasinoSimilar extends Widget
{
    public $casino;

    public function run()
    {
        $query = $this->casino->getCasinoSimilar_query();
        if ($count = $query->count()) {
            $items = $query->offset(rand(1, $count) - 4)->limit(4)->all();
            return $this->render('casino-similar/index', [
                'items' => $items,
                'casino' => $this->casino,
            ]);
        }
    }

}