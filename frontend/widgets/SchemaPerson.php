<?php

namespace app\widgets;

use yii\base\Widget;

class SchemaPerson extends Widget {

    public $model;
    public $name;

    public function init() {
        $this->name = 'SlotoTOP';
    }

    public function run() {
        return $this->render('schema-person/index', [
            'model' => $this->model,
            'name' => $this->name,
        ]);
    }

}