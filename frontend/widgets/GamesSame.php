<?php

namespace app\widgets;

use yii\base\Widget;

use common\models\slots\Slots;

/**
 * Class GamesSame
 * @package app\widgets
 *
 * @property Slots $game
 */
class GamesSame extends Widget
{
    public $game;

    public function run()
    {
        $query = $this->game->getGamesSameQuery();
        $count = $query->count();

        if ($count) {
            $games = $query->offset(rand(1, $count) - 5)->limit(5)->all();
            return $this->render('games-same/index', [
                'games' => $games,
                'game' => $this->game,
            ]);
        }
    }

}