<?php

namespace app\widgets;

use yii\base\Widget;

use common\models\casino\Casino;

use app\modules\casino\models\Sort;

/**
 * Class NewCasino
 * @package app\widgets
 */
class NewCasino extends Widget
{
    public function run()
    {
        $sort = new Sort();

        $query = Casino::find()
            ->select(['id', 'title', 'short_description', 'slug', 'enabled'])
            ->where(['enabled' => true])
            ->groupBy('id')->orderBy(['id' => SORT_DESC]);

        $casino = $query->limit(3)->all();

        return $this->render('new-casino/index', [
            'items' => $casino,
            'sort' => $sort,
        ]);
    }

}