<?php

namespace app\widgets;

use yii\base\Widget;
class SNShare extends Widget {

    public function run() {
        return $this->render('sn-share/index');
    }

}