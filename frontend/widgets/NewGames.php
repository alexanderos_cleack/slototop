<?php

namespace app\widgets;

use yii\base\Widget;

use common\models\slots\Slots;

use app\modules\games\models\Sort;

/**
 * Class NewGames
 * @package app\widgets
 */
class NewGames extends Widget
{
    public function run()
    {
        $sort = new Sort();

        $query = Slots::find()
            ->select(['id', 'title', 'slug', 'enabled'])
            ->where(['enabled' => true])->groupBy('id')
            ->orderBy(['id' => SORT_DESC]);

        $games = $query->limit(3)->all();

        return $this->render('new-games/index', [
            'items' => $games,
            'sort' => $sort,
        ]);
    }

}