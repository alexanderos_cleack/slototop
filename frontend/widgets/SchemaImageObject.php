<?php

namespace app\widgets;

use yii\base\Widget;

class SchemaImageObject extends Widget {

    public $image;

    public function run() {
        return $this->render('schema-image-object/index', [
            'image' => $this->image,
        ]);
    }

}