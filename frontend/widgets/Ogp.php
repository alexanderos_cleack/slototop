<?php

namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Url;

class Ogp extends Widget {

    public $model;
    public $article_section;
    public $url;
    public $image;

    public function run() {
        $seo_main = \common\models\seotext\SeoText::getSeoMain();

        return $this->render('ogp/index', [
            'article_section' => $this->article_section,
            'image' => $this->image,
            'model' => $this->model,
            'seo_main' => $seo_main,
            'url' => $this->url,
        ]);
    }

}