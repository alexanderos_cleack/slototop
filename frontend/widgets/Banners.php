<?php

namespace app\widgets;

use Yii;
use yii\base\Widget;

class Banners extends Widget
{
    public function run()
    {
        $banners = [
            '<a target="_blank" data-href="//salatsmayonezom.com/ref/11004649/"><img class="img-responsive" src="//salatsmayonezom.com/uploads/promo/300h250.jpg" alt="Vulkan Original"/></a>',
            '<a target="_blank" data-href="//salatsmayonezom.com/ref/79019852/"><img class="img-responsive" src="//salatsmayonezom.com/uploads/promo/Privetstvenny-j.gif" alt="Netgame Casino"/></a>',
            '<a target="_blank" data-href="//salatsmayonezom.com/ref/69355658/"><img class="img-responsive" src="//salatsmayonezom.com/uploads/promo/300x250-87207.gif" alt="Reel Emperor"/></a>',
            '<a target="_blank" data-href="//salatsmayonezom.com/ref/11921135/"><img class="img-responsive" src="//salatsmayonezom.com/uploads/promo/300h250-39981.gif" alt="SlotoKing"/></a>',
        ];

        $banner_id = Yii::$app->session->get('banner_id', 0);
        if ($banner_id != (count($banners) - 1)) {
            $banner_id += 1;
        } else {
            $banner_id = 0;
        }
        $banner = $banners[$banner_id];
        Yii::$app->session->set('banner_id', $banner_id);

        return $this->render('banners/index', [
            'banner' => $banner,
        ]);
    }
}