<?php

namespace app\widgets;

use yii\base\Widget;

use common\models\bonuses\Bonuses;

/**
 * Class BonusesCasino
 * @package app\widgets
 *
 * @property Bonuses $bonus
 */
class BonusesCasino extends Widget
{
    public $bonus;

    public function run()
    {
        $query = $this->bonus->getBonuses_query();
        if ($count = $query->count()) {
            $items = $query->offset(rand(1, $count) - 4)->limit(4)->all();
            return $this->render('bonuses-casino/index', [
                'items' => $items,
                'bonus' => $this->bonus,
            ]);
        }
    }

}