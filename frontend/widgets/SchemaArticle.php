<?php

namespace app\widgets;

use yii\base\Widget;

class SchemaArticle extends Widget {

    public $model;
    public $image;
    public $add_content;

    public function run() {
        return $this->render('schema-article/index', [
            'model' => $this->model,
            'image' => $this->image,
            'add_content' => $this->add_content,
        ]);
    }

}