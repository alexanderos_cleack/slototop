<?php

namespace app\widgets;

use yii\base\Widget;

class SchemaOrganization extends Widget {

    public $model;
    public $name;
    public $description;

    public function init() {
        $this->name = 'SlotoTOP';
        $this->description = 'Каталог игровых автоматов и лучших интернет казино';
    }

    public function run() {
        return $this->render('schema-organization/index', [
            'model' => $this->model,
            'name' => $this->name,
            'description' => $this->description,
        ]);
    }

}