<?php

namespace app\widgets;

use yii\base\Widget;

use common\models\seotext\SeoFiltersRequests;
use common\models\seotext\SeoSortsRequests;

/**
 * Class AdminSeo
 * @package app\widgets
 *
 * @property SeoFiltersRequests $filter
 * @property SeoSortsRequests $sort
 */
class AdminSeo extends Widget
{
    public $filter;
    public $sort;

    public function run()
    {
        return $this->render('adminSeo', [
            'context' => $this,
        ]);
    }

}