<?php

namespace app\widgets;

use yii\base\Widget;

use common\models\methods\MethodsPayment;

/**
 * Class PayMethodCasino
 * @package app\widgets
 *
 * @property MethodsPayment $method
 */
class PayMethodCasino extends Widget
{

    public $method;

    public function run()
    {
        $query = $this->method->getCasino_query();
        if ($count = $query->count()) {
            $items = $query->offset(rand(1, $count) - 4)->limit(4)->all();
            return $this->render('pay-method-casino/index', [
                'items' => $items,
                'method' => $this->method,
            ]);
        }
    }

}