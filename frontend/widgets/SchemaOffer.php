<?php

namespace app\widgets;

use yii\base\Widget;

class SchemaOffer extends Widget {

    public $model;

    public function run() {
        return $this->render('schema-offer/index', [
            'model' => $this->model,
        ]);
    }

}