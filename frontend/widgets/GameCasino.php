<?php

namespace app\widgets;

use yii\base\Widget;

use common\models\slots\Slots;

/**
 * Class GameCasino
 * @package app\widgets
 *
 * @property Slots $game
 */
class GameCasino extends Widget
{
    public $game;

    public function run()
    {
        $query = $this->game->getCasino_query();
        if ($count = $query->count()) {
            $items = $query->offset(rand(1, $count) - 4)->limit(4)->all();
            return $this->render('game-casino/index', [
                'items' => $items,
                'game' => $this->game,
            ]);
        }
    }

}