<?php

namespace app\widgets;

use yii\base\Widget;

use app\models\vendors\Vendors;
use app\modules\games\models\Sort;

/**
 * Class VendorNewGames
 * @package app\widgets
 *
 * @property Vendors $vendor
 */
class VendorNewGames extends Widget
{
    public $vendor;

    public function run()
    {
        $sort = new Sort();

        $query = $this->vendor->getVendorsNewGames();
        $query->limit(4);
        if ($query->count()) {
            $games = $query->all();
            return $this->render('vendor-new-games/index', [
                'items' => $games,
                'vendor' => $this->vendor,
                'sort' => $sort,
            ]);
        }
    }

}