<?php

namespace app\widgets;

use yii\base\Widget;

/**
 * Class Breadcrumbs
 * @package app\widgets
 */
class Breadcrumbs extends Widget
{
    public function run()
    {
        return $this->render('breadcrumbs');
    }

}