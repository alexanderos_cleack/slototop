<?php

namespace app\widgets;

use yii\base\Widget;

use common\models\bonuses\Bonuses;

use app\modules\bonuses\models\Sort;

/**
 * Class NewBonuses
 * @package app\widgets
 */
class NewBonuses extends Widget
{
    public function run()
    {
        $sort = new Sort();

        $query = Bonuses::find()
            ->select(['id', 'title', 'bonus_type_id', 'slug', 'enabled'])
            ->where(['enabled' => true])
            ->groupBy('id')->orderBy(['id' => SORT_DESC]);

        $bonuses = $query->limit(3)->all();

        return $this->render('new-bonuses/index', [
            'items' => $bonuses,
            'sort' => $sort,
        ]);
    }

}