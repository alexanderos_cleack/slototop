<?php

namespace app\components\actions;

use Yii;
use yii\base\Action;
use yii\db\ActiveRecord;

/**
 * Class RatingAction
 * @package app\components\actions
 *
 * @property ActiveRecord $model
 */
class RatingAction extends Action
{
    public $model;

    public function run($slug)
    {
        $model = $this->model;
        if ($model = $model::find()->where(['slug' => $slug])->one()) {
            if ($score = Yii::$app->request->post('score')) {
                $model->setRating(Yii::$app->request->post('score'), Yii::$app->request->getUserIP());
            }
        }
    }

}