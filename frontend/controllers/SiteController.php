<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

use common\models\bonuses\Bonuses;
use common\models\casino\Casino;
use common\models\pages\Pages;
use common\models\slots\Slots;
use common\models\seotext\SeoText;

/**
 * Class SiteController
 * @package app\controllers
 */
class SiteController extends Controller
{
    public $layout = 'default';

    public function actionIndex()
    {
        $query = Casino::find()->where(['enabled' => true])->limit(4)->orderBy(['main_page' => SORT_DESC, '(rating_users + rating_admins)' => SORT_DESC]);
        $query->select(['id', 'title', 'enabled', 'main_page', 'rating_users', 'rating_admins', 'slug']);
        $query->with(['logo']);
        $casino = $query->all();

        $query = Bonuses::find()->where(['enabled' => true])->limit(4)->orderBy(['main_page' => SORT_DESC, '(rating_users + rating_admins)' => SORT_DESC]);
        $query->select(['id', 'title', 'enabled', 'main_page', 'rating_users', 'rating_admins', 'slug', 'bonus_type_id', 'value_on_picture']);
        $query->with(['casino.logo', 'bonusType']);
        $bonuses = $query->all();

        $query = Slots::find()->where(['enabled' => true])->limit(4)->orderBy(['main_page' => SORT_DESC, 'rating_users' => SORT_DESC    ]);
        $query->select(['id', 'title', 'enabled', 'main_page', 'rating_users', 'slug']);
        $query->with(['logo', 'vendor']);
        $games = $query->all();

        $seo_main = SeoText::getSeoMain();

        return $this->render('index', [
            'casino' => $casino,
            'bonuses' => $bonuses,
            'games' => $games,
            'seo_main' => $seo_main,
        ]);
    }

    public function actionOffline()
    {
    }

    public function actionError()
    {
        return $this->render('error');
    }

    public function actionContacts()
    {
        $seo = \common\models\seotext\SeoText::getSeoContacts();
        return $this->render('contacts', [
            'seo' => $seo,
        ]);
    }

    public function actionPage($slug)
    {
        $modelPage = Pages::find()->where(['slug' => $slug/*, 'enabled' => true*/])->one();
        if (!$modelPage) throw new \yii\web\NotFoundHttpException(Yii::t('yii', 'Page not found.'));

        return $this->render('page', [
            'modelPage' => $modelPage,
        ]);
    }

    public function actionSearch()
    {
        if (!($search = \app\models\BaseSearch::set())) {
            $seo = \common\models\seotext\SeoText::getSeoSearch();
            return $this->render('search_form', [
                'seo' => $seo,
            ]);
        }

        $casino = $search->searchCasino();
        $games = $search->searchGames();
        $bonuses = $search->searchBonuses();
        $vendors = $search->searchVendors();
        $payments = $search->searchPayments();

        $count = count($casino) + count($games) + count($bonuses) + count($vendors) + count($payments);

        return $this->render('search', [
            'count' => $count,
            'casino' => $casino,
            'games' => $games,
            'bonuses' => $bonuses,
            'vendors' => $vendors,
            'payments' => $payments,
        ]);
    }

    public function actionRegistration()
    {
        $model = new \app\models\RegisterForm;
        if ($model->load(Yii::$app->request->post()) && $model->register()) {
            return $this->goHome()->send();
        }
    }

    public function actionLogin()
    {
        $model = new \app\models\LoginForm;
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goHome()->send();
        }
    }

    public function actionGpwa()
    {
        return $this->renderPartial('gpwa');
    }

    public function actionSlash()
    {
    }

}

?>