<?php

namespace app\controllers;

use yii\helpers\Url;
use yii\web\Controller;

use common\models\bonuses\Bonuses;
use common\models\casino\Casino;
use common\models\methods\MethodsPayment;
use common\models\seotext\SeoFiltersRequests;
use common\models\slots\Slots;
use common\models\vendors\Vendors;

/**
 * Class DeveloperController
 * @package app\controllers
 */
class DeveloperController extends Controller
{
    public function actionIndex()
    {
        echo __METHOD__;
    }

    public function actionCheckUrls()
    {
        set_time_limit(0);
        $ch = curl_init();

        $request = function ($ch, $url) {
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_exec($ch);
            $info = curl_getinfo($ch);
            echo $url . ' - ' . $info['http_code'] . '<br>';
        };

        $items = Casino::find()->where(['enabled' => true])->orderBy(['title' => SORT_ASC])->asArray()->all();
        foreach ($items as $item) {
            $url = Url::to(['/casino/casino/show', 'slug' => $item['slug']], true);
            $request($ch, $url);
        }

        $items = Slots::find()->where(['enabled' => true])->orderBy(['title' => SORT_ASC])->asArray()->all();
        foreach ($items as $item) {
            $url = Url::to(['/games/games/show', 'slug' => $item['slug']], true);
            $request($ch, $url);
        }

        $items = Bonuses::find()->where(['enabled' => true])->orderBy(['title' => SORT_ASC])->asArray()->all();
        foreach ($items as $item) {
            $url = Url::to(['/bonuses/bonuses/show', 'slug' => $item['slug']], true);
            $request($ch, $url);
        }

        $items = Vendors::find()->where(['enabled' => true])->orderBy(['title' => SORT_ASC])->asArray()->all();
        foreach ($items as $item) {
            $url = Url::to(['/vendors/vendors/show', 'slug' => $item['slug']], true);
            $request($ch, $url);
        }

        $items = MethodsPayment::find()->where(['enabled' => true])->orderBy(['title' => SORT_ASC])->asArray()->all();
        foreach ($items as $item) {
            $url = Url::to(['/methods/payments/show', 'slug' => $item['slug']], true);
            $request($ch, $url);
        }

        $items = SeoFiltersRequests::find()->where(['page' => SeoFiltersRequests::PAGE_CASINO])->andWhere(['!=', 'slug', ''])->asArray()->all();
        foreach ($items as $item) {
            $url = Url::to(['/casino/casino/filter', 'slug' => $item['slug']], true);
            $request($ch, $url);
        }

        $items = SeoFiltersRequests::find()->where(['page' => SeoFiltersRequests::PAGE_GAMES])->andWhere(['!=', 'slug', ''])->asArray()->all();
        foreach ($items as $item) {
            $url = Url::to(['/games/games/filter', 'slug' => $item['slug']], true);
            $request($ch, $url);
        }

        $items = SeoFiltersRequests::find()->where(['page' => SeoFiltersRequests::PAGE_BONUSES])->andWhere(['!=', 'slug', ''])->asArray()->all();
        foreach ($items as $item) {
            $url = Url::to(['/bonuses/bonuses/filter', 'slug' => $item['slug']], true);
            $request($ch, $url);
        }

        $items = SeoFiltersRequests::find()->where(['page' => SeoFiltersRequests::PAGE_VENDORS])->andWhere(['!=', 'slug', ''])->asArray()->all();
        foreach ($items as $item) {
            $url = Url::to(['/vendors/vendors/filter', 'slug' => $item['slug']], true);
            $request($ch, $url);
        }
    }

}