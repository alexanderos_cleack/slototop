<?php

namespace app\models;

use Yii;

/**
 * Class RegisterForm
 * @package app\models
 */
class RegisterForm extends \yii\base\Model
{
    public $login;
    public $password;
    public $password_repeat;

    public function rules()
    {
        return [
            [['login', 'password', 'password_repeat'], 'required'],
            [['login'], 'string', 'max' => 256],
            [['password', 'password_repeat'], 'string', 'min' => 6, 'max' => 256],
            [['password_repeat'], 'compare', 'compareAttribute' => 'password'],
        ];
    }

    public function register()
    {
        if ($this->validate()) {
            $user = new \app\models\users\Users;

            $user->attributes = array(
                'login' => $this->login,
                'password' => $this->password,
            );

            if ($user->save()) {
                return Yii::$app->user->login($user);
            } else {
                $this->addErrors($user->getErrors());
            }
        }
        return false;
    }

}