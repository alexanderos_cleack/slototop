<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Class BaseSearch
 * @package app\models
 *
 * @property string $search
 */
class BaseSearch extends Model
{
    public $search;

    public function rules()
    {
        return [
            [['search'], 'required'],
            [['search'], 'string', 'min' => 3, 'max' => 128],
        ];
    }

    public static function set()
    {
        $search = new BaseSearch;

        if ($search->load(Yii::$app->request->queryParams)) {
            if ($search->validate()) {
                return $search;
            }
        }
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function searchBonuses()
    {
        $query = \app\models\bonuses\Bonuses::find()
            ->select(['t.id', 't.title', 't.slug', 't.bonus_type_id', 't.value_on_picture', 't.enabled'])
            ->from(['t' => \app\models\bonuses\Bonuses::tableName()])
            ->where(['t.enabled' => true])
            ->andWhere(['ilike', 't.title', $this->search])
            ->with(['casino.logo', 'bonusType']);
        return $query->all();
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function searchCasino()
    {
        $query = \app\models\casino\Casino::find()
            ->select(['t.id', 't.title', 't.slug', 't.rating_users', 't.rating_admins', 't.enabled', '(t.rating_users + t.rating_admins)'])
            ->from(['t' => \app\models\casino\Casino::tableName()])
            ->where(['t.enabled' => true])
            ->andWhere(['ilike', 't.title', $this->search])
            ->with(['logo']);
        return $query->all();
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function searchGames()
    {
        $query = \app\models\slots\Slots::find()
            ->select(['t.id', 't.title', 't.slug', 't.enabled'])
            ->from(['t' => \app\models\slots\Slots::tableName()])
            ->where(['t.enabled' => true])
            ->andWhere(['OR',
                ['ilike', 't.title', $this->search],
                ['ilike', 't.another_titles', $this->search]])
            ->orWhere(['AND',
                ['ilike', 'tags.title', $this->search],
                ['tags2table.model_schema' => 'slots']
            ])
            ->leftJoin('tags2table', 't.id = tags2table.model_id')
            ->leftJoin('tags', 'tags2table.tag_id = tags.id')
            ->with(['logo', 'vendor']);

        return $query->all();
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function searchPayments()
    {
        $query = \app\models\methods\MethodsPayment::find()
            ->select(['t.id', 't.title', 't.slug', 't.enabled'])
            ->from(['t' => \app\models\methods\MethodsPayment::tableName()])
            ->where(['t.enabled' => true])
            ->andWhere(['ilike', 't.title', $this->search])
            ->with(['logo']);
        return $query->all();
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function searchVendors()
    {
        $query = \app\models\vendors\Vendors::find()
            ->select(['t.id', 't.title', 't.slug', 't.enabled'])
            ->from(['t' => \app\models\vendors\Vendors::tableName()])
            ->where(['t.enabled' => true])
            ->andWhere(['ilike', 't.title', $this->search])
            ->with(['logo']);
        return $query->all();
    }

}
