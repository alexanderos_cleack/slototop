<?php

namespace app\models;

use yii\base\Model;
use yii\db\ActiveQuery;

use common\models\seotext\SeoFiltersRequests;

/**
 * Class BaseFilters
 * @package app\models
 *
 * @property bool $isValidate
 * @property SeoFiltersRequests $seo
 *
 * @property ActiveQuery $query
 */
class BaseFilters extends Model
{
    public $isValidate = false;
    public $seo;

    protected $query;

    /**
     * @param ActiveQuery $query
     */
    public function setQuery(ActiveQuery $query) {
        $this->query = $query;
    }

    /**
     * @return ActiveQuery
     */
    public function getQuery() {
        return $this->query;
    }
}