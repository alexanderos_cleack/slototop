<?php

namespace app\models\casino;

use common\models\images\Images;
use yii\db\ActiveQuery;

/**
 * Class Casino
 * @package app\models\casino
 */
class Casino extends \common\models\casino\Casino
{
    public function behaviors()
    {
        return [
            [
                'class' => \common\components\behaviors\ArRatingBehavior::class,
            ],
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getLogo()
    {
        return $this->hasOne(Images::class, ['model_id' => 'id'])->where(['model_schema' => $this::tableName(), 'attribute' => 'logo']);
    }
}