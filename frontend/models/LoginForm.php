<?php

namespace app\models;

use Yii;

/**
 * Class LoginForm
 * @package app\models
 */
class LoginForm extends \yii\base\Model
{
    public $login;
    public $password;

    private $_user = false;

    public function rules()
    {
        return [
            [['login', 'password'], 'required'],
            [['login'], 'string', 'max' => 256],
            [['password'], 'string', 'min' => 6, 'max' => 256],
            [['password'], 'validatePassword'],
        ];
    }

    public function validatePassword()
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError('password', 'Incorrect username or password.');
            }
        }
    }

    public function login()
    {
        if ($this->validate()) {
            $this->getUser()->touch('updated_at');
            return Yii::$app->user->login($this->getUser());
        }
        return false;
    }

    public function getUser()
    {
        if (!$this->_user) {
            $this->_user = \app\models\users\Users::findByLogin($this->login);
        }
        return $this->_user;
    }

}