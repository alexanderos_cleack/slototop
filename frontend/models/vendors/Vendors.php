<?php

namespace app\models\vendors;

use app\models\bonuses\Bonuses;
use app\models\casino\Casino;
use app\models\slots\Slots;
use common\models\images\Images;

/**
 * Class Vendors
 * @package app\models\vendors
 */
class Vendors extends \common\models\vendors\Vendors
{

    public function behaviors()
    {
        return [
            [
                'class' => \common\components\behaviors\ArMetaTagsBehavior::class,
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCasino()
    {
        return $this->hasMany(Casino::class, ['id' => 'casino_id'])->via('vendors2casino');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogo()
    {
        return $this->hasOne(Images::class, ['model_id' => 'id'])->where(['model_schema' => $this::tableName(), 'attribute' => 'logo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlots()
    {
        return $this->hasMany(Slots::class, ['id' => 'slot_id'])->from(['t' => Slots::tableName()])->via('slots2vendors');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScreens()
    {
        return $this->hasOne(Images::class, ['model_id' => 'id'])->where(['model_schema' => $this::tableName(), 'attribute' => 'screens']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendorsNewGames()
    {
        $query = $this->hasMany(Slots::class, ['id' => 'slot_id'])->from(['t' => Slots::tableName()])->via('slots2vendors');
        $query->where(['t.enabled' => true]);
        $query->with(['logo', 'vendor']);
        $query->groupBy('t.id');
        $query->orderBy(['t.id' => SORT_DESC]);
        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendorsBonusesCasino()
    {
        $query = $this->hasMany(Bonuses::class, ['id' => 'bonus_id'])->from(['t' => Bonuses::tableName()])->via('bonuses2casino');
        $query->where(['t.enabled' => true]);
        $query->with(['casino.logo', 'bonusType', 'bonusType.logoMini']);
        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendorsPopularCasino()
    {
        $query = $this->getCasino()->from(['t' => Casino::tableName()]);
        $query->where(['t.enabled' => true]);
        $query->orderBy('t.title');
        $query->with(['logo']);
        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlotsCategories()
    {
        $query = \common\models\slots\SlotsCategories::find()
            ->distinct()
            ->select(['slots_categories.id', 'slots_categories.title'])
            ->joinWith('slots', false, 'INNER JOIN')
            ->joinWith('slots2vendors', false, 'INNER JOIN')
            ->where(['slots.enabled' => true, 'slots2vendors.vendor_id' => $this->id])
            ->orderBy('slots_categories.title');
        return $query;
    }

}