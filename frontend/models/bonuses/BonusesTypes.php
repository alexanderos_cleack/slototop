<?php

namespace app\models\bonuses;

use common\models\images\Images;

/**
 * Class BonusesTypes
 * @package app\models\bonuses
 */
class BonusesTypes extends \common\models\bonuses\BonusesTypes
{
    public function behaviors()
    {
        return [];
    }

    public function getLogoMini()
    {
        return $this->hasOne(Images::class, ['model_id' => 'id'])->where(['model_schema' => $this::tableName(), 'attribute' => 'logo_mini']);
    }

}