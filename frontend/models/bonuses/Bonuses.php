<?php

namespace app\models\bonuses;

use app\models\casino\Casino;
use common\models\bonuses\Bonuses2Casino;
use common\models\images\Images;

/**
 * Class Bonuses
 * @package app\models\bonuses
 */
class Bonuses extends \common\models\bonuses\Bonuses
{
    public function behaviors()
    {
        return [];
    }

    public function getLogo()
    {
        return $this->hasOne(Images::class, ['model_id' => 'id'])->where(['model_schema' => $this::tableName(), 'attribute' => 'logo']);
    }

    public function getCasino()
    {
        return $this->hasOne(Casino::class, ['id' => 'casino_id'])->viaTable(Bonuses2Casino::tableName(), ['bonus_id' => 'id']);
    }

    public function getBonusType()
    {
        return $this->hasOne(BonusesTypes::class, ['id' => 'bonus_type_id']);
    }

}