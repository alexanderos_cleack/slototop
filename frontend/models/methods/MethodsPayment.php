<?php

namespace app\models\methods;

use common\models\images\Images;

/**
 * Class MethodsPayment
 * @package app\models\methods
 */
class MethodsPayment extends \common\models\methods\MethodsPayment
{
    public function behaviors()
    {
        return [];
    }

    public function getLogo()
    {
        return $this->hasOne(Images::class, ['model_id' => 'id'])->where(['model_schema' => $this::tableName(), 'attribute' => 'logo']);
    }

}