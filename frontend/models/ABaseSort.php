<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

use common\models\seotext\SeoFiltersRequests;
use common\models\seotext\SeoSortsRequests;

/**
 * Class BaseSort
 * @package app\models
 *
 * @property string $title
 * @property string $rating
 * @property string $new
 * @property bool $isValidate
 * @property array $url_params
 * @property SeoFiltersRequests $seo_filter
 * @property SeoSortsRequests $seo
 *
 * @property string $sortAttribute
 * @property array $sortAttributes
 * @property ActiveQuery $query
 */
abstract class ABaseSort extends Model
{
    public $title;
    public $rating;
    public $new;
    public $isValidate = false;
    public $url_params = [];
    public $seo_filter;
    public $seo;

    protected $sortAttribute;
    protected $sortAttributes;

    protected $query;

    /**
     * @return ABaseSort
     */
    abstract public static function createModel();

    abstract public function defaultSort();

    /**
     * @param  ABaseSort $sort
     * @return SeoSortsRequests
     */
    abstract public function getSeoBySort($sort);

    /**
     * @param string $slug
     * @return string
     */
    abstract public function getUrlSort($slug);

    /**
     * @param ABaseSort $sort
     * @param integer $filter_id
     */
    abstract public function addSort($sort, $filter_id = null);

    /**
     * @return string
     */
    abstract public function getUrlFilter();

    /**
     * @return string
     */
    abstract public function getUrlBase();

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['title', 'rating', 'new'], 'safe'],
        ];
    }

    /**
     * @param ActiveQuery $query
     * @param array $url_params
     * @param array $attributes
     * @param SeoFiltersRequests|null $seo_filter
     * @return ABaseSort
     */
    public static function set(ActiveQuery $query, $url_params = [], $attributes = [], SeoFiltersRequests $seo_filter = null)
    {
        /** @var ABaseSort $model */
        $model = static::createModel();
        $model->query = $query;

        $model->url_params = $url_params;
        $model->seo_filter = $seo_filter;

        if ($attributes) {
            $model->attributes = $attributes;
            if ($model->validate()) {
                $model->checkTitle();
                $model->checkRating();
                $model->checkNew();

                if ($seo = $model->getSeoBySort($model)) {
                    $model->seo = $seo;
                }
            }
        } elseif ($model->load(Yii::$app->request->queryParams)) {
            if ($model->validate()) {
                if ($seo = $model->getSeoBySort($model)) {
                    if ($seo->isFull()) {
                        Yii::$app->getResponse()->redirect($model->getUrlSort($seo->slug))->send();
                    }
                }
                $model->isValidate = true;

                $model->checkTitle();
                $model->checkRating();
                $model->checkNew();

                $model->addSort($model, $seo_filter ? $seo_filter->id : null);
            }
        } else {
            $model->defaultSort();
        }

        $query->orderBy($model->sortAttributes);

        return $model;
    }

    /**
     * @return bool
     */
    public function isRatingAsc()
    {
        if ($this->rating == 'asc') return true;
        return false;
    }

    /**
     * @return bool
     */
    public function isRatingDesc()
    {
        if ($this->rating == 'desc') return true;
        return false;
    }

    /**
     * @return bool
     */
    public function isTitleAsc()
    {
        if ($this->title == 'asc') return true;
        return false;
    }

    /**
     * @return bool
     */
    public function isTitleDesc()
    {
        if ($this->title == 'desc') return true;
        return false;
    }

    /**
     * @return bool
     */
    public function isNewAsc()
    {
        if ($this->new == 'asc') return true;
        return false;
    }

    /**
     * @return bool
     */
    public function isNewDesc()
    {
        if ($this->new == 'desc') return true;
        return false;
    }

    /**
     * @param string $url
     * @return string
     */
    public function getRatingAscUrl($url = null)
    {
        $params = [
            $url ?: '',
            'Sort[rating]' => 'asc',
        ];
        return Url::to(ArrayHelper::merge($params, $this->url_params));
    }

    /**
     * @param string $url
     * @return string
     */
    public function getRatingDescUrl($url = null)
    {
        $params = [
            $url ?: '',
            'Sort[rating]' => 'desc',
        ];
        return Url::to(ArrayHelper::merge($params, $this->url_params));
    }

    /**
     * @param string $url
     * @return string
     */
    public function getTitleAscUrl($url = null)
    {
        $params = [
            $url ?: '',
            'Sort[title]' => 'asc',
        ];
        return Url::to(ArrayHelper::merge($params, $this->url_params));
    }

    /**
     * @param string $url
     * @return string
     */
    public function getTitleDescUrl($url = null)
    {
        $params = [
            $url ?: '',
            'Sort[title]' => 'desc',
        ];
        return Url::to(ArrayHelper::merge($params, $this->url_params));
    }

    /**
     * @param string $url
     * @return string
     */
    public function getNewAscUrl($url = null)
    {
        $params = [
            $url ?: '',
            'Sort[new]' => 'asc',
        ];
        return Url::to(ArrayHelper::merge($params, $this->url_params));
    }

    /**
     * @param string $url
     * @return string
     */
    public function getNewDescUrl($url = null)
    {
        $params = [
            $url ?: '',
            'Sort[new]' => 'desc',
        ];
        return Url::to(ArrayHelper::merge($params, $this->url_params));
    }

    /**
     * @return string
     */
    public function getSortRatingAscUrl()
    {
        $model = static::createModel();

        if ($this->seo_filter) {
            $model->url_params = $this->url_params;
        } else {
            $this->url_params = [];
        }

        $model->rating = 'asc';
        $seo_sort = $model->getSeoBySort($model);

        if ($seo_sort && $seo_sort->isFull()) {
            return $model->getUrlSort($seo_sort->slug);
        }

        if ($this->seo_filter) {
            return $this->getRatingAscUrl($model->getUrlFilter());
        }

        return $this->getRatingAscUrl($model->getUrlBase());
    }

    /**
     * @return string
     */
    public function getSortRatingDescUrl()
    {
        $model = static::createModel();

        if ($this->seo_filter) {
            $model->url_params = $this->url_params;
        } else {
            $this->url_params = [];
        }

        $model->rating = 'desc';
        $seo_sort = $model->getSeoBySort($model);

        if ($seo_sort && $seo_sort->isFull()) {
            return $model->getUrlSort($seo_sort->slug);
        }

        if ($this->seo_filter) {
            return $this->getRatingDescUrl($model->getUrlFilter());
        }

        return $this->getRatingDescUrl($model->getUrlBase());
    }

    /**
     * @return string
     */
    public function getSortNewAscUrl()
    {
        $model = static::createModel();

        if ($this->seo_filter) {
            $model->url_params = $this->url_params;
        } else {
            $this->url_params = [];
        }

        $model->new = 'asc';
        $seo_sort = $model->getSeoBySort($model);

        if ($seo_sort && $seo_sort->isFull()) {
            return $model->getUrlSort($seo_sort->slug);
        }

        if ($this->seo_filter) {
            return $this->getNewAscUrl($model->getUrlFilter());
        }

        return $this->getNewAscUrl($model->getUrlBase());
    }

    /**
     * @return string
     */
    public function getSortNewDescUrl()
    {
        $model = static::createModel();

        if ($this->seo_filter) {
            $model->url_params = $this->url_params;
        } else {
            $this->url_params = [];
        }

        $model->new = 'desc';
        $seo_sort = $model->getSeoBySort($model);

        if ($seo_sort && $seo_sort->isFull()) {
            return $model->getUrlSort($seo_sort->slug);
        }

        if ($this->seo_filter) {
            return $this->getNewDescUrl($model->getUrlFilter());
        }

        return $this->getNewDescUrl($model->getUrlBase());
    }

    /**
     * @return string
     */
    public function getSortTitleAscUrl()
    {
        $model = static::createModel();

        if ($this->seo_filter) {
            $model->url_params = $this->url_params;
        } else {
            $this->url_params = [];
        }

        $model->title = 'asc';
        $seo_sort = $model->getSeoBySort($model);

        if ($seo_sort && $seo_sort->isFull()) {
            return $model->getUrlSort($seo_sort->slug);
        }

        if ($this->seo_filter) {
            return $this->getTitleAscUrl($model->getUrlFilter());
        }

        return $this->getTitleAscUrl($model->getUrlBase());
    }

    /**
     * @return string
     */
    public function getSortTitleDescUrl()
    {
        $model = static::createModel();

        if ($this->seo_filter) {
            $model->url_params = $this->url_params;
        } else {
            $this->url_params = [];
        }

        $model->title = 'desc';
        $seo_sort = $model->getSeoBySort($model);

        if ($seo_sort && $seo_sort->isFull()) {
            return $model->getUrlSort($seo_sort->slug);
        }

        if ($this->seo_filter) {
            return $this->getTitleDescUrl($model->getUrlFilter());
        }

        return $this->getTitleDescUrl($model->getUrlBase());
    }

    public function checkTitle()
    {
        if ($this->title == 'asc') {
            $this->sortAttributes = ['t.title' => SORT_ASC];
            $this->query->orderBy($this->sortAttributes);
        } elseif ($this->title == 'desc') {
            $this->sortAttributes = ['t.title' => SORT_DESC];
            $this->query->orderBy($this->sortAttributes);
        }
    }

    public function checkRating()
    {
        if ($this->rating == 'asc') {
            $this->sortAttributes = [
                '(t.rating_users + t.rating_admins)' => SORT_ASC,
                't.title' => SORT_ASC,
            ];
            $this->query->orderBy($this->sortAttributes);
        } elseif ($this->rating == 'desc') {
            $this->sortAttributes = [
                '(t.rating_users + t.rating_admins)' => SORT_DESC,
                't.title' => SORT_ASC,
            ];
            $this->query->orderBy($this->sortAttributes);
        }
    }

    public function checkNew()
    {
        if ($this->new == 'asc') {
            $this->sortAttributes = ['t.id' => SORT_ASC];
            $this->query->orderBy($this->sortAttributes);
        } elseif ($this->new == 'desc') {
            $this->sortAttributes = ['t.id' => SORT_DESC];
            $this->query->orderBy($this->sortAttributes);
        }
    }

    /**
     * @return $this
     */
    public function setTitle()
    {
        $this->sortAttribute = 'title';
        return $this;
    }

    /**
     * @return $this
     */
    public function setRating()
    {
        $this->sortAttribute = 'rating';
        return $this;
    }

    /**
     * @return $this
     */
    public function setNew()
    {
        $this->sortAttribute = 'new';
        return $this;
    }

    public function desc()
    {
        $this->{$this->sortAttribute} = 'desc';
    }

    public function asc()
    {
        $this->{$this->sortAttribute} = 'asc';
    }
}
