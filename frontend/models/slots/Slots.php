<?php

namespace app\models\slots;

use common\components\behaviors\ArTagsBehavior;
use common\models\images\Images;

/**
 * Class Slots
 * @package app\models\slots
 */
class Slots extends \common\models\slots\Slots
{
    public function behaviors()
    {
        return [
            [
                'class' => ArTagsBehavior::class,
            ],
        ];
    }

    public function getLogo()
    {
        return $this->hasOne(Images::class, ['model_id' => 'id'])->where(['model_schema' => $this::tableName(), 'attribute' => 'logo']);
    }

}