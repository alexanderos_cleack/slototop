<?php

namespace app\helpers;

use Yii;
use yii\helpers\Url;

/**
 * Class UrlHelpers
 * @package app\helpers
 */
class UrlHelpers
{
    /**
     * @return string
     */
    public static function referrer()
    {
        return Yii::$app->request->referrer;
    }


    /**
     * @return string
     */
    public static function games()
    {
        return Url::to(['/games/games/index/']);
    }

    /**
     * @param string $slug
     * @return string
     */
    public static function gamesFilter($slug)
    {
        return Url::to(['/games/games/filter/', 'slug' => $slug]);
    }

    /**
     * @param string $slug
     * @return string
     */
    public static function game($slug)
    {
        return Url::to(['/games/games/show/', 'slug' => $slug]);
    }

    /**
     * @return string
     */
    public static function gamesItems()
    {
        return Url::to(['/games/games/items/']);
    }

    /**
     * @param string
     * @return string
     */
    public static function gamesSort($slug)
    {
        return Url::to(['/games/games/sort/', 'slug' => $slug]);
    }


    /**
     * @param string $slug
     * @return string
     */
    public static function casino($slug = null)
    {
        if ($slug) {
            return Url::to(['/casino/casino/show/', 'slug' => $slug]);
        }

        return Url::to(['/casino/casino/index/']);
    }

    /**
     * @param string $slug
     * @return string
     */
    public static function casinoFilter($slug)
    {
        return Url::to(['/casino/casino/filter/', 'slug' => $slug]);
    }

    /**
     * @return string
     */
    public static function casinoItems()
    {
        return Url::to(['/casino/casino/items/']);
    }

    /**
     * @param string
     * @return string
     */
    public static function casinoSort($slug)
    {
        return Url::to(['/casino/casino/sort/', 'slug' => $slug]);
    }


    /**
     * @return string
     */
    public static function vendors()
    {
        return Url::to(['/vendors/vendors/index/']);
    }

    /**
     * @param string $slug
     * @return string
     */
    public static function vendorsFilter($slug)
    {
        return Url::to(['/vendors/vendors/filter/', 'slug' => $slug]);
    }

    /**
     * @param string $slug
     * @return string
     */
    public static function vendor($slug)
    {
        return Url::to(['/vendors/vendors/show/', 'slug' => $slug]);
    }

    /**
     * @return string
     */
    public static function vendorsItems()
    {
        return Url::to(['/vendors/vendors/items/']);
    }

    /**
     * @param string
     * @return string
     */
    public static function vendorsSort($slug)
    {
        return Url::to(['/vendors/vendors/sort/', 'slug' => $slug]);
    }

    /**
     * @return string
     */
    public static function methodsPayments()
    {
        return Url::to(['/methods/payments/index/']);
    }

    /**
     * @param string $slug
     * @return string
     */
    public static function methodsPaymentsFilter($slug)
    {
        return Url::to(['/methods/payments/filter/', 'slug' => $slug]);
    }

    /**
     * @param string $slug
     * @return string
     */
    public static function methodPayment($slug)
    {
        return Url::to(['/methods/payments/show/', 'slug' => $slug]);
    }

    /**
     * @return string
     */
    public static function methodsPaymentsItems()
    {
        return Url::to(['/methods/payments/items/']);
    }

    /**
     * @param string
     * @return string
     */
    public static function methodsPaymentsSort($slug)
    {
        return Url::to(['/methods/payments/sort', 'slug' => $slug]);
    }

    /**
     * @return string
     */
    public static function bonuses()
    {
        return Url::to(['/bonuses/bonuses/index/']);
    }

    /**
     * @param string $slug
     * @return string
     */
    public static function bonusesFilter($slug)
    {
        return Url::to(['/bonuses/bonuses/filter/', 'slug' => $slug]);
    }

    /**
     * @param string $slug
     * @return string
     */
    public static function bonus($slug)
    {
        return Url::to(['/bonuses/bonuses/show', 'slug' => $slug]);
    }

    /**
     * @return string
     */
    public static function bonusesItems()
    {
        return Url::to(['/bonuses/bonuses/items/']);
    }

    /**
     * @param string
     * @return string
     */
    public static function bonusesSort($slug)
    {
        return Url::to(['/bonuses/bonuses/sort/', 'slug' => $slug]);
    }
}