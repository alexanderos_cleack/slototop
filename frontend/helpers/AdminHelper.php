<?php

namespace app\helpers;

/**
 * Class AdminHelper
 * @package app\helpers
 */
class AdminHelper
{
    /**
     * @param integer $id
     * @return string
     */
    public static function getBonusesItemsUpdateUrl($id)
    {
        $url = '/bonuses/bonuses-items/update/' . $id;
        return self::getUrl($url);
    }

    /**
     * @param integer $id
     * @return string
     */
    public static function getCasinoItemsUpdateUrl($id)
    {
        $url = '/casino/casino-items/update/' . $id;
        return self::getUrl($url);
    }

    /**
     * @param integer $id
     * @return string
     */
    public static function getSlotsItemsUpdateUrl($id)
    {
        $url = '/slots/slots-items/update/' . $id;
        return self::getUrl($url);
    }

    /**
     * @param integer $id
     * @return string
     */
    public static function getMethodsItemsUpdateUrl($id)
    {
        $url = '/methods/methods-items/update/' . $id;
        return self::getUrl($url);
    }

    /**
     * @param integer $id
     * @return string
     */
    public static function getVendorsItemsUpdateUrl($id)
    {
        $url = '/vendors/vendors-items/update/' . $id;
        return self::getUrl($url);
    }

    /**
     * @param integer $id
     * @return string
     */
    public static function getSeoFiltersUpdateUrl($id)
    {
        $url = '/seo/seo-filters/update/' . $id;
        return self::getUrl($url);
    }

    /**
     * @param integer $id
     * @return string
     */
    public static function getSeoSortsUpdateUrl($id)
    {
        $url = '/seo/seo-sorts/update/' . $id;
        return self::getUrl($url);
    }

    /**
     * @return string
     */
    public static function getDomain()
    {
        return \Yii::$app->request->hostName;
    }

    /**
     * @return bool
     */
    public static function isAllowed()
    {
        $ip = \Yii::$app->request->userIP;

        if (isset($_SERVER['HTTP_CF_CONNECTING_IP'])) {
            $ip = $_SERVER['HTTP_CF_CONNECTING_IP'];
        }

        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }

        return in_array($ip, ['127.0.0.1', '88.198.240.53', '5.9.38.81']);
    }

    /**
     * @param string $url
     * @return string
     */
    protected static function getUrl($url)
    {
        return '//admin.' . self::getDomain() . $url;
    }
}