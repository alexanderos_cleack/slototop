<?php

namespace app\helpers;

use common\models\images\Images;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Class ImageHelper
 * @package app\helpers
 */
class ImageHelper
{
    /**
     * @param Images|string $image
     * @param string $alt
     * @param array $htmlOptions
     * @param bool $lazyLoad
     * @param string $noImageSrc
     * @return string
     */
    public static function image($image, $alt = null, $htmlOptions = [], bool $lazyLoad = false, $noImageSrc = null)
    {
        $src = null;

        if ($image instanceof Images) {
            $src = $image->getUrl();
            $alt = !empty($image->alt) ? $image->alt : $alt;
        } elseif (is_string($image)) {
            $src = $image;
        }

        if ($src) {
            return Html::img($src, ArrayHelper::merge([
                'alt' => Html::encode($alt),
            ], $htmlOptions));
        }
    }

}