<?php

namespace app\helpers;

use Yii;

/**
 * Class BreadcrumbsHelper
 * @package app\helpers
 */
class BreadcrumbsHelper
{
    protected static $breadcrumbs = [];

    /**
     * @param string $label
     * @param string $url
     */
    public static function add($label, $url = null)
    {
        self::$breadcrumbs[] = [
            'label' => $label,
            'url' => $url,
        ];
    }

    /**
     * @return array
     */
    public static function get()
    {
        return self::$breadcrumbs;
    }
}