<?php

use yii\db\Schema;
use yii\db\Migration;

class m151115_231749_users_countries extends Migration
{
    public function safeUp()
    {
        $this->addColumn('countries', 'user_list', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('countries', 'user_list');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
