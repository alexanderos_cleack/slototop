<?php

use yii\db\Schema;
use yii\db\Migration;

class m150922_090115_create_table_demo_games extends Migration
{
    public function safeUp()
    {
        $this->createTable('demo_games', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'provider' => $this->string(255),
            'options' => $this->string(255),
        ]);

        $this->createIndex('idx_demo_games_provider', 'demo_games', 'provider');

        $this->addColumn('slots', 'demo_game_id', $this->integer());

        $this->addForeignKey('fk_slots_demo_game_id', 'slots', 'demo_game_id', 'demo_games', 'id', 'SET NULL');
    }

    public function safeDown()
    {
        $this->dropColumn('slots', 'demo_game_id');
        $this->dropTable('demo_games');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
