<?php

use yii\db\Schema;
use yii\db\Migration;

class m160211_121127_create_table_vendors2vendors extends Migration
{
    public function safeUp()
    {
        $this->createTable('vendors2vendors', [
            'vendor_id' => $this->integer()->notNull(),
            'vendor_attach_id' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey('vendors2vendors_pk', 'vendors2vendors', ['vendor_id', 'vendor_attach_id']);
        $this->addForeignKey('vendors2vendors_vendor_id_fkey', 'vendors2vendors', 'vendor_id', 'vendors', 'id', 'CASCADE');
        $this->addForeignKey('vendors2vendors_vendor_attach_id_fkey', 'vendors2vendors', 'vendor_attach_id', 'vendors', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable('vendors2vendors');
    }

}
