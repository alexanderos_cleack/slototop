<?php

use yii\db\Schema;
use yii\db\Migration;

class m150914_123227_edit_fields extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('casino', 'new_to_date');
        $this->addColumn('casino', 'new_to_date', Schema::TYPE_DATE);
        $this->addColumn('casino', 'slug', Schema::TYPE_STRING);
        $this->addColumn('casino', 'casino_url', Schema::TYPE_STRING);
        $this->addColumn('slots', 'slug', Schema::TYPE_STRING);
        $this->createIndex('slots_title_unique', 'slots', 'title', true);
    }

    public function safeDown()
    {
        $this->dropColumn('casino', 'slug');
        $this->dropColumn('casino', 'casino_url');
        $this->dropColumn('slots', 'slug');
        $this->dropIndex('slots_title_unique', 'slots');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
