<?php

use yii\db\Schema;
use yii\db\Migration;

class m151016_130640_casino extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('casino', 'license_id');
        $this->addColumn('casino', 'license_id', $this->integer()->defaultValue(null));
        $this->addForeignKey('casino_license_id_fkey', 'casino', 'license_id', 'licenses', 'id', 'SET NULL');
    }

    public function safeDown()
    {
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
