<?php

use yii\db\Migration;

/**
 * Class m180303_121024_create_table_tags
 */
class m180303_121024_create_table_tags extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tags', [
            'id' => $this->primaryKey(),
            'model_schema' => $this->string(128)->notNull(),
            'title' => $this->string(128)->notNull(),
        ]);

        $this->createIndex('tags_model_schema_title_unique', 'tags', ['model_schema', 'title'], true);

        $this->createTable('tags2table', [
            'model_schema' => $this->string(128)->notNull(),
            'model_id' => $this->integer()->notNull(),
            'tag_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('tags2table_tag_id_fk', 'tags2table', 'tag_id', 'tags', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tags2table');
        $this->dropTable('tags');
    }

}
