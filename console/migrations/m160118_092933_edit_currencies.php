<?php

use yii\db\Schema;
use yii\db\Migration;

class m160118_092933_edit_currencies extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('currencies', 'iso', $this->string(4));
    }

    public function safeDown()
    {
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
