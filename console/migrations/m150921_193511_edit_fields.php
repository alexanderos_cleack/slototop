<?php

use yii\db\Schema;
use yii\db\Migration;

class m150921_193511_edit_fields extends Migration
{
    public function safeUp()
    {
        $this->addColumn('slots_categories', 'slug', Schema::TYPE_STRING);

        $this->addColumn('slots_categories', 'jackpot', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'bet_min_number', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'bet_max_number', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'doubling_games', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'zero', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'bet_min_hand', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'bet_max_hand', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'retake', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'splite', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'surrender', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'insurance', Schema::TYPE_BOOLEAN);

        $this->addColumn('slots', 'jackpot', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots', 'bet_min_number', Schema::TYPE_FLOAT);
        $this->addColumn('slots', 'bet_max_number', Schema::TYPE_FLOAT);
        $this->addColumn('slots', 'doubling_games', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots', 'zero', Schema::TYPE_INTEGER);
        $this->addColumn('slots', 'bet_min_hand', Schema::TYPE_FLOAT);
        $this->addColumn('slots', 'bet_max_hand', Schema::TYPE_FLOAT);
        $this->addColumn('slots', 'retake', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots', 'splite', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots', 'surrender', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots', 'insurance', Schema::TYPE_BOOLEAN);

        $this->dropColumn('slots', 'numeric_series');
        $this->dropColumn('slots', 'win_max');

        $this->addColumn('slots', 'numeric_series', Schema::TYPE_STRING);
        $this->addColumn('slots', 'win_max', Schema::TYPE_FLOAT);
    }

    public function safeDown()
    {
        $this->dropColumn('slots_categories', 'slug');

        $this->dropColumn('slots_categories', 'jackpot');
        $this->dropColumn('slots_categories', 'bet_min_number');
        $this->dropColumn('slots_categories', 'bet_max_number');
        $this->dropColumn('slots_categories', 'doubling_games');
        $this->dropColumn('slots_categories', 'zero');
        $this->dropColumn('slots_categories', 'bet_min_hand');
        $this->dropColumn('slots_categories', 'bet_max_hand');
        $this->dropColumn('slots_categories', 'retake');
        $this->dropColumn('slots_categories', 'splite');
        $this->dropColumn('slots_categories', 'surrender');
        $this->dropColumn('slots_categories', 'insurance');

        $this->dropColumn('slots', 'jackpot');
        $this->dropColumn('slots', 'bet_min_number');
        $this->dropColumn('slots', 'bet_max_number');
        $this->dropColumn('slots', 'doubling_games');
        $this->dropColumn('slots', 'zero');
        $this->dropColumn('slots', 'bet_min_hand');
        $this->dropColumn('slots', 'bet_max_hand');
        $this->dropColumn('slots', 'retake');
        $this->dropColumn('slots', 'splite');
        $this->dropColumn('slots', 'surrender');
        $this->dropColumn('slots', 'insurance');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
