<?php

use yii\db\Schema;
use yii\db\Migration;

class m150928_203655_create_table_metatags extends Migration
{
    public function safeUp()
    {
        $this->createTable('meta_tags', [
            'id' => $this->primaryKey(),
            'model_schema' => $this->string(255),
            'model_id' => $this->integer(),
            'title' => $this->string(255),
            'description' => $this->string(255),
            'keywords' => $this->string(255),
        ]);

        $this->createIndex('idx_meta_tags_model', 'meta_tags', ['model_schema', 'model_id']);
    }

    public function safeDown()
    {
        $this->dropTable('meta_tags');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
