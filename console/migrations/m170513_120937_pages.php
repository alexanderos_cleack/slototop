<?php

use yii\db\Migration;

class m170513_120937_pages extends Migration
{
    public function safeUp()
    {
        $this->createTable('pages', [
            'id' => $this->primaryKey(),
            'title' => $this->text()->notNull(),
            'description' => $this->text()->notNull(),
            'slug' => $this->text()->notNull(),
            'enabled' => $this->boolean()->defaultValue(false),
        ]);
        $this->createIndex('pages_title_unique', 'pages', 'title', true);
        $this->createIndex('pages_slug_unique', 'pages', 'slug', true);
    }

    public function safeDown()
    {
        $this->dropTable('pages');
    }

}
