<?php

use yii\db\Schema;
use yii\db\Migration;

class m160607_090125_add_field_url_to_demo_games extends Migration
{
    public function safeUp()
    {
        $this->addColumn('demo_games', 'url', $this->string(1000));
    }

    public function safeDown()
    {
        $this->dropColumn('demo_games', 'url');
    }

}
