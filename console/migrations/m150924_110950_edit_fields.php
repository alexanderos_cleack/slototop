<?php

use yii\db\Schema;
use yii\db\Migration;

class m150924_110950_edit_fields extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('slots_categories', 'progressive_jackpot');
        $this->dropColumn('slots', 'progressive_jackpot');
        $this->dropColumn('slots', 'jackpot');
        $this->addColumn('slots', 'jackpot', $this->integer());

        $this->update('slots', ['jackpot' => 2], ['id' => 4]);
        $this->update('slots', ['jackpot' => 2], ['id' => 5]);
        $this->update('slots', ['jackpot' => 1], ['id' => 6]);
        $this->update('slots', ['jackpot' => 2], ['id' => 7]);
        $this->update('slots', ['jackpot' => 1], ['id' => 8]);
        $this->update('slots', ['jackpot' => 2], ['id' => 9]);
        $this->update('slots', ['jackpot' => 2], ['id' => 10]);
        $this->update('slots', ['jackpot' => 2], ['id' => 11]);
        $this->update('slots', ['jackpot' => 1], ['id' => 14]);
        $this->update('slots', ['jackpot' => 2], ['id' => 15]);
        $this->update('slots', ['jackpot' => 2], ['id' => 17]);

    }

    public function safeDown()
    {
        $this->addColumn('slots_categories', 'progressive_jackpot', $this->boolean());
        $this->addColumn('slots', 'progressive_jackpot', $this->boolean());
        $this->dropColumn('slots', 'jackpot');
        $this->addColumn('slots', 'jackpot', $this->boolean());
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
