<?php

use yii\db\Schema;
use yii\db\Migration;

class m160129_113107_add_title_filters_requestes extends Migration
{
    public function safeUp()
    {
        $this->addColumn('seo_filters_requests', 'title', $this->string(255));
    }

    public function safeDown()
    {
        $this->dropColumn('seo_filters_requests', 'title');
    }

}
