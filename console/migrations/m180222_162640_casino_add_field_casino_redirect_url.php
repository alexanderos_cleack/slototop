<?php

use yii\db\Migration;

/**
 * Class m180222_162640_casino_add_field_casino_redirect_url
 */
class m180222_162640_casino_add_field_casino_redirect_url extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('casino', 'casino_redirect_url', $this->string(256));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('casino', 'casino_redirect_url');
    }

}
