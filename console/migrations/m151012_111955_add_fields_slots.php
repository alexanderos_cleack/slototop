<?php

use yii\db\Schema;
use yii\db\Migration;

use common\models\slots\Slots;

class m151012_111955_add_fields_slots extends Migration
{
    public function safeUp()
    {
        $this->addColumn('slots', 'enabled', $this->boolean()->defaultValue(false));
        Slots::updateAll(['enabled' => true]);
    }

    public function safeDown()
    {
        $this->dropColumn('slots', 'enabled'); 
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
