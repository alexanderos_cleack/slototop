<?php

use yii\db\Schema;
use yii\db\Migration;

class m150929_151610_create_indexes extends Migration
{
    public function safeUp()
    {
        $this->createIndex('slots_slug', 'slots', 'slug');
        $this->createIndex('symbols_slot_id', 'symbols', 'slot_id');
    }

    public function safeDown()
    {
        $this->dropIndex('slots_slug', 'slots');
        $this->dropIndex('symbols_slot_id', 'symbols');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
