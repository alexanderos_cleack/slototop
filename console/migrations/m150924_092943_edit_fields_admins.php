<?php

use yii\db\Schema;
use yii\db\Migration;

class m150924_092943_edit_fields_admins extends Migration
{
    public function safeUp()
    {
        $this->addColumn('admins', 'auth_key', $this->string(32));
        $this->update('admins', ['auth_key' => 'Mf_4QtNXBee5AXiljuvZS9rRrVpZc1JA'], ['login' => 'root']);
    }

    public function safeDown()
    {
        $this->dropColumn('admins', 'auth_key');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
