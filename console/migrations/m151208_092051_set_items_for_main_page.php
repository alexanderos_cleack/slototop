<?php

use yii\db\Schema;
use yii\db\Migration;

class m151208_092051_set_items_for_main_page extends Migration
{
    public function safeUp()
    {
        $this->addColumn('casino', 'main_page', $this->boolean()->defaultValue(false));
        $this->addColumn('slots', 'main_page', $this->boolean()->defaultValue(false));
        $this->addColumn('bonuses', 'main_page', $this->boolean()->defaultValue(false));
        $this->addColumn('vendors', 'main_page', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('casino', 'main_page');
        $this->dropColumn('slots', 'main_page');
        $this->dropColumn('bonuses', 'main_page');
        $this->dropColumn('vendors', 'main_page');
    }

}
