<?php

use yii\db\Schema;
use yii\db\Migration;

class m151021_120922_casino extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('casino', 'min_deposit');
        $this->dropColumn('casino', 'min_output');
        $this->dropColumn('casino', 'amount_games');

        $this->addColumn('casino', 'email', $this->string(256)->defaultValue(null));
        $this->addColumn('casino', 'phone', $this->string(64)->defaultValue(null));
        $this->addColumn('casino', 'chat', $this->boolean()->defaultValue(false));
        $this->addColumn('casino', 'amount_games', $this->string(32)->defaultValue(null));
        $this->addColumn('casino', 'description_view', $this->text()->defaultValue(null));
        $this->addColumn('casino', 'description_registration', $this->text()->defaultValue(null));
        $this->addColumn('casino', 'description_games', $this->text()->defaultValue(null));

        $this->addColumn('currencies2casino', 'min_deposit', $this->float()->defaultValue(null));
        $this->addColumn('currencies2casino', 'min_output', $this->float()->defaultValue(null));

        $this->createTable('countries2casino', [
            'casino_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),
        ]);
        $this->addPrimaryKey('countries2casino_pkey', 'countries2casino', ['casino_id', 'country_id']);
        $this->addForeignKey('countries2casino_casino_id_fkey', 'countries2casino', 'casino_id', 'casino', 'id', 'CASCADE');
        $this->addForeignKey('countries2casino_country_id_fkey', 'countries2casino', 'country_id', 'countries', 'id', 'CASCADE');

        $this->createTable('vendors2casino', [
            'casino_id' => $this->integer()->notNull(),
            'vendor_id' => $this->integer()->notNull(),
        ]);
        $this->addPrimaryKey('vendors2casino_pkey', 'vendors2casino', ['casino_id', 'vendor_id']);
        $this->addForeignKey('vendors2casino_casino_id_fkey', 'vendors2casino', 'casino_id', 'casino', 'id', 'CASCADE');
        $this->addForeignKey('vendors2casino_vendor_id_fkey', 'vendors2casino', 'vendor_id', 'vendors', 'id', 'CASCADE');

        $this->dropTable('methods_output2casino');
        $this->createTable('methods_output2casino', [
            'casino_id' => $this->integer()->notNull(),
            'method_id' => $this->integer()->notNull(),
        ]);
        $this->addPrimaryKey('methods_output2casino_pkey', 'methods_output2casino', ['casino_id', 'method_id']);
        $this->addForeignKey('methods_output2casino_casino_id_fkey', 'methods_output2casino', 'casino_id', 'casino', 'id', 'CASCADE');
        $this->addForeignKey('methods_output2casino_vendor_id_fkey', 'methods_output2casino', 'method_id', 'methods_payment', 'id', 'CASCADE');

    }

    public function safeDown()
    {
        $this->addColumn('casino', 'min_deposit', $this->float()->defaultValue(null));
        $this->addColumn('casino', 'min_output', $this->float()->defaultValue(null));

        $this->dropColumn('casino', 'email');
        $this->dropColumn('casino', 'phone');
        $this->dropColumn('casino', 'chat');
        $this->dropColumn('casino', 'description_view');
        $this->dropColumn('casino', 'description_registration');
        $this->dropColumn('casino', 'description_games');

        $this->dropColumn('currencies2casino', 'min_deposit');
        $this->dropColumn('currencies2casino', 'min_output');

        $this->dropTable('countries2casino');
        $this->dropTable('vendors2casino');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
