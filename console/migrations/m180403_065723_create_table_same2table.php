<?php

use yii\db\Migration;

/**
 * Class m180403_065723_create_table_same2table
 */
class m180403_065723_create_table_same2table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('same2table', [
            'model_schema' => $this->string(128)->notNull(),
            'model_id' => $this->integer()->notNull(),
            'same_id' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('same2table');
    }

}
