<?php

use yii\db\Schema;
use yii\db\Migration;

class m151105_095941_seo_text extends Migration
{
    public function safeUp()
    {
        $this->createTable('seo_text', [
            'id' => $this->primaryKey(),
            'type' => $this->string()->notNull(),
            'description' => $this->text()->notNull(),
        ]);

        $this->createIndex('seo_text_type_unique', 'seo_text', 'type', true);

        $this->addColumn('casino', 'short_description', $this->string());
    }

    public function safeDown()
    {
        $this->dropTable('seo_text');
        $this->dropColumn('casino', 'short_description');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
