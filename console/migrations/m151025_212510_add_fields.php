<?php

use yii\db\Schema;
use yii\db\Migration;

use common\models\bonuses\Bonuses;
use common\models\casino\Casino;
use common\models\vendors\Vendors;
use common\models\methods\MethodsPayment;

class m151025_212510_add_fields extends Migration
{
    public function safeUp()
    {
        $this->addColumn('bonuses', 'enabled', $this->boolean()->defaultValue(false));
        Bonuses::updateAll(['enabled' => true]);

        $this->addColumn('casino', 'enabled', $this->boolean()->defaultValue(false));
        Casino::updateAll(['enabled' => true]);

        $this->addColumn('vendors', 'enabled', $this->boolean()->defaultValue(false));
        Vendors::updateAll(['enabled' => true]);

        $this->addColumn('methods_payment', 'enabled', $this->boolean()->defaultValue(false));
        MethodsPayment::updateAll(['enabled' => true]);
    }

    public function safeDown()
    {
        $this->dropColumn('bonuses', 'enabled');
        $this->dropColumn('casino', 'enabled');
        $this->dropColumn('vendors', 'enabled');
        $this->dropColumn('methods_payment', 'enabled');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
