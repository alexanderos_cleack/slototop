<?php

use yii\db\Schema;
use yii\db\Migration;

class m151022_133848_bonuses extends Migration
{
    public function safeUp()
    {
        $this->addColumn('bonuses', 'conditions', $this->text()->defaultValue(null));
        $this->addColumn('bonuses', 'url_bonus', $this->string(256)->defaultvalue(null));
    }

    public function safeDown()
    {
        $this->dropColumn('bonuses', 'conditions');
        $this->dropColumn('bonuses', 'url_bonus');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
