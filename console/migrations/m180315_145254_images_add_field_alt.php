<?php

use yii\db\Migration;

/**
 * Class m180315_145254_images_add_field_alt
 */
class m180315_145254_images_add_field_alt extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\common\models\images\Images::tableName(), 'alt', $this->string(128));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(\common\models\images\Images::tableName(), 'alt');
    }

}
