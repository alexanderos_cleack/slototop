<?php

use yii\db\Schema;
use yii\db\Migration;

class m160127_094828_add_field_meta_tags extends Migration
{
    public function safeUp()
    {
        $this->addColumn('meta_tags', 'h1', $this->string(255));
    }

    public function safeDown()
    {
        $this->dropColumn('meta_tags', 'h1');
    }

}
