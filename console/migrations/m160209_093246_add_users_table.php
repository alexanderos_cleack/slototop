<?php

use yii\db\Schema;
use yii\db\Migration;

class m160209_093246_add_users_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'login' => $this->string(256)->notNull(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string(256)->notNull(),
            'password_reset_token' => $this->string(256),
            'email' => $this->string(256),

            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('users');
    }

}
