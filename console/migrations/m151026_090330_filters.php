<?php

use yii\db\Schema;
use yii\db\Migration;

class m151026_090330_filters extends Migration
{
    public function safeUp()
    {
        $this->addColumn('slots_categories', 'filter', $this->string(64)->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropColumn('slots_categories', 'filter');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
