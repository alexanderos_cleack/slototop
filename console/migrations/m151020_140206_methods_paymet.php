<?php

use yii\db\Schema;
use yii\db\Migration;

class m151020_140206_methods_paymet extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('methods_payment', 'commission', $this->string(256)->defaultValue(null));

        $this->createTable('methods_payment2categories', [
            'method_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
        ]);
        $this->addPrimaryKey('methods_payment2categories_pk', 'methods_payment2categories', ['method_id', 'category_id']);
        $this->addForeignKey('methods_payment2categories_method_id_fkey', 'methods_payment2categories', 'method_id', 'methods_payment', 'id', 'CASCADE');
        $this->addForeignKey('methods_payment2categories_category_id_fkey', 'methods_payment2categories', 'category_id', 'methods_categories', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        // $this->addColumn('methods_payment', 'category_id', $this->integer()->notNull());
        // $this->addForeignKey('methods_payment_category_id_fkey', 'methods_payment', 'category_id', 'methods_categories', 'id', 'SET NULL');
        $this->dropTable('methods_payment2categories');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
