<?php

use yii\db\Schema;
use yii\db\Migration;

class m151023_075028_vendors extends Migration
{
    public function safeUp()
    {
        $this->addColumn('vendors', 'slug', $this->string()->defaultValue(null));
        $this->addColumn('vendors', 'country_id', $this->integer()->defaultValue(null));
        $this->addForeignKey('vendors_country_id_fkey', 'vendors', 'country_id', 'countries', 'id', 'SET NULL');
        $this->addColumn('vendors', 'year_established', $this->integer());
        $this->addColumn('vendors', 'rating_users', $this->integer()->defaultValue(null));
        $this->addColumn('vendors', 'rating_admins', $this->integer()->defaultValue(null));
        $this->addColumn('vendors', 'amount_games', $this->string(64)->defaultValue(null));

        $this->dropColumn('vendors', 'payout');
        $this->addColumn('vendors', 'payout', $this->integer()->defaultValue(null));

        $this->createTable('languages2vendors', [
            'vendor_id' => $this->integer()->notNull(),
            'language_id' => $this->integer()->notNull(),
        ]);
        $this->addPrimaryKey('languages2vendors_pkey', 'languages2vendors', ['vendor_id', 'language_id']);
        $this->addForeignKey('languages2vendors_vendor_id_fkey', 'languages2vendors', 'vendor_id', 'vendors', 'id', 'CASCADE');
        $this->addForeignKey('languages2vendors_language_id_fkey', 'languages2vendors', 'language_id', 'languages', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropColumn('vendors', 'slug');
        $this->dropColumn('vendors', 'country_id');
        $this->dropColumn('vendors', 'year_established');
        $this->dropColumn('vendors', 'rating_users');
        $this->dropColumn('vendors', 'rating_admins');
        $this->dropColumn('vendors', 'amount_games');

        $this->dropTable('languages2vendors');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
