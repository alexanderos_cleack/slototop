<?php

use yii\db\Schema;
use yii\db\Migration;

class m160310_141912_seo_sorts_requests extends Migration
{
    public function safeUp()
    {
        $this->createTable('seo_sorts_requests', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'filter_id' => $this->integer()->defaultValue(null),
            'sort' => $this->string(500),
            'url' => $this->string(500),
            'description' => $this->text(),
            'slug' => $this->string(),
            'count_elements' => $this->integer()->defaultValue(1),
            'page' => $this->string(255),
        ]);

        $this->createIndex('seo_sorts_requests_idx', 'seo_sorts_requests', ['sort', 'page']);
        $this->addForeignKey('seo_sorts_requests_filter_id_fkey', 'seo_sorts_requests', 'filter_id', 'seo_filters_requests', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable('seo_sorts_requests');
    }
}
