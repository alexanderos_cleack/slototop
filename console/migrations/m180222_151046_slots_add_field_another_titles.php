<?php

use yii\db\Migration;

/**
 * Class m180222_151046_slots_add_field_another_titles
 */
class m180222_151046_slots_add_field_another_titles extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('slots', 'another_titles', $this->string(512));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('slots', 'another_titles');
    }

}
