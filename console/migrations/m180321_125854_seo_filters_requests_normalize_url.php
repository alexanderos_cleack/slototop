<?php

use yii\db\Migration;

/**
 * Class m180321_125854_seo_filters_requests_normalize_url
 */
class m180321_125854_seo_filters_requests_normalize_url extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        foreach (\common\models\seotext\SeoFiltersRequests::find()->select(['id'])->createCommand()->query() as $row) {
            $model = \common\models\seotext\SeoFiltersRequests::findOne(['id' => $row['id']]);
            $attributes = json_decode($model->filter, true);

            $_attributes = [];
            foreach ($attributes as $k => $v) {
                if (!in_array($k, ['search_rating', 'search_lines', 'search_bet', 'search_win'])) {
                    $_attributes[$k] = $v;
                }
            }

            $attributes = $_attributes;
            $attributes = \common\models\seotext\SeoFiltersRequests::removeEmptyFields($attributes);
            $attributes_encode = json_encode($attributes);
            $count_elements = count($attributes);

            $_attributes = ['/' . $model->page];

            foreach ($attributes as $key => $value) {
                $_attributes[sprintf('Filters[%s]', $key)] = $value;
            }

            $model->attributes = [
                'filter' => $attributes_encode,
                'url' => \yii\helpers\Url::to($_attributes), // Т.к. тип запрос POST, а для правильного формирования фильтра нужен GET
                'count_elements' => $count_elements,
            ];
            $model->save();
        }

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
