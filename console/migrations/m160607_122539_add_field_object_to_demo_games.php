<?php

use yii\db\Schema;
use yii\db\Migration;

class m160607_122539_add_field_object_to_demo_games extends Migration
{
    public function safeUp()
    {
        $this->addColumn('demo_games', 'object', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn('demo_games', 'object');
    }

}
