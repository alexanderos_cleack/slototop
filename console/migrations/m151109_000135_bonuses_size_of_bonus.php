<?php

use yii\db\Schema;
use yii\db\Migration;

class m151109_000135_bonuses_size_of_bonus extends Migration
{
    public function safeUp()
    {
        $this->addColumn('bonuses', 'size_of_bonus', $this->string(128)->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropColumn('bonuses', 'size_of_bonus');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
