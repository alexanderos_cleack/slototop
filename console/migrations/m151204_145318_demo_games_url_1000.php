<?php

use yii\db\Schema;
use yii\db\Migration;

class m151204_145318_demo_games_url_1000 extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('demo_games', 'options', $this->string(1000));
    }

    public function safeDown()
    {
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
