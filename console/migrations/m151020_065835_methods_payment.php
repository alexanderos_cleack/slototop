<?php

use yii\db\Schema;
use yii\db\Migration;

class m151020_065835_methods_payment extends Migration
{
    public function SafeUp()
    {
        $this->createTable('methods_categories', [
            'id' => $this->primaryKey(),
            'title' => $this->string(256)->notNull(),
        ]);

        $this->batchInsert('methods_categories', ['title'], [['Электронные деньги'], ['Пластиковые карты'], ['Оплата через терминалы'], ['Платежные СМС-сервисы'], ['Системы денежных переводов'], ['Платежи через онлайн-банки'], ['Платежи со счетов мобильных операторатов']]);

        $this->addColumn('methods_payment', 'description', $this->text()->notNull());
        $this->addColumn('methods_payment', 'how_to_use', $this->text());
        $this->addColumn('methods_payment', 'category_id', $this->integer()->notNull());
        $this->addColumn('methods_payment', 'url_site', $this->string(256)->defaultValue(null));
        $this->addColumn('methods_payment', 'commission', $this->float()->defaultValue(null));
        $this->addColumn('methods_payment', 'terms_payment', $this->string(256)->defaultValue(null));
        $this->addColumn('methods_payment', 'slug', $this->string(256));
        $this->addForeignKey('methods_payment_category_id_fkey', 'methods_payment', 'category_id', 'methods_categories', 'id', 'SET NULL');

        $this->createTable('methods_payment2currencies', [
            'method_id' => $this->integer()->notNull(),
            'currency_id' => $this->integer()->notNull(),
            'output' => $this->float(),
        ]);
        $this->addPrimaryKey('methods_payment2currencies_pk', 'methods_payment2currencies', ['method_id', 'currency_id']);
        $this->addForeignKey('methods_payment2currencies_method_id_fkey', 'methods_payment2currencies', 'method_id', 'methods_payment', 'id', 'CASCADE');
        $this->addForeignKey('methods_payment2currencies_currency_id_fkey', 'methods_payment2currencies', 'currency_id', 'currencies', 'id', 'CASCADE');
    }

    public function SafeDown()
    {
        $this->dropColumn('methods_payment', 'description');
        $this->dropColumn('methods_payment', 'how_to_use');
        $this->dropColumn('methods_payment', 'category_id');
        $this->dropColumn('methods_payment', 'url_site');
        $this->dropColumn('methods_payment', 'commission');
        $this->dropColumn('methods_payment', 'terms_payment');
        $this->dropColumn('methods_payment', 'slug');
        $this->dropTable('methods_categories');
        $this->dropTable('methods_payment2currencies');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
