<?php

use yii\db\Schema;
use yii\db\Migration;

use common\models\slots\Slots;

class m151028_100950_add_fields extends Migration
{
    public function safeUp()
    {
        $this->addColumn('slots_categories', 'kind_of_cards', $this->boolean()->defaultValue(false));
        $this->addColumn('slots', 'kind_of_cards', $this->string()->defaultValue(null));

        Slots::updateAll(['joker' => 0], 'joker = \'нет\'');
        Slots::updateAll(['joker' => 1], 'joker = \'есть\'');
        Slots::updateAll(['joker' => 1], 'joker = \'да\'');
    }

    public function safeDown()
    {
        $this->dropColumn('slots_categories', 'kind_of_cards');
        $this->dropColumn('slots', 'kind_of_cards');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
