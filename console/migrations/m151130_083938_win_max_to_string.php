<?php

use yii\db\Schema;
use yii\db\Migration;

class m151130_083938_win_max_to_string extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('slots', 'win_max', $this->string(128));
    }

    public function safeDown()
    {
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
