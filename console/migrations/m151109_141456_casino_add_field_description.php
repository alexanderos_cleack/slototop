<?php

use yii\db\Schema;
use yii\db\Migration;

class m151109_141456_casino_add_field_description extends Migration
{
    public function safeUp()
    {
        $this->addColumn('casino', 'description_additional', $this->text()->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropColumn('casino', 'description_additional');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
