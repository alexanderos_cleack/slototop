<?php

use yii\db\Schema;
use yii\db\Migration;

class m151124_081928_add_fields_seo_text extends Migration
{
    public function safeUp()
    {
        $this->addColumn('seo_text', 'model_id', $this->integer()->defaultValue(null));
        $this->addColumn('seo_text', 'model_schema', $this->string()->defaultValue(null));
        $this->createIndex('seo_text_model_idx', 'seo_text', ['model_schema', 'model_id']);
        $this->dropIndex('seo_text_type_unique', 'seo_text');
    }

    public function safeDown()
    {
        $this->dropIndex('seo_text_model_idx', 'seo_text');
        $this->dropColumn('seo_text', 'model_id');
        $this->dropColumn('seo_text', 'model_schema');
        $this->createIndex('seo_text_type_unique', 'seo_text', 'type', true);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
