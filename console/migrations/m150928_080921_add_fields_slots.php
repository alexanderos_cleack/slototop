<?php

use yii\db\Schema;
use yii\db\Migration;

class m150928_080921_add_fields_slots extends Migration
{
    public function safeUp()
    {
        $this->addColumn('slots', 'amount_combinations', $this->integer());
        $this->addColumn('slots_categories', 'amount_combinations', $this->boolean());
    }

    public function safeDown()
    {
        $this->dropColumn('slots', 'amount_combinations');
        $this->dropColumn('slots_categories', 'amount_combinations');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
