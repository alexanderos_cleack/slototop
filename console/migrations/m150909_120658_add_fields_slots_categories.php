<?php

use yii\db\Schema;
use yii\db\Migration;

class m150909_120658_add_fields_slots_categories extends Migration
{
    public function safeUp()
    {
        $this->addColumn('slots_categories', 'amount_lines', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'amount_reels', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'bet_color', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'bet_more_less', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'bet_even_odd', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'bet_dozen', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'bet_line', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'risk_game', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'triplication_game', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'kind_of_roulette', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'kind_of_poker', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'kind_of_lottery', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'win_max', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'amount_wheels', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'amount_hands', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'numeric_series', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'joker', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'min_combination', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'special_combination', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'min_boxes', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'max_boxes', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'version', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'bet_min', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'bet_max', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots_categories', 'video_viewer_url', Schema::TYPE_BOOLEAN);

        $this->dropColumn('slots', 'amount_lines');
        $this->dropColumn('slots', 'amount_reels');
        $this->dropColumn('slots', 'bet_min');
        $this->dropColumn('slots', 'bet_max');
        $this->dropColumn('slots', 'payout');
        $this->dropColumn('slots', 'new_to_date');

        $this->addColumn('slots', 'bet_color', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots', 'bet_more_less', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots', 'bet_even_odd', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots', 'bet_dozen', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots', 'bet_line', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots', 'risk_game', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots', 'triplication_game', Schema::TYPE_BOOLEAN);
        $this->addColumn('slots', 'kind_of_roulette', Schema::TYPE_STRING);
        $this->addColumn('slots', 'kind_of_poker', Schema::TYPE_STRING);
        $this->addColumn('slots', 'kind_of_lottery', Schema::TYPE_STRING);
        $this->addColumn('slots', 'win_max', Schema::TYPE_INTEGER);
        $this->addColumn('slots', 'amount_wheels', Schema::TYPE_INTEGER);
        $this->addColumn('slots', 'amount_hands', Schema::TYPE_INTEGER);
        $this->addColumn('slots', 'numeric_series', Schema::TYPE_INTEGER);
        $this->addColumn('slots', 'joker', Schema::TYPE_STRING);
        $this->addColumn('slots', 'min_combination', Schema::TYPE_STRING);
        $this->addColumn('slots', 'special_combination', Schema::TYPE_STRING);
        $this->addColumn('slots', 'min_boxes', Schema::TYPE_INTEGER);
        $this->addColumn('slots', 'max_boxes', Schema::TYPE_INTEGER);
        $this->addColumn('slots', 'version', Schema::TYPE_STRING);
        $this->addColumn('slots', 'amount_lines', Schema::TYPE_INTEGER);
        $this->addColumn('slots', 'amount_reels', Schema::TYPE_INTEGER);
        $this->addColumn('slots', 'bet_min', Schema::TYPE_INTEGER);
        $this->addColumn('slots', 'bet_max', Schema::TYPE_INTEGER);
        $this->addColumn('slots', 'payout', Schema::TYPE_STRING);
        $this->addColumn('slots', 'new_to_date', Schema::TYPE_DATE);
    }

    public function safeDown()
    {
        echo "m150909_120658_add_fields_slots_categories cannot be reverted.\n";

        $this->dropColumn('slots_categories', 'amount_lines');
        $this->dropColumn('slots_categories', 'amount_reels');
        $this->dropColumn('slots_categories', 'bet_color');
        $this->dropColumn('slots_categories', 'bet_more_less');
        $this->dropColumn('slots_categories', 'bet_even_odd');
        $this->dropColumn('slots_categories', 'bet_dozen');
        $this->dropColumn('slots_categories', 'bet_line');
        $this->dropColumn('slots_categories', 'risk_game');
        $this->dropColumn('slots_categories', 'triplication_game');
        $this->dropColumn('slots_categories', 'kind_of_roulette');
        $this->dropColumn('slots_categories', 'kind_of_poker');
        $this->dropColumn('slots_categories', 'kind_of_lottery');
        $this->dropColumn('slots_categories', 'win_max');
        $this->dropColumn('slots_categories', 'amount_wheels');
        $this->dropColumn('slots_categories', 'amount_hands');
        $this->dropColumn('slots_categories', 'numeric_series');
        $this->dropColumn('slots_categories', 'joker');
        $this->dropColumn('slots_categories', 'min_combination');
        $this->dropColumn('slots_categories', 'special_combination');
        $this->dropColumn('slots_categories', 'min_boxes');
        $this->dropColumn('slots_categories', 'max_boxes');
        $this->dropColumn('slots_categories', 'version');
        $this->dropColumn('slots_categories', 'bet_min');
        $this->dropColumn('slots_categories', 'bet_max');
        $this->dropColumn('slots_categories', 'video_viewer_url');

        $this->dropColumn('slots', 'bet_color');
        $this->dropColumn('slots', 'bet_more_less');
        $this->dropColumn('slots', 'bet_even_odd');
        $this->dropColumn('slots', 'bet_dozen');
        $this->dropColumn('slots', 'bet_line');
        $this->dropColumn('slots', 'risk_game');
        $this->dropColumn('slots', 'triplication_game');
        $this->dropColumn('slots', 'kind_of_roulette');
        $this->dropColumn('slots', 'kind_of_poker');
        $this->dropColumn('slots', 'kind_of_lottery');
        $this->dropColumn('slots', 'win_max');
        $this->dropColumn('slots', 'amount_wheels');
        $this->dropColumn('slots', 'amount_hands');
        $this->dropColumn('slots', 'numeric_series');
        $this->dropColumn('slots', 'joker');
        $this->dropColumn('slots', 'min_combination');
        $this->dropColumn('slots', 'special_combination');
        $this->dropColumn('slots', 'min_boxes');
        $this->dropColumn('slots', 'max_boxes');
        $this->dropColumn('slots', 'version');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
