<?php

use yii\db\Schema;
use yii\db\Migration;

class m151113_085220_add_field_bonus extends Migration
{
    public function safeUp()
    {
        $this->addColumn('bonuses','value_on_picture', $this->string(32)->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropColumn('bonuses','value_on_picture');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
