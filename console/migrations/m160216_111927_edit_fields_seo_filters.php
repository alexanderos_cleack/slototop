<?php

use yii\db\Schema;
use yii\db\Migration;

class m160216_111927_edit_fields_seo_filters extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('seo_filters_requests', 'filter', $this->text());
        $this->alterColumn('seo_filters_requests', 'url', $this->text());

        $this->addColumn('seo_filters_requests', 'count_elements', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('seo_filters_requests', 'count_elements');
    }

}
