<?php

use yii\db\Schema;
use yii\db\Migration;

class m160126_153502_filters_requests extends Migration
{
    public function safeUp()
    {
        $this->createTable('seo_filters_requests', [
            'id' => $this->primaryKey(),
            'filter' => $this->string(500),
            'url' => $this->string(500),
            'description' => $this->text(),
            'slug' => $this->string(),
            'count' => $this->integer()->defaultValue(1),
            'page' => $this->string(255),
        ]);

        $this->createIndex('seo_filters_requests_idx', 'seo_filters_requests', ['filter', 'page']);
    }

    public function safeDown()
    {
        $this->dropTable('seo_filters_requests');
    }

}
