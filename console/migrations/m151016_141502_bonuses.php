<?php

use yii\db\Schema;
use yii\db\Migration;

class m151016_141502_bonuses extends Migration
{
    public function safeUp()
    {
        $this->createIndex('bonuses_title_unique', 'bonuses', 'title', true);

        $this->addColumn('bonuses', 'amount_uses', $this->integer()->defaultValue(0));
        $this->createIndex('bonuses_amount_uses_ikey', 'bonuses', 'amount_uses');

        $this->addColumn('bonuses', 'slug', $this->string()->defaultValue(null));
        $this->createIndex('bonuses_slug_ikey', 'bonuses', 'slug');

        $this->alterColumn('bonuses', 'bonus_code', $this->string()->defaultValue(null));
        $this->alterColumn('bonuses', 'wager', $this->string()->defaultValue(null));

        $this->addColumn('bonuses2currencies', 'max_sum', $this->float()->defaultValue(null));
        $this->addColumn('bonuses2currencies', 'max_payout', $this->float()->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropIndex('bonuses_title_unique', 'bonuses');

        $this->dropColumn('bonuses', 'amount_uses');
        $this->dropColumn('bonuses', 'slug');

        $this->dropColumn('bonuses2currencies', 'max_sum');
        $this->dropColumn('bonuses2currencies', 'max_payout');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
