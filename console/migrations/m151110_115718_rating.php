<?php

use yii\db\Schema;
use yii\db\Migration;

use common\models\casino\Casino;
use common\models\slots\Slots;
use common\models\vendors\Vendors;
use common\models\bonuses\Bonuses;

class m151110_115718_rating extends Migration
{
    public function safeUp()
    {
        $this->createTable('rating', [
            'id' => $this->primaryKey(),
            'model_schema' => $this->string(128)->notNull(),
            'model_id' => $this->integer(),
            'ip' => 'inet',
            'date' => $this->timestamp(),
            'score' => $this->float(),
        ]);
        $this->createIndex('rating_ip_idx', 'rating', 'ip');

        Casino::updateAll(['rating_users' => 0], 'rating_users IS NULL');
        Casino::updateAll(['rating_admins' => 0], 'rating_admins IS NULL');

        Slots::updateAll(['rating_users' => 0], 'rating_users IS NULL');
        Slots::updateAll(['rating_admins' => 0], 'rating_admins IS NULL');

        Vendors::updateAll(['rating_users' => 0], 'rating_users IS NULL');
        Vendors::updateAll(['rating_admins' => 0], 'rating_admins IS NULL');

        Bonuses::updateAll(['rating_users' => 0], 'rating_users IS NULL');
        Bonuses::updateAll(['rating_admins' => 0], 'rating_admins IS NULL');

    }

    public function safeDown()
    {
        $this->dropTable('rating');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
