<?php

use yii\db\Migration;

/**
 * Class m180228_125500_slots_alter_field_win_max
 */
class m180228_125500_slots_alter_field_win_max extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->db->createCommand("UPDATE slots SET win_max = replace(win_max, 'x', '')")->execute();
        $this->db->createCommand("UPDATE slots SET win_max = replace(win_max, 'X', '')")->execute();
        $this->db->createCommand("UPDATE slots SET win_max = replace(win_max, ' ', '')")->execute();
        $this->db->createCommand("UPDATE slots SET win_max = replace(win_max, 'х', '')")->execute();
        $this->db->createCommand("UPDATE slots SET win_max = replace(win_max, 'Х', '')")->execute();
        $this->db->createCommand("UPDATE slots SET win_max = replace(win_max, 'м', '')")->execute();
        $this->db->createCommand("UPDATE slots SET win_max = 0 where win_max = ''")->execute();

        $this->alterColumn('slots', 'win_max', 'float using win_max::float');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

}
