<?php

use yii\db\Schema;
use yii\db\Migration;

class m151126_102825_add_fields_create_update_time extends Migration
{
    public function safeUp()
    {
        $this->addColumn('casino', 'created_time', $this->timestamp());
        $this->addColumn('casino', 'updated_time', $this->timestamp());

        $this->addColumn('slots', 'created_time', $this->timestamp());
        $this->addColumn('slots', 'updated_time', $this->timestamp());

        $this->addColumn('bonuses', 'created_time', $this->timestamp());
        $this->addColumn('bonuses', 'updated_time', $this->timestamp());

        $this->addColumn('vendors', 'created_time', $this->timestamp());
        $this->addColumn('vendors', 'updated_time', $this->timestamp());

        $this->addColumn('methods_payment', 'created_time', $this->timestamp());
        $this->addColumn('methods_payment', 'updated_time', $this->timestamp());
    }

    public function safeDown()
    {
        $this->dropColumn('casino', 'created_time');
        $this->dropColumn('casino', 'updated_time');

        $this->dropColumn('slots', 'created_time');
        $this->dropColumn('slots', 'updated_time');

        $this->dropColumn('bonuses', 'created_time');
        $this->dropColumn('bonuses', 'updated_time');

        $this->dropColumn('vendors', 'created_time');
        $this->dropColumn('vendors', 'updated_time');

        $this->dropColumn('methods_payment', 'created_time');
        $this->dropColumn('methods_payment', 'updated_time');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
