<?php

namespace app\controllers;

use Yii;
use yii\console\Controller;

use common\models\demogames\DemoGames;


/**
 * Description.
 */

class DemoGamesController extends Controller {

    public function actionIndex() {
        foreach (DemoGames::find()->each() as $item) {
            $url = $item->getIframeUrl();
            $item->url = $url;
            $item->save();
            echo $item->id . "\n";
        }
    }

}