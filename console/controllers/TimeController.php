<?php

namespace app\controllers;

use Yii;
use yii\console\Controller;

use common\models\bonuses\Bonuses;
use common\models\casino\Casino;
use common\models\methods\MethodsPayment;
use common\models\slots\Slots;
use common\models\vendors\Vendors;

/**
 * Updates attributes created_time and updated_time.
 */

class TimeController extends Controller {

    public function actionIndex() {
        $time = new \yii\db\Expression('NOW()');
        Casino::updateAll(['created_time' => $time, 'updated_time' => $time]);
        Slots::updateAll(['created_time' => $time, 'updated_time' => $time]);
        Bonuses::updateAll(['created_time' => $time, 'updated_time' => $time]);
        Vendors::updateAll(['created_time' => $time, 'updated_time' => $time]);
        MethodsPayment::updateAll(['created_time' => $time, 'updated_time' => $time]);
    }

}