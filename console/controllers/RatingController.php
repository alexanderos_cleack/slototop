<?php

namespace app\controllers;

use Yii;
use yii\console\Controller;

use common\models\bonuses\Bonuses;
use common\models\casino\Casino;
use common\models\methods\MethodsPayment;
use common\models\slots\Slots;
use common\models\vendors\Vendors;
use common\models\rating\Rating;

/**
 * Description.
 */

class RatingController extends Controller {

    public function actionIndex() {
        $min = 1700;
        $max = 6400;

        $del = 10000;

        echo "Casino\n";
        Rating::deleteAll(['model_schema' => Casino::tableName()]);
        foreach (Casino::find()->each() as $item) {
            $per = rand($min, $max);
            $score = 5 * ($per / $del);
            $item->setRating($score);

            $per = rand($min, $max);
            $rating_admins = round($per / 100);
            $item::updateAll(['rating_admins' => $rating_admins], ['id' => $item->id]);

            echo $item->title . ' - ' . $per . ' - ' . $score . "\n";
        }

        echo "\nGames\n";
        Rating::deleteAll(['model_schema' => Slots::tableName()]);
        foreach (Slots::find()->each() as $item) {
            $per = rand($min, $max);
            $score = 5 * ($per / $del);
            $item->setRating($score);

            $per = rand($min, $max);
            $rating_admins = round($per / 100);
            $item::updateAll(['rating_admins' => $rating_admins], ['id' => $item->id]);

            echo $item->title . ' - ' . $per . ' - ' . $score . "\n";
        }

        echo "\nVendors\n";
        Rating::deleteAll(['model_schema' => Vendors::tableName()]);
        foreach (Vendors::find()->each() as $item) {
            $per = rand($min, $max);
            $rating_admins = round($per / 100);
            $item::updateAll(['rating_admins' => $rating_admins], ['id' => $item->id]);
        }

        echo "\nBonuses\n";
        Rating::deleteAll(['model_schema' => Bonuses::tableName()]);
        foreach (Bonuses::find()->each() as $item) {
            $per = rand($min, $max);
            $rating_admins = round($per / 100);
            $item::updateAll(['rating_admins' => $rating_admins], ['id' => $item->id]);
        }

    }

}