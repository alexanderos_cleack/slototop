<?php

namespace app\controllers;

use Yii;
use yii\console\Controller;

use common\models\seotext\SeoFiltersRequests;

/**
 * Description.
 */

class SeoFilterController extends Controller {

    public function actionIndex() {

        foreach (SeoFiltersRequests::find()->orderBy(['id' => SORT_ASC])->each() as $item) {

            echo $item->id . "\n";

            $filter = json_decode($item->filter, true);
            $_filter = [];

            foreach ($filter as $key => $value) {
                if (is_array($value)) {
                    $_filter[$key] = $value;
                } elseif (strlen(trim($value))) {
                    $_filter[$key] = $value;
                }
            }

            if (count($_filter)) {
                $item->filter = json_encode($_filter);
                $item->count_elements = count($_filter);
                $item->save();
            } else {
                $item->delete();
            }

        }

    }

}