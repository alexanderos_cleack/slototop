<?php

namespace app\controllers;

use Yii;
use yii\console\Controller;

/**
 * Description.
 */

class RbacController extends Controller {

	public function actionIndex() {
		$auth = Yii::$app->authManager;

		$root = $auth->createRole('root');
		$auth->add($root);

		// Assign roles to users. 1 and 2 are IDs returned by IdentityInterface::getId()
		// usually implemented in your User model.
		$auth->assign($root, 1);
	}

}