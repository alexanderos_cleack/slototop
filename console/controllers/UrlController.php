<?php

namespace app\controllers;

use Yii;
use yii\console\Controller;

use common\models\bonuses\Bonuses;
use common\models\casino\Casino;
use common\models\methods\MethodsPayment;
use common\models\slots\Slots;
use common\models\vendors\Vendors;

/**
 * Description.
 */

class UrlController extends Controller {

    public function actionIndex() {
        //  foreach (Casino::find()->each() as $item) {
        //     $url = $item->casino_url;
        //     $url = preg_replace('/^(http|https)\:\/\//', '', $url);
        //     Casino::updateAll(['casino_url' => $url], "id = {$item->id}");
        // }

        // foreach (Bonuses::find()->each() as $item) {
        //     $url = $item->url_bonus;
        //     $url = preg_replace('/^(http|https)\:\/\//', '', $url);
        //     Bonuses::updateAll(['url_bonus' => $url], "id = {$item->id}");
        // }

        foreach (MethodsPayment::find()->each() as $item) {
            $url = $item->url_site;
            $url = preg_replace('/^(http|https)\:\/\//', '', $url);
            MethodsPayment::updateAll(['url_site' => $url], "id = {$item->id}");
        }

    }

}