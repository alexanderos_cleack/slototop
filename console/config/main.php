<?php

// Config console

require(__DIR__ . '/../../common/config/alias.php');

return [
    'id' => 'Console Application',
    'basePath' => dirname(__DIR__),

    'components' => [
        'i18n' => require(__DIR__ . '/../../common/config/i18n.php'),
        'db' => require(__DIR__ . '/../../common/config/db.php'),
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],

        'urlManager' => [
            'baseUrl' => '/',
            'suffix' => '/',
            'normalizer' => [
                'class' => \yii\web\UrlNormalizer::class,
                'action' => \yii\web\UrlNormalizer::ACTION_REDIRECT_TEMPORARY,
            ],
            'showScriptName' => false,
            'enablePrettyUrl' => true,
        ],
    ],

    'controllerMap' => [
        'migrate' => [
            'class' => 'yii\console\controllers\MigrateController',
        ],
    ],

];

?>