<?php

defined('YII_DEBUG') or define('YII_DEBUG', false);

require(__DIR__ . '/../common/vendor/autoload.php');
require(__DIR__ . '/../common/vendor/yiisoft/yii2/Yii.php');

$config_app = require(__DIR__ . '/config/main.php');

$application = new yii\console\Application($config_app);
$exitCode = $application->run();
exit($exitCode);